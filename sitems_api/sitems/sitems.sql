-- --------------------------------------------------------
-- 主机:                           10.10.4.222
-- 服务器版本:                        5.7.22 - MySQL Community Server (GPL)
-- 服务器OS:                        linux-glibc2.12
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for data_sitems
CREATE DATABASE IF NOT EXISTS `data_sitems` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `data_sitems`;

-- Dumping structure for table data_sitems.db_alert_category
CREATE TABLE IF NOT EXISTS `db_alert_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL COMMENT '专题名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '顺序',
  `isalert` int(11) NOT NULL DEFAULT '1' COMMENT '是否预警开关：1表示开；0表示关',
  `isshow` int(11) NOT NULL DEFAULT '1' COMMENT '是否在首页显示：1表示显示；0表示不显示',
  `account_id` int(11) NOT NULL DEFAULT '0' COMMENT '创建该专题用户的id',
  `alert_receiver` text COMMENT '预警接收人:多个ID列表',
  `alert_type` text COMMENT '预警方式：邮箱或短信',
  `begin_time` varchar(128) DEFAULT NULL COMMENT '预警开始时间',
  `end_time` varchar(128) DEFAULT NULL COMMENT '预警结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='巡查预警的专题分类';

-- Dumping data for table data_sitems.db_alert_category: ~16 rows (大约)
/*!40000 ALTER TABLE `db_alert_category` DISABLE KEYS */;
INSERT INTO `db_alert_category` (`id`, `name`, `parent_id`, `sort`, `isalert`, `isshow`, `account_id`, `alert_receiver`, `alert_type`, `begin_time`, `end_time`) VALUES
	(1, '传媒', 0, 1, 1, 1, 11, '[1,9,2]', '["短信"]', '', ''),
	(2, '新闻媒体', 1, 2, 1, 1, 11, '[1,9]', '["短信"]', '', ''),
	(3, '视频网站', 1, 1, 1, 1, 11, '[5,2]', '["邮箱"]', '', ''),
	(4, '体育资讯', 1, 3, 1, 1, 11, '[9,5,11]', '["短信"]', '', ''),
	(10, '地域', 0, 2, 1, 1, 11, NULL, NULL, NULL, NULL),
	(11, '本省', 10, 1, 1, 1, 11, '[2,9]', '["短信"]', '', ''),
	(12, '外省', 10, 2, 1, 1, 11, NULL, NULL, NULL, NULL),
	(13, '测试', 0, 1, 1, 1, 17, NULL, NULL, NULL, NULL),
	(14, '测试222', 13, 1, 1, 1, 17, '[1]', '["短信"]', '01:00', '21:02'),
	(15, '是市图书馆', 1, 4, 1, 1, 11, NULL, NULL, NULL, NULL),
	(16, '电费v', 10, 3, 1, 1, 11, NULL, NULL, NULL, NULL),
	(17, '石膏板地方', 10, 4, 1, 1, 11, NULL, NULL, NULL, NULL),
	(18, '网购', 0, 3, 1, 1, 11, NULL, NULL, NULL, NULL),
	(19, '百度', 18, 1, 1, 1, 11, '[1]', '["短信"]', '', ''),
	(20, '京东', 18, 2, 1, 1, 11, NULL, NULL, NULL, NULL),
	(21, '1', 0, 4, 1, 1, 11, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `db_alert_category` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_alert_receiver
CREATE TABLE IF NOT EXISTS `db_alert_receiver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(520) NOT NULL DEFAULT '' COMMENT '预警接收人姓名',
  `email` varchar(520) NOT NULL DEFAULT '' COMMENT '邮箱',
  `phone` varchar(64) NOT NULL COMMENT '电话',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='预警接受人：巡查预警功能';

-- Dumping data for table data_sitems.db_alert_receiver: ~7 rows (大约)
/*!40000 ALTER TABLE `db_alert_receiver` DISABLE KEYS */;
INSERT INTO `db_alert_receiver` (`id`, `name`, `email`, `phone`) VALUES
	(1, '陈林01', '1212121212@qq.com', '121212121212'),
	(2, '林林111', '2323232323@qq.com', '23232323232323'),
	(5, 'sun', 'ssfssdfsa@qq.com', '15996313709'),
	(8, '321', '123@qq.com', '13543267898'),
	(9, 'sun002', 'qwee@qq.com', '15996333332'),
	(11, '王学聪', '1031684347@qq.com', '18600886845'),
	(12, 'twelling', 'txlwelling@icloud.com', '18136489133');
/*!40000 ALTER TABLE `db_alert_receiver` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_alert_word_subject
CREATE TABLE IF NOT EXISTS `db_alert_word_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(520) NOT NULL COMMENT '预警关键词',
  `subject_id` int(11) NOT NULL DEFAULT '0' COMMENT '专题id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='预警关键词和专题的映射关系表';

-- Dumping data for table data_sitems.db_alert_word_subject: ~7 rows (大约)
/*!40000 ALTER TABLE `db_alert_word_subject` DISABLE KEYS */;
INSERT INTO `db_alert_word_subject` (`id`, `word`, `subject_id`) VALUES
	(4, '你好', 1),
	(6, '人心，刀', 2),
	(8, '暴力，庆祝', 3),
	(10, '国庆', 14),
	(11, '人身，钉钉', 4),
	(12, '本地，排外', 11),
	(13, '百度', 19);
/*!40000 ALTER TABLE `db_alert_word_subject` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_captcha
CREATE TABLE IF NOT EXISTS `db_captcha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(520) DEFAULT NULL COMMENT '创建、使用该验证码的帐号',
  `contact` varchar(50) DEFAULT NULL COMMENT '电话号码',
  `code` varchar(10) DEFAULT NULL COMMENT '验证码',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间：用于判断是否过期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='验证码数据表：保存登录时忘记密码后，获取的验证码';

-- Dumping data for table data_sitems.db_captcha: ~2 rows (大约)
/*!40000 ALTER TABLE `db_captcha` DISABLE KEYS */;
INSERT INTO `db_captcha` (`id`, `account`, `contact`, `code`, `create_time`) VALUES
	(1, '00002', '15996313709', '835761', 1572947736775),
	(3, 'root', '18136489133', '796009', 1573028170447);
/*!40000 ALTER TABLE `db_captcha` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_illegal_handle_records
CREATE TABLE IF NOT EXISTS `db_illegal_handle_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domainName` varchar(50) DEFAULT NULL COMMENT '网站id',
  `userId` int(11) NOT NULL DEFAULT '0' COMMENT '处理者id',
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '处理的数量',
  `scheme` varchar(520) DEFAULT NULL COMMENT '协查方案',
  `feedBack` varchar(520) DEFAULT NULL COMMENT '协查反馈',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间：时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='已处理违规协查网站记录表：保存网站id，协查方案，协查反馈等内容';

-- Dumping data for table data_sitems.db_illegal_handle_records: ~7 rows (大约)
/*!40000 ALTER TABLE `db_illegal_handle_records` DISABLE KEYS */;
INSERT INTO `db_illegal_handle_records` (`id`, `domainName`, `userId`, `count`, `scheme`, `feedBack`, `createTime`) VALUES
	(1, '51xiancheng.com', 3, 0, '协查方案0001', '反馈001', 1569573918459),
	(2, '51xiancheng.com', 11, 0, '先打个吧', '导入以及纳税人有', 1570514590660),
	(3, 'sina.com.cn', 11, 0, '是多少', '是的发v史蒂夫', 1571128459029),
	(4, '51xiancheng.com', 11, 0, '的烦不烦', '是的发v史蒂夫s\'d\'f\'v\'s', 1571128466930),
	(5, '51xiancheng.com', 11, 0, '风格不舍得', '代表性的分布式', 1571128498848),
	(6, 'weibo.com', 11, 0, '222', '代表性的分布式都是发v', 1571128586098),
	(7, 'longyan.cn', 11, 0, '123', 'trt', 1572595592230),
	(8, 'longyan.cn', 11, 128, '测试违规数量', '测试处理协查反馈', 1573008813239);
/*!40000 ALTER TABLE `db_illegal_handle_records` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_illegal_web
CREATE TABLE IF NOT EXISTS `db_illegal_web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webId` varchar(128) DEFAULT NULL COMMENT '网站id',
  `illegalCount` int(11) NOT NULL DEFAULT '0' COMMENT '违规数量',
  `siteName` varchar(128) DEFAULT NULL COMMENT '网站名称',
  `domainName` varchar(128) DEFAULT NULL COMMENT '网站的域名：判断网站的唯一字段',
  `organizerNature` varchar(128) DEFAULT NULL COMMENT '主办单位',
  `level` varchar(32) DEFAULT NULL COMMENT '网站等级',
  `city` varchar(32) DEFAULT NULL COMMENT '城市',
  `province` varchar(32) DEFAULT NULL COMMENT '省份',
  `subject` int(11) DEFAULT '0' COMMENT '主题',
  `contact` varchar(64) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(64) DEFAULT NULL COMMENT '电话',
  `status` int(11) DEFAULT '0' COMMENT '网站状态',
  `categoryTitle` varchar(128) DEFAULT NULL COMMENT '网站分类',
  `scheme` varchar(520) DEFAULT NULL COMMENT '协查方案',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间：时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='待处理的违规网站表：只添加了协查方案的待处理的网站数据，同一网站可以被多次添加，时间，id不同，域名相同';

-- Dumping data for table data_sitems.db_illegal_web: ~1 rows (大约)
/*!40000 ALTER TABLE `db_illegal_web` DISABLE KEYS */;
INSERT INTO `db_illegal_web` (`id`, `webId`, `illegalCount`, `siteName`, `domainName`, `organizerNature`, `level`, `city`, `province`, `subject`, `contact`, `phone`, `status`, `categoryTitle`, `scheme`, `createTime`) VALUES
	(1, 'e9a75ba3e0e6b5620b12f0eb3f12d4ba', 0, '龙岩公共服务网', 'longyan.cn', '事业单位', 'C', '龙岩', '福建省', 0, '福建省龙岩市人民*屏蔽的关键字*办公室', '', 0, '未分类', 'zdfv', 1572859880021);
/*!40000 ALTER TABLE `db_illegal_web` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_illegal_web_handled
CREATE TABLE IF NOT EXISTS `db_illegal_web_handled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webId` varchar(128) DEFAULT NULL COMMENT '网站id',
  `siteName` varchar(128) DEFAULT NULL COMMENT '网站名称',
  `domainName` varchar(128) DEFAULT NULL COMMENT '网站的域名：判断网站的唯一字段',
  `organizerNature` varchar(128) DEFAULT NULL COMMENT '主办单位',
  `level` varchar(32) DEFAULT NULL COMMENT '网站等级',
  `city` varchar(32) DEFAULT NULL COMMENT '城市',
  `province` varchar(32) DEFAULT NULL COMMENT '省份',
  `subject` int(11) DEFAULT '0' COMMENT '主题',
  `contact` varchar(64) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(64) DEFAULT NULL COMMENT '电话',
  `status` int(11) DEFAULT '0' COMMENT '网站状态',
  `categoryTitle` varchar(128) DEFAULT NULL COMMENT '网站分类',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间：时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='待处理的违规网站表：只添加了协查方案的待处理的网站数据，同一网站可以被多次添加，时间，id不同，域名相同';

-- Dumping data for table data_sitems.db_illegal_web_handled: ~4 rows (大约)
/*!40000 ALTER TABLE `db_illegal_web_handled` DISABLE KEYS */;
INSERT INTO `db_illegal_web_handled` (`id`, `webId`, `siteName`, `domainName`, `organizerNature`, `level`, `city`, `province`, `subject`, `contact`, `phone`, `status`, `categoryTitle`, `createTime`) VALUES
	(2, 'c2fdb39ec815b282337350dd23db742e', '个人技术文章', 'sina.com.cn', '个人', 'C', '', '', 0, '山黎明', '', 0, '未分类', 1571128459062),
	(4, 'f5e2ec9e719b666c7216a1a8e81be81d', '个人技术文章', '51xiancheng.com', '个人', 'C', '', '', 0, '山黎明', '', 0, '未分类', 1571128498910),
	(5, '4ea43b877864e79085843e843daa6b22', NULL, 'weibo.com', '个人', 'C', '', '', 0, '山黎明', '', 0, '未分类', 1571128586123),
	(7, 'e9a75ba3e0e6b5620b12f0eb3f12d4ba', '龙岩公共服务网', 'longyan.cn', '事业单位', 'C', '龙岩', '福建省', 0, '福建省龙岩市人民*屏蔽的关键字*办公室', '', 0, '未分类', 1573008813323);
/*!40000 ALTER TABLE `db_illegal_web_handled` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_permission
CREATE TABLE IF NOT EXISTS `db_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL DEFAULT '0' COMMENT '权限模块id',
  `permission_name` varchar(520) NOT NULL DEFAULT '' COMMENT '权限名称',
  `permission_info` varchar(520) NOT NULL DEFAULT '' COMMENT '权限的备注信息',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- Dumping data for table data_sitems.db_permission: ~39 rows (大约)
/*!40000 ALTER TABLE `db_permission` DISABLE KEYS */;
INSERT INTO `db_permission` (`id`, `module_id`, `permission_name`, `permission_info`, `create_time`) VALUES
	(1, 1, '添加', '网站管理-添加', 1565076127),
	(2, 1, '修改', '网站管理-修改', 1565076127),
	(3, 1, '删除', '网站管理-删除', 1565076127),
	(4, 1, '导入网站', '网站管理-导入网站', 1565076127),
	(5, 1, '导出数据', '网站管理-导出数据', 1565076127),
	(6, 1, '批量删除', '网站管理-批量删除', 1565076127),
	(7, 2, '导出数据', '巡查预警-导出数据', 1565076127),
	(8, 2, '加入协查', '巡查预警-加入协查', 1565076127),
	(9, 2, '查看网站负责人', '巡查预警-查看网站负责人', 1565076127),
	(10, 2, '加入简报', '巡查预警-加入简报', 1565076127),
	(12, 7, '新建专题', '巡查设置-新建专题', 1565076127),
	(13, 7, '编辑', '巡查设置-编辑', 1565076127),
	(14, 7, '删除', '巡查设置-删除', 1565076127),
	(15, 7, '上移/下移', '巡查设置-上移/下移', 1565076127),
	(16, 7, '首页显示/隐藏', '巡查设置-首页显示/隐藏', 1565076127),
	(17, 8, '添加', '预警接收人设置-添加', 1565076127),
	(18, 8, '修改', '预警接收人设置-修改', 1565076127),
	(19, 8, '删除', '预警接收人设置-删除', 1565076127),
	(20, 3, '协查反馈', '违规协查-协查反馈', 1565076127),
	(21, 3, '处理记录', '违规协查-处理记录', 1565076127),
	(22, 4, '排查', '事件排查-排查', 1565076127),
	(23, 4, '加入协查', '事件排查-加入协查', 1565076127),
	(24, 4, '查看网站负责人', '事件排查-查看网站负责人', 1565076127),
	(25, 5, '生成报告', '属地简报-生成报告', 1565076127),
	(26, 5, '发送报告', '属地简报-发送报告', 1565076127),
	(27, 5, '下载', '属地简报-下载', 1565076127),
	(28, 5, '查看报告', '属地简报-查看报告', 1565076127),
	(29, 9, '添加', '接收人管理-添加', 1565076127),
	(30, 9, '删除', '接收人管理-删除', 1565076127),
	(31, 10, '修改姓名', '账号信息-修改姓名', 1565076127),
	(32, 10, '修改平台名称', '账号信息-修改平台名称', 1565076127),
	(33, 10, '修改联系方式', '账号信息-修改联系方式', 1565076127),
	(34, 10, '重置密码', '账号信息-重置密码', 1565076127),
	(35, 11, '添加', '角色管理-添加', 1565076127),
	(36, 11, '修改', '角色管理-修改', 1565076127),
	(37, 11, '删除', '角色管理-删除', 1565076127),
	(38, 12, '添加', '用户管理-添加', 1565076127),
	(39, 12, '修改', '用户管理-修改', 1565076127),
	(40, 12, '删除', '用户管理-删除', 1565076127);
/*!40000 ALTER TABLE `db_permission` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_permission_module
CREATE TABLE IF NOT EXISTS `db_permission_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(520) NOT NULL DEFAULT '' COMMENT '权限模块名称',
  `module_info` varchar(520) NOT NULL DEFAULT '' COMMENT '模块的备注信息',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级模块的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='权限模块表';

-- Dumping data for table data_sitems.db_permission_module: ~12 rows (大约)
/*!40000 ALTER TABLE `db_permission_module` DISABLE KEYS */;
INSERT INTO `db_permission_module` (`id`, `module_name`, `module_info`, `parent_id`) VALUES
	(1, '网站管理', '网站管理权限模块', 0),
	(2, '巡查预警', '巡查预警权限模块', 0),
	(3, '违规协查', '违规协查权限模块', 0),
	(4, '事件排查', '事件排查权限模块', 0),
	(5, '属地简报', '属地简报权限模块', 0),
	(6, '个人中心', '个人中心权限模块', 0),
	(7, '巡查设置', '巡查预警-巡查设置', 2),
	(8, '预警接收人设置', '巡查预警-预警接收人设置', 2),
	(9, '接收人管理', '属地简报-接收人管理', 5),
	(10, '账号信息', '个人中心-账号信息', 6),
	(11, '角色管理', '个人中心-角色管理', 6),
	(12, '用户管理', '个人中心-用户管理', 6);
/*!40000 ALTER TABLE `db_permission_module` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_region
CREATE TABLE IF NOT EXISTS `db_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT '区域名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '上级区域id，行政区域的上级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='区域信息表';

-- Dumping data for table data_sitems.db_region: ~25 rows (大约)
/*!40000 ALTER TABLE `db_region` DISABLE KEYS */;
INSERT INTO `db_region` (`id`, `name`, `parent_id`) VALUES
	(1, '湖南省', 0),
	(2, '长沙市', 1),
	(3, '株洲市', 1),
	(4, '湘潭市', 1),
	(5, '衡阳市', 1),
	(6, '邵阳市', 1),
	(7, '岳阳市', 1),
	(8, '常德市', 1),
	(9, '张家界市', 1),
	(10, '益阳市', 1),
	(11, '郴州市', 1),
	(12, '永州市', 1),
	(13, '怀化市', 1),
	(14, '娄底地区', 1),
	(15, '湘西土家族苗族自治州', 1),
	(16, '福建省', 0),
	(17, '福州市', 16),
	(18, '厦门市', 16),
	(19, '宁德市', 16),
	(20, '莆田市', 16),
	(21, '泉州市', 16),
	(22, '漳州市', 16),
	(23, '龙岩市', 16),
	(24, '三明市', 16),
	(25, '南平市', 16);
/*!40000 ALTER TABLE `db_region` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_report
CREATE TABLE IF NOT EXISTS `db_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(520) DEFAULT NULL COMMENT '简报名称',
  `start_time` bigint(20) DEFAULT NULL COMMENT '开始时间：时间戳',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结束时间：时间戳',
  `report_desc` text COMMENT '概述',
  `webs_illegal` text COMMENT '未处理的违规网站列表',
  `webs_illegal_handled` text COMMENT '已处理的违规网站',
  `page_new` text COMMENT '最新违规信息',
  `page_important` text COMMENT '重点违规信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='简报表';

-- Dumping data for table data_sitems.db_report: ~2 rows (大约)
/*!40000 ALTER TABLE `db_report` DISABLE KEYS */;
INSERT INTO `db_report` (`id`, `report_name`, `start_time`, `end_time`, `report_desc`, `webs_illegal`, `webs_illegal_handled`, `page_new`, `page_important`) VALUES
	(1, '湖北省网信办简报(2019-10-21 00:00)', 1571587200000, 1571759999000, '2019-10-21 00:00--2019-10-22 23:59期间，系统共监测到0个网站存在违规信息共0条,其中已处理0个网站, 还有0个网站待处理；详细报告如下：', NULL, NULL, NULL, '[{"cleanTitle":"震撼史上最全版本卡门来南京了鲜城","createTime":0,"domainName":"51xiancheng.com","id":"11","title":"震撼！史上最全版本《卡门》来南京了-鲜城","url":"http://nanjing.51xiancheng.com/article/76149"},{"cleanTitle":"岁月里你有别样的美鲜城","createTime":0,"domainName":"51xiancheng.com","id":"12","title":"岁月里 你有别样的美-鲜城","url":"http://hefei.51xiancheng.com/article/42086"},{"cleanTitle":"震撼史上最全版本卡门来南京了鲜城","createTime":1569309690000,"domainName":"51xiancheng.com","id":"13","title":"震撼！史上最全版本《卡门》来南京了-鲜城","url":"http://nanjing.51xiancheng.com/article/76149"},{"cleanTitle":"岁月里你有别样的美鲜城","createTime":1568274813000,"domainName":"51xiancheng.com","id":"14","title":"岁月里 你有别样的美-鲜城","url":"http://hefei.51xiancheng.com/article/42086"}]'),
	(2, '湖北省网信办简报(2019-10-31 00:00)', 1572451200000, 1572537599000, '2019-10-31 00:00--2019-10-31 23:59期间，系统共监测到0个网站存在违规信息共0条,其中已处理0个网站, 还有0个网站待处理；详细报告如下：', NULL, NULL, NULL, '[{"cleanTitle":"震撼史上最全版本卡门来南京了鲜城","createTime":0,"domainName":"51xiancheng.com","id":"11","title":"震撼！史上最全版本《卡门》来南京了-鲜城","url":"http://nanjing.51xiancheng.com/article/76149"},{"cleanTitle":"岁月里你有别样的美鲜城","createTime":0,"domainName":"51xiancheng.com","id":"12","title":"岁月里 你有别样的美-鲜城","url":"http://hefei.51xiancheng.com/article/42086"},{"cleanTitle":"震撼史上最全版本卡门来南京了鲜城","createTime":1569309690000,"domainName":"51xiancheng.com","id":"13","title":"震撼！史上最全版本《卡门》来南京了-鲜城","url":"http://nanjing.51xiancheng.com/article/76149"},{"cleanTitle":"岁月里你有别样的美鲜城","createTime":1568274813000,"domainName":"51xiancheng.com","id":"14","title":"岁月里 你有别样的美-鲜城","url":"http://hefei.51xiancheng.com/article/42086"}]');
/*!40000 ALTER TABLE `db_report` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_report_email
CREATE TABLE IF NOT EXISTS `db_report_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(520) DEFAULT NULL COMMENT '邮箱',
  `userId` int(11) DEFAULT NULL COMMENT '创建者id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='属地简报中邮箱表';

-- Dumping data for table data_sitems.db_report_email: ~14 rows (大约)
/*!40000 ALTER TABLE `db_report_email` DISABLE KEYS */;
INSERT INTO `db_report_email` (`id`, `email`, `userId`) VALUES
	(1, '624762746@qq.com', NULL),
	(2, 'dfgndn@www.com', NULL),
	(3, 'sdfvsdfvsdfvsdfv@qw.com', NULL),
	(4, 'gdbsgb@qq.com', NULL),
	(5, 'dvfsvsdfv@qq.com', NULL),
	(6, 'sgbdgf@qq.com', NULL),
	(7, 'sdgvsvs@qq.com', NULL),
	(8, 'sfgbdfgb@qq.com', NULL),
	(9, 'zdfv@qq.com', NULL),
	(10, 'cvsfgs@qq.com', NULL),
	(11, '624762746@qq.com', NULL),
	(12, '624762746@qq.com', NULL),
	(13, '624762746@qq.com', NULL),
	(15, 'txlwelling@icloud.com', 11);
/*!40000 ALTER TABLE `db_report_email` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_report_page
CREATE TABLE IF NOT EXISTS `db_report_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(520) DEFAULT NULL COMMENT 'url地址',
  `domainName` varchar(520) DEFAULT NULL COMMENT '域名',
  `title` text COMMENT '标题',
  `cleanTitle` text COMMENT '精简标题',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间：时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='违规信息表：添加到简报中的违规信息';

-- Dumping data for table data_sitems.db_report_page: ~10 rows (大约)
/*!40000 ALTER TABLE `db_report_page` DISABLE KEYS */;
INSERT INTO `db_report_page` (`id`, `url`, `domainName`, `title`, `cleanTitle`, `createTime`) VALUES
	(11, 'http://nanjing.51xiancheng.com/article/76149', '51xiancheng.com', '震撼！史上最全版本《卡门》来南京了-鲜城', '震撼史上最全版本卡门来南京了鲜城', NULL),
	(12, 'http://hefei.51xiancheng.com/article/42086', '51xiancheng.com', '岁月里 你有别样的美-鲜城', '岁月里你有别样的美鲜城', NULL),
	(13, 'http://nanjing.51xiancheng.com/article/76149', '51xiancheng.com', '震撼！史上最全版本《卡门》来南京了-鲜城', '震撼史上最全版本卡门来南京了鲜城', 1569309690000),
	(14, 'http://hefei.51xiancheng.com/article/42086', '51xiancheng.com', '岁月里 你有别样的美-鲜城', '岁月里你有别样的美鲜城', 1568274813000),
	(15, 'http://www.longyan.cn/wsbs/jsp/public/deptService.jsp?region=350802', 'longyan.cn', '部门办事-龙岩公共服务网-longyan.cn', '部门办事龙岩公共服务网longyancn', 1571910367000),
	(16, 'http://www.longyan.cn/wsbs/jsp/public/deptService.jsp?region=350802', 'longyan.cn', '部门办事-龙岩公共服务网-longyan.cn', '部门办事龙岩公共服务网longyancn', 1571910367000),
	(17, 'http://www.longyan.cn/wsbs/jsp/public/deptService.jsp?region=350802', 'longyan.cn', '部门办事-龙岩公共服务网-longyan.cn', '部门办事龙岩公共服务网longyancn', 1571910367000),
	(18, 'http://www.longyan.cn/news/hotcontent/hotconcern/23333.jsp', 'longyan.cn', '身份证遗失补领需要什么材料？', '身份证遗失补领需要什么材料', 1571910363000),
	(19, 'http://www.longyan.cn/news/newsinfo/news/23502.jsp', 'longyan.cn', '上杭古田：发力古蛟组团建设 确保重点征迁任务', '上杭古田发力古蛟组团建设确保重点征迁任务', 1571910362000),
	(20, 'http://www.longyan.cn/news/tourism/tynr/cxyw/zjy/zjyinfo/index.jsp?type=zjyright', 'longyan.cn', '\r\n热门资讯-旅游服务-龙岩公共服务网-longyan.cn', '热门资讯旅游服务龙岩公共服务网longyancn', 1571910361000);
/*!40000 ALTER TABLE `db_report_page` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_role
CREATE TABLE IF NOT EXISTS `db_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(520) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_info` varchar(520) NOT NULL DEFAULT '' COMMENT '备注信息',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间戳',
  `permissions` text COMMENT '角色所关联的权限信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- Dumping data for table data_sitems.db_role: ~8 rows (大约)
/*!40000 ALTER TABLE `db_role` DISABLE KEYS */;
INSERT INTO `db_role` (`id`, `role_name`, `role_info`, `create_time`, `permissions`) VALUES
	(1, '管理员', '创建管理员角色', 1565245035098, '[{"id":1,"module_info":"网站管理权限模块","module_name":"网站管理","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":1,"module_id":1,"permission_info":"网站管理-添加","permission_name":"添加","status":1},{"create_Time":1565076127,"id":2,"module_id":1,"permission_info":"网站管理-修改","permission_name":"修改","status":1},{"create_Time":1565076127,"id":3,"module_id":1,"permission_info":"网站管理-删除","permission_name":"删除","status":1},{"create_Time":1565076127,"id":4,"module_id":1,"permission_info":"网站管理-导入网站","permission_name":"导入网站","status":1},{"create_Time":1565076127,"id":5,"module_id":1,"permission_info":"网站管理-导出数据","permission_name":"导出数据","status":1},{"create_Time":1565076127,"id":6,"module_id":1,"permission_info":"网站管理-批量删除","permission_name":"批量删除","status":1}],"status":1},{"id":2,"moduleList":[{"id":7,"module_info":"巡查预警-巡查设置","module_name":"巡查设置","parent_id":2,"permissionList":[{"create_Time":1565076127,"id":12,"module_id":7,"permission_info":"巡查设置-新建专题","permission_name":"新建专题","status":1},{"create_Time":1565076127,"id":13,"module_id":7,"permission_info":"巡查设置-编辑","permission_name":"编辑","status":1},{"create_Time":1565076127,"id":14,"module_id":7,"permission_info":"巡查设置-删除","permission_name":"删除","status":1},{"create_Time":1565076127,"id":15,"module_id":7,"permission_info":"巡查设置-上移/下移","permission_name":"上移/下移","status":1},{"create_Time":1565076127,"id":16,"module_id":7,"permission_info":"巡查设置-首页显示/隐藏","permission_name":"首页显示/隐藏","status":1}],"status":1},{"id":8,"module_info":"巡查预警-预警接收人设置","module_name":"预警接收人设置","parent_id":2,"permissionList":[{"create_Time":1565076127,"id":17,"module_id":8,"permission_info":"预警接收人设置-添加","permission_name":"添加","status":1},{"create_Time":1565076127,"id":18,"module_id":8,"permission_info":"预警接收人设置-修改","permission_name":"修改","status":1},{"create_Time":1565076127,"id":19,"module_id":8,"permission_info":"预警接收人设置-删除","permission_name":"删除","status":1}],"status":1}],"module_info":"巡查预警权限模块","module_name":"巡查预警","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":7,"module_id":2,"permission_info":"巡查预警-导出数据","permission_name":"导出数据","status":1},{"create_Time":1565076127,"id":8,"module_id":2,"permission_info":"巡查预警-加入协查","permission_name":"加入协查","status":1},{"create_Time":1565076127,"id":9,"module_id":2,"permission_info":"巡查预警-查看网站负责人","permission_name":"查看网站负责人","status":1},{"create_Time":1565076127,"id":10,"module_id":2,"permission_info":"巡查预警-加入简报","permission_name":"加入简报","status":1}],"status":1},{"id":3,"module_info":"违规协查权限模块","module_name":"违规协查","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":20,"module_id":3,"permission_info":"违规协查-协查反馈","permission_name":"协查反馈","status":1},{"create_Time":1565076127,"id":21,"module_id":3,"permission_info":"违规协查-处理记录","permission_name":"处理记录","status":1}],"status":1},{"id":4,"module_info":"事件排查权限模块","module_name":"事件排查","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":22,"module_id":4,"permission_info":"事件排查-排查","permission_name":"排查","status":1},{"create_Time":1565076127,"id":23,"module_id":4,"permission_info":"事件排查-加入协查","permission_name":"加入协查","status":1},{"create_Time":1565076127,"id":24,"module_id":4,"permission_info":"事件排查-查看网站负责人","permission_name":"查看网站负责人","status":1}],"status":1},{"id":5,"moduleList":[{"id":9,"module_info":"属地简报-接收人管理","module_name":"接收人管理","parent_id":5,"permissionList":[{"create_Time":1565076127,"id":29,"module_id":9,"permission_info":"接收人管理-添加","permission_name":"添加","status":1},{"create_Time":1565076127,"id":30,"module_id":9,"permission_info":"接收人管理-删除","permission_name":"删除","status":1}],"status":1}],"module_info":"属地简报权限模块","module_name":"属地简报","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":25,"module_id":5,"permission_info":"属地简报-生成报告","permission_name":"生成报告","status":1},{"create_Time":1565076127,"id":26,"module_id":5,"permission_info":"属地简报-发送报告","permission_name":"发送报告","status":1},{"create_Time":1565076127,"id":27,"module_id":5,"permission_info":"属地简报-下载","permission_name":"下载","status":1},{"create_Time":1565076127,"id":28,"module_id":5,"permission_info":"属地简报-查看报告","permission_name":"查看报告","status":1}],"status":1},{"id":6,"moduleList":[{"id":10,"module_info":"个人中心-账号信息","module_name":"账号信息","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":31,"module_id":10,"permission_info":"账号信息-修改姓名","permission_name":"修改姓名","status":1},{"create_Time":1565076127,"id":32,"module_id":10,"permission_info":"账号信息-修改平台名称","permission_name":"修改平台名称","status":1},{"create_Time":1565076127,"id":33,"module_id":10,"permission_info":"账号信息-修改联系方式","permission_name":"修改联系方式","status":1},{"create_Time":1565076127,"id":34,"module_id":10,"permission_info":"账号信息-重置密码","permission_name":"重置密码","status":1}],"status":1},{"id":11,"module_info":"个人中心-角色管理","module_name":"角色管理","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":35,"module_id":11,"permission_info":"角色管理-添加","permission_name":"添加","status":1},{"create_Time":1565076127,"id":36,"module_id":11,"permission_info":"角色管理-修改","permission_name":"修改","status":1},{"create_Time":1565076127,"id":37,"module_id":11,"permission_info":"角色管理-删除","permission_name":"删除","status":1}],"status":1},{"id":12,"module_info":"个人中心-用户管理","module_name":"用户管理","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":38,"module_id":12,"permission_info":"用户管理-添加","permission_name":"添加","status":1},{"create_Time":1565076127,"id":39,"module_id":12,"permission_info":"用户管理-修改","permission_name":"修改","status":1},{"create_Time":1565076127,"id":40,"module_id":12,"permission_info":"用户管理-删除","permission_name":"删除","status":1}],"status":1}],"module_info":"个人中心权限模块","module_name":"个人中心","parent_id":0,"permissionList":[],"status":1}]'),
	(2, '审核员', '创建审核员角色', 1565245587099, '[{"id": 1, "status": 1, "parent_id": 0, "module_info": "网站管理权限模块", "module_name": "网站管理", "permissionList": [{"id": 1, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-添加", "permission_name": "添加"}, {"id": 2, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-修改", "permission_name": "修改"}, {"id": 3, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-删除", "permission_name": "删除"}, {"id": 4, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导入网站", "permission_name": "导入网站"}, {"id": 5, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导出数据", "permission_name": "导出数据"}, {"id": 6, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-批量删除", "permission_name": "批量删除"}]}, {"id": 2, "status": 0, "parent_id": 0, "moduleList": [{"id": 7, "status": 0, "parent_id": 2, "module_info": "巡查预警-巡查设置", "module_name": "巡查设置", "permissionList": [{"id": 12, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-新建专题", "permission_name": "新建专题"}, {"id": 13, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-编辑", "permission_name": "编辑"}, {"id": 14, "status": 1, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-删除", "permission_name": "删除"}, {"id": 15, "status": 1, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-上移/下移", "permission_name": "上移/下移"}, {"id": 16, "status": 1, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-首页显示/隐藏", "permission_name": "首页显示/隐藏"}]}, {"id": 8, "status": 0, "parent_id": 2, "module_info": "巡查预警-预警接收人设置", "module_name": "预警接收人设置", "permissionList": [{"id": 17, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-添加", "permission_name": "添加"}, {"id": 18, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-修改", "permission_name": "修改"}, {"id": 19, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-删除", "permission_name": "删除"}]}], "module_info": "巡查预警权限模块", "module_name": "巡查预警", "permissionList": [{"id": 7, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-导出数据", "permission_name": "导出数据"}, {"id": 8, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入协查", "permission_name": "加入协查"}, {"id": 9, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-查看网站负责人", "permission_name": "查看网站负责人"}, {"id": 10, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入简报", "permission_name": "加入简报"}]}, {"id": 3, "status": 0, "parent_id": 0, "module_info": "违规协查权限模块", "module_name": "违规协查", "permissionList": [{"id": 20, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-协查反馈", "permission_name": "协查反馈"}, {"id": 21, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-处理记录", "permission_name": "处理记录"}]}, {"id": 4, "status": 0, "parent_id": 0, "module_info": "事件排查权限模块", "module_name": "事件排查", "permissionList": [{"id": 22, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-排查", "permission_name": "排查"}, {"id": 23, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-加入协查", "permission_name": "加入协查"}, {"id": 24, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-查看网站负责人", "permission_name": "查看网站负责人"}]}, {"id": 5, "status": 0, "parent_id": 0, "moduleList": [{"id": 9, "status": 0, "parent_id": 5, "module_info": "属地简报-接收人管理", "module_name": "接收人管理", "permissionList": [{"id": 29, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-添加", "permission_name": "添加"}, {"id": 30, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-删除", "permission_name": "删除"}]}], "module_info": "属地简报权限模块", "module_name": "属地简报", "permissionList": [{"id": 25, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-生成报告", "permission_name": "生成报告"}, {"id": 26, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-发送报告", "permission_name": "发送报告"}, {"id": 27, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-下载", "permission_name": "下载"}, {"id": 28, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-查看报告", "permission_name": "查看报告"}]}, {"id": 6, "status": 0, "parent_id": 0, "moduleList": [{"id": 10, "status": 0, "parent_id": 6, "module_info": "个人中心-账号信息", "module_name": "账号信息", "permissionList": [{"id": 31, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改姓名", "permission_name": "修改姓名"}, {"id": 32, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改平台名称", "permission_name": "修改平台名称"}, {"id": 33, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改联系方式", "permission_name": "修改联系方式"}, {"id": 34, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-重置密码", "permission_name": "重置密码"}]}, {"id": 11, "status": 0, "parent_id": 6, "module_info": "个人中心-角色管理", "module_name": "角色管理", "permissionList": [{"id": 35, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-添加", "permission_name": "添加"}, {"id": 36, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-修改", "permission_name": "修改"}, {"id": 37, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-删除", "permission_name": "删除"}]}, {"id": 12, "status": 0, "parent_id": 6, "module_info": "个人中心-用户管理", "module_name": "用户管理", "permissionList": [{"id": 38, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-添加", "permission_name": "添加"}, {"id": 39, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-修改", "permission_name": "修改"}, {"id": 40, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-删除", "permission_name": "删除"}]}], "module_info": "个人中心权限模块", "module_name": "个人中心", "permissionList": []}]'),
	(3, '主管角色', '主管角色hhh', 1565342723127, '[{"id": 1, "status": 1, "parent_id": 0, "module_info": "网站管理权限模块", "module_name": "网站管理", "permissionList": [{"id": 1, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-添加", "permission_name": "添加"}, {"id": 2, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-修改", "permission_name": "修改"}, {"id": 3, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-删除", "permission_name": "删除"}, {"id": 4, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导入网站", "permission_name": "导入网站"}, {"id": 5, "status": 1, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导出数据", "permission_name": "导出数据"}, {"id": 6, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-批量删除", "permission_name": "批量删除"}]}, {"id": 2, "status": 0, "parent_id": 0, "moduleList": [{"id": 7, "status": 0, "parent_id": 2, "module_info": "巡查预警-巡查设置", "module_name": "巡查设置", "permissionList": [{"id": 12, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-新建专题", "permission_name": "新建专题"}, {"id": 13, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-编辑", "permission_name": "编辑"}, {"id": 14, "status": 1, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-删除", "permission_name": "删除"}, {"id": 15, "status": 1, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-上移/下移", "permission_name": "上移/下移"}, {"id": 16, "status": 1, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-首页显示/隐藏", "permission_name": "首页显示/隐藏"}]}, {"id": 8, "status": 0, "parent_id": 2, "module_info": "巡查预警-预警接收人设置", "module_name": "预警接收人设置", "permissionList": [{"id": 17, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-添加", "permission_name": "添加"}, {"id": 18, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-修改", "permission_name": "修改"}, {"id": 19, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-删除", "permission_name": "删除"}]}], "module_info": "巡查预警权限模块", "module_name": "巡查预警", "permissionList": [{"id": 7, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-导出数据", "permission_name": "导出数据"}, {"id": 8, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入协查", "permission_name": "加入协查"}, {"id": 9, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-查看网站负责人", "permission_name": "查看网站负责人"}, {"id": 10, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入简报", "permission_name": "加入简报"}]}, {"id": 3, "status": 0, "parent_id": 0, "module_info": "违规协查权限模块", "module_name": "违规协查", "permissionList": [{"id": 20, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-协查反馈", "permission_name": "协查反馈"}, {"id": 21, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-处理记录", "permission_name": "处理记录"}]}, {"id": 4, "status": 0, "parent_id": 0, "module_info": "事件排查权限模块", "module_name": "事件排查", "permissionList": [{"id": 22, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-排查", "permission_name": "排查"}, {"id": 23, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-加入协查", "permission_name": "加入协查"}, {"id": 24, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-查看网站负责人", "permission_name": "查看网站负责人"}]}, {"id": 5, "status": 0, "parent_id": 0, "moduleList": [{"id": 9, "status": 0, "parent_id": 5, "module_info": "属地简报-接收人管理", "module_name": "接收人管理", "permissionList": [{"id": 29, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-添加", "permission_name": "添加"}, {"id": 30, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-删除", "permission_name": "删除"}]}], "module_info": "属地简报权限模块", "module_name": "属地简报", "permissionList": [{"id": 25, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-生成报告", "permission_name": "生成报告"}, {"id": 26, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-发送报告", "permission_name": "发送报告"}, {"id": 27, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-下载", "permission_name": "下载"}, {"id": 28, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-查看报告", "permission_name": "查看报告"}]}, {"id": 6, "status": 0, "parent_id": 0, "moduleList": [{"id": 10, "status": 0, "parent_id": 6, "module_info": "个人中心-账号信息", "module_name": "账号信息", "permissionList": [{"id": 31, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改姓名", "permission_name": "修改姓名"}, {"id": 32, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改平台名称", "permission_name": "修改平台名称"}, {"id": 33, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改联系方式", "permission_name": "修改联系方式"}, {"id": 34, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-重置密码", "permission_name": "重置密码"}]}, {"id": 11, "status": 0, "parent_id": 6, "module_info": "个人中心-角色管理", "module_name": "角色管理", "permissionList": [{"id": 35, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-添加", "permission_name": "添加"}, {"id": 36, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-修改", "permission_name": "修改"}, {"id": 37, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-删除", "permission_name": "删除"}]}, {"id": 12, "status": 0, "parent_id": 6, "module_info": "个人中心-用户管理", "module_name": "用户管理", "permissionList": [{"id": 38, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-添加", "permission_name": "添加"}, {"id": 39, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-修改", "permission_name": "修改"}, {"id": 40, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-删除", "permission_name": "删除"}]}], "module_info": "个人中心权限模块", "module_name": "个人中心", "permissionList": []}]'),
	(4, '33', '', 1565344598599, '[{"id": 1, "status": 0, "parent_id": 0, "module_info": "网站管理权限模块", "module_name": "网站管理", "permissionList": [{"id": 1, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-添加", "permission_name": "添加"}, {"id": 2, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-修改", "permission_name": "修改"}, {"id": 3, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-删除", "permission_name": "删除"}, {"id": 4, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导入网站", "permission_name": "导入网站"}, {"id": 5, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导出数据", "permission_name": "导出数据"}, {"id": 6, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-批量删除", "permission_name": "批量删除"}]}, {"id": 2, "status": 0, "parent_id": 0, "moduleList": [{"id": 7, "status": 0, "parent_id": 2, "module_info": "巡查预警-巡查设置", "module_name": "巡查设置", "permissionList": [{"id": 12, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-新建专题", "permission_name": "新建专题"}, {"id": 13, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-编辑", "permission_name": "编辑"}, {"id": 14, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-删除", "permission_name": "删除"}, {"id": 15, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-上移/下移", "permission_name": "上移/下移"}, {"id": 16, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-首页显示/隐藏", "permission_name": "首页显示/隐藏"}]}, {"id": 8, "status": 0, "parent_id": 2, "module_info": "巡查预警-预警接收人设置", "module_name": "预警接收人设置", "permissionList": [{"id": 17, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-添加", "permission_name": "添加"}, {"id": 18, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-修改", "permission_name": "修改"}, {"id": 19, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-删除", "permission_name": "删除"}]}], "module_info": "巡查预警权限模块", "module_name": "巡查预警", "permissionList": [{"id": 7, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-导出数据", "permission_name": "导出数据"}, {"id": 8, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入协查", "permission_name": "加入协查"}, {"id": 9, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-查看网站负责人", "permission_name": "查看网站负责人"}, {"id": 10, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入简报", "permission_name": "加入简报"}]}, {"id": 3, "status": 0, "parent_id": 0, "module_info": "违规协查权限模块", "module_name": "违规协查", "permissionList": [{"id": 20, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-协查反馈", "permission_name": "协查反馈"}, {"id": 21, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-处理记录", "permission_name": "处理记录"}]}, {"id": 4, "status": 0, "parent_id": 0, "module_info": "事件排查权限模块", "module_name": "事件排查", "permissionList": [{"id": 22, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-排查", "permission_name": "排查"}, {"id": 23, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-加入协查", "permission_name": "加入协查"}, {"id": 24, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-查看网站负责人", "permission_name": "查看网站负责人"}]}, {"id": 5, "status": 0, "parent_id": 0, "moduleList": [{"id": 9, "status": 0, "parent_id": 5, "module_info": "属地简报-接收人管理", "module_name": "接收人管理", "permissionList": [{"id": 29, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-添加", "permission_name": "添加"}, {"id": 30, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-删除", "permission_name": "删除"}]}], "module_info": "属地简报权限模块", "module_name": "属地简报", "permissionList": [{"id": 25, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-生成报告", "permission_name": "生成报告"}, {"id": 26, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-发送报告", "permission_name": "发送报告"}, {"id": 27, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-下载", "permission_name": "下载"}, {"id": 28, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-查看报告", "permission_name": "查看报告"}]}, {"id": 6, "status": 0, "parent_id": 0, "moduleList": [{"id": 10, "status": 0, "parent_id": 6, "module_info": "个人中心-账号信息", "module_name": "账号信息", "permissionList": [{"id": 31, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改姓名", "permission_name": "修改姓名"}, {"id": 32, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改平台名称", "permission_name": "修改平台名称"}, {"id": 33, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改联系方式", "permission_name": "修改联系方式"}, {"id": 34, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-重置密码", "permission_name": "重置密码"}]}, {"id": 11, "status": 0, "parent_id": 6, "module_info": "个人中心-角色管理", "module_name": "角色管理", "permissionList": [{"id": 35, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-添加", "permission_name": "添加"}, {"id": 36, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-修改", "permission_name": "修改"}, {"id": 37, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-删除", "permission_name": "删除"}]}, {"id": 12, "status": 0, "parent_id": 6, "module_info": "个人中心-用户管理", "module_name": "用户管理", "permissionList": [{"id": 38, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-添加", "permission_name": "添加"}, {"id": 39, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-修改", "permission_name": "修改"}, {"id": 40, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-删除", "permission_name": "删除"}]}], "module_info": "个人中心权限模块", "module_name": "个人中心"}]'),
	(15, 'r1', '', 1565602009542, '[{"id": 1, "status": 0, "parent_id": 0, "module_info": "网站管理权限模块", "module_name": "网站管理", "permissionList": [{"id": 1, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-添加", "permission_name": "添加"}, {"id": 2, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-修改", "permission_name": "修改"}, {"id": 3, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-删除", "permission_name": "删除"}, {"id": 4, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导入网站", "permission_name": "导入网站"}, {"id": 5, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导出数据", "permission_name": "导出数据"}, {"id": 6, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-批量删除", "permission_name": "批量删除"}]}, {"id": 2, "status": 0, "parent_id": 0, "moduleList": [{"id": 7, "status": 0, "parent_id": 2, "module_info": "巡查预警-巡查设置", "module_name": "巡查设置", "permissionList": [{"id": 12, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-新建专题", "permission_name": "新建专题"}, {"id": 13, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-编辑", "permission_name": "编辑"}, {"id": 14, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-删除", "permission_name": "删除"}, {"id": 15, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-上移/下移", "permission_name": "上移/下移"}, {"id": 16, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-首页显示/隐藏", "permission_name": "首页显示/隐藏"}]}, {"id": 8, "status": 0, "parent_id": 2, "module_info": "巡查预警-预警接收人设置", "module_name": "预警接收人设置", "permissionList": [{"id": 17, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-添加", "permission_name": "添加"}, {"id": 18, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-修改", "permission_name": "修改"}, {"id": 19, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-删除", "permission_name": "删除"}]}], "module_info": "巡查预警权限模块", "module_name": "巡查预警", "permissionList": [{"id": 7, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-导出数据", "permission_name": "导出数据"}, {"id": 8, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入协查", "permission_name": "加入协查"}, {"id": 9, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-查看网站负责人", "permission_name": "查看网站负责人"}, {"id": 10, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入简报", "permission_name": "加入简报"}]}, {"id": 3, "status": 0, "parent_id": 0, "module_info": "违规协查权限模块", "module_name": "违规协查", "permissionList": [{"id": 20, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-协查反馈", "permission_name": "协查反馈"}, {"id": 21, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-处理记录", "permission_name": "处理记录"}]}, {"id": 4, "status": 0, "parent_id": 0, "module_info": "事件排查权限模块", "module_name": "事件排查", "permissionList": [{"id": 22, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-排查", "permission_name": "排查"}, {"id": 23, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-加入协查", "permission_name": "加入协查"}, {"id": 24, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-查看网站负责人", "permission_name": "查看网站负责人"}]}, {"id": 5, "status": 0, "parent_id": 0, "moduleList": [{"id": 9, "status": 0, "parent_id": 5, "module_info": "属地简报-接收人管理", "module_name": "接收人管理", "permissionList": [{"id": 29, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-添加", "permission_name": "添加"}, {"id": 30, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-删除", "permission_name": "删除"}]}], "module_info": "属地简报权限模块", "module_name": "属地简报", "permissionList": [{"id": 25, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-生成报告", "permission_name": "生成报告"}, {"id": 26, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-发送报告", "permission_name": "发送报告"}, {"id": 27, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-下载", "permission_name": "下载"}, {"id": 28, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-查看报告", "permission_name": "查看报告"}]}, {"id": 6, "status": 0, "parent_id": 0, "moduleList": [{"id": 10, "status": 0, "parent_id": 6, "module_info": "个人中心-账号信息", "module_name": "账号信息", "permissionList": [{"id": 31, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改姓名", "permission_name": "修改姓名"}, {"id": 32, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改平台名称", "permission_name": "修改平台名称"}, {"id": 33, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改联系方式", "permission_name": "修改联系方式"}, {"id": 34, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-重置密码", "permission_name": "重置密码"}]}, {"id": 11, "status": 0, "parent_id": 6, "module_info": "个人中心-角色管理", "module_name": "角色管理", "permissionList": [{"id": 35, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-添加", "permission_name": "添加"}, {"id": 36, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-修改", "permission_name": "修改"}, {"id": 37, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-删除", "permission_name": "删除"}]}, {"id": 12, "status": 0, "parent_id": 6, "module_info": "个人中心-用户管理", "module_name": "用户管理", "permissionList": [{"id": 38, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-添加", "permission_name": "添加"}, {"id": 39, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-修改", "permission_name": "修改"}, {"id": 40, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-删除", "permission_name": "删除"}]}], "module_info": "个人中心权限模块", "module_name": "个人中心"}]'),
	(17, 'r3', '', 1565602038517, '[{"id": 1, "status": 0, "parent_id": 0, "module_info": "网站管理权限模块", "module_name": "网站管理", "permissionList": [{"id": 1, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-添加", "permission_name": "添加"}, {"id": 2, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-修改", "permission_name": "修改"}, {"id": 3, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-删除", "permission_name": "删除"}, {"id": 4, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导入网站", "permission_name": "导入网站"}, {"id": 5, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-导出数据", "permission_name": "导出数据"}, {"id": 6, "status": 0, "module_id": 1, "create_Time": 1565076127, "permission_info": "网站管理-批量删除", "permission_name": "批量删除"}]}, {"id": 2, "status": 0, "parent_id": 0, "moduleList": [{"id": 7, "status": 0, "parent_id": 2, "module_info": "巡查预警-巡查设置", "module_name": "巡查设置", "permissionList": [{"id": 12, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-新建专题", "permission_name": "新建专题"}, {"id": 13, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-编辑", "permission_name": "编辑"}, {"id": 14, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-删除", "permission_name": "删除"}, {"id": 15, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-上移/下移", "permission_name": "上移/下移"}, {"id": 16, "status": 0, "module_id": 7, "create_Time": 1565076127, "permission_info": "巡查设置-首页显示/隐藏", "permission_name": "首页显示/隐藏"}]}, {"id": 8, "status": 0, "parent_id": 2, "module_info": "巡查预警-预警接收人设置", "module_name": "预警接收人设置", "permissionList": [{"id": 17, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-添加", "permission_name": "添加"}, {"id": 18, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-修改", "permission_name": "修改"}, {"id": 19, "status": 0, "module_id": 8, "create_Time": 1565076127, "permission_info": "预警接收人设置-删除", "permission_name": "删除"}]}], "module_info": "巡查预警权限模块", "module_name": "巡查预警", "permissionList": [{"id": 7, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-导出数据", "permission_name": "导出数据"}, {"id": 8, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入协查", "permission_name": "加入协查"}, {"id": 9, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-查看网站负责人", "permission_name": "查看网站负责人"}, {"id": 10, "status": 0, "module_id": 2, "create_Time": 1565076127, "permission_info": "巡查预警-加入简报", "permission_name": "加入简报"}]}, {"id": 3, "status": 0, "parent_id": 0, "module_info": "违规协查权限模块", "module_name": "违规协查", "permissionList": [{"id": 20, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-协查反馈", "permission_name": "协查反馈"}, {"id": 21, "status": 0, "module_id": 3, "create_Time": 1565076127, "permission_info": "违规协查-处理记录", "permission_name": "处理记录"}]}, {"id": 4, "status": 0, "parent_id": 0, "module_info": "事件排查权限模块", "module_name": "事件排查", "permissionList": [{"id": 22, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-排查", "permission_name": "排查"}, {"id": 23, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-加入协查", "permission_name": "加入协查"}, {"id": 24, "status": 0, "module_id": 4, "create_Time": 1565076127, "permission_info": "事件排查-查看网站负责人", "permission_name": "查看网站负责人"}]}, {"id": 5, "status": 0, "parent_id": 0, "moduleList": [{"id": 9, "status": 0, "parent_id": 5, "module_info": "属地简报-接收人管理", "module_name": "接收人管理", "permissionList": [{"id": 29, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-添加", "permission_name": "添加"}, {"id": 30, "status": 0, "module_id": 9, "create_Time": 1565076127, "permission_info": "接收人管理-删除", "permission_name": "删除"}]}], "module_info": "属地简报权限模块", "module_name": "属地简报", "permissionList": [{"id": 25, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-生成报告", "permission_name": "生成报告"}, {"id": 26, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-发送报告", "permission_name": "发送报告"}, {"id": 27, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-下载", "permission_name": "下载"}, {"id": 28, "status": 0, "module_id": 5, "create_Time": 1565076127, "permission_info": "属地简报-查看报告", "permission_name": "查看报告"}]}, {"id": 6, "status": 0, "parent_id": 0, "moduleList": [{"id": 10, "status": 0, "parent_id": 6, "module_info": "个人中心-账号信息", "module_name": "账号信息", "permissionList": [{"id": 31, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改姓名", "permission_name": "修改姓名"}, {"id": 32, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改平台名称", "permission_name": "修改平台名称"}, {"id": 33, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-修改联系方式", "permission_name": "修改联系方式"}, {"id": 34, "status": 0, "module_id": 10, "create_Time": 1565076127, "permission_info": "账号信息-重置密码", "permission_name": "重置密码"}]}, {"id": 11, "status": 0, "parent_id": 6, "module_info": "个人中心-角色管理", "module_name": "角色管理", "permissionList": [{"id": 35, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-添加", "permission_name": "添加"}, {"id": 36, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-修改", "permission_name": "修改"}, {"id": 37, "status": 0, "module_id": 11, "create_Time": 1565076127, "permission_info": "角色管理-删除", "permission_name": "删除"}]}, {"id": 12, "status": 0, "parent_id": 6, "module_info": "个人中心-用户管理", "module_name": "用户管理", "permissionList": [{"id": 38, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-添加", "permission_name": "添加"}, {"id": 39, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-修改", "permission_name": "修改"}, {"id": 40, "status": 0, "module_id": 12, "create_Time": 1565076127, "permission_info": "用户管理-删除", "permission_name": "删除"}]}], "module_info": "个人中心权限模块", "module_name": "个人中心"}]'),
	(20, '测试员', 'test', 1566380391334, '[{"id":1,"module_info":"网站管理权限模块","module_name":"网站测试","parent_id":0,"permissionList":[{"create_Time":156507613,"id":1,"module_id":1,"permission_info":"网站管理-添加","permission_name":"添加","status":1}],"status":1}]'),
	(21, '测试', '', 1569378043063, '[{"id":1,"module_info":"网站管理权限模块","module_name":"网站管理","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":1,"module_id":1,"permission_info":"网站管理-添加","permission_name":"添加","status":1},{"create_Time":1565076127,"id":2,"module_id":1,"permission_info":"网站管理-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":3,"module_id":1,"permission_info":"网站管理-删除","permission_name":"删除","status":0},{"create_Time":1565076127,"id":4,"module_id":1,"permission_info":"网站管理-导入网站","permission_name":"导入网站","status":0},{"create_Time":1565076127,"id":5,"module_id":1,"permission_info":"网站管理-导出数据","permission_name":"导出数据","status":0},{"create_Time":1565076127,"id":6,"module_id":1,"permission_info":"网站管理-批量删除","permission_name":"批量删除","status":0}],"status":1},{"id":2,"moduleList":[{"id":7,"module_info":"巡查预警-巡查设置","module_name":"巡查设置","parent_id":2,"permissionList":[{"create_Time":1565076127,"id":12,"module_id":7,"permission_info":"巡查设置-新建专题","permission_name":"新建专题","status":0},{"create_Time":1565076127,"id":13,"module_id":7,"permission_info":"巡查设置-编辑","permission_name":"编辑","status":0},{"create_Time":1565076127,"id":14,"module_id":7,"permission_info":"巡查设置-删除","permission_name":"删除","status":0},{"create_Time":1565076127,"id":15,"module_id":7,"permission_info":"巡查设置-上移/下移","permission_name":"上移/下移","status":0},{"create_Time":1565076127,"id":16,"module_id":7,"permission_info":"巡查设置-首页显示/隐藏","permission_name":"首页显示/隐藏","status":0}],"status":0},{"id":8,"module_info":"巡查预警-预警接收人设置","module_name":"预警接收人设置","parent_id":2,"permissionList":[{"create_Time":1565076127,"id":17,"module_id":8,"permission_info":"预警接收人设置-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":18,"module_id":8,"permission_info":"预警接收人设置-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":19,"module_id":8,"permission_info":"预警接收人设置-删除","permission_name":"删除","status":0}],"status":0}],"module_info":"巡查预警权限模块","module_name":"巡查预警","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":7,"module_id":2,"permission_info":"巡查预警-导出数据","permission_name":"导出数据","status":0},{"create_Time":1565076127,"id":8,"module_id":2,"permission_info":"巡查预警-加入协查","permission_name":"加入协查","status":0},{"create_Time":1565076127,"id":9,"module_id":2,"permission_info":"巡查预警-查看网站负责人","permission_name":"查看网站负责人","status":0},{"create_Time":1565076127,"id":10,"module_id":2,"permission_info":"巡查预警-加入简报","permission_name":"加入简报","status":0}],"status":1},{"id":3,"module_info":"违规协查权限模块","module_name":"违规协查","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":20,"module_id":3,"permission_info":"违规协查-协查反馈","permission_name":"协查反馈","status":0},{"create_Time":1565076127,"id":21,"module_id":3,"permission_info":"违规协查-处理记录","permission_name":"处理记录","status":0}],"status":1},{"id":4,"module_info":"事件排查权限模块","module_name":"事件排查","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":22,"module_id":4,"permission_info":"事件排查-排查","permission_name":"排查","status":0},{"create_Time":1565076127,"id":23,"module_id":4,"permission_info":"事件排查-加入协查","permission_name":"加入协查","status":0},{"create_Time":1565076127,"id":24,"module_id":4,"permission_info":"事件排查-查看网站负责人","permission_name":"查看网站负责人","status":0}],"status":1},{"id":5,"moduleList":[{"id":9,"module_info":"属地简报-接收人管理","module_name":"接收人管理","parent_id":5,"permissionList":[{"create_Time":1565076127,"id":29,"module_id":9,"permission_info":"接收人管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":30,"module_id":9,"permission_info":"接收人管理-删除","permission_name":"删除","status":0}],"status":0}],"module_info":"属地简报权限模块","module_name":"属地简报","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":25,"module_id":5,"permission_info":"属地简报-生成报告","permission_name":"生成报告","status":0},{"create_Time":1565076127,"id":26,"module_id":5,"permission_info":"属地简报-发送报告","permission_name":"发送报告","status":0},{"create_Time":1565076127,"id":27,"module_id":5,"permission_info":"属地简报-下载","permission_name":"下载","status":0},{"create_Time":1565076127,"id":28,"module_id":5,"permission_info":"属地简报-查看报告","permission_name":"查看报告","status":0}],"status":1},{"id":6,"moduleList":[{"id":10,"module_info":"个人中心-账号信息","module_name":"账号信息","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":31,"module_id":10,"permission_info":"账号信息-修改姓名","permission_name":"修改姓名","status":0},{"create_Time":1565076127,"id":32,"module_id":10,"permission_info":"账号信息-修改平台名称","permission_name":"修改平台名称","status":0},{"create_Time":1565076127,"id":33,"module_id":10,"permission_info":"账号信息-修改联系方式","permission_name":"修改联系方式","status":0},{"create_Time":1565076127,"id":34,"module_id":10,"permission_info":"账号信息-重置密码","permission_name":"重置密码","status":0}],"status":0},{"id":11,"module_info":"个人中心-角色管理","module_name":"角色管理","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":35,"module_id":11,"permission_info":"角色管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":36,"module_id":11,"permission_info":"角色管理-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":37,"module_id":11,"permission_info":"角色管理-删除","permission_name":"删除","status":0}],"status":0},{"id":12,"module_info":"个人中心-用户管理","module_name":"用户管理","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":38,"module_id":12,"permission_info":"用户管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":39,"module_id":12,"permission_info":"用户管理-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":40,"module_id":12,"permission_info":"用户管理-删除","permission_name":"删除","status":0}],"status":0}],"module_info":"个人中心权限模块","module_name":"个人中心","parent_id":0,"status":1}]'),
	(22, '不能添加网站', '', 1572920835928, '[{"id":1,"module_info":"网站管理权限模块","module_name":"网站管理","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":1,"module_id":1,"permission_info":"网站管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":2,"module_id":1,"permission_info":"网站管理-修改","permission_name":"修改","status":1},{"create_Time":1565076127,"id":3,"module_id":1,"permission_info":"网站管理-删除","permission_name":"删除","status":1},{"create_Time":1565076127,"id":4,"module_id":1,"permission_info":"网站管理-导入网站","permission_name":"导入网站","status":1},{"create_Time":1565076127,"id":5,"module_id":1,"permission_info":"网站管理-导出数据","permission_name":"导出数据","status":1},{"create_Time":1565076127,"id":6,"module_id":1,"permission_info":"网站管理-批量删除","permission_name":"批量删除","status":1}],"status":1},{"id":2,"moduleList":[{"id":7,"module_info":"巡查预警-巡查设置","module_name":"巡查设置","parent_id":2,"permissionList":[{"create_Time":1565076127,"id":12,"module_id":7,"permission_info":"巡查设置-新建专题","permission_name":"新建专题","status":0},{"create_Time":1565076127,"id":13,"module_id":7,"permission_info":"巡查设置-编辑","permission_name":"编辑","status":0},{"create_Time":1565076127,"id":14,"module_id":7,"permission_info":"巡查设置-删除","permission_name":"删除","status":0},{"create_Time":1565076127,"id":15,"module_id":7,"permission_info":"巡查设置-上移/下移","permission_name":"上移/下移","status":0},{"create_Time":1565076127,"id":16,"module_id":7,"permission_info":"巡查设置-首页显示/隐藏","permission_name":"首页显示/隐藏","status":0}],"status":0},{"id":8,"module_info":"巡查预警-预警接收人设置","module_name":"预警接收人设置","parent_id":2,"permissionList":[{"create_Time":1565076127,"id":17,"module_id":8,"permission_info":"预警接收人设置-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":18,"module_id":8,"permission_info":"预警接收人设置-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":19,"module_id":8,"permission_info":"预警接收人设置-删除","permission_name":"删除","status":0}],"status":0}],"module_info":"巡查预警权限模块","module_name":"巡查预警","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":7,"module_id":2,"permission_info":"巡查预警-导出数据","permission_name":"导出数据","status":0},{"create_Time":1565076127,"id":8,"module_id":2,"permission_info":"巡查预警-加入协查","permission_name":"加入协查","status":0},{"create_Time":1565076127,"id":9,"module_id":2,"permission_info":"巡查预警-查看网站负责人","permission_name":"查看网站负责人","status":0},{"create_Time":1565076127,"id":10,"module_id":2,"permission_info":"巡查预警-加入简报","permission_name":"加入简报","status":0}],"status":1},{"id":3,"module_info":"违规协查权限模块","module_name":"违规协查","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":20,"module_id":3,"permission_info":"违规协查-协查反馈","permission_name":"协查反馈","status":0},{"create_Time":1565076127,"id":21,"module_id":3,"permission_info":"违规协查-处理记录","permission_name":"处理记录","status":0}],"status":1},{"id":4,"module_info":"事件排查权限模块","module_name":"事件排查","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":22,"module_id":4,"permission_info":"事件排查-排查","permission_name":"排查","status":0},{"create_Time":1565076127,"id":23,"module_id":4,"permission_info":"事件排查-加入协查","permission_name":"加入协查","status":0},{"create_Time":1565076127,"id":24,"module_id":4,"permission_info":"事件排查-查看网站负责人","permission_name":"查看网站负责人","status":0}],"status":1},{"id":5,"moduleList":[{"id":9,"module_info":"属地简报-接收人管理","module_name":"接收人管理","parent_id":5,"permissionList":[{"create_Time":1565076127,"id":29,"module_id":9,"permission_info":"接收人管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":30,"module_id":9,"permission_info":"接收人管理-删除","permission_name":"删除","status":0}],"status":0}],"module_info":"属地简报权限模块","module_name":"属地简报","parent_id":0,"permissionList":[{"create_Time":1565076127,"id":25,"module_id":5,"permission_info":"属地简报-生成报告","permission_name":"生成报告","status":0},{"create_Time":1565076127,"id":26,"module_id":5,"permission_info":"属地简报-发送报告","permission_name":"发送报告","status":0},{"create_Time":1565076127,"id":27,"module_id":5,"permission_info":"属地简报-下载","permission_name":"下载","status":0},{"create_Time":1565076127,"id":28,"module_id":5,"permission_info":"属地简报-查看报告","permission_name":"查看报告","status":0}],"status":1},{"id":6,"moduleList":[{"id":10,"module_info":"个人中心-账号信息","module_name":"账号信息","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":31,"module_id":10,"permission_info":"账号信息-修改姓名","permission_name":"修改姓名","status":0},{"create_Time":1565076127,"id":32,"module_id":10,"permission_info":"账号信息-修改平台名称","permission_name":"修改平台名称","status":0},{"create_Time":1565076127,"id":33,"module_id":10,"permission_info":"账号信息-修改联系方式","permission_name":"修改联系方式","status":0},{"create_Time":1565076127,"id":34,"module_id":10,"permission_info":"账号信息-重置密码","permission_name":"重置密码","status":0}],"status":0},{"id":11,"module_info":"个人中心-角色管理","module_name":"角色管理","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":35,"module_id":11,"permission_info":"角色管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":36,"module_id":11,"permission_info":"角色管理-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":37,"module_id":11,"permission_info":"角色管理-删除","permission_name":"删除","status":0}],"status":0},{"id":12,"module_info":"个人中心-用户管理","module_name":"用户管理","parent_id":6,"permissionList":[{"create_Time":1565076127,"id":38,"module_id":12,"permission_info":"用户管理-添加","permission_name":"添加","status":0},{"create_Time":1565076127,"id":39,"module_id":12,"permission_info":"用户管理-修改","permission_name":"修改","status":0},{"create_Time":1565076127,"id":40,"module_id":12,"permission_info":"用户管理-删除","permission_name":"删除","status":0}],"status":0}],"module_info":"个人中心权限模块","module_name":"个人中心","parent_id":0,"status":1}]');
/*!40000 ALTER TABLE `db_role` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_site_category
CREATE TABLE IF NOT EXISTS `db_site_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(128) NOT NULL COMMENT '分类名称',
  `parent_id` int(11) NOT NULL COMMENT '父级分类的id',
  `sort` int(11) NOT NULL COMMENT '同级别排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='网站管理中有关网站的分类';

-- Dumping data for table data_sitems.db_site_category: ~25 rows (大约)
/*!40000 ALTER TABLE `db_site_category` DISABLE KEYS */;
INSERT INTO `db_site_category` (`id`, `category_name`, `parent_id`, `sort`) VALUES
	(4, '四级分类', -1, 1),
	(11, '444', 4, 15),
	(14, '11223', 2, 14),
	(15, '222', 4, 11),
	(17, '123', 15, 17),
	(18, '345', 15, 18),
	(20, '12233', 4, 20),
	(21, '请问', -1, 6),
	(22, '777', 11, 22),
	(23, '3333', 15, 23),
	(38, '123', -1, 38),
	(39, '22', 27, 39),
	(40, '21', 39, 40),
	(41, '11', 27, 41),
	(42, '213', 41, 42),
	(43, '5', 30, 43),
	(44, '15', 31, 44),
	(45, '555', 43, 45),
	(46, '111', -1, 46),
	(51, '我是赵国荣我是赵国荣', -1, 51),
	(52, '隧道股V222222', -1, 52),
	(55, '新闻01', -1, 55),
	(56, '测试验收分类', -1, 56),
	(57, '子分类1', 56, 57),
	(58, '子分类2', 57, 58);
/*!40000 ALTER TABLE `db_site_category` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_system
CREATE TABLE IF NOT EXISTS `db_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` varchar(520) NOT NULL DEFAULT '' COMMENT '平台名称',
  `alerturl` varchar(520) DEFAULT NULL COMMENT '巡查预警部分api地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- Dumping data for table data_sitems.db_system: ~1 rows (大约)
/*!40000 ALTER TABLE `db_system` DISABLE KEYS */;
INSERT INTO `db_system` (`id`, `platform`, `alerturl`) VALUES
	(1, '属地网站管理平台1', 'http://116.62.63.211:8085');
/*!40000 ALTER TABLE `db_system` ENABLE KEYS */;

-- Dumping structure for table data_sitems.db_user
CREATE TABLE IF NOT EXISTS `db_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '账户关联角色',
  `account` varchar(64) NOT NULL DEFAULT '' COMMENT '帐号，登录账户，唯一的',
  `user_name` varchar(520) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) NOT NULL DEFAULT '' COMMENT 'md5 加密密码',
  `contact` varchar(64) DEFAULT '' COMMENT '联系手机号',
  `avatar` varchar(520) DEFAULT '' COMMENT '头像url',
  `token` varchar(520) DEFAULT '' COMMENT 'accessToken认证',
  `account_begints` bigint(20) DEFAULT '0' COMMENT '账号开始时间戳',
  `account_endts` bigint(20) DEFAULT '0' COMMENT '账号结束时间戳',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '帐号创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- Dumping data for table data_sitems.db_user: ~12 rows (大约)
/*!40000 ALTER TABLE `db_user` DISABLE KEYS */;
INSERT INTO `db_user` (`id`, `role_id`, `account`, `user_name`, `password`, `contact`, `avatar`, `token`, `account_begints`, `account_endts`, `create_time`) VALUES
	(2, 1, '00002', 'user002', 'e10adc3949ba59abbe56e057f20f883e', '15996313709', '', '000021565664865739', NULL, 1571962080000, 1565602121118),
	(3, 2, '00003', 'sun5555', 'e10adc3949ba59abbe56e057f20f883e', '1234566', '', '000031567647814388', NULL, 1571846400000, 1565168659554),
	(4, 2, '00005', 'sun5555', 'e10adc3949ba59abbe56e057f20f883e', '1234566', '', '000051565680326629', NULL, NULL, 1565334304331),
	(5, 1, '00004', 'sun4444', 'e10adc3949ba59abbe56e057f20f883e', '1335465222', '', '000041571643623000', NULL, NULL, 1565334343581),
	(6, 1, '110', '仓木麻衣', 'e10adc3949ba59abbe56e057f20f883e', '123123123', '', NULL, NULL, NULL, 1565502847425),
	(9, 1, '111110', '你好', 'e10adc3949ba59abbe56e057f20f883e', '123', '', NULL, NULL, NULL, 1565517971488),
	(10, 15, 'ddd', '好1', '77963b7a931377ad4ab5ad6a9cd718aa', '', '', NULL, NULL, NULL, 1565602121118),
	(11, 1, 'root', 'root', 'e10adc3949ba59abbe56e057f20f883e', '18136489133', '', 'root1571987138835', 0, 0, 1565602121118),
	(12, 1, 'zhangsan', '张三', 'e10adc3949ba59abbe56e057f20f883e', '', '', NULL, NULL, 1567180800000, 1565662261763),
	(13, 3, 'lisi', '李四', 'e10adc3949ba59abbe56e057f20f883e', '', '', NULL, NULL, 1565884800000, 1565663232793),
	(14, 4, 'wangwu', '王五', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'wangwu1565663744159', NULL, 1565539200000, 1565663286329),
	(15, 1, '1234561', 'chenlin3', '123456', '111111', '', '12345611566465230466', NULL, NULL, 1566373194128),
	(16, 1, '123456', 'chenlin', '12345678', NULL, '', NULL, NULL, NULL, 1566373714136),
	(17, 20, 'wangxuecong', '王学聪', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'wangxuecong1569738950832', NULL, 1632326400000, 1569378081388),
	(18, 22, 'syx', '施莹旭', 'e10adc3949ba59abbe56e057f20f883e', '', '', NULL, NULL, 1574956800000, 1572920862856);
/*!40000 ALTER TABLE `db_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
