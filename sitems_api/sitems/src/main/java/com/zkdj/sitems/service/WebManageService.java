package com.zkdj.sitems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.WebObject;
import com.zkdj.sitems.bean.WebParam;
import com.zkdj.sitems.http.HttpConfig;
import com.zkdj.sitems.http.HttpService;
import com.zkdj.sitems.http.TokenUtil;
import com.zkdj.sitems.mapper.WebManageMapper;
import com.zkdj.sitems.model.DRegion;
import com.zkdj.sitems.result.ResultObject;

@Service
public class WebManageService {
	@Autowired
	private WebManageMapper mWebManageMapper;

	@Autowired
	private HttpService mHttpService;

	// 获取区域列表
	public JSONObject getRegionList() {
		JSONObject mJSONObject = new JSONObject();

		DRegion mDRegion = mWebManageMapper.getRegion("湖南省");
		if (mDRegion != null) {
			int parent_id = mDRegion.getId();
			List<DRegion> mlist = mWebManageMapper.getRegionList(parent_id);

			mJSONObject.put("id", mDRegion.getId());
			mJSONObject.put("name", mDRegion.getName());
			mJSONObject.put("object", mlist);
			return mJSONObject;
		} else {
			return null;
		}
	}

	// 查询网站数据列表
	public String getWebList(WebParam mParams) {
		int page_no = 0;
		int page_size = 0;

		if (mParams.getPage_no() == 0) {
			page_no = 1;
		} else {
			page_no = mParams.getPage_no();
		}

		if (mParams.getPage_size() == 0) {
			page_size = 10; // 默认10条
		} else {
			page_size = mParams.getPage_size();
		}

		String path = "/web_restserver/index/query/fullQuery?page_no=" + page_no + "&page_size=" + page_size + "&";
		JSONObject json = new JSONObject();
		// 必填
		json.put("province", mParams.getProvince());

		if (mParams.getCity() != null && mParams.getCity().length() > 0) {
			json.put("city", mParams.getCity());
		}

		if (mParams.getOrganizerNature() != null && mParams.getOrganizerNature().length() > 0) {
			json.put("organizerNature", mParams.getOrganizerNature());
		}

		if (mParams.getLevel() != null && mParams.getLevel().length() > 0) {
			json.put("level", mParams.getLevel());
		}

		if (mParams.getCategory() != null && mParams.getCategory().length() > 0) {
			json.put("category", mParams.getCategory());
		}

		if (mParams.getSearchField() != null && mParams.getSearchField().length() > 0) {
			json.put("searchField", mParams.getSearchField());
			json.put("searchValue", mParams.getSearchValue());
		}

		if (mParams.getSortField() != null && mParams.getSortField().length() > 0) {
			json.put("sortField", mParams.getSortField());
			json.put("sortType", mParams.getSortType());
		}

		String result = mHttpService.postData(path, json.toString());

		return result;
	}

	// 添加新的网站数据
	public ResultObject<WebObject> addNewWeb(WebObject mWebObject) {
		ResultObject<WebObject> mResult = new ResultObject<WebObject>();
		String path = "/web_restserver/index/modify/saveOrUpdate?";

		String url = mHttpService.handleURL(HttpConfig.address,path); // 完整的url
		String id = TokenUtil.MD5(url);
		mWebObject.setId(id);

		String json = JSONObject.toJSONString(mWebObject);
		String result = mHttpService.postTotalURLData(url, json.toString());
		JSONObject mJSONObject = JSONObject.parseObject(result);
		if (mJSONObject == null) {
			mResult.setCode(-2);
			mResult.setMessage("添加新网站失败！");
			return mResult;
		}

		if (mJSONObject.getString("code").equals("0")) {
			mResult.setCode(0);
			mResult.setMessage("添加新网站成功！");
			mResult.setObject(mWebObject);
		} else {
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}
		return mResult;
	}

	// 修改网站数据
	public String updateWebData(WebObject mWebObject) {
		String path = "/web_restserver/index/modify/saveOrUpdate?";

		String json = JSONObject.toJSONString(mWebObject);
		String result = mHttpService.postData(path, json.toString());
		return result;
	}

	// 修改网站数据:批量修改分类
	public String updateWebCategoryBulk(List<String> ids, List<String> category) {
		String path = "/web_restserver/index/modify/bulkUpdate?";

		JSONObject mJSONObject = new JSONObject();
		mJSONObject.put("ids", ids);
		mJSONObject.put("category", category);
		String result = mHttpService.postData(path, mJSONObject.toString());
		return result;
	}

	// 删除网站：根据id
	public String deleteWeb(String id) {
		String path = "/web_restserver/index/modify/delete?";

		JSONObject mJSONObject = new JSONObject();
		mJSONObject.put("id", id);
		String result = mHttpService.postData(path, mJSONObject.toString());
		return result;
	}

	// 删除网站：批量删除
	public String deleteBulkWeb(List<String> ids) {
		String path = "/web_restserver/index/modify/bulkDelete?";
		JSONObject mJSONObject = new JSONObject();
		mJSONObject.put("ids", ids);
		String result = mHttpService.postData(path, mJSONObject.toString());
		return result;
	}

	// 删除网站：根据分类id
	public String deleteBulkWebByCategoryID(int category_id) {
		String path = "/web_restserver/index/modify/bulkDelete?";
		JSONObject mJSONObject = new JSONObject();
		String id = category_id + "";
		mJSONObject.put("category", id);
		String result = mHttpService.postData(path, mJSONObject.toString());
		return result;
	}

	// 统计：查询全部
	public String getAllWebCounts(WebParam mParams) {
		String path = "/web_restserver/aggr/dataStatistics/aggrAllCounts?";

		JSONObject json = new JSONObject();
		// 必填
		json.put("province", mParams.getProvince());

		if (mParams.getCity() != null && mParams.getCity().length() > 0) {
			json.put("city", mParams.getCity());
		}
		String result = mHttpService.postData(path, json.toString());
		return result;
	}

	// 统计：根据分类
	public String getWebCountsByCategory(WebParam mParams) {
		String path = "/web_restserver/aggr/dataStatistics/aggrCountsByCategory?";
		JSONObject json = new JSONObject();
		// 必填
		json.put("province", mParams.getProvince());

		if (mParams.getCity() != null && mParams.getCity().length() > 0) {
			json.put("city", mParams.getCity());
		}

		if (mParams.getOrganizerNature() != null && mParams.getOrganizerNature().length() > 0) {
			json.put("organizerNature", mParams.getOrganizerNature());
		}

		if (mParams.getLevel() != null && mParams.getLevel().length() > 0) {
			json.put("level", mParams.getLevel());
		}

		if (mParams.getSearchField() != null && mParams.getSearchField().length() > 0) {
			json.put("searchField", mParams.getSearchField());
			json.put("searchValue", mParams.getSearchValue());
		}

		String result = mHttpService.postData(path, json.toString());

		return result;
	}

	// 导出：查询网站数据，根据ids
	public String getWebByIDs(List<String> ids) {
		String path = "/web_restserver/index/query/queryByIDs?";
		JSONObject json = new JSONObject();
		json.put("ids", ids);
		String result = mHttpService.postData(path, json.toString());

		return result;
	}

	// 导出：查询网站数据，根据条件
	public String getWebByQuerys(WebParam mParams) {
		String path = "/web_restserver/data/export/exportByquerys?";
		JSONObject json = new JSONObject();
		// 必填
		json.put("province", mParams.getProvince());

		if (mParams.getCity() != null && mParams.getCity().length() > 0) {
			json.put("city", mParams.getCity());
		}

		if (mParams.getOrganizerNature() != null && mParams.getOrganizerNature().length() > 0) {
			json.put("organizerNature", mParams.getOrganizerNature());
		}

		if (mParams.getLevel() != null && mParams.getLevel().length() > 0) {
			json.put("level", mParams.getLevel());
		}

		if (mParams.getCategory() != null && mParams.getCategory().length() > 0) {
			json.put("category", mParams.getCategory());
		}

		if (mParams.getSearchField() != null && mParams.getSearchField().length() > 0) {
			json.put("searchField", mParams.getSearchField());
			json.put("searchValue", mParams.getSearchValue());
		}

		String result = mHttpService.postData(path, json.toString());

		return result;
	}

	// 添加新的网站数据:批量添加
	public ResultObject<Object> bulkSaveNewWeb(List<WebObject> mWebObjectList) {
		ResultObject<Object> mResult = new ResultObject<Object>();
		String path = "/web_restserver/index/modify/bulkSave?";

		for (int i = 0; i < mWebObjectList.size(); i++) {
			WebObject mWebObject = mWebObjectList.get(i);
			if (mWebObject.getProvince() == null || mWebObject.getProvince().isEmpty()) {
				continue;
			}

			if (mWebObject.getSiteName() == null || mWebObject.getSiteName().isEmpty()) {
				continue;
			}

			if (mWebObject.getDomainName() == null || mWebObject.getDomainName().isEmpty()) {
				continue;
			}
			String url = mHttpService.handleURL(HttpConfig.address,path + i); // 完整的url
			String id = TokenUtil.MD5(url);
			mWebObject.setId(id);
		}

		JSONObject json = new JSONObject();
		// 必填
		json.put("indexes", mWebObjectList);
		String result = mHttpService.postData(path, json.toString());
		JSONObject mJSONObject = JSONObject.parseObject(result);
		if (mJSONObject == null) {
			mResult.setCode(-2);
			mResult.setMessage("批量添加新网站失败！");
			return mResult;
		}

		if (mJSONObject.getString("code").equals("0")) {
			mResult.setCode(0);
			mResult.setMessage("批量添加新网站成功！");
		} else {
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}
		return mResult;
	}
}
