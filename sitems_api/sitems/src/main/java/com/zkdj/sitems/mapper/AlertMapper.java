package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import com.zkdj.sitems.model.DAlertReceiver;
import com.zkdj.sitems.model.DAlertWordSubject;
import com.zkdj.sitems.model.DAlertCategory;

@Mapper
public interface AlertMapper {
	// db_alert_category
	// 查询巡查预警的专题分类：根据父级ID parent_id
	@Select("SELECT * FROM db_alert_category WHERE parent_id = #{parent_id} and account_id = #{account_id} order by sort")
	List<DAlertCategory> getAlertCategoryListByParentID(@Param("parent_id") int parent_id,@Param("account_id")int account_id);

	// 专题分类:根据id删除
	@Delete("DELETE FROM db_alert_category WHERE id = #{id}")
	int deleteAlertCategoryByID(int id);

	// 专题分类:添加
	@Insert("INSERT INTO db_alert_category(name,parent_id,sort,account_id) VALUES(#{name}, #{parent_id}, #{sort}, #{account_id})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertAlertCategory(DAlertCategory receiver);
	
	// 专题分类:重命名
	@Update("UPDATE db_alert_category SET name=#{name} WHERE id =#{id}")
	int renameAlertCategory(DAlertCategory receiver);
	
	// 专题分类:更新设置信息
	@Update("UPDATE db_alert_category SET isalert=#{isalert},alert_receiver=#{alert_receiver},alert_type=#{alert_type}"
			+ ",begin_time=#{begin_time},end_time=#{end_time} WHERE id =#{id}")
	int updateAlertCategory(DAlertCategory receiver);
	
	// 专题分类:设置是否在首页显示
	@Update("UPDATE db_alert_category SET isshow=#{isshow} WHERE id =#{id}")
	int setAlertCategoryShow(DAlertCategory receiver);
	
	// 专题分类:交换顺序
	@Update("update db_alert_category as q1 join db_alert_category as q2 on (q1.id=#{upid} and q2.id = #{downid})" + 
			" or (q1.id = #{downid} and q2.id=#{upid}) set q1.sort = q2.sort,q2.sort=q1.sort;")
	int setAlertCategorySort(int upid, int downid);
	
	// 专题分类:根据名称查询
	@Select("SELECT * FROM db_alert_category WHERE name = #{name}")
	List<DAlertCategory> getAlertCategoryByName(String name);

	// 专题分类:根据id查询
	@Select("SELECT * FROM db_alert_category WHERE id = #{id}")
	DAlertCategory getAlertCategoryByID(int id);

	// db_alert_receiver
	// 预警接收人:获取全部列表
	@Select("SELECT * FROM db_alert_receiver")
	List<DAlertReceiver> getAlertReceiverList();

	// 预警接收人:根据id删除
	@Delete("DELETE FROM db_alert_receiver WHERE id = #{id}")
	int deleteAlertReceiverByID(int id);

	// 预警接收人:添加
	@Insert("INSERT INTO db_alert_receiver(name,email,phone) VALUES(#{name}, #{email}, #{phone})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertAlertReceiver(DAlertReceiver receiver);

	// 预警接收人:修改
	@Update("UPDATE db_alert_receiver SET name=#{name},email=#{email}, phone=#{phone} WHERE id =#{id}")
	int updateAlertReceiver(DAlertReceiver receiver);

	// 预警接收人:根据名称查询
	@Select("SELECT * FROM db_alert_receiver WHERE name = #{name}")
	List<DAlertReceiver> getAlertReceiverByName(String name);

	// 预警接收人:根据id查询
	@Select("SELECT * FROM db_alert_receiver WHERE id = #{id}")
	DAlertReceiver getAlertReceiverByID(int id);

	// db_alert_word_subject
	// 批量插入预警关键词和专题映射
	@Insert({
		 "<script>",
		 "insert into db_alert_word_subject(word, subject_id) values ",
		 "<foreach collection='mlist' item='item' index='index' separator=','>",
		 "(#{item.word}, #{item.subject_id})",
		 "</foreach>",
		 "</script>"
		})
	int insertBulkAlertWord(@Param(value="mlist") List<DAlertWordSubject> mlist);
	
	// 保存预警关键词:根据词查询
	@Select("SELECT * FROM db_alert_word_subject WHERE word = #{word}")
	List<DAlertWordSubject> getAlertWordSubject(String word);
	
	// 保存预警关键词:根据专题id
	@Select("SELECT * FROM db_alert_word_subject WHERE subject_id = #{subject_id}")
	List<DAlertWordSubject> getAlertWordSubjectBySubjectID(int subject_id);
	
    // 保存预警关键词:根据专题ID删除
	@Delete("DELETE FROM db_alert_word_subject WHERE subject_id = #{subject_id}")
	int deleteAlertWordSubject(int subject_id);
	
	@Select("SELECT * FROM db_alert_word_subject WHERE subject_id = #{subject_id}")
	List<DAlertWordSubject> getAlertWordBySubjectId(int subject_id);
}
