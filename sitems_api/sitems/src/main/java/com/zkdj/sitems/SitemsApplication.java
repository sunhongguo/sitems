package com.zkdj.sitems;

import org.springframework.boot.SpringApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SitemsApplication {
	
	public final static Logger logger = LoggerFactory.getLogger(SitemsApplication.class);

	public static void main(String[] args) {
		
		logger.info("sitems application is running ....");
		
		SpringApplication.run(SitemsApplication.class, args);
	}

}

