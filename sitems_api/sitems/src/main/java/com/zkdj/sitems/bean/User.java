package com.zkdj.sitems.bean;

import java.io.Serializable;

public class User implements Serializable{
	
	/**
	 * 返回的用户对象
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String account;
	private String user_name;
	private String password;  // 密码
	private String contact;    // 联系电话
	private String token; // 访问令牌
	private int role_id;  // 关联的角色id
	private String account_begints; // 账号开始时间
	private String account_endts; // 账号结束时间
	private String create_time;   // 帐号创建时间
	private int account_day;   // 剩余天数

	public User(){
		super();
	}

	public User(String account,String user_name, String password, String contact,String avatar,String token,
			int role_id,String begints, String endts, String create_time,int account_day) {
		this.account = account;
		this.user_name = user_name;
		this.password = password;
		this.contact = contact;
		this.token = token;
		
		this.role_id = role_id;
		this.account_begints = begints;
		this.account_endts = endts;
		this.create_time = create_time;
		this.account_day = account_day;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// account
	public void setAccount(String account){
		this.account = account;
	}
	public String getAccount(){
		return account;
	}
		
	// user_name
	public void setUser_name(String user_name){
		this.user_name = user_name;
	}
	public String getUser_name(){
		return user_name;
	}
	
	// password
	public void setPassword(String password){
		this.password = password;
	}
	public String getPassword(){
		return password;
	}
	
	// contact
	public void setContact(String phone){
		this.contact = phone;
	}
	public String getContact(){
		return contact;
	}
	
	// token
	public void setToken(String token){
		this.token = token;
	}
	public String getToken(){
		return token;
	}
	
	// role_id
	public void setRole_id(int role_id){
		this.role_id = role_id;
	}
	public int getRole_id(){
		return role_id;
	}
	
	// account_begints
	public void setAccount_begints(String begints){
		this.account_begints = begints;
	}
	public String getAccount_begints(){
		return account_begints;
	}
	
	// account_endts
	public void setAccount_endts(String endts){
		this.account_endts = endts;
	}
	public String getAccount_endts(){
		return account_endts;
	}
	
	// account_day
	public void setAccount_day(int days){
		this.account_day = days;
	}
	public int getAccount_day(){
		return account_day;
	}
	
	// create_time
	public void setCreate_time(String create_time){
		this.create_time = create_time;
	}
	public String getCreate_time(){
		return create_time;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "account: " + this.account + " ,userName: " + this.user_name + ", role_id: " + role_id;
	}
}

