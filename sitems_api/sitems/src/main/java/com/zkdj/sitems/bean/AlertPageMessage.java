package com.zkdj.sitems.bean;

import java.util.List;

/**
 * 查询巡查预警网站时，接受条件参数的类
 * 
 * @author DELL
 *
 */
public class AlertPageMessage {
	private List<String> mustKeyWords; // 全部被包含关键词，默认为空
	private List<String> shouldKeyWords; // 任意被包含关键词，默认为空
	private List<String> unrelated; // 排除关键词，默认为空
	private List<String> domainName; // 网站域名
	private String isWarning; // 是否预警，枚举值: 0 未预警 1 已预警 默认全部
	private String start_time; // 开始时间：“2019-09-12 00:00:00”
	private String end_time; // 结束时间
	private String sortField; // 排序字段，支持createTime和domainName字段
	private String sortType; // 排序方式，枚举值：asc和desc
	private List<String> subjectIds;  // 专题id
	
	private int page_no; // 页数
	private int page_size; // 条数
	
	private List<String> ids; // 多个文章ids: 根据勾选的ids导出违规文章

	// subjectIds
	public List<String> getSubjectIds() {
		return subjectIds;
	}

	public void setSubjectIds(List<String> subjectIds) {
		this.subjectIds = subjectIds;
	}

	// mustKeyWords
	public void setMustKeyWords(List<String> mustKeyWords) {
		this.mustKeyWords = mustKeyWords;
	}

	public List<String> getMustKeyWords() {
		return mustKeyWords;
	}

	// shouldKeyWords
	public void setShouldKeyWords(List<String> shouldKeyWords) {
		this.shouldKeyWords = shouldKeyWords;
	}

	public List<String> getShouldKeyWords() {
		return shouldKeyWords;
	}

	// unrelated
	public void setUnrelated(List<String> unrelated) {
		this.unrelated = unrelated;
	}

	public List<String> getUnrelated() {
		return unrelated;
	}

	// domainName
	public void setDomainName(List<String> domainName) {
		this.domainName = domainName;
	}

	public List<String> getDomainName() {
		return domainName;
	}

	// isWarning
	public void setIsWarning(String isWarning) {
		this.isWarning = isWarning;
	}

	public String getIsWarning() {
		return isWarning;
	}

	// start_time
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getStart_time() {
		return start_time;
	}

	// end_time
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	// sortField
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	// sortType
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getSortType() {
		return sortType;
	}

	// page_no
	public void setPage_no(int no) {
		this.page_no = no;
	}

	public int getPage_no() {
		return page_no;
	}

	// page_size
	public void setPage_size(int size) {
		this.page_size = size;
	}

	public int getPage_size() {
		return page_size;
	}
	
	// ids
	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	@Override
	public String toString() {
		return "start_time: " + start_time + ",end_time: " + end_time;
	}

}
