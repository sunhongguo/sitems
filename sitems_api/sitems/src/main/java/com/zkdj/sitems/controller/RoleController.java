package com.zkdj.sitems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.Role;
import com.zkdj.sitems.model.DRole;
import com.zkdj.sitems.result.ResultList;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.service.RoleService;

/*
 * 角色API控制器：接受并处理和角色API相关的url
 */

@RestController
@RequestMapping("/role")
public class RoleController {
	@Autowired
	private RoleService mRoleService;
	@Autowired
	private PermissionsService mPermissionsService;
	
	@RequestMapping("/create")
	public ResultObject<?> createNewRole(@RequestBody DRole mRole){
		SitemsApplication.logger.info("----create New Role-----" + mRole);
		ResultObject<?> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","角色管理","添加");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}	
		
		if (mRole == null){
			mResult.setCode(-1);
			mResult.setMessage("上传的参数错误！");
			return mResult;
		}
		
		if (mRole.getRole_name() != null && mRole.getRole_name().length() > 0){			
			try {
				mResult = mRoleService.createNewRole(mRole);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			mResult.setCode(-2);
			mResult.setMessage("角色名称为空！");
			return mResult;
		}
			
		return mResult;
	}

	@RequestMapping("/update")
	public ResultObject<?> updateRole(@RequestBody DRole mRole){
		SitemsApplication.logger.info("----update Role-----" + mRole);
		ResultObject<?> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","角色管理","修改");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mRole == null){
			mResult.setCode(-1);
			mResult.setMessage("上传的参数错误！");
			return mResult;
		}
		
		if (mRole.getRole_name() != null && mRole.getRole_name().length() > 0){			
			try {
				mResult = mRoleService.updateRole(mRole);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			mResult.setCode(-2);
			mResult.setMessage("角色名称为空！");
			return mResult;
		}
			
		return mResult;
	}
	
	// 获取角色列表
	@RequestMapping("/querylist")
	public ResultList<Role> getRoleList(){
		ResultList<Role> mResult = new ResultList<Role>();
		List<Role> mList = null;
		try {
			mList = mRoleService.getRoleList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(mList != null){
			mResult.setCode(0);
			mResult.setMessage("查询角色列表成功！");
			mResult.setObject(mList);
		}else{
			mResult.setCode(-1);
			mResult.setMessage("查询角色列表失败！");
		}
		return mResult;		
	}
	
	//删除角色
	@RequestMapping("/delete")
	public ResultObject<?> deleteRole(int role_id){
		ResultObject<?> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","角色管理","删除");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		mResult = mRoleService.deleteRole(role_id);
		return mResult;		
	}
	
}
