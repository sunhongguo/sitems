package com.zkdj.sitems.interceptor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.redis.RedisUtil;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.JwtUtil;

import io.jsonwebtoken.Claims;

public class LoginInterceptor implements HandlerInterceptor {
	@Resource
	private RedisUtil mRedisUtil;
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	//请求进入这个拦截器
    	SitemsApplication.logger.info("--------LoginInterceptor----url-----" + request.getRequestURL());
    	String token = request.getHeader("sitems_token");
    	if(token != null && token.length() > 0){
    		SitemsApplication.logger.info("--------LoginInterceptor----sitems_token-----" + token);    		
            // 验证 token            
    		Claims claims = JwtUtil.parseJWT(token,Config.JWT_PWD);
            if(claims == null){
            	sendResultMessage(response,-1002,"Token验证失败,无效或被修改！");
            	return false;
            }

            String userId = (String) claims.get("userid");
            if(userId == null || userId.length() == 0) {
            	sendResultMessage(response,-1003,"Token无效:用户id不存在！");
            	return false;
            }
            if(mRedisUtil != null) {
            	boolean log = mRedisUtil.set(Config.REDIS_USERID_KEY, userId);
            	if(!log) {
            		sendResultMessage(response,-1004,"数据保存错误！");
                	return false;
            	}
            }          
            return true;        //有的话就继续操作
    	}else{
    		SitemsApplication.logger.info("--------LoginInterceptor----sitems_token null-----");
    		sendResultMessage(response,-1000,"Token无效或不存在！");
    		return false;
    	} 
//    	return true;
    }
    
    private void sendResultMessage(HttpServletResponse response,int code,String message)throws Exception {
    	ResultObject<Object> mResult = new ResultObject<>();
		mResult.setCode(code);
		mResult.setMessage(message);
		String jsonObjectStr = JSONObject.toJSONString(mResult);
        returnJson(response,jsonObjectStr);
    }

    private void returnJson(HttpServletResponse response, String json) throws Exception{
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", "sitems_token,Content-Type");
        
        try {
            writer = response.getWriter();
            writer.print(json);
 
        } catch (IOException e) {
        	SitemsApplication.logger.info("-----Login Interceptor-----response error--------");
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
