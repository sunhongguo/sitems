package com.zkdj.sitems.bean;

/*
 * 查询简报的参数
 */
public class ReportMessage {
	private int page;  // 第几页,从1开始
    private int count;  // 每页条数
	
	private long start_time;
	private long end_time;
	
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public long getStart_time() {
		return start_time;
	}
	public void setStart_time(long start_time) {
		this.start_time = start_time;
	}
	public long getEnd_time() {
		return end_time;
	}
	public void setEnd_time(long end_time) {
		this.end_time = end_time;
	}
	@Override
	public String toString() {
		return "ReportMessage [page=" + page + ", count=" + count + ", start_time=" + start_time + ", end_time="
				+ end_time + "]";
	}
}
