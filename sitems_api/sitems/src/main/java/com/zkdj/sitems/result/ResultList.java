package com.zkdj.sitems.result;

import java.util.List;

/*
 * 返回的结果：json数据格式
 * code：数值，表示结果不同的状态，0表示成功
 * message：结果不同状态的原因描述
 * object： 对象的列表
 */
public class ResultList<T> {
	private int code;
	private String message;
	private List<T> object;
	
	public ResultList(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(List<T> object){
		this.object = object;
	}
	public List<T> getObject(){
		return object;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}
