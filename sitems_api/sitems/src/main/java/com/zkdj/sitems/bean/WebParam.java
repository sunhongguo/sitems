package com.zkdj.sitems.bean;

import java.util.List;

/**
 * 网站管理API中接受post提交的参数的类
 * 
 * @author DELL
 *
 */
public class WebParam {
	private String searchField;	//	查询字段，枚举值:siteName和contact,siteName 网站名称,contact 联系人
	private String searchValue;	//	搜索条件
	
	private String province; // 所在省份,必须
	private String city; // 所在城市，默认全部,非必须
	private String organizerNature; // 主办单位,非必须
	private String category; // 网站分类,非必须
	private String level; // 等级,非必须 枚举值: A B C 默认全部
	
	private String sortField;	//排序字段，支持Index中除siteName和contact之外的所有字段
	private String sortType;	// 排序方式，枚举值：asc和desc

	
	private int page_no;  // 页数
	private int page_size;  // 条数
	
	private String id; // 网站id
	private List<String> ids; // 多个网站ids
	
	private List<String> category_ids; // 多个分类ids

	// searchField
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public String getSearchField() {
		return searchField;
	}
		
	// searchValue
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getSearchValue() {
		return searchValue;
	}
	
	// province
	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvince() {
		return province;
	}

	// city
	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	// organizerNature
	public void setOrganizerNature(String organizerNature) {
		this.organizerNature = organizerNature;
	}

	public String getOrganizerNature() {
		return organizerNature;
	}

	// category
	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	// level
	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}
	
	// sortField
		public void setSortField(String sortField) {
			this.sortField = sortField;
		}

		public String getSortField() {
			return sortField;
		}
			
		// sortType
		public void setSortType(String sortType) {
			this.sortType = sortType;
		}

		public String getSortType() {
			return sortType;
		}
		
	// page_no
	public void setPage_no(int no){
		this.page_no = no;
	}
	public int getPage_no(){
		return page_no;
	}
	
	// page_size
	public void setPage_size(int size){
		this.page_size = size;
	}
	public int getPage_size(){
		return page_size;
	}
	
	// id
	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	// ids
	public void setIds(List<String> id) {
		this.ids = id;
	}

	public List<String> getIds() {
		return ids;
	}
	
	// category_ids
	public void setCategory_ids(List<String> category_ids) {
		this.category_ids = category_ids;
	}

	public List<String> getCategory_ids() {
		return category_ids;
	}
	
	@Override
	public String toString(){
		return "province：" + province + ",city: " + city + ", organizerNature: " + organizerNature
				+ ", category: " + category + ",level: " + level + ",category_ids: " + category_ids;
	}
}
