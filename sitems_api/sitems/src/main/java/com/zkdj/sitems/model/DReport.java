package com.zkdj.sitems.model;

/*
  *  属地简报
 */
public class DReport {
	private int id;
	private String report_name;
	private long start_time;
	private long end_time;

	private String report_desc;          //概述
	private String webs_illegal;          // 违规网站:未处理
	private String webs_illegal_handled;  // 违规网站:已处理
	private String page_new;      // 最新违规信息
	private String page_important;  // 重点违规信息
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReport_name() {
		return report_name;
	}
	public void setReport_name(String report_name) {
		this.report_name = report_name;
	}
	public long getStart_time() {
		return start_time;
	}
	public void setStart_time(long start_time) {
		this.start_time = start_time;
	}
	public long getEnd_time() {
		return end_time;
	}
	public void setEnd_time(long end_time) {
		this.end_time = end_time;
	}
	public String getWebs_illegal() {
		return webs_illegal;
	}
	public void setWebs_illegal(String webs_illegal) {
		this.webs_illegal = webs_illegal;
	}
	public String getWebs_illegal_handled() {
		return webs_illegal_handled;
	}
	public void setWebs_illegal_handled(String webs_illegal_handled) {
		this.webs_illegal_handled = webs_illegal_handled;
	}
	public String getReport_desc() {
		return report_desc;
	}
	public void setReport_desc(String report_desc) {
		this.report_desc = report_desc;
	}
	public String getPage_new() {
		return page_new;
	}
	public void setPage_new(String page_new) {
		this.page_new = page_new;
	}
	public String getPage_important() {
		return page_important;
	}
	public void setPage_important(String page_important) {
		this.page_important = page_important;
	}
	@Override
	public String toString() {
		return "DReport [id=" + id + ", report_name=" + report_name + ", start_time=" + start_time + ", end_time="
				+ end_time + ", report_desc=" + report_desc + ", page_new=" + page_new + ", page_important=" + page_important + "]";
	}
}
