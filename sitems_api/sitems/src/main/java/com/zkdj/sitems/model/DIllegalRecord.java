package com.zkdj.sitems.model;

/**
 * 违规处理记录表
 * @author DELL
 *
 */
public class DIllegalRecord {
	
	private long id;
	private String domainName;
	private int count;
    private String scheme;
    private String feedBack;
    private long createTime;
    
    private int userId;
    private String userName;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getScheme() {
		return scheme;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public String getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "DIllegalRecord [id=" + id + ", domainName=" + domainName + ", userId=" + userId + ", count=" + count
				+ ", scheme=" + scheme + ", feedBack=" + feedBack + ", createTime=" + createTime + "]";
	}
}
