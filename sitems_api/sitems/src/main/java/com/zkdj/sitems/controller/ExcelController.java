package com.zkdj.sitems.controller;

import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.AlertPageMessage;
import com.zkdj.sitems.bean.AlertPageResult;
import com.zkdj.sitems.bean.AlertWebMessage;
import com.zkdj.sitems.bean.AlertWebResult;
import com.zkdj.sitems.bean.WebListResult;
import com.zkdj.sitems.bean.WebParam;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.AlertService;
import com.zkdj.sitems.service.ExcelService;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.service.WebManageService;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.ExcelUtils;
import com.zkdj.sitems.util.TimeUtil;

@RestController
public class ExcelController {

	@Autowired
	private ExcelService mExcelService;

	@Autowired
	private AlertService mAlertService;

	@Autowired
	private WebManageService mWebService;
	
	@Autowired
	private PermissionsService mPermissionsService;

	@PostMapping("/web/excel_upload")
	@ResponseBody
	public ResultObject<Object> uploadExcel(HttpServletRequest request, @Param("count") Integer count)
			throws Exception {
		ResultObject<Object> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","导入网站",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
//		List<WebObject> mWebObjectList = new ArrayList<WebObject>();		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		int number = 0;
		if (count != null && count > 0) {
			number = count;
		} else {
			// 每次上传的次数：默认10000条
			number = 10000;
		}

		MultipartFile file = multipartRequest.getFile("filename");
		if (file == null || file.isEmpty()) {
			mResult.setCode(-1);
			mResult.setMessage("文件为空！");
			return mResult;
		}

		String filename = file.getOriginalFilename();
		String fileType = filename.substring(filename.lastIndexOf("."));
		if (".xls".equals(fileType) || ".xlsx".equals(fileType)) {
			InputStream inputStream = file.getInputStream();
			mResult = mExcelService.getBankListByExcel(inputStream, file.getOriginalFilename(), number);
			inputStream.close();
		} else {
			mResult.setCode(-3);
			mResult.setMessage("文件格式错误！");
			return mResult;
		}

//		if(mWebObjectList == null) {
//			mResult.setCode(-2);
//			mResult.setMessage("解析文件出错！");
//			return mResult;
//		}
//		System.out.println("--------uploadExcel------------");
//		mResult = mWebService.bulkSaveNewWeb(mWebObjectList);
		return mResult;
	}

	// 数据导出到excel表格：获取导入比例
	@RequestMapping("/web/excel_percent")
	public ResultObject<JSONObject> getExcelPercent() {
		ResultObject<JSONObject> mResult = new ResultObject<>();
		double percent = mExcelService.getPercent();
		JSONObject mJSONObject = new JSONObject();
		mJSONObject.put("percent", percent);

		mResult.setCode(0);
		mResult.setMessage("获取Excel导入进度成功！");
		mResult.setObject(mJSONObject);
		return mResult;
	}

	// 数据导出到excel表格：根据选择的ids
	@RequestMapping("/web/export_ids")
	public ResultObject<String> exportWebsByIDS(@RequestBody WebParam mParams) throws Exception {
		ResultObject<String> mResult = new ResultObject<>();

		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","导出数据",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getIds() == null || mParams.getIds().size() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("ids错误！");
			return mResult;
		}

		String sResult = mWebService.getWebByIDs(mParams.getIds());
		if (sResult != "" && sResult.length() > 0) {
			WebListResult mWebListResult = JSONObject.parseObject(sResult, WebListResult.class);
			if (mWebListResult != null && mWebListResult.getCode().equals("0")) {
				ExcelUtils mExcelUtils = new ExcelUtils();
				String name = "Excel_" + TimeUtil.getNewTime() + ".xlsx";
				String path = Config.path_excel + name;
				mExcelUtils.generateExcel(mWebListResult.getResult(), path);

				mResult.setCode(0);
				mResult.setMessage("生成网站信息Excel表格！");
				String url = Config.url_excel + name;
				mResult.setObject(url);
			} else {
				mResult.setCode(-2);
				mResult.setMessage("解析网站信息失败！");
				return mResult;
			}
		} else {
			mResult.setCode(-2);
			mResult.setMessage("获取网站信息失败！");
			return mResult;
		}

		return mResult;
	}

	// 数据导出到excel表格：根据条件
	@RequestMapping("/web/export_querys")
	public ResultObject<String> exportWebsByQuerys(@RequestBody WebParam mParams) throws Exception {
		ResultObject<String> mResult = new ResultObject<>();

		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","导出数据",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getProvince() == "" || mParams.getProvince().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("省份信息为空！");
			return mResult;
		}

		String sResult = mWebService.getWebByQuerys(mParams);
		if (sResult != "" && sResult.length() > 0) {
			WebListResult mWebListResult = JSONObject.parseObject(sResult, WebListResult.class);
			if (mWebListResult != null && mWebListResult.getCode().equals("0")) {
				ExcelUtils mExcelUtils = new ExcelUtils();
				String name = "Excel_" + TimeUtil.getNewTime() + ".xlsx";
				String path = Config.path_excel + name;
				mExcelUtils.generateExcel(mWebListResult.getResult(), path);

				mResult.setCode(0);
				mResult.setMessage("生成网站信息Excel表格！");
				String url = Config.url_excel + name;
				mResult.setObject(url);
			} else {
				mResult.setCode(-2);
				mResult.setMessage("解析网站信息失败！");
				return mResult;
			}
		} else {
			mResult.setCode(-2);
			mResult.setMessage("获取网站信息失败！");
			return mResult;
		}

		return mResult;
	}

	// 巡查预警：数据导出到excel表格; 根据条件导出预警网站列表
	@RequestMapping("/alert/export_weblist")
	public ResultObject<String> exportAlertWebsByQuerys(@RequestBody AlertWebMessage mParams) throws Exception {
		ResultObject<String> mResult = new ResultObject<>();

		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","导出数据",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getProvince() == "" || mParams.getProvince().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("省份信息为空！");
			return mResult;
		}

		AlertWebResult mAlertWebListResult = mAlertService.exportAlertWebList(mParams);
		if (mAlertWebListResult != null && mAlertWebListResult.getCode().equals("0")) {
			if (mAlertWebListResult.getResult() != null && mAlertWebListResult.getResult().size() > 0) {
				ExcelUtils mExcelUtils = new ExcelUtils();
				String name = "Excel_AlertWeb_" + TimeUtil.getNewTime() + ".xlsx";
				String path = Config.path_excel + name;
				int index = mExcelUtils.generateAlertWebExcel(mAlertWebListResult.getResult(), path);

				if (index > 0) {
					mResult.setCode(0);
					mResult.setMessage("生成Excel表格成功！");
					String url = Config.url_excel + name;
					mResult.setObject(url);
				} else {
					mResult.setCode(-3);
					mResult.setMessage("生成Excel表格失败！");
				}
			} else {
				mResult.setCode(-4);
				mResult.setMessage("查询网站信息为空！");
			}

		} else {
			mResult.setCode(-2);
			mResult.setMessage("查询网站信息失败！");
		}
		return mResult;
	}

	// 巡查预警：数据导出到excel表格; 根据IDs导出预警网站列表
	@RequestMapping("/alert/export_weblist_ids")
	public ResultObject<String> exportAlertWebsByIDs(@RequestBody AlertWebMessage mParams) throws Exception {
		ResultObject<String> mResult = new ResultObject<>();

		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","导出数据",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getIds() == null || mParams.getIds().size() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("ids错误！");
			return mResult;
		}

		AlertWebResult mAlertWebListResult = mAlertService.exportAlertWebListByIDs(mParams.getIds());
		if (mAlertWebListResult != null && mAlertWebListResult.getCode().equals("0")) {
			if (mAlertWebListResult.getResult() != null && mAlertWebListResult.getResult().size() > 0) {
				ExcelUtils mExcelUtils = new ExcelUtils();
				String name = "Excel_AlertWeb_" + TimeUtil.getNewTime() + ".xlsx";
				String path = Config.path_excel + name;
				int index = mExcelUtils.generateAlertWebExcel(mAlertWebListResult.getResult(), path);

				if (index > 0) {
					mResult.setCode(0);
					mResult.setMessage("生成Excel表格成功！");
					String url = Config.url_excel + name;
					mResult.setObject(url);
				} else {
					mResult.setCode(-3);
					mResult.setMessage("生成Excel表格失败！");
				}
			} else {
				mResult.setCode(-4);
				mResult.setMessage("查询网站信息为空！");
			}

		} else {
			mResult.setCode(-2);
			mResult.setMessage("查询网站信息失败！");
		}
		return mResult;
	}

	// 巡查预警：数据导出到excel表格; 根据条件导出违规文章列表
	@RequestMapping("/alert/export_pagelist")
	public ResultObject<String> exportAlertPagesByQuerys(@RequestBody AlertPageMessage mParams) throws Exception {
		ResultObject<String> mResult = new ResultObject<>();

		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","导出数据",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		String sResult = mAlertService.exportAlertPageList(mParams);
		if (sResult != "" && sResult.length() > 0) {
			AlertPageResult mAlertPageResult = JSONObject.parseObject(sResult, AlertPageResult.class);
			if (mAlertPageResult != null && mAlertPageResult.getCode().equals("0")) {
				if (mAlertPageResult.getResult() != null && mAlertPageResult.getResult().size() > 0) {
					ExcelUtils mExcelUtils = new ExcelUtils();
					String name = "Excel_AlertPage_" + TimeUtil.getNewTime() + ".xlsx";
					String path = Config.path_excel + name;
					int index = mExcelUtils.generateAlertPageExcel(mAlertPageResult.getResult(), path);

					if (index > 0) {
						mResult.setCode(0);
						mResult.setMessage("生成Excel表格成功！");
						String url = Config.url_excel + name;
						mResult.setObject(url);
					} else {
						mResult.setCode(-3);
						mResult.setMessage("生成Excel表格失败！");
					}
				} else {
					mResult.setCode(-4);
					mResult.setMessage("查询违规文章数据为空！");
				}

			} else {
				mResult.setCode(-2);
				mResult.setMessage("解析信息失败！");
			}
		} else {
			mResult.setCode(-2);
			mResult.setMessage("获取网站信息失败！");
		}

		return mResult;
	}

	// 巡查预警：数据导出到excel表格; 根据IDs导出违规文章列表
	@RequestMapping("/alert/export_pagelist_ids")
	public ResultObject<String> exportAlertPagesByIDs(@RequestBody AlertPageMessage mParams) throws Exception {
		ResultObject<String> mResult = new ResultObject<>();

		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","导出数据",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getIds() == null || mParams.getIds().size() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("ids错误！");
			return mResult;
		}

		String sResult = mAlertService.exportAlertPageListByIDs(mParams.getIds());
		if (sResult != "" && sResult.length() > 0) {
			AlertPageResult mAlertPageResult = JSONObject.parseObject(sResult, AlertPageResult.class);
			if (mAlertPageResult != null && mAlertPageResult.getCode().equals("0")) {
				if (mAlertPageResult.getResult() != null && mAlertPageResult.getResult().size() > 0) {
					ExcelUtils mExcelUtils = new ExcelUtils();
					String name = "Excel_AlertPage_" + TimeUtil.getNewTime() + ".xlsx";
					String path = Config.path_excel + name;
					int index = mExcelUtils.generateAlertPageExcel(mAlertPageResult.getResult(), path);

					if (index > 0) {
						mResult.setCode(0);
						mResult.setMessage("生成Excel表格成功！");
						String url = Config.url_excel + name;
						mResult.setObject(url);
					} else {
						mResult.setCode(-3);
						mResult.setMessage("生成Excel表格失败！");
					}
				} else {
					mResult.setCode(-4);
					mResult.setMessage("查询违规文章数据为空！");
				}

			} else {
				mResult.setCode(-2);
				mResult.setMessage("解析信息失败！");
			}
		} else {
			mResult.setCode(-2);
			mResult.setMessage("获取网站信息失败！");
		}

		return mResult;
	}
}
