package com.zkdj.sitems.bean;

import java.util.List;

/**
 *   巡查预警中：违规文章的内容类
 * @author DELL
 *
 */
public class AlertPage {
	private String id;
	private String url;
	private String domainName;
	private long urlMainHashCode;
	private String createTime;
	private String title;
	private String cleanTitle;
	private String content;
	private List<String> hotWords;
	private List<Integer> warning;
	private int isWarning;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public long getUrlMainHashCode() {
		return urlMainHashCode;
	}
	public void setUrlMainHashCode(long urlMainHashCode) {
		this.urlMainHashCode = urlMainHashCode;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCleanTitle() {
		return cleanTitle;
	}
	public void setCleanTitle(String cleanTitle) {
		this.cleanTitle = cleanTitle;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<String> getHotWords() {
		return hotWords;
	}
	public void setHotWords(List<String> hotWords) {
		this.hotWords = hotWords;
	}
	public List<Integer> getWarning() {
		return warning;
	}
	public void setWarning(List<Integer> warning) {
		this.warning = warning;
	}
	public int getIsWarning() {
		return isWarning;
	}
	public void setIsWarning(int isWarning) {
		this.isWarning = isWarning;
	}
	@Override
	public String toString() {
		return "AlertPage [id=" + id + ", url=" + url + ", domainName=" + domainName + ", urlMainHashCode="
				+ urlMainHashCode + ", createTime=" + createTime + ", title=" + title + ", cleanTitle=" + cleanTitle
				+ ", hotWords=" + hotWords + ", warning=" + warning + ", isWarning=" + isWarning + "]";
	}

}
