package com.zkdj.sitems.bean;

import java.util.List;

/**
 * 查询巡查预警网站时，接受条件参数的类
 * 
 * @author DELL
 *
 */
public class AlertWebMessage {
	private String searchValue; // 搜索条件：网站名称

	private String province; // 所在省份
	private String city; // 所在城市，默认全部,非必须
	private String organizerNature; // 主办单位,非必须
	private String level; // 等级,非必须 枚举值: A B C 默认全部
	private String subjectId;   // 专题id
	
	private String start_time;  // 开始时间：“2019-09-12 00:00:00”
	private String end_time;    // 结束时间

	private String sortField; // 排序字段，支持domainName和siteName字段
	private String sortType; // 排序方式，枚举值：asc和desc

	private int page_no; // 页数
	private int page_size; // 条数
	
	private String category; // 网站分类id：导出预警网站时可能出现的参数
	private List<String> ids; // 多个网站ids: 根据勾选的ids导出预警网站

	// searchValue
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getSearchValue() {
		return searchValue;
	}

	// province
	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvince() {
		return province;
	}

	// city
	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	// organizerNature
	public void setOrganizerNature(String organizerNature) {
		this.organizerNature = organizerNature;
	}

	public String getOrganizerNature() {
		return organizerNature;
	}

	// level
	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}
	
	// subjectId
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectId() {
		return subjectId;
	}

	// start_time
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getStart_time() {
		return start_time;
	}
	
	// end_time
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	
	// sortField
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	// sortType
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getSortType() {
		return sortType;
	}

	// page_no
	public void setPage_no(int no) {
		this.page_no = no;
	}

	public int getPage_no() {
		return page_no;
	}

	// page_size
	public void setPage_size(int size) {
		this.page_size = size;
	}

	public int getPage_size() {
		return page_size;
	}

    // category
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	// ids
	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	@Override
	public String toString() {
		return "province：" + province + ",city: " + city + ", organizerNature: " + organizerNature + ",level: " + level
				+ ",subjectId: " + subjectId + ",start_time: " + start_time + ",end_time: " + end_time;
	}

}
