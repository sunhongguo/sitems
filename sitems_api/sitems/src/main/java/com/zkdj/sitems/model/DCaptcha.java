package com.zkdj.sitems.model;

/*
 * 验证码：保存登录时忘记密码，获取的验证码，有效时间为30分钟
 */
public class DCaptcha {
	private int id;
	private String account;  // 帐号
	private String contact;    // 联系电话
	private String code;     // 验证码
	private Long create_time; // 创建时间
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Long create_time) {
		this.create_time = create_time;
	}
	@Override
	public String toString() {
		return "DCaptcha [account=" + account + ", contact=" + contact + ", code=" + code + ", create_time="
				+ create_time + "]";
	}

}
