package com.zkdj.sitems.http;

/**
 * 与第三方服务器通信的参数
 * @author DELL
 *
 */
public class HttpConfig {
	public static final String address = "116.62.63.211:8199"; // 外网服务器地址
	public static final String address_warn = "116.62.63.211:8887"; // 外网服务器地址:预警关键词
	public static final String address_message = "116.62.63.211:8383"; // 外网服务器地址:发送短信、邮件
	
	public static final String GEN_TOKEN_KEY = "zkdj_yuQing"; // 生成TOKEN需要的key
	public static final long TOKEN_LIVE_TIME = 100; // TOKEN 有效期

}
