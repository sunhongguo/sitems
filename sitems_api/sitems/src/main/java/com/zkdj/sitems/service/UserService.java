package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.User;
import com.zkdj.sitems.bean.UserList;
import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.redis.RedisUtil;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.TimeUtil;

@Service
public class UserService {
	@Autowired
	private UserMapper mUserMapper;
	@Resource
    private RedisUtil redisUtil;
	
	// 获取用户列表
	public ResultObject<UserList> getUserList(int page, int count){
		ResultObject<UserList> mResult = new ResultObject<UserList>();
		UserList mUserData = new UserList();
		int mpage = 0,mcount = 10;
		// 参数中页数为空时，默认为第一页,即“0”
		if(page > 0){
			mpage = page-1;
		}else{
			mpage = 0;
		}
				
		// 参数中条数为空时，默认为10
		if(count > 0){
			mcount = count;
		}else{
			mcount = 10;
		}
				
		int index = mpage*mcount;
	
		List<DUser> mList = mUserMapper.getUserList(index,mcount);
		int number = mUserMapper.getUserNumber();;
			
		if(mList != null){
			List<User> mUserList = new ArrayList<User>();
			for (DUser user:mList){
				User mUser = handleUserData(user);
				mUserList.add(mUser);
			}
			mUserData.setUserList(mUserList);
			mUserData.setIcount(number);
		}else{
			mUserData = null;
		}
				
		if(mUserData != null){
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mUserData);
		}else{
			mResult.setCode(-1);
			mResult.setMessage("查询失败！");
		}
		return mResult;
	}
		

	// 根据id查询帐号信息
	public ResultObject<User> getUserByID(int id){
		ResultObject<User> mResult = new ResultObject<User>();
		DUser user = mUserMapper.getUserByID(id);
    	if(user != null){
    		mResult.setCode(0);
			mResult.setMessage("查询成功！");
			User mUser = handleUserData(user);
			mResult.setObject(mUser);
		}else{
			mResult.setCode(-1);
			mResult.setMessage("查找失败！");
		}
    	return mResult;
	}
	
	// 根据account查询帐号信息
	public ResultObject<User> getUserByAccount(String account){
		ResultObject<User> mResult = new ResultObject<User>();
		DUser user = mUserMapper.getUserByAccount(account);
	    if(user != null){
	    	mResult.setCode(0);
			mResult.setMessage("查询成功！");
			User mUser = handleUserData(user);
			mResult.setObject(mUser);
		}else{
			mResult.setCode(-1);
			mResult.setMessage("查找失败！");
		}
	    return mResult;
	}
	
	public DUser getDUserByID(int id){
		DUser user = mUserMapper.getUserByID(id);
		return user;
	}
	
	// 根据token查询帐号信息
	public DUser getUserByToken(String token){
		DUser user = mUserMapper.getUserByToken(token);
		return user;
	}
	
	// 创建新帐号
	public ResultObject<User> createNewUser(DUser user){
		ResultObject<User> mResult = new ResultObject<User>();
		DUser old_user = mUserMapper.getUserByAccount(user.getAccount());
		if (old_user != null && old_user.getAccount() != null && old_user.getAccount().length() > 0){
			mResult.setCode(-2);
    		mResult.setMessage("帐号已存在！");
		}else{
			user.setCreate_time(TimeUtil.getNewTime());
			mUserMapper.insertUser(user);
        	
        	DUser nuser = mUserMapper.getUserByAccount(user.getAccount());
        	User mUser = handleUserData(nuser);
        	mResult.setCode(0);
    		mResult.setMessage("创建成功！");
    		mResult.setObject(mUser);
		} 
		return mResult;
	}
	
	// 修改帐号
	public ResultObject<User> updateUser(DUser user){
		ResultObject<User> mResult = new ResultObject<User>();
		DUser old_user = mUserMapper.getUserByID(user.getId());
		if(old_user == null){
			mResult.setCode(-2);
	    	mResult.setMessage("帐号不存在！");
	    	return mResult;
		}
		if (!old_user.getAccount().equals(user.getAccount())){
			mResult.setCode(-3);
	    	mResult.setMessage("帐号已被修改！");
	    	return mResult;
		}

		int i = mUserMapper.updateUser(user);
	    if(i == 1){    	
	    	DUser nuser = mUserMapper.getUserByID(user.getId());
		    User mUser = handleUserData(nuser);
		    redisUtil.hdel(Config.REDIS_KEY_USER, user.getId()+""); // 删除缓存
		    mResult.setCode(0);
		    mResult.setMessage("修改成功！");
		    mResult.setObject(mUser);
	    }else{
	    	mResult.setCode(-5);
		    mResult.setMessage("修改失败！");
	    }		
		return mResult;
	}
	
	// 筛选要返回的数据
	public User handleUserData(DUser user){
		User mUser = new User();  //返回的用户数据
		mUser.setId(user.getId());
		mUser.setAccount(user.getAccount());
		mUser.setPassword(user.getPassword());
		mUser.setUser_name(user.getUser_name());
		mUser.setContact(user.getContact());
		mUser.setRole_id(user.getRole_id());
		String time_begints = TimeUtil.getFormatTime(user.getCreate_time());
		mUser.setAccount_begints(time_begints);
		String time_endts = TimeUtil.getFormatTime(user.getAccount_endts());
		mUser.setAccount_endts(time_endts);
		String time = TimeUtil.getFormatTime(user.getCreate_time());
		mUser.setCreate_time(time);
		return mUser;
	}
	
	 //查询用户信息
    public ResultObject<DUser> GetUserInfo(String account){
      SitemsApplication.logger.info("---GetUserInfo------");
      ResultObject<DUser> mResult = new ResultObject<DUser>();
      
      if(account == null || account.isEmpty()){
          mResult.setCode(-2);
          mResult.setMessage("账号不能为空！");
          return mResult;
        }
		
      DUser user = mUserMapper.getUserByAccount(account);
      if(user == null){
			mResult.setCode(-3);
			mResult.setMessage("账号错误");	
			return mResult;
      }
      mResult.setCode(0);
      mResult.setMessage("查询用户成功！");
      mResult.setObject(user);
      return mResult;
    }
    
    //删除账户
    public ResultObject<DUser> deleteUserByID(int id){
    	ResultObject<DUser> mResult = new ResultObject<DUser>();
    	if(id >= 0)
		{
    		DUser user = mUserMapper.getUserByID(id);
    		if(user != null){
    			mUserMapper.deleteUserByID(id);
    			mResult.setCode(0);
    		    mResult.setMessage("账户已删除！");
    	    	return mResult;
    		}
		}
    	SitemsApplication.logger.info("---deleteUserByID id<0------");
	    mResult.setCode(-1);
	    mResult.setMessage("账户不存在！");
    	return mResult;
    }
   
    //修改用户信息（修改用户信息列表）
    
    
    //修改用户角色
    public ResultObject<DUser> UpdataUserRole(int userid,int role_id){
    	SitemsApplication.logger.info("---UpdataUserRole------");
      ResultObject<DUser> mResult = new ResultObject<DUser>();
		
      if(role_id < 0){
        mResult.setCode(-1);
        mResult.setMessage(" 角色ID错误！");
        return mResult;
    }
      
      if(userid < 0){
          mResult.setCode(-2);
          mResult.setMessage(" 用户ID错误！");
          return mResult;
      }
		
    DUser user = mUserMapper.getUserByID(userid);
    if(user == null){
        mResult.setCode(-3);
		mResult.setMessage("账号错误");	
		return mResult;
    }
      
    mUserMapper.updateUserRoleByID(userid,role_id);
    user = mUserMapper.getUserByID(userid);
    mResult.setCode(0);
    mResult.setMessage("修改用户角色成功！");
    mResult.setObject(user);
    return mResult;
  }
    
	//修改用户姓名
    public ResultObject<DUser> UpdataUserName(String account,String name){
    	SitemsApplication.logger.info("---UpdataUserName------");
      ResultObject<DUser> mResult = new ResultObject<DUser>();
			
      if(name == null || name.isEmpty()){
        mResult.setCode(-1);
        mResult.setMessage("用户姓名不能为空！");
        return mResult;
    }
      
    if(account == null || account.isEmpty()){
        mResult.setCode(-2);
        mResult.setMessage("账号不能为空！");
        return mResult;
    }
		
    DUser user = mUserMapper.getUserByAccount(account);
    if(user == null){
        mResult.setCode(-3);
		mResult.setMessage("账号错误");	
		return mResult;
    }
      
    mUserMapper.updateUserNameByAccount(account,name);
    user = mUserMapper.getUserByAccount(account);
    mResult.setCode(0);
    mResult.setMessage("修改用户姓名成功！");
    mResult.setObject(user);
    return mResult;
  }
   
    //修改用户联系方式
    public ResultObject<DUser> UpdataUserContact(String account,String phone){
    	SitemsApplication.logger.info("---UpdataUserContact------");
      ResultObject<DUser> mResult = new ResultObject<DUser>();
			
      if(phone == null || phone.isEmpty()){
        mResult.setCode(-1);
        mResult.setMessage("联系方式不能为空！");
        return mResult;
      }
      
      if(account == null || account.isEmpty()){
        mResult.setCode(-2);
        mResult.setMessage("账号不能为空！");
        return mResult;
      }
  		
      DUser user = mUserMapper.getUserByAccount(account);
      if(user == null){
        mResult.setCode(-3);
        mResult.setMessage("账号错误");	
        return mResult;
      }
      
      mUserMapper.updataUserContactByAccount(account,phone);
      user = mUserMapper.getUserByAccount(account);
      mResult.setCode(0);
	  mResult.setMessage("修改联系方式成功！");
	  mResult.setObject(user);
      return mResult;
    }
    
    //修改用户密码
    public ResultObject<DUser> UpdataUserPassWord(String account,String oldPassWord,String newPassWord){
    	SitemsApplication.logger.info("---UpdataUserPassWord------");
      ResultObject<DUser> mResult = new ResultObject<DUser>();
			
      if(newPassWord == null || newPassWord.isEmpty()){
        mResult.setCode(-1);
        mResult.setMessage("用户密码不能为空！");
        return mResult;
      }
      
      if(account == null || account.isEmpty()){
        mResult.setCode(-2);
        mResult.setMessage("账号不能为空！");
        return mResult;
      }
  		
      DUser user = mUserMapper.getUserByAccount(account);
      if(user == null){
        mResult.setCode(-3);
        mResult.setMessage("账号错误");	
        return mResult;
      }
      
      if(user.getPassword().equals(oldPassWord)){
    	  mUserMapper.updataUserPassWordByAccount(account,newPassWord);
        user = mUserMapper.getUserByAccount(account);
        mResult.setCode(0);
        mResult.setMessage("修改用户密码成功！");
        mResult.setObject(user);
        return mResult;	
	  }
      else{
		 mResult.setCode(-4);
		 mResult.setMessage("密码错误");
      }
      return mResult;	
   }
}
