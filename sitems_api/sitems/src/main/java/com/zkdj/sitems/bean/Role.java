package com.zkdj.sitems.bean;

import java.io.Serializable;
import java.util.List;

import com.zkdj.sitems.model.DPermissionModule;

public class Role implements Serializable{

	/**
	 * 返回数据：角色
	 */
	private static final long serialVersionUID = 1L;
	private int id; // id
	private String role_name; // 角色名称
	private String create_time; // 创建时间
	private String role_info; // 备注
	private List<DPermissionModule> permissions; // 角色所包含的权限和权限模块信息
	
	public Role(){
		super();
	}
	
	public Role(int id,String role_name,String create_time,String role_info,List<DPermissionModule> permissions,
			String msg){
		this.id = id;
		this.role_name = role_name;
		this.create_time = create_time;
		this.role_info = role_info;
		this.permissions = permissions;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
		
	// role_name
	public void setRole_name(String role_name){
		this.role_name = role_name;
	}
	public String getRole_name(){
		return role_name;
	}
	
	// create_time
	public void setCreate_time(String create_time){
		this.create_time = create_time;
	}
	public String getCreate_time(){
		return create_time;
	}

	// role_info
	public void setRole_info(String role_info){
		this.role_info = role_info;
	}
	public String getRole_info(){
		return role_info;
	}
	
	// permissions
	public void setPermissions(List<DPermissionModule> permissions){
		this.permissions = permissions;
	}
	public List<DPermissionModule> getPermissions(){
		return permissions;
	}
	
	@Override
	public String toString(){
		return "role_name: " + this.role_name + ", create_time: " + this.create_time + ", role_info: " + role_info
				+ " , permissions: " + permissions;
	}

}
