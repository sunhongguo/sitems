package com.zkdj.sitems.bean;

import java.util.List;

import com.zkdj.sitems.model.DReport;

/**
  * 查询简报列表返回的结果类
 * @author DELL
 *
 */
public class ReportListResult {
    private int count;   // 简报总条数
    private List<DReport> mlist;   // 简报列表
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<DReport> getMlist() {
		return mlist;
	}
	public void setMlist(List<DReport> mlist) {
		this.mlist = mlist;
	}
    
    
}
