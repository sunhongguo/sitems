package com.zkdj.sitems.model;

import java.io.Serializable;
import java.util.List;

// db_role关联的角色对象
public class DRole implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id; // id
	private String role_name; // 角色名称
	private Long create_time; // 创建时间
	private String role_info; // 备注
	private List<DPermissionModule> permissions; // 角色所包含的权限和权限模块信息
	private String msg;
	
	public DRole(){
		super();
	}
	
	public DRole(String role_name,Long create_time,String role_info,List<DPermissionModule> permissions,
			String msg){
		this.role_name = role_name;
		this.create_time = create_time;
		this.role_info = role_info;
		this.permissions = permissions;
		this.msg = msg;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// msg
	public void setMsg(String msg){
		this.msg = msg;
	}
	public String getMsg(){
		return msg;
	}
		
	// role_name
	public void setRole_name(String role_name){
		this.role_name = role_name;
	}
	public String getRole_name(){
		return role_name;
	}
	
	// create_time
	public void setCreate_time(Long create_time){
		this.create_time = create_time;
	}
	public Long getCreate_time(){
		return create_time;
	}

	// role_info
	public void setRole_info(String role_info){
		this.role_info = role_info;
	}
	public String getRole_info(){
		return role_info;
	}
	
	// permissions
	public void setPermissions(List<DPermissionModule> permissions){
		this.permissions = permissions;
	}
	public List<DPermissionModule> getPermissions(){
		return permissions;
	}
	
	@Override
	public String toString(){
		return "role_name: " + this.role_name + ", create_time: " + this.create_time + ", role_info: " + role_info
				+ " , permissions: " + permissions + " , msg: " + msg;
	}

}

