package com.zkdj.sitems.http;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * 生成Token:用于“发送短信”和“发送邮件”
 * @author DELL
 *
 */
public class Token2Util {
	private static String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAI4xqnxY2SzDiR4KG6SIhy7itVIqe1cC+jeudN+0TAnmPMLJpxPfDc1Jh26uoK6svt5Zf46wbU+kF/kmOuVzH5cCAwEAAQ==";
	private static String prefix = "zkdj";
	private static String suffix = "yuqingguanjia@!123";
	
	  //非对称密钥算法
    private static final String KEY_ALGORITHM = "RSA";
    /**
     * 公钥加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPublicKey(byte[] data, byte[] key) throws Exception {

        //实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //初始化公钥
        //密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        //产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);

        //数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

	public static MultiValueMap<String, String> createdHeader() {
	    long timeMillis = System.currentTimeMillis();

	    byte[] code1 = new byte[0];
	    try {
	        code1 = encryptByPublicKey((prefix + timeMillis + suffix).getBytes(), Base64.decodeBase64(publicKey));
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    String token = Base64.encodeBase64String(code1);
	    MultiValueMap<String, String> headerMap = new LinkedMultiValueMap<String, String>();
	    headerMap.add("token", token);
	    headerMap.add("timestamp", String.valueOf(timeMillis));
	    return headerMap;
	}
}
