package com.zkdj.sitems.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.model.DPlatform;
import com.zkdj.sitems.result.ResultObject;

@Service
public class PlatformService {
	
	@Autowired
	private UserMapper userMapper;
	
	//修改平台名称
    public ResultObject<DPlatform> UpdataSystemName(String Sname){
    	SitemsApplication.logger.info("---UpdataSystemName------");
      ResultObject<DPlatform> mResult = new ResultObject<DPlatform>();
			
      if(Sname == null || Sname.isEmpty()){
        mResult.setCode(-1);
        mResult.setMessage("平台名称不能为空！");
        return mResult;
      }
      DPlatform Platform;
      userMapper.updateSystemName(Sname);
      Platform = userMapper.getSystemName();
      mResult.setCode(0);
	  mResult.setMessage("修改平台名称成功！");
      mResult.setObject(Platform);
      return mResult;
    }
    //获取平台名称
	public ResultObject<DPlatform> GetSystemName(){
		SitemsApplication.logger.info("---UpdataSystemName------");
        ResultObject<DPlatform> mResult = new ResultObject<DPlatform>();
        
        DPlatform Platform;
        Platform = userMapper.getSystemName();
       
        mResult.setCode(0);
  	    mResult.setMessage("获取成功！");
  	    mResult.setObject(Platform);
  	
        return mResult;
      }
}
