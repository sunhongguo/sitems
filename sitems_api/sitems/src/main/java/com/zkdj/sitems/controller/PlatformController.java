package com.zkdj.sitems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.model.DPlatform;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.service.PlatformService;

@RestController
public class PlatformController {

	@Autowired
	private PlatformService mPlatformService;
	@Autowired
	private PermissionsService mPermissionsService;
	
	//修改平台名称
    @RequestMapping("/update/platform")
    public ResultObject<DPlatform> updataSystemName(@RequestBody String Sname){
    	ResultObject<DPlatform> mResult = new ResultObject<DPlatform>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","账号信息","修改平台名称");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}

    	JSONObject object = JSON.parseObject(Sname);
    	String name = object.getString("Sname");
    	mResult = mPlatformService.UpdataSystemName(name);
        return mResult;
    }
    //获取平台名称
    @RequestMapping("/platform")
    public ResultObject<DPlatform> getSystemName(){
    	ResultObject<DPlatform> mResult = mPlatformService.GetSystemName();
    	SitemsApplication.logger.info("----getSystemName-----" + mResult);
        return mResult;
    }
}
