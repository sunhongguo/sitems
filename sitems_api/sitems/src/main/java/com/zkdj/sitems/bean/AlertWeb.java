package com.zkdj.sitems.bean;

import java.util.List;

/**
 *   预警网站列表中某个网站的信息
 * @author DELL
 *
 */
public class AlertWeb {
	private int illegalCount; // 违规文章数量
	private String city; // 城市
	private String level; // 等级："C"
	private String organizerNature; // 主办单位："个人"
	private int subject;// 专题：177
	private String siteName; // 标题："个人技术文章"
	private String updateTime;// 更新时间："2019-09-12T16:08:02.625Z"
	private String province;// 省份
	private String phone;// 电话
	private String contact; // 联系人
	private String domainName; // 域名
	private String id; // 网站id
	private List<String> category; // 网站分类id
	private List<String> categoryTitle;  // 分类标题
	private int status;
	
	
	public int getIllegalCount() {
		return illegalCount;
	}
	public void setIllegalCount(int illegalCount) {
		this.illegalCount = illegalCount;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getOrganizerNature() {
		return organizerNature;
	}
	public void setOrganizerNature(String organizerNature) {
		this.organizerNature = organizerNature;
	}
	public int getSubject() {
		return subject;
	}
	public void setSubject(int subject) {
		this.subject = subject;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getCategory() {
		return category;
	}
	public void setCategory(List<String> category) {
		this.category = category;
	}
	public List<String> getCategoryTitle() {
		return categoryTitle;
	}
	public void setCategoryTitle(List<String> categoryTitle) {
		this.categoryTitle = categoryTitle;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "AlertWeb [illegalCount=" + illegalCount + ", city=" + city + ", level=" + level + ", organizerNature="
				+ organizerNature + ", subject=" + subject + ", siteName=" + siteName + ", updateTime=" + updateTime
				+ ", province=" + province + ", phone=" + phone + ", contact=" + contact + ", domainName=" + domainName
				+ ", id=" + id + ", category=" + category + ", categoryTitle=" + categoryTitle + ", status=" + status
				+ "]";
	}
}
