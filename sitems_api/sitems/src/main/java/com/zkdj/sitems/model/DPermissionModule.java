package com.zkdj.sitems.model;

import java.io.Serializable;
import java.util.List;

//db_permission_module权限模块
public class DPermissionModule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id; // id
	private int parent_id;    // 父级ID
	private String module_name; // 角色名称
	private String module_info; // 备注
	private int status;    // 状态：表示角色是否选择了该内容；1表示选中；0表示未选中
	private List<DPermission> mPermissionList;  // 权限列表：该模块下所属的权限列表
	private List<DPermissionModule>  mModuleList; // 模块
	
	public DPermissionModule(){
		super();
	}
	
	public DPermissionModule(int parent_id,String name,String info){
		this.parent_id = parent_id;
		this.module_name = name;
		this.module_info = info;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// parent_id
	public void setParent_id(int parent_id){
		this.parent_id = parent_id;
	}
	public int getParent_id(){
		return parent_id;
	}
	
	// status
	public void setStatus(int status){
		this.status = status;
	}
	public int getStatus(){
		return status;
	}
		
	// module_name
	public void setModule_name(String module_name){
		this.module_name = module_name;
	}
	public String getModule_name(){
		return module_name;
	}

	// module_info
	public void setModule_info(String module_info){
		this.module_info = module_info;
	}
	public String getModule_info(){
		return module_info;
	}
		
	// mPermissionList
	public void setPermissionList(List<DPermission> mlist){
		this.mPermissionList = mlist;
	}
	public List<DPermission> getPermissionList(){
		return this.mPermissionList;
	}
	
	// mModule2List
	public void setModuleList(List<DPermissionModule> mlist){
		this.mModuleList = mlist;
	}
	public List<DPermissionModule> getModuleList(){
		return mModuleList;
	}
	
	@Override
	public String toString(){
		return "module_name: " + this.module_name + ", module_info: " + module_info;
	}


}
