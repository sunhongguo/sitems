package com.zkdj.sitems.service;

import java.util.List;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.model.DPermission;
import com.zkdj.sitems.model.DPermissionModule;
import com.zkdj.sitems.model.DRole;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.redis.RedisUtil;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;
/**
 * 权限服务：根据角色的权限设置，判断某个id是否有某项权限
 * @author DELL
 *
 */
@Service
public class PermissionsService {
	@Autowired
	private UserMapper userMapper;
	@Resource
    private RedisUtil redisUtil;
	
	/**
	 * 
	 * @param param1:一级参数，最上级
	 * @param param2：二级参数
	 * @param param3：三级参数
	 * @return
	 */
	@SuppressWarnings("null")
	public ResultObject<String> judgeUserPermission(String param1,String param2,String param3) {
		ResultObject<String> result = new ResultObject<>();
		String sRedis_userid = (String) redisUtil.get(Config.REDIS_USERID_KEY);
		if(sRedis_userid == null || sRedis_userid.length() == 0) {
			result.setCode(-100);
			result.setMessage("用户ID参数错误！");
			return result;
		}

		List<DPermissionModule> permissions = getPermissions(sRedis_userid);
		if(permissions != null && permissions.size() > 0) {
			// 第一层			
			if(param1 == null || param1.length() == 0) {
				result.setCode(-101);
				result.setMessage("参数错误！");
				return result;
			}
			DPermissionModule mDPermissionModule1 = null;
			for(DPermissionModule m:permissions) {
				if(m.getModule_name().equals(param1)) {
					mDPermissionModule1 = m;
				}
			}

			// 第二层
			if(param2 == null || param2.length() == 0) {
				if(mDPermissionModule1 != null) {
					int status = mDPermissionModule1.getStatus();
					if(status == 1) {
						result.setCode(101);
						result.setMessage("有权限！");
						return result;
					}else {
						result.setCode(100);
						result.setMessage("没有权限！");
						return result;
					}
				}else {
					result.setCode(-102);
					result.setMessage("一级参数的权限不存在!");
					return result;
				}
			}else {
				if(mDPermissionModule1 == null) {
					result.setCode(-102);
					result.setMessage("一级参数的权限不存在!");
					return result;
				}
			}
			
			List<DPermission> mPermissionList = mDPermissionModule1.getPermissionList();
			if(mPermissionList != null && mPermissionList.size() > 0) {
				for(DPermission m:mPermissionList) {
					if(m.getPermission_name().equals(param2)) {
						int status = m.getStatus();
						if(status == 1) {
							result.setCode(101);
							result.setMessage("有权限！");
							return result;
						}else {
							result.setCode(100);
							result.setMessage("没有权限！");
							return result;
						}
					}
				}
			}
			List<DPermissionModule>  mModuleList = mDPermissionModule1.getModuleList();
			DPermissionModule mDPermissionModule2 = null;
			if(mModuleList != null && mModuleList.size() > 0) {				
				for(DPermissionModule m:mModuleList) {
					if(m.getModule_name().equals(param2)) {
						mDPermissionModule2 = m;
					}
				}
			}

			// 第三层：只有DPermission的列表
			if(param3 == null || param3.length() == 0) {
				if(mDPermissionModule2 != null) {
					int status = mDPermissionModule2.getStatus();
					if(status == 1) {
						result.setCode(101);
						result.setMessage("有权限！");
						return result;
					}else {
						result.setCode(100);
						result.setMessage("没有权限！");
						return result;
					}
				}else {
					result.setCode(-102);
					result.setMessage("二级参数的权限不存在!");
					return result;
				}
			}else {
				if(mDPermissionModule2 == null) {
					result.setCode(-102);
					result.setMessage("二级参数的权限不存在!");
					return result;
				}
			}
			List<DPermission> mPermissionList3 = mDPermissionModule2.getPermissionList();
			if(mPermissionList3 != null && mPermissionList3.size() > 0) {
				for(DPermission m:mPermissionList3) {
					if(m.getPermission_name().equals(param3)) {
						int status = m.getStatus();
						if(status == 1) {
							result.setCode(101);
							result.setMessage("有权限！");
							return result;
						}else {
							result.setCode(100);
							result.setMessage("没有权限！");
							return result;
						}
					}
				}
			}			
		}else {
			result.setCode(-101);
			result.setMessage("权限数据错误!");
			return result;
		}
		
		result.setCode(-102);
		result.setMessage("验证权限失败!");
		return result;
	}
	
	private List<DPermissionModule> getPermissions(String sUserID){
		DUser user = (DUser) redisUtil.hget(Config.REDIS_KEY_USER, sUserID);
		if(user == null) {
			int iUserID = Integer.parseInt(sUserID);
			if(iUserID == 0) {
				return null;
			}
			
			user = userMapper.getUserByID(iUserID);
			if(user != null){
				redisUtil.hset(Config.REDIS_KEY_USER, sUserID, user);
			}else {
				return null;
			}
		}
		
		
		int role_id = user.getRole_id();
		if(role_id == 0){
			return null;
		}
		String sRoleID = role_id + "";
		
		@SuppressWarnings("unchecked")
		List<DPermissionModule> permissions = (List<DPermissionModule>) redisUtil.hget(Config.REIDS_KEY_ROLE,sRoleID);
		if(permissions == null) {
			DRole role = userMapper.getRoleByID(role_id);
			permissions = JSON.parseArray(role.getMsg(), DPermissionModule.class);
			if(permissions != null && permissions.size() > 0) {
				redisUtil.hset(Config.REIDS_KEY_ROLE, sRoleID , permissions);
			}
		}		
		return permissions;
	}
}
