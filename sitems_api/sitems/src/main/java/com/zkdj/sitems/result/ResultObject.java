package com.zkdj.sitems.result;

/*
 * 返回的结果：json数据格式
 * code：数值，表示结果不同的状态，0表示成功
 * message：结果不同状态的原因描述
 * object： 单个对象
 */
public class ResultObject<T> {
	private int code;
	private String message;
	private String errmsg;
	private T object;
	
	public ResultObject(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(T object){
		this.object = object;
	}
	public T getObject(){
		return object;
	}
		
	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}
