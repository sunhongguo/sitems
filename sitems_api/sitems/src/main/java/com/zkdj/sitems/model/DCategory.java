package com.zkdj.sitems.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DCategory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int id;
	@JsonProperty("title")
	private String category_name;
	private int parent_id;
	private int sort;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", category_name=" + category_name + ", parent_id=" + parent_id + ", sort=" + sort
				+ "]";
	}
	
	@JsonProperty("children")
	public List<DCategory> subCategory =  null ;


	public List<DCategory> getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(List<DCategory> subCategory) {
		this.subCategory = subCategory;
	}
	
	
}
