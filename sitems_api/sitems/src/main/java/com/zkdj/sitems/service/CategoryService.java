package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.Category;
import com.zkdj.sitems.mapper.CategoryMapper;
import com.zkdj.sitems.model.DCategory;
import com.zkdj.sitems.result.ResultObject;

@Service
public class CategoryService {
	
	private final  Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CategoryMapper mCategoryMapper;
	@Autowired
	private WebManageService mWebService;
	
	// 插入分类
	public ResultObject<Category> InsertCategory(Category category){
		ResultObject<Category> mResult = new ResultObject<Category>();
		
		if (category.getParent_id()<0 && category.getParent_id() != -1) {
			mResult.setCode(-1);
			mResult.setMessage("parent_id 参数错误");
			return mResult;
		}
		
		// category_name 检测
		if (category.getCategory_name()==null || category.getCategory_name().trim().isEmpty()) {
			mResult.setCode(-1);
			mResult.setMessage("分类名称不能为空");
			return mResult;
		}
		
		List<Category> mCategoryList = mCategoryMapper.getCategoryByName(category.getCategory_name());
		if(mCategoryList != null && mCategoryList.size() > 0) {
			mResult.setCode(-1);
			mResult.setMessage("名称已经被占用！");
			return mResult;
		}
		
		// parent_id 是否存在检测		
		if (category.getParent_id() !=-1 ) {
			Category c = mCategoryMapper.getCategoryById(category.getParent_id());
			if (c ==  null) {
				mResult.setCode(-1);
				mResult.setMessage("上级分类不存在");
				mResult.setObject(null);
				return mResult;
			}
		}
		
		
		int nRet = mCategoryMapper.insertCategory(category);
		
		if (nRet>0) {
			// 更新sort 
			mCategoryMapper.updateCategorySort(category.getId(),category.getId());
			category.setSort(category.getId());
		}
		
		logger.info(category.toString());
		
		
		mResult.setCode(0);
		mResult.setMessage("创建成功！");
		mResult.setObject(category);
		
		return mResult;
	}
	
	public ResultObject<Category> UpdateCategoryName(Category category){
		
		ResultObject<Category> mResult = new ResultObject<Category>();
			
		    Category c =  mCategoryMapper.getCategoryById(category.getId());
		    if (c == null) {
		    	mResult.setCode(-1);
				mResult.setMessage("未查询到分类");
				return mResult;
			}
		    
		 // category_name 检测
			if (category.getCategory_name()==null || category.getCategory_name().trim().isEmpty()) {
				mResult.setCode(-1);
				mResult.setMessage("分类名称不能为空");
				return mResult;
			}
		
			int nRet = mCategoryMapper.updateCategoryName(category.getId(),category.getCategory_name());
			
			mResult.setCode(0);
			mResult.setMessage("重命名分类成功！");
			mResult.setObject(category);
			return mResult;
			
		}
	
	  private List<DCategory> getTreeList(List<DCategory> entityList){
		  List<DCategory> resultList = new ArrayList<>();
		   
		           //获取顶层元素集合
		          int parentCode;
		           for (DCategory entity : entityList) {
		              parentCode = entity.getParent_id();
		               //顶层元素的parentCode==null或者为0
		               if (parentCode == -1) {
		                   resultList.add(entity);
		               }
		           }
		           //  list 元素按照sort 字段进行排序
		           Collections.sort(resultList, new Comparator<DCategory>() {
		        	   @Override
		        	   public int compare(DCategory o1, DCategory o2) {
		        	    return o1.getSort() - o2.getSort();
		        	   }
		        	  });
		   
		           //获取每个顶层元素的子数据集合
		           for (DCategory entity : resultList) {
		               entity.setSubCategory(getSubList(entity.getId(), entityList));
		           }
		   
		           return resultList;
	  }
	
	   
	
	
	   public  List<DCategory>  GetAllCategories() {
		   
		   List<DCategory> categoryList = mCategoryMapper.getAllCategory();
		   
		   return getTreeList(categoryList);
		
	    }
	   
	   private static List<DCategory> getSubList(int id, List<DCategory> entityList) {
		            List<DCategory> childList = new ArrayList<>();
		            int parentId;
		    
		            //子集的直接子对象
		           for (DCategory entity : entityList) {
		                parentId = entity.getParent_id();
		                if (id == parentId) {
		                    childList.add(entity);
		                }
		            }
		           
		           Collections.sort(childList, new Comparator<DCategory>() {
		        	   @Override
		        	   public int compare(DCategory o1, DCategory o2) {
		        	    return o1.getSort() - o2.getSort();
		        	   }
		        	  });
		    
		            //子集的间接子对象
		            for (DCategory entity : childList) {
		                entity.setSubCategory(getSubList(entity.getId(), entityList));
		            }
		    
		            //递归退出条件
		            if (childList.size() == 0) {
		                return null;
		            }
		    
		            return childList;
		        }
	   
	   public ResultObject<Object> deleteCategoryByID(int id){
	    	ResultObject<Object> mResult = new ResultObject<Object>();
	    	
//	    	int nCount = mCategoryMapper.getCountByParentID(id);
//	    	if (nCount>0) {
//	    		mResult.setCode(-1);
//			    mResult.setMessage("分类下包含子分类，请先删除子分类");
//		    	return mResult;
//			}
	    	
	    	if(id == 0){
				mResult.setCode(-1);
				mResult.setMessage("参数错误！");
				return mResult;
			}
	    	
	    	String sResult = mWebService.deleteBulkWebByCategoryID(id);
	    	JSONObject mJSONObject = JSONObject.parseObject(sResult);
			if(mJSONObject == null){
				mResult.setCode(-2);
				mResult.setMessage("删除分类下网站失败！");
				return mResult;
			}
			
			if(mJSONObject.getString("code").equals("0")){
				int nRet = mCategoryMapper.deleteCategoryById(id);				
		    	logger.info("delete categorybyid nret "+nRet);		    	
		    	if (nRet>=0) {
					mResult.setCode(0);
				    mResult.setMessage("分类删除成功");
			    	return mResult;
				}else {
					mResult.setCode(-1);
				    mResult.setMessage("删除异常");
			    	return mResult;
				}
			}else{
				mResult.setCode(-1);
				mResult.setMessage(mJSONObject.getString("msg"));
				return mResult;
			}    
	    }
   
   public ResultObject<Object> changeCategory(int id1,int id2){
    	ResultObject<Object> mResult = new ResultObject<Object>();
    	
       Category c1 = mCategoryMapper.getCategoryById(id1);
       Category c2 = mCategoryMapper.getCategoryById(id2);
	       
       if (c1 == null || c2 == null) {
    	   mResult.setCode(-1);
		    mResult.setMessage("分类不存在");
	    	return mResult;
	    }
       if (c1.getParent_id() != c2.getParent_id()) {
    	   mResult.setCode(-1);
		    mResult.setMessage("分类不属于同级分类");
	    	return mResult;
		}
	       
	    // 更新sort
       
       mCategoryMapper.updateCategorySort(c1.getId(), c2.getSort());
       mCategoryMapper.updateCategorySort(c2.getId(), c1.getSort());
    	
	   mResult.setCode(0);
	   mResult.setMessage("success");
	   
	    return mResult;
   }
	
}
