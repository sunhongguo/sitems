package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.AlertCountsMessage;
import com.zkdj.sitems.bean.AlertPageMessage;
import com.zkdj.sitems.bean.AlertWeb;
import com.zkdj.sitems.bean.AlertWebMessage;
import com.zkdj.sitems.bean.AlertWebResult;
import com.zkdj.sitems.bean.AlertWordMessage;
import com.zkdj.sitems.bean.Category;
import com.zkdj.sitems.http.HttpService;
import com.zkdj.sitems.mapper.AlertMapper;
import com.zkdj.sitems.mapper.CategoryMapper;
import com.zkdj.sitems.model.DAlertReceiver;
import com.zkdj.sitems.model.DAlertWordSubject;
import com.zkdj.sitems.model.DAlertCategory;
import com.zkdj.sitems.result.ResultObject;

/**
 * 巡查预警的api处理类
 * 
 * @author DELL
 *
 */
@Service
public class AlertService {
	@Autowired
	private AlertMapper mAlertMapper;
	@Autowired
	private CategoryMapper mCategoryMapper;
	@Autowired
	private HttpService mHttpService;

	// 预警网站：获取列表
	public AlertWebResult getAlertWebList(AlertWebMessage mParams) {
		int page_no = 0;
		int page_size = 0;

		if (mParams.getPage_no() == 0) {
			page_no = 1;
		} else {
			page_no = mParams.getPage_no();
		}

		if (mParams.getPage_size() == 0) {
			page_size = 10; // 默认10条
		} else {
			page_size = mParams.getPage_size();
		}

		String path = "/restserver/illegal/query/fullQuery?page_no=" + page_no + "&page_size=" + page_size + "&";
		JSONObject json = new JSONObject();

		if (mParams.getProvince() != null && mParams.getProvince().length() > 0) {
			json.put("province", mParams.getProvince());
		}

		if (mParams.getCity() != null && mParams.getCity().length() > 0) {
			json.put("city", mParams.getCity());
		}

		if (mParams.getOrganizerNature() != null && mParams.getOrganizerNature().length() > 0) {
			json.put("organizerNature", mParams.getOrganizerNature());
		}

		if (mParams.getLevel() != null && mParams.getLevel().length() > 0) {
			json.put("level", mParams.getLevel());
		}

		if (mParams.getSubjectId() != null && mParams.getSubjectId().length() > 0) {
			json.put("subjectId", mParams.getSubjectId());
		}

		if (mParams.getStart_time() != null && mParams.getStart_time().length() > 0) {
			json.put("start_time", mParams.getStart_time());
		}

		if (mParams.getEnd_time() != null && mParams.getEnd_time().length() > 0) {
			json.put("end_time", mParams.getEnd_time());
		}

		if (mParams.getSearchValue() != null && mParams.getSearchValue().length() > 0) {
			json.put("searchValue", mParams.getSearchValue());
		}

		if (mParams.getSortField() != null && mParams.getSortField().length() > 0) {
			json.put("sortField", mParams.getSortField());
			json.put("sortType", mParams.getSortType());
		}

		String result = mHttpService.postData(path, json.toString());
		AlertWebResult mAlertWebResult = null;
		if (result != null && result.length() > 0) {
			mAlertWebResult = JSONObject.parseObject(result, AlertWebResult.class);
			// 根据网站分类id获取分类名称
			List<AlertWeb> mlist = mAlertWebResult.getResult();
			if (mlist != null && mlist.size() > 0) {
				for (AlertWeb mAlertWeb : mlist) {
					List<String> categorylist = mAlertWeb.getCategory();
					if (categorylist != null && categorylist.size() > 0) {
						List<String> categoryTitle = new ArrayList<String>();
						for (String str : categorylist) {
							if (str.equals("0")) {
								categoryTitle.add("未分类");
							} else {
								try {
									int id = Integer.parseInt(str);
									Category mCategory = mCategoryMapper.getCategoryById(id);
									categoryTitle.add(mCategory.getCategory_name());
								} catch (NumberFormatException e) {
									e.printStackTrace();
								}
							}
						}

						if (categoryTitle != null) {
							mAlertWeb.setCategoryTitle(categoryTitle);
						}
					}
				}
			}
		}
		return mAlertWebResult;
	}

	// 违规文章：根据条件，获取违规文章列表
	public String getAlertPageListByName(AlertPageMessage mParams) {
		int page_no = 0;
		int page_size = 0;

		if (mParams.getPage_no() == 0) {
			page_no = 1;
		} else {
			page_no = mParams.getPage_no();
		}

		if (mParams.getPage_size() == 0) {
			page_size = 10; // 默认10条
		} else {
			page_size = mParams.getPage_size();
		}

		String path = "/restserver/page/query/fullQuery?page_no=" + page_no + "&page_size=" + page_size + "&";
		JSONObject json = new JSONObject();

		if (mParams.getMustKeyWords() != null && mParams.getMustKeyWords().size() > 0) {
			json.put("mustKeyWords", mParams.getMustKeyWords());
		}

		if (mParams.getShouldKeyWords() != null && mParams.getShouldKeyWords().size() > 0) {
			json.put("shouldKeyWords", mParams.getShouldKeyWords());
		}

		if (mParams.getUnrelated() != null && mParams.getUnrelated().size() > 0) {
			json.put("unrelated", mParams.getUnrelated());
		}

		if (mParams.getDomainName() != null && mParams.getDomainName().size() > 0) {
			json.put("domainName", mParams.getDomainName());
		}

		if (mParams.getIsWarning() != null && mParams.getIsWarning().length() > 0) {
			json.put("isWarning", mParams.getIsWarning());
		}

		if (mParams.getStart_time() != null && mParams.getStart_time().length() > 0) {
			json.put("start_time", mParams.getStart_time());
		}

		if (mParams.getEnd_time() != null && mParams.getEnd_time().length() > 0) {
			json.put("end_time", mParams.getEnd_time());
		}

		if (mParams.getSortField() != null && mParams.getSortField().length() > 0) {
			json.put("sortField", mParams.getSortField());
			json.put("sortType", mParams.getSortType());
		}
		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		return result;
	}

	// 违规文章：根据id查询单个
	public String getAlertPageByID(String id) {
		String path = "/restserver/page/query/queryByID?";
		JSONObject json = new JSONObject();
		json.put("id", id);

		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		return result;
	}

	// 专题分类：获取列表
	public List<JSONObject> getWarnCategoryList(int account_id) {
		List<DAlertCategory> parentlist = mAlertMapper.getAlertCategoryListByParentID(0, account_id);
		List<JSONObject> jsonlist = new ArrayList<JSONObject>();
		if (parentlist != null) {
			for (DAlertCategory m : parentlist) {
				JSONObject mJSONObject = new JSONObject();
				List<DAlertCategory> mlist = mAlertMapper.getAlertCategoryListByParentID(m.getId(), account_id);
				mJSONObject.put("id", m.getId());
				mJSONObject.put("name", m.getName());
				mJSONObject.put("sort", m.getSort());
				mJSONObject.put("isalert", m.getIsalert());
				mJSONObject.put("isshow", m.getIsshow());
				mJSONObject.put("account_id", m.getAccount_id());
				mJSONObject.put("object", mlist);

				jsonlist.add(mJSONObject);
			}
		}
		return jsonlist;
	}

	// 专题分类：新建
	public ResultObject<DAlertCategory> addNewAlertCategory(DAlertCategory mParams) {
		ResultObject<DAlertCategory> mResult = new ResultObject<>();
		List<DAlertCategory> mlistByName = mAlertMapper.getAlertCategoryByName(mParams.getName());
		if (mlistByName != null && mlistByName.size() > 0) {
			mResult.setCode(-2);
			mResult.setMessage("该专题分类已存在！");
			return mResult;
		}

		List<DAlertCategory> mlist = mAlertMapper.getAlertCategoryListByParentID(mParams.getParent_id(),
				mParams.getAccount_id());
		if (mlist == null) {
			mResult.setCode(-4);
			mResult.setMessage("查询数据库信息失败！");
			return mResult;
		}

		int sort = 0;
		if (mlist.size() > 0) {
			sort = mlist.get(mlist.size() - 1).getSort() + 1;
		} else {
			sort = 1;
		}
		mParams.setSort(sort);
		int i = mAlertMapper.insertAlertCategory(mParams);
		if (i > 0) {
			DAlertCategory mDAlertCategory = mAlertMapper.getAlertCategoryByID(mParams.getId());
			mResult.setCode(0);
			mResult.setMessage("添加新的专题分类成功！");
			mResult.setObject(mDAlertCategory);
		} else {
			mResult.setCode(-3);
			mResult.setMessage("添加新的专题分类失败！");
		}
		return mResult;
	}

	// 专题分类：重命名
	public ResultObject<String> renameAlertCategory(DAlertCategory mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		DAlertCategory mDAlertCategory = mAlertMapper.getAlertCategoryByID(mParams.getId());
		if (mDAlertCategory == null) {
			mResult.setCode(-2);
			mResult.setMessage("该专题不存在！");
			return mResult;
		}

		int i = mAlertMapper.renameAlertCategory(mParams);
		if (1 == i) {
			mResult.setCode(0);
			mResult.setMessage("重命名成功！");
		} else {
			mResult.setCode(-3);
			mResult.setMessage("重命名失败！");
		}

		return mResult;
	}

	// 专题分类：设置是否在首页显示
	public ResultObject<String> setAlertCategoryShow(DAlertCategory mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		DAlertCategory mDAlertCategory = mAlertMapper.getAlertCategoryByID(mParams.getId());
		if (mDAlertCategory == null) {
			mResult.setCode(-2);
			mResult.setMessage("该专题不存在！");
			return mResult;
		}

		int i = mAlertMapper.setAlertCategoryShow(mParams);
		if (1 == i) {
			mResult.setCode(0);
			mResult.setMessage("修改首页显示设置成功！");
		} else {
			mResult.setCode(-3);
			mResult.setMessage("修改首页显示设置失败！");
		}

		return mResult;
	}

	// 专题分类：交互顺序; upid表示修改后的上面专题的id；downid表示修改后下面专题的id
	public ResultObject<String> setAlertCategorySort(int upid, int downid) {
		ResultObject<String> mResult = new ResultObject<>();
		DAlertCategory mUpCategory = mAlertMapper.getAlertCategoryByID(upid);
		DAlertCategory mDownCategory = mAlertMapper.getAlertCategoryByID(downid);
		if (mUpCategory == null || mDownCategory == null) {
			mResult.setCode(-2);
			mResult.setMessage("查询数据错误！");
			return mResult;
		}

		int i = mAlertMapper.setAlertCategorySort(upid, downid);
		if (i > 0) {
			mResult.setCode(0);
			mResult.setMessage("移动顺序设置成功！");
		} else {
			mResult.setCode(-3);
			mResult.setMessage("移动顺序设置失败！");
		}

		return mResult;
	}

	// 专题分类：删除
	public ResultObject<String> deleteAlertCategoryById(int id) {
		ResultObject<String> mResult = new ResultObject<>();
		DAlertCategory mDAlertCategory = mAlertMapper.getAlertCategoryByID(id);
		if (mDAlertCategory == null) {
			mResult.setCode(-2);
			mResult.setMessage("该预警接收人不存在！");
			return mResult;
		}
		// 删除该专题下的预警词:"subject:delete:all:12": {"subject": 12}
		JSONObject mJSONObject = new JSONObject();
		String url = "/api/page/sync/warning?";
		JSONObject mJSONDeleteAll = new JSONObject();
		mJSONDeleteAll.put("subject", id);
		String key = "subject:delete:all:" + id;
		mJSONObject.put(key, mJSONDeleteAll);
		String result = mHttpService.postWarnWord(url, mJSONObject.toString());
		JSONObject mJSONResult = JSONObject.parseObject(result);
		if(mJSONResult != null && mJSONResult.getObject("meta", JSONObject.class).getInteger("code") == 0) {
			int i = mAlertMapper.deleteAlertCategoryByID(id);
			if (i == 1) {
				mAlertMapper.deleteAlertWordSubject(id);
				mResult.setCode(0);
				mResult.setMessage("删除成功！");
			} else {
				mResult.setCode(-3);
				mResult.setMessage("删除失败！");
			}
		}else {
			mResult.setCode(-1);
			mResult.setMessage("删除专题失败！");
		}	
		return mResult;
	}

	// 根据专题id获取关键词信息
	public AlertWordMessage getAlertWordMessage(int subjectid) {
		AlertWordMessage mAlertWordMessage = new AlertWordMessage();
		String sWord = "";
		List<DAlertWordSubject> mWordSubjectList = mAlertMapper.getAlertWordBySubjectId(subjectid);
		if(mWordSubjectList != null && mWordSubjectList.size() > 0) {
			for(DAlertWordSubject wordSubject:mWordSubjectList) {
				sWord = sWord + wordSubject.getWord() + ",";
			}
		}
		mAlertWordMessage.setWord(sWord);
		DAlertCategory mDAlertCategory = mAlertMapper.getAlertCategoryByID(subjectid);
		mAlertWordMessage.setIsalert(mDAlertCategory.getIsalert());
		mAlertWordMessage.setSubject_id(subjectid);
		
		String sReceiver = mDAlertCategory.getAlert_receiver();
		if(sReceiver != null && sReceiver.length() > 0) {
			List<Integer> listReceiver = JSON.parseArray(sReceiver, Integer.class);
			mAlertWordMessage.setAlert_receiver(listReceiver);
		}
		
		
		String sType = mDAlertCategory.getAlert_type();
		if(sType != null && sType.length() > 0) {
			List<String> listType = JSON.parseArray(sType, String.class); 
			mAlertWordMessage.setAlert_type(listType);
		}
		mAlertWordMessage.setBegin_time(mDAlertCategory.getBegin_time());
		mAlertWordMessage.setEnd_time(mDAlertCategory.getEnd_time());
		return mAlertWordMessage;
	}
	
	// 保存预警关键词
	public ResultObject<String> settingAlertWord(AlertWordMessage mParams) {
		ResultObject<String> mResult = new ResultObject<>();

		List<DAlertWordSubject> mWordSubjectList = new ArrayList<DAlertWordSubject>();
		String[] arrayString = mParams.getWord().split(",");
		if(arrayString == null || arrayString.length == 0) {
			mResult.setCode(-3);
			mResult.setMessage("解析关键词错误！");
			return mResult;
		}
		
		for (int i = 0; i < arrayString.length; i++) {
			DAlertWordSubject mobject = new DAlertWordSubject();
			mobject.setWord(arrayString[i]);
			mobject.setSubject_id(mParams.getSubject_id());
			System.out.println("------alert word mobject-------" + mobject);
			mWordSubjectList.add(mobject);
		}

		if (mWordSubjectList != null && mWordSubjectList.size() > 0) {
			try {
				// 上传到外部服务器
				mResult = saveWordToServer(arrayString, mParams.getSubject_id(),mParams.getIsalert());
				if (mResult.getCode() == 0) {
					// 插入之前先根据专题ID删除
					mAlertMapper.deleteAlertWordSubject(mParams.getSubject_id());
					// 批量插入数据库中
					mAlertMapper.insertBulkAlertWord(mWordSubjectList);
					updateAlertCategoryMessage(mParams);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mResult.setCode(-2);
				mResult.setMessage("连接外部服务器错误！");
			}
			return mResult;
		}

		return mResult;
	}

	// 根据专题id更新专题信息
	public void updateAlertCategoryMessage(AlertWordMessage mParams) {
		DAlertCategory mDAlertCategory = new DAlertCategory();
		mDAlertCategory.setId(mParams.getSubject_id());
		mDAlertCategory.setIsalert(mParams.getIsalert());
		List<Integer> listReceivers = mParams.getAlert_receiver();
		if(listReceivers != null && listReceivers.size() > 0) {
			String sReceivers = JSONObject.toJSONString(listReceivers);
			mDAlertCategory.setAlert_receiver(sReceivers);
		}
		
		List<String> listTypes = mParams.getAlert_type();
		if(listTypes != null && listTypes.size() > 0) {
			String sTypes = JSONObject.toJSONString(listTypes);
			mDAlertCategory.setAlert_type(sTypes);
		}
		
		mDAlertCategory.setBegin_time(mParams.getBegin_time());
		mDAlertCategory.setEnd_time(mParams.getEnd_time());
		mAlertMapper.updateAlertCategory(mDAlertCategory);
	}

	// 新的方式 "subject:add:12": {"single": ["网信办"],"rule": ["北京*海淀", "天坛*故宫"],"subject": 12,"isWarning":true}
	public ResultObject<String> saveWordToServer(String[] arrayString, int subject_id, int isAlert) {
		ResultObject<String> mResult = new ResultObject<>();
		JSONObject mJSONObject = new JSONObject();
		List<String> ruleList = new ArrayList<String>();
		List<DAlertWordSubject> mDBWordSubjectList = mAlertMapper.getAlertWordSubjectBySubjectID(subject_id);
		// 是否已经保存
		if(mDBWordSubjectList != null && mDBWordSubjectList.size() > 0) {
			// 添加的新词
			List<String> addWordList = new ArrayList<String>();
			for (int i = 0; i < arrayString.length; i++) {	
				boolean tag = false;
				for (DAlertWordSubject o : mDBWordSubjectList) {
					if(o.getWord().equals(arrayString[i])) {
						tag = true;	
					}					
				}
				if(!tag) {
					addWordList.add(arrayString[i]);
				}				
			}
			
			// 删除的词
			List<String> deleteWordList = new ArrayList<String>();
			for (DAlertWordSubject o : mDBWordSubjectList) {
				boolean tag = false;
				for (int j = 0; j < arrayString.length; j++) {	
					if(o.getWord().equals(arrayString[j])) {
						tag = true;	
					}
				}
				if(!tag) {
					deleteWordList.add(o.getWord());
				}						
			}
			
			if(addWordList != null && addWordList.size() > 0) {
				String key_add = "subject:add:" + subject_id;
				JSONObject mJSONAdd = new JSONObject();
				mJSONAdd.put("single", addWordList);
				mJSONAdd.put("rule", ruleList);
				mJSONAdd.put("subject", subject_id);
				if(isAlert == 1) {
					mJSONAdd.put("isWarning", true);
				}else {
					mJSONAdd.put("isWarning", false);
				}
				mJSONObject.put(key_add, mJSONAdd);
			}else {	
				// 可能出现：预警词不修改，修改其它内容
				mResult.setCode(0);
				mResult.setMessage("预警词未改变,修改其它内容！");
				return mResult;
			}
			
			if(deleteWordList != null && deleteWordList.size() > 0) {
				String key_delete = "subject:delete:" + subject_id;
				JSONObject mJSONDelete = new JSONObject();
				mJSONDelete.put("single", deleteWordList);
				mJSONDelete.put("rule", ruleList);
				mJSONDelete.put("subject", subject_id);
				if(isAlert == 1) {
					mJSONDelete.put("isWarning", true);
				}else {
					mJSONDelete.put("isWarning", false);
				}
				mJSONObject.put(key_delete, mJSONDelete);
			}			
		}else {
			// 没有设置，则直接添加
			String key = "subject:add:" + subject_id;
			JSONObject mJSONAdd = new JSONObject();
			mJSONAdd.put("single", arrayString);
			mJSONAdd.put("rule", ruleList);
			mJSONAdd.put("subject", subject_id);
			if(isAlert == 1) {
				mJSONAdd.put("isWarning", true);
			}else {
				mJSONAdd.put("isWarning", false);
			}
			mJSONObject.put(key, mJSONAdd);
		}
		
		System.out.println("------alert word mJSONObject-------" + mJSONObject.toString());
		String url = "/api/page/sync/warning?";
		String result = mHttpService.postWarnWord(url, mJSONObject.toString());
		System.out.println("------alert word result-------" + result);
		// 返回值：{"meta":{"msg":"success","code":0,"success":true,"timestamp":"2019-09-29 17:03:37"},"data":{}}
		JSONObject mJSONResult = JSONObject.parseObject(result);
		if(mJSONResult != null && mJSONResult.getObject("meta", JSONObject.class).getInteger("code") == 0) {
			mResult.setCode(0);
			mResult.setMessage("保存预警词成功！");
		}else {
			mResult.setCode(-1);
			mResult.setMessage("保存预警词失败！");
		}	
		return mResult;
	}

	// 预警接收人:获取全部列表
	public List<DAlertReceiver> getAlertReceiverList() {
		List<DAlertReceiver> mlist = mAlertMapper.getAlertReceiverList();
		return mlist;
	}

	// 预警接收人:添加
	public ResultObject<DAlertReceiver> addNewAlertReceiver(DAlertReceiver mParams) {
		ResultObject<DAlertReceiver> mResult = new ResultObject<>();
		List<DAlertReceiver> mlist = mAlertMapper.getAlertReceiverByName(mParams.getName());
		if (mlist != null && mlist.size() > 0) {
			mResult.setCode(-2);
			mResult.setMessage("该预警接收人已存在！");
			return mResult;
		}

		int i = mAlertMapper.insertAlertReceiver(mParams);
		if (i > 0) {
			mResult.setCode(0);
			mResult.setMessage("添加新的预警接收人成功！");
			mResult.setObject(mParams);
		} else {
			mResult.setCode(-3);
			mResult.setMessage("添加新的预警接收人失败！");
		}
		return mResult;
	}

	// 预警接收人:修改
	public ResultObject<String> updateAlertReceiver(DAlertReceiver mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		DAlertReceiver mDAlertReceiver = mAlertMapper.getAlertReceiverByID(mParams.getId());
		if (mDAlertReceiver == null) {
			mResult.setCode(-2);
			mResult.setMessage("该预警接收人不存在！");
			return mResult;
		}

		int i = mAlertMapper.updateAlertReceiver(mParams);
		if (1 == i) {
			mResult.setCode(0);
			mResult.setMessage("修改接收人信息成功！");
		} else {
			mResult.setCode(-3);
			mResult.setMessage("修改接收人信息失败！");
		}

		return mResult;
	}

	// 预警接收人:删除
	public ResultObject<String> deleteAlertReceiverById(int id) {
		ResultObject<String> mResult = new ResultObject<>();
		DAlertReceiver mDAlertReceiver = mAlertMapper.getAlertReceiverByID(id);
		if (mDAlertReceiver == null) {
			mResult.setCode(-2);
			mResult.setMessage("该预警接收人不存在！");
			return mResult;
		}
		int i = mAlertMapper.deleteAlertReceiverByID(id);
		if (i == 1) {
			mResult.setCode(0);
			mResult.setMessage("删除成功！");
		} else {
			mResult.setCode(-3);
			mResult.setMessage("删除失败！");
		}
		return mResult;
	}

	// 统计数量：根据城市地域
	public String getCountsByTerms(AlertCountsMessage mParams) {
		String path = "/restserver/aggr/illegalDataStatistics/aggrCountsByTerms?";
		JSONObject json = new JSONObject();

		if (mParams.getKey() != null && mParams.getKey().length() > 0) {
			json.put("key", mParams.getKey());
		}

		if (mParams.getCounts() > 0) {
			json.put("counts", mParams.getCounts());
		}

		if (mParams.getProvince() != null && mParams.getProvince().length() > 0) {
			json.put("province", mParams.getProvince());
		}

		if (mParams.getStart_time() != null && mParams.getStart_time().length() > 0) {
			json.put("start_time", mParams.getStart_time());
		}

		if (mParams.getEnd_time() != null && mParams.getEnd_time().length() > 0) {
			json.put("end_time", mParams.getEnd_time());
		}
		System.out.println("------json.toString()-------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		return result;
	}

	// 预警网站：导出网站列表，根据条件
	public AlertWebResult exportAlertWebList(AlertWebMessage mParams) {
		String path = "/restserver/data/export/exportIllegalDatas?";
		JSONObject json = new JSONObject();

		if (mParams.getProvince() != null && mParams.getProvince().length() > 0) {
			json.put("province", mParams.getProvince());
		}

		if (mParams.getCity() != null && mParams.getCity().length() > 0) {
			json.put("city", mParams.getCity());
		}

		if (mParams.getOrganizerNature() != null && mParams.getOrganizerNature().length() > 0) {
			json.put("organizerNature", mParams.getOrganizerNature());
		}

		if (mParams.getLevel() != null && mParams.getLevel().length() > 0) {
			json.put("level", mParams.getLevel());
		}

		if (mParams.getCategory() != null && mParams.getCategory().length() > 0) {
			json.put("category", mParams.getCategory());
		}

		String result = mHttpService.postData(path, json.toString());
		AlertWebResult mAlertWebResult = null;
		if (result != null && result.length() > 0) {
			mAlertWebResult = JSONObject.parseObject(result, AlertWebResult.class);
		}
		return mAlertWebResult;
	}
	
	//  预警网站：导出网站列表，根据ids
	public AlertWebResult exportAlertWebListByIDs(List<String> ids) {
		String path = "/restserver/illegal/query/queryByIDs?";
		JSONObject json = new JSONObject();
		json.put("ids", ids);
		String result = mHttpService.postData(path, json.toString());
		AlertWebResult mAlertWebResult = null;
		if (result != null && result.length() > 0) {
			mAlertWebResult = JSONObject.parseObject(result, AlertWebResult.class);
		}
		return mAlertWebResult;
	}

	// 违规文章：导出违规文章列表
	public String exportAlertPageList(AlertPageMessage mParams) {
		String path = "/restserver/data/export/exportPageDatas?";
		JSONObject json = new JSONObject();

		if (mParams.getMustKeyWords() != null && mParams.getMustKeyWords().size() > 0) {
			json.put("mustKeyWords", mParams.getMustKeyWords());
		}

		if (mParams.getShouldKeyWords() != null && mParams.getShouldKeyWords().size() > 0) {
			json.put("shouldKeyWords", mParams.getShouldKeyWords());
		}

		if (mParams.getUnrelated() != null && mParams.getUnrelated().size() > 0) {
			json.put("unrelated", mParams.getUnrelated());
		}

		if (mParams.getDomainName() != null && mParams.getDomainName().size() > 0) {
			json.put("domainName", mParams.getDomainName());
		}

		if (mParams.getIsWarning() != null && mParams.getIsWarning().length() > 0) {
			json.put("isWarning", mParams.getIsWarning());
		}

		if (mParams.getStart_time() != null && mParams.getStart_time().length() > 0) {
			json.put("start_time", mParams.getStart_time());
		}

		if (mParams.getEnd_time() != null && mParams.getEnd_time().length() > 0) {
			json.put("end_time", mParams.getEnd_time());
		}

		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		return result;
	}
	
    //  违规文章：导出违规文章列表，根据ids
	public String exportAlertPageListByIDs(List<String> ids) {
		String path = "/restserver/page/query/queryByIDs?";
		JSONObject json = new JSONObject();
		json.put("ids", ids);
		String result = mHttpService.postData(path, json.toString());
		return result;
	}
}
