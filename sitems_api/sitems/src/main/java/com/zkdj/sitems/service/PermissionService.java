package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.model.DPermission;
import com.zkdj.sitems.model.DPermissionModule;

@Service
public class PermissionService {
	
	@Autowired
	private UserMapper mUserMapper;
	
	// 获取全部的模块和权限的信息
	public List<DPermissionModule> getAllPermissionMessage(){
		// 获取第一级模块信息
		List<DPermissionModule> mLevel1Module = mUserMapper.getModulesByParentId(0);
		List<DPermissionModule> mAllList = getModuleMsg(mLevel1Module);
		return mAllList;		
	}
	
	// 循环获取各级模块的下级模块和权限列表
	public List<DPermissionModule> getModuleMsg(List<DPermissionModule> mModuleList){
		List<DPermissionModule> mAllModuleList = new ArrayList<DPermissionModule>();
		if(mModuleList != null && mModuleList.size() > 0){
			for(DPermissionModule mModule:mModuleList){
				int id = mModule.getId();
				List<DPermissionModule> mLevel2Module = mUserMapper.getModulesByParentId(id);
				if(mLevel2Module != null && mLevel2Module.size() > 0){
					// 该一级模块下，有二级模块
					List<DPermissionModule> mAll2Module = getModuleMsg(mLevel2Module);
					mModule.setModuleList(mAll2Module);
				}
				
				// 无论一级、二级模块，都要获取权限列表
				List<DPermission> mPermissions = mUserMapper.getPermissionsByModuleId(id);
				if(mPermissions != null && mPermissions.size() > 0){
					mModule.setPermissionList(mPermissions);
				}									
				mAllModuleList.add(mModule);
			}
		}
		return mAllModuleList;
	}

}
