package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zkdj.sitems.model.DPermission;
import com.zkdj.sitems.model.DPermissionModule;
import com.zkdj.sitems.model.DPlatform;
import com.zkdj.sitems.model.DRole;
import com.zkdj.sitems.model.DUser;

@Mapper
public interface UserMapper {
	// db_user
	// 参数开始行数，每页行数
	@Select("SELECT * FROM db_user limit #{index}, #{count}")
	List<DUser> getUserList(@Param("index")int index, @Param("count")int count);
	// 总行数		
	@Select("SELECT COUNT(*) FROM db_user")
	int getUserNumber();
	
	@Select("SELECT * FROM db_user WHERE role_id = #{role_id}")
	List<DUser> getUserListByRoleID(int role_id);
	
	@Select("SELECT * FROM db_user WHERE account = #{account}")
	DUser getUserByAccount(String account);
	
	@Select("SELECT * FROM db_user WHERE id = #{id}")
	DUser getUserByID(int id);
	
	@Select("SELECT * FROM db_user WHERE token = #{token}")
	DUser getUserByToken(String token);
	
	@Delete("DELETE FROM db_user WHERE id = #{id}")
	void deleteUserByID(int id);
	
	@Insert("INSERT INTO db_user(account,user_name,password,contact,token,role_id,account_begints,account_endts,create_time) "
			+ "VALUES(#{account}, #{user_name}, #{password}, #{contact}, #{token}, #{role_id}, #{account_begints}, #{account_endts}, #{create_time})")
	int insertUser(DUser user);
	
	 //db_user  更新用户姓名
    @Update("UPDATE db_user SET user_name=#{name} WHERE account =#{account}")
    void updateUserNameByAccount(String account,String name);
    
    //db_user  更新用户角色
    @Update("UPDATE db_user SET role_id=#{role_id} WHERE id =#{id}")
    void updateUserRoleByID(int id,int role_id);
		
    //db_user  更新用户联系方式
    @Update("UPDATE db_user SET contact=#{phone} WHERE account =#{account}")
    void updataUserContactByAccount(String account,String phone);
	    
    //db_user  更新用户密码
    @Update("UPDATE db_user SET passWord=#{PassWord} WHERE account =#{account}")
    int updataUserPassWordByAccount(String account,String PassWord);
    
    //db_user  更新Token
    @Update("UPDATE db_user SET token=#{token} WHERE id =#{id}")
    int updateUserToken(String token,int id);
	
    @Update("UPDATE db_user SET user_name=#{user_name},password=#{password}, contact=#{contact}, role_id=#{role_id},"
    		+ "account_begints=#{account_begints},account_endts=#{account_endts} WHERE id =#{id}")
	int updateUser(DUser user);
	
    //db_system  更新平台名称
    @Update("UPDATE db_system SET platform=#{Sname}")
    void updateSystemName(String sname);
		
    //db_system  获取平台名称
    @Select("SELECT * FROM db_system")
    DPlatform getSystemName();
	
	// db_role
	@Select("SELECT * FROM db_role")
	@Results({
		@Result(property = "msg", column = "permissions")
	})
	List<DRole> getRoleList();
	
	@Select("SELECT * FROM db_role WHERE id = #{role_id}")
	@Results({
		@Result(property = "msg", column = "permissions")
	})
	DRole getRoleByID(int role_id);
	
	@Select("SELECT * FROM db_role WHERE role_name = #{role_name}")
	@Results({
		@Result(property = "msg", column = "permissions")
	})
	List<DRole> getRoleByName(String role_name);
	
	@Insert("INSERT INTO db_role(role_name,create_time,role_info,permissions) VALUES(#{role_name}, #{create_time}, #{role_info}, #{jsonstr})")
	int insertRole(@Param("role_name")String role_name,@Param("create_time")Long create_time,@Param("role_info")String role_info,@Param("jsonstr")String jsonstr);

	@Delete("DELETE FROM db_role WHERE id = #{role_id}")
	void deleteRoleByID(int role_id);

	@Update("UPDATE db_role SET role_name=#{role_name},role_info=#{role_info}, permissions=#{jsonstr} WHERE id =#{id}")
	int updateRole(@Param("id")int id,@Param("role_name")String role_name,@Param("role_info")String role_info,@Param("jsonstr")String jsonstr);

	
	// db_permission_module
	@Select("SELECT * FROM db_permission_module WHERE parent_id = #{parent_id}")
	List<DPermissionModule> getModulesByParentId(int parent_id);
	
	// db_permission
	@Select("SELECT * FROM db_permission WHERE module_id = #{module_id}")
	List<DPermission> getPermissionsByModuleId(int module_id);
}
