package com.zkdj.sitems.bean;

import java.util.List;

/**
 *   查询预警网站下违规文章列表时，获取的返回数据
 * @author DELL
 *
 */
public class AlertPageResult {
    private String msg; 
    private List<AlertPage> result;
    private String code;
    private String count;
    private String page;
    private String maxPage;
    
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<AlertPage> getResult() {
		return result;
	}
	public void setResult(List<AlertPage> result) {
		this.result = result;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(String maxPage) {
		this.maxPage = maxPage;
	}
	
	@Override
	public String toString() {
		return "AlertWebMessage [msg=" + msg + ", result=" + result + ", code=" + code + ", count=" + count + ", page="
				+ page + ", maxPage=" + maxPage + "]";
	} 
}
