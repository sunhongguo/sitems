package com.zkdj.sitems.bean;

/**
  *  违规网站中：返回的网站数据
 * @author DELL
 *
 */
public class IllegalWeb {
	private int id;
    private String domainName;
    private String organizerNature;
    private int illegalCount;
    private String level;
    private String city;
    private String province;
    private String siteName;
    private String contact;
    private String phone;
    private String categoryTitle;  // 网站分类
    private long handleTime;   // 处理时间
    
    private String scheme;     // 方案

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getOrganizerNature() {
		return organizerNature;
	}

	public void setOrganizerNature(String organizerNature) {
		this.organizerNature = organizerNature;
	}

	public int getIllegalCount() {
		return illegalCount;
	}

	public void setIllegalCount(int illegalCount) {
		this.illegalCount = illegalCount;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	public long getHandleTime() {
		return handleTime;
	}

	public void setHandleTime(long handleTime) {
		this.handleTime = handleTime;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	@Override
	public String toString() {
		return "IllegalWeb [id=" + id + ", domainName=" + domainName + ", organizerNature=" + organizerNature
				+ ", illegalCount=" + illegalCount + ", level=" + level + ", city=" + city + ", province=" + province
				+ ", siteName=" + siteName + ", contact=" + contact + ", phone=" + phone + ", categoryTitle="
				+ categoryTitle + ", handleTime=" + handleTime + ", scheme=" + scheme + "]";
	}
    
    
}
