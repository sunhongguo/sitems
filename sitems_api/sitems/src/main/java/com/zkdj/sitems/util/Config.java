package com.zkdj.sitems.util;

public class Config {	
	// 网站数据导出：生成的excel文档保存位置
//	public static String path_excel = "E:\\fileUpload\\";
	public static String path_excel = "/home/sitems/file/";

	// 返回生成的excel文档地址
	public static String url_excel = "sitems/file/excel?excelname=";
	// 返回生成的word文档地址
	public static String url_word = "sitems/file/word?wordname=";
	
	// JWT Signature 签名密钥
	public static String JWT_PWD = "qianyan_sitems";
	
	// redis中保存user_id的key
	public static String REDIS_USERID_KEY = "user_id";
	public static String REDIS_KEY_USER = "key_user_message";
	public static String REIDS_KEY_ROLE = "key_role_message";
}
