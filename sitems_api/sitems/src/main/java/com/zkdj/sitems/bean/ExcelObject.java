package com.zkdj.sitems.bean;

/*
 * excel表中读取的对象
 */
public class ExcelObject {
	private String name;
	private String address;

	public ExcelObject(){
		super();
	}
	
	public ExcelObject(String name, String address){
		this.name = name;
		this.address = address;
	}

}
