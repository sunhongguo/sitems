package com.zkdj.sitems.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zkdj.sitems.model.DPlatform;

@Mapper
public interface SystemMapper {
	 //db_system  更新预警地址
    @Update("UPDATE db_system SET alerturl=#{alerturl}")
    void updateSystemAlertURL(String alertur);
		
    //db_system  获取系统信息
    @Select("SELECT * FROM db_system")
    DPlatform getSystemContent();
}
