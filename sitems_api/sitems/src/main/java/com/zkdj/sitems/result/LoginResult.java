package com.zkdj.sitems.result;

import com.zkdj.sitems.bean.LoginData;

public class LoginResult {
	private int code;
	private String message;
	private LoginData object;
	
	public LoginResult(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(LoginData object){
		this.object = object;
	}
	public LoginData getObject(){
		return object;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}
