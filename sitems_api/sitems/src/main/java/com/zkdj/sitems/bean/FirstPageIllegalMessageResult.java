package com.zkdj.sitems.bean;

/**
  * 查询违规概况的返回结果
 * @author DELL
 *
 */
public class FirstPageIllegalMessageResult {
	private int count_all_illegal_webs_today;   // 今日内违规网站总数
	private int count_all_illegal_webs_7;   // 7天内违规网站总数
	 
    private int count_illegal_webs;  // 今日待处理网站数
    private int count_illegal_webs_Handled;  // 今日待处理网站数
    
    private int count_level_A;   // A级网站数
    private int count_level_B;   // B级网站数
    private int count_level_C;   // C级网站数
    
    
	public int getCount_all_illegal_webs_today() {
		return count_all_illegal_webs_today;
	}
	public void setCount_all_illegal_webs_today(int count_all_illegal_webs_today) {
		this.count_all_illegal_webs_today = count_all_illegal_webs_today;
	}
	public int getCount_all_illegal_webs_7() {
		return count_all_illegal_webs_7;
	}
	public void setCount_all_illegal_webs_7(int count_all_illegal_webs_7) {
		this.count_all_illegal_webs_7 = count_all_illegal_webs_7;
	}
	public int getCount_illegal_webs() {
		return count_illegal_webs;
	}
	public void setCount_illegal_webs(int count_illegal_webs) {
		this.count_illegal_webs = count_illegal_webs;
	}
	public int getCount_illegal_webs_Handled() {
		return count_illegal_webs_Handled;
	}
	public void setCount_illegal_webs_Handled(int count_illegal_webs_Handled) {
		this.count_illegal_webs_Handled = count_illegal_webs_Handled;
	}
	public int getCount_level_A() {
		return count_level_A;
	}
	public void setCount_level_A(int count_level_A) {
		this.count_level_A = count_level_A;
	}
	public int getCount_level_B() {
		return count_level_B;
	}
	public void setCount_level_B(int count_level_B) {
		this.count_level_B = count_level_B;
	}
	public int getCount_level_C() {
		return count_level_C;
	}
	public void setCount_level_C(int count_level_C) {
		this.count_level_C = count_level_C;
	}
	@Override
	public String toString() {
		return "FirstPageIllegalMessageResult [count_illegal_webs=" + count_illegal_webs
				+ ", count_illegal_webs_Handled=" + count_illegal_webs_Handled + ", count_level_A=" + count_level_A
				+ ", count_level_B=" + count_level_B + ", count_level_C=" + count_level_C + "]";
	}  
}
