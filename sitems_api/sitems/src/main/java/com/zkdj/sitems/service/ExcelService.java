package com.zkdj.sitems.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.WebObject;
import com.zkdj.sitems.result.ResultObject;

/*
 * 将Excel表格中的数据解析到对象，并保存到数据库
 */
@Service
public class ExcelService {
	@Autowired
	private WebManageService mWebService;
	
    private int allAccount = 0; // excel文件中总条数
    private int updateAccount = 0; // 已上传条数
    private double percent = 0;    // 上传的百分比
    /**
     * 处理上传的文件
     *
     * @param in
     * @param fileName
     * @return
     * @throws Exception
     */
    public ResultObject<Object> getBankListByExcel(InputStream in, String fileName,int number) throws Exception {
    	SitemsApplication.logger.info("------getBankListByExcel--------" + number);
    	ResultObject<Object> mResult = new ResultObject<>();
        List<WebObject> list = new ArrayList<>();
        //创建Excel工作薄
        Workbook work = this.getWorkbook(in, fileName);
        if (null == work) {
            mResult.setCode(-5);
    		mResult.setMessage("创建Excel工作薄为空！");
    		return mResult;
        }
        Sheet sheet = null;
        Row row = null;
        allAccount = 0;
        updateAccount = 0;
        percent = 0;

        // 获取数据总条数
        for (int i = 0; i < work.getNumberOfSheets(); i++) {
        	if (work.getSheetAt(i) == null) {
                continue;
            }
        	sheet = work.getSheetAt(i);
        	row = sheet.getRow(sheet.getFirstRowNum());
        	if(row == null) {
        		mResult.setCode(-4);
        		mResult.setMessage("Excel表格行错误！");
        		return mResult;
        	}
        	allAccount = allAccount + work.getSheetAt(i).getLastRowNum();
        }
        
        for (int i = 0; i < work.getNumberOfSheets(); i++) {
            sheet = work.getSheetAt(i);
            if (sheet == null) {
                continue;
            }
            // 第一行为标题，从下一行开始
            int ifirst = sheet.getFirstRowNum();
            Row mTitleRow = sheet.getRow(ifirst); // 处理excel的标题，判断每行数据的属性
            for (int j = ifirst+1; j <= sheet.getLastRowNum(); j++) {           	
            	row = sheet.getRow(j);              
                if (row == null) {
                     continue;
                }                   
                WebObject mWebObject = getObjectFromRow(mTitleRow,row);  
                list.add(mWebObject);
            	 
            	
            	if(list!= null && list.size() > number) {
            		mResult = sendDataToServer(list);
            		if(mResult != null && mResult.getCode() != 0) {
            			return mResult;
            		}
            		list.clear();
            	}
            	
            }
        }
        // 不足指定条数
        if(list!= null && list.size() > 0) {
        	mResult = sendDataToServer(list);
    		list.clear();
    	}
        work.close();
        
        if(mResult != null && mResult.getCode() == 0) {
        	mResult.setMessage("Excel表格导入成功！");
        }
        return mResult;
    }
    
    // 将数据上传到服务器
    public ResultObject<Object> sendDataToServer(List<WebObject> list) { 
    	ResultObject<Object> mResult = new ResultObject<>();
    	updateAccount = updateAccount + list.size();
        int repeat = 0;  // 重复上传的次数
    	SitemsApplication.logger.info("------sendDataToServer----updateAccount----" + updateAccount);
    	mResult = mWebService.bulkSaveNewWeb(list);  
    	if(mResult.getCode() != 0 && repeat < 1) {
    		repeat++;
    		mResult = mWebService.bulkSaveNewWeb(list);
    	}
    	percent = updateAccount/allAccount;
    	return mResult;
    }

    // 获取百分比
    public double getPercent() {
    	return percent;
    }
    
    public WebObject getObjectFromRow(Row mTitleRow, Row row) {
    	WebObject mWebObject = new WebObject();
    	int firstNum = mTitleRow.getFirstCellNum();
    	int lastNum = mTitleRow.getLastCellNum();
    	for(int i=firstNum; i<= lastNum; i++) {
    		// 获取每列的标题，再根据标题设置对应列的数据
    		mTitleRow.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
    		String title = mTitleRow.getCell(i).getStringCellValue();
    		if(title.equals("域名")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setDomainName(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("主办单位")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setOrganizer(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("单位性质")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	String nature = row.getCell(i).getStringCellValue();
            	mWebObject.setOrganizerNature(getOrganizerNatureNum(nature));
            	continue;
    		}
    		if(title.contains("备案许可证")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setIcpNo(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("名称")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setSiteName(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("邮箱")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setEmail(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("电话")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setPhone(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("省份")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setProvince(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("地级市")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setCity(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("注册商")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setRegister(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("联系人")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setContact(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("创建时间")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setRegisterTime(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("过期时间")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setEndTime(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("采集时间")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setGatherTime(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.equals("域名服务器")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setDomainNameServer(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("网站状态")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_NUMERIC);
            	double d = row.getCell(i).getNumericCellValue();
            	int status = (int) d;
            	mWebObject.setDomainNameStatus(status);
            	continue;
    		}
    		if(title.equalsIgnoreCase("dns")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setDns(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		if(title.contains("pr") || title.contains("PR")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_NUMERIC);
            	double d = row.getCell(i).getNumericCellValue();
            	int score = (int) d;
            	mWebObject.setPrScore(score);
            	continue;
    		}
    		if(title.contains("级别")) {
    			row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
            	mWebObject.setLevel(row.getCell(i).getStringCellValue());
            	continue;
    		}
    		
    	}
//    	int y = row.getFirstCellNum();
//        int allnum = row.getLastCellNum();        
//        if(y < allnum) {
//        	row.getCell(y).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setId(row.getCell(y).getStringCellValue());
//        }       
//        if(y+1 < allnum) {
//        	row.getCell(y+1).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setProvince(row.getCell(y+1).getStringCellValue());
//        }
//        if(y+2 < allnum) {
//        	row.getCell(y+2).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setCity(row.getCell(y+2).getStringCellValue());
//        }
//        if(y+3 < allnum) {
//        	row.getCell(y+3).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setDns(row.getCell(y+3).getStringCellValue());
//        }
//        if(y+4 < allnum) {
//        	row.getCell(y+4).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setDomainName(row.getCell(y+4).getStringCellValue());
//        }
//        if(y+5 < allnum) {
//        	row.getCell(y+5).setCellType(Cell.CELL_TYPE_NUMERIC);
//        	double d = row.getCell(y+5).getNumericCellValue();
//        	int status = (int) d;
//        	mWebObject.setDomainNameStatus(status);
//        }
//        if(y+6 < allnum) {
//        	row.getCell(y+6).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setOrganizer(row.getCell(y+6).getStringCellValue());
//        }
//        if(y+7 < allnum) {
//        	row.getCell(y+7).setCellType(Cell.CELL_TYPE_STRING);
//        	String nature = row.getCell(y+7).getStringCellValue();
//        	mWebObject.setOrganizerNature(getOrganizerNatureNum(nature));
//        }
//        if(y+8 < allnum) {
//        	row.getCell(y+8).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setIcpNo(row.getCell(y+8).getStringCellValue());
//        }
//        if(y+9 < allnum) {
//        	row.getCell(y+9).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setSiteName(row.getCell(y+9).getStringCellValue());
//        }
//        if(y+10 < allnum) {
//        	row.getCell(y+10).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setEmail(row.getCell(y+10).getStringCellValue());
//        }
//        if(y+11 < allnum) {
//        	row.getCell(y+11).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setPhone(row.getCell(y+11).getStringCellValue());
//        }
//        if(y+12 < allnum) {
//        	row.getCell(y+12).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setRegister(row.getCell(y+12).getStringCellValue());
//        }
//        if(y+13 < allnum) {
//        	row.getCell(y+13).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setContact(row.getCell(y+13).getStringCellValue());
//        }
//        if(y+14 < allnum) {
//        	row.getCell(y+14).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setLevel(row.getCell(y+14).getStringCellValue());
//        }
//        if(y+15 < allnum) {
//        	row.getCell(y+15).setCellType(Cell.CELL_TYPE_NUMERIC);
//        	double d = row.getCell(y+15).getNumericCellValue();
//        	int score = (int) d;
//        	mWebObject.setPrScore(score);
//        }
//        if(y+16 < allnum) {
//        	row.getCell(y+16).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setRegisterTime(row.getCell(y+16).getStringCellValue());
//        }
//        if(y+17 < allnum) {
//        	row.getCell(y+17).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setEndTime(row.getCell(y+17).getStringCellValue());
//        }
//        if(y+18 < allnum) {
//        	row.getCell(y+18).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setGatherTime(row.getCell(y+18).getStringCellValue());
//        }
//        if(y+19 < allnum) {
//        	row.getCell(y+19).setCellType(Cell.CELL_TYPE_STRING);
//        	mWebObject.setDomainNameServer(row.getCell(y+19).getStringCellValue());
//        }
        
        return mWebObject;
    }
    
    private String getOrganizerNatureNum(String str) {
    	String organizerNature = "";
    	if(str.equals("个人")) {
    		organizerNature = "2000";
    	}else if(str.equals("企业")) {
    		organizerNature = "2001";
    	}else if(str.equals("事业单位")) {
    		organizerNature = "2002";
    	}else if(str.equals("社会团体")) {
    		organizerNature = "2003";
    	}else if(str.equals("政府机关")) {
    		organizerNature = "2004";
    	}else if(str.equals("其他")) {
    		organizerNature = "2999";
    	}else {
    		organizerNature = "2999";
    	}
    	
    	return organizerNature;
    }
    
    /**
     * 判断文件格式
     * HSSFWorkbook:是操作Excel2003以前（包括2003）的版本，扩展名是.xls
     * XSSFWorkbook:是操作Excel2007的版本，扩展名是.xlsx
     * 根据后缀名判断的问题：将xlsx的后缀改为xls时，如果使用xlsx的函数来读取，结果是报错；虽然后缀名对了，但是文件内容编码等都不对
     * 推荐使用poi-ooxml中的WorkbookFactory.create(inputStream)来创建Workbook，因为HSSFWorkbook和XSSFWorkbook都实现了Workbook接口
     * @param inStr
     * @param fileName
     * @return
     * @throws Exception
     */
    public Workbook getWorkbook(InputStream inStr, String fileName) throws Exception {
        Workbook workbook = null;
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        if (".xls".equals(fileType)) {
//            workbook = new HSSFWorkbook(inStr);
        	workbook = WorkbookFactory.create(inStr);
        } else if (".xlsx".equals(fileType)) {
//            workbook = new XSSFWorkbook(inStr);
        	workbook = WorkbookFactory.create(inStr);
        } else {
            throw new Exception("请上传excel文件！");
        }
        return workbook;
    }

}
