package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zkdj.sitems.bean.Category;
import com.zkdj.sitems.model.DCategory;

@Mapper
public interface CategoryMapper {
	
	@Insert("INSERT INTO db_site_category(category_name,parent_id,sort) "
			+ "VALUES(#{category_name}, #{parent_id}, #{sort})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	int insertCategory(Category category);
	
	@Select("SELECT * FROM db_site_category WHERE id = #{category_id}")
	Category getCategoryById(int category_id);
	
	@Select("SELECT * FROM db_site_category WHERE category_name = #{category_name}")
	List<Category> getCategoryByName(String category_name);
	
	@Select("SELECT count(*) FROM db_site_category WHERE parent_id = #{parent_id}")
	int getCountByParentID(int parent_id);
	
	@Delete("DELETE FROM db_site_category WHERE id = #{category_id}")
	int deleteCategoryById(int category_id);
	
	//更新分类名称
    @Update("UPDATE db_site_category SET category_name=#{category_name} where id = #{category_id}")
    int updateCategoryName(int category_id,String category_name);
    
    //@ 更新分类sort字段
    @Update("UPDATE db_site_category SET sort=#{sort} where id = #{category_id}")
    int updateCategorySort(int category_id,int sort);
    
    @Select("SELECT * FROM db_site_category")
	List<DCategory> getAllCategory();
		
}
