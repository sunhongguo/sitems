package com.zkdj.sitems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.AlertCountsMessage;
import com.zkdj.sitems.bean.AlertPageMessage;
import com.zkdj.sitems.bean.AlertWebMessage;
import com.zkdj.sitems.bean.AlertWebResult;
import com.zkdj.sitems.bean.AlertWordMessage;
import com.zkdj.sitems.model.DAlertCategory;
import com.zkdj.sitems.model.DAlertReceiver;
import com.zkdj.sitems.result.ResultList;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.AlertService;
import com.zkdj.sitems.service.PermissionsService;

/**
 * 巡查预警API的控制器
 * 
 * @author DELL
 *
 */
@RestController
@RequestMapping("/alert")
public class AlertController {
	@Autowired
	private AlertService mAlertService;
	@Autowired
	private PermissionsService mPermissionsService;
	
	// 预警网站：获取列表
	@RequestMapping("/web_list")
	public ResultObject<AlertWebResult> getAlertWebListC(@RequestBody AlertWebMessage mParams) {
		SitemsApplication.logger.info("-------getAlertWebList---------" + mParams);
		ResultObject<AlertWebResult> mResult = new ResultObject<AlertWebResult>();
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		AlertWebResult mAlertWebResult = mAlertService.getAlertWebList(mParams);
		if(mAlertWebResult != null) {
			mResult.setObject(mAlertWebResult);
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
		}else {
			mResult.setCode(-2);
			mResult.setMessage("查询预警网站列表错误！");
		}	
		return mResult;
	}
	
	// 违规文章：根据条件，获取违规文章列表
	@RequestMapping("/get_pagelist")
	public ResultObject<JSONObject> getAlertPageListByNameC(@RequestBody AlertPageMessage mParams) {
		SitemsApplication.logger.info("-------getAlertPageListByNameC---------" + mParams);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
			
		String sResult = mAlertService.getAlertPageListByName(mParams);
	    JSONObject mJSONObject = JSONObject.parseObject(sResult);
			
		mResult.setObject(mJSONObject);
		mResult.setCode(0);
		mResult.setMessage("查询成功！");
		return mResult;
	}
	
	// 违规文章：根据id查询单个
		@RequestMapping("/get_pagebyid")
		public ResultObject<JSONObject> getAlertPageByByIDC(@RequestParam("id")String id) {
			SitemsApplication.logger.info("-------getAlertPageByByIDC---------" + id);
			ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
			if(id == null || id.length() == 0){
				mResult.setCode(-1);
				mResult.setMessage("参数错误！");
				return mResult;
			}
				
			String sResult = mAlertService.getAlertPageByID(id);
		    JSONObject mJSONObject = JSONObject.parseObject(sResult);
				
			mResult.setObject(mJSONObject);
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			return mResult;
		}

	// 专题分类：获取列表
	@RequestMapping("/category_list")
	public ResultList<JSONObject> getAlertCategoryListC(@RequestParam("account_id") int account_id) {
		ResultList<JSONObject> mResult = new ResultList<JSONObject>();
		if(account_id == 0) {
			mResult.setCode(-2);
			mResult.setMessage("帐号ID为空！");
			return mResult;
		}
		List<JSONObject> mlist = mAlertService.getWarnCategoryList(account_id);

		if (mlist != null) {
			mResult.setObject(mlist);
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
		} else {
			mResult.setCode(-1);
			mResult.setMessage("查询失败！");
		}

		return mResult;
	}

	// 专题分类：新建
	@RequestMapping("/category_add")
	public ResultObject<DAlertCategory> addNewAlertCategoryC(@RequestBody DAlertCategory mParams) {
		ResultObject<DAlertCategory> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","巡查设置","新建专题");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getName() == null || mParams.getName().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("专题名称为空！");
			return mResult;
		}
		
		if (mParams.getAccount_id() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("创建者ID为空！");
			return mResult;
		}
		
		mResult = mAlertService.addNewAlertCategory(mParams);
		return mResult;
	}

	// 专题分类：重命名
	@RequestMapping("/category_rename")
	public ResultObject<String> renameAlertCategoryC(@RequestBody DAlertCategory mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","巡查设置","编辑");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getId() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("专题ID为空！");
			return mResult;
		}

		if (mParams.getName() == null || mParams.getName().length() == 0) {
			mResult.setCode(-3);
			mResult.setMessage("专题新名称为空！");
			return mResult;
		}
		mResult = mAlertService.renameAlertCategory(mParams);

		return mResult;
	}

	// 专题分类：设置是否在首页显示
	@RequestMapping("/category_show")
	public ResultObject<String> setAlertReceiverShowC(@RequestBody DAlertCategory mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","巡查设置","首页显示/隐藏");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null || mParams.getId() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数错误！");
			return mResult;
		}
		mResult = mAlertService.setAlertCategoryShow(mParams);
		return mResult;
	}
	
	// 专题分类：交互顺序; upid表示修改后的上面专题的id；downid表示修改后下面专题的id
	@RequestMapping("/category_sort")
	public ResultObject<String> setAlertReceiverSortC(int firstid, int secondid) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","巡查设置","上移/下移");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (firstid == 0|| secondid == 0) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数错误！");
			return mResult;
		}
		mResult = mAlertService.setAlertCategorySort(firstid, secondid);
		return mResult;
	}

	// 专题分类：删除
	@RequestMapping("/category_delete")
	public ResultObject<String> deleteAlertReceiverByIdC(@RequestBody DAlertCategory mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","巡查设置","删除");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null || mParams.getId() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数错误！");
			return mResult;
		}
		mResult = mAlertService.deleteAlertCategoryById(mParams.getId());
		return mResult;
	}
	
	// 保存预警关键词
	@RequestMapping("/setting_word")
	public ResultObject<String> settingAlertWordC(@RequestBody AlertWordMessage mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数错误！");
			return mResult;
		}
		
		if(mParams.getSubject_id() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("专题ID错误！");
			return mResult;
		}
		
		if(mParams.getWord() == null || mParams.getWord().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("关键词为空！");
			return mResult;
		}
		mResult = mAlertService.settingAlertWord(mParams);		
		return mResult;
	}
	
	// 预警关键词:根据专题id获取信息
	@RequestMapping("/get_word_message")
	public ResultObject<AlertWordMessage> getAlertWordMessageC(int subjectid) {
		ResultObject<AlertWordMessage> mResult = new ResultObject<>();
		if(subjectid == 0) {
			mResult.setCode(-1);
			mResult.setMessage("专题ID错误！");
			return mResult;
		}
		AlertWordMessage mAlertWordMessage = mAlertService.getAlertWordMessage(subjectid);
		if(mAlertWordMessage != null) {
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mAlertWordMessage);
		}else {
			mResult.setCode(-2);
			mResult.setMessage("查询失败！");
		}
		return mResult;
	}

	// 预警接收人:获取全部列表
	@RequestMapping("/receiver_list")
	public ResultList<DAlertReceiver> getAlertReceiverListC() {
		ResultList<DAlertReceiver> mResult = new ResultList<DAlertReceiver>();
		List<DAlertReceiver> mlist = mAlertService.getAlertReceiverList();
		if (mlist != null) {
			mResult.setCode(0);
			mResult.setMessage("查询预警接收人成功！");
			mResult.setObject(mlist);
			return mResult;
		}

		mResult.setCode(-1);
		mResult.setMessage("查询预警接收人失败！");
		return mResult;
	}

	// 预警接收人:添加
	@RequestMapping("/receiver_add")
	public ResultObject<DAlertReceiver> addNewAlertReceiverC(@RequestBody DAlertReceiver mParams) {
		ResultObject<DAlertReceiver> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","预警接收人设置","添加");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getName() == null || mParams.getName().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("预警接受人名称为空！");
			return mResult;
		}
		mResult = mAlertService.addNewAlertReceiver(mParams);
		return mResult;
	}

	// 预警接收人:修改
	@RequestMapping("/receiver_update")
	public ResultObject<String> updateAlertReceiverC(@RequestBody DAlertReceiver mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","预警接收人设置","修改");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getId() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("预警接受人ID为空！");
			return mResult;
		}

		if (mParams.getName() == null || mParams.getName().length() == 0) {
			mResult.setCode(-3);
			mResult.setMessage("预警接受人名称为空！");
			return mResult;
		}
		mResult = mAlertService.updateAlertReceiver(mParams);

		return mResult;
	}

	// 预警接收人:删除
	@RequestMapping("/receiver_delete")
	public ResultObject<String> deleteAlertReceiverByIdC(@RequestBody DAlertReceiver mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","预警接收人设置","删除");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null || mParams.getId() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数错误！");
			return mResult;
		}
		mResult = mAlertService.deleteAlertReceiverById(mParams.getId());
		return mResult;
	}
	
	// 统计数量：根据城市地域
	@RequestMapping("/count_term")
	public ResultObject<JSONObject> getCountsByTermsC(@RequestBody AlertCountsMessage mParams) {
		SitemsApplication.logger.info("-------getCountsByTermsC---------" + mParams);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getProvince() == null || mParams.getProvince().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("省份信息错误！");
			return mResult;
		}
		
		if(mParams.getKey() == null || mParams.getKey().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("key错误！");
			return mResult;
		}
				
		String sResult = mAlertService.getCountsByTerms(mParams);
		JSONObject mJSONObject = JSONObject.parseObject(sResult);
				
		mResult.setObject(mJSONObject);
		mResult.setCode(0);
		mResult.setMessage("查询成功！");
		return mResult;
	}
}
