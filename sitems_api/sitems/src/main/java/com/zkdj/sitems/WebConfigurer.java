package com.zkdj.sitems;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.zkdj.sitems.interceptor.LoginInterceptor;

/**
 * 和springmvc的webmvc拦截配置一样
 * 
 * @author BIANP
 */
@Configuration
public class WebConfigurer implements WebMvcConfigurer {
	@Bean
	public LoginInterceptor getLoginInterceptor() {
		return new LoginInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 添加拦截的请求，并排除几个不拦截的请求
		registry.addInterceptor(getLoginInterceptor()).addPathPatterns("/**").excludePathPatterns("/", "/login",
				"/login/**", "/file/excel", "/file/word");
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*").allowCredentials(true)
				.allowedMethods("GET", "POST", "DELETE", "PUT").allowedHeaders("sitems_token", "Content-Type")
				.maxAge(3600);
	}
}