package com.zkdj.sitems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.WebObject;
import com.zkdj.sitems.bean.WebParam;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.service.WebManageService;

/**
 * 网站管理API的控制器
 * @author DELL
 *
 */
@RestController
@RequestMapping("/web")
public class WebManageController {
	@Autowired
	private WebManageService mWebService;
	@Autowired
	private PermissionsService mPermissionsService;

	// 获取城市列表
	@RequestMapping("/regionlist")
	public ResultObject<JSONObject> getRegionList(){
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		JSONObject mJSONObject = mWebService.getRegionList();
			
		mResult.setObject(mJSONObject);
		mResult.setCode(0);
		mResult.setMessage("查询成功！");
			
		return mResult;
	}
		
	// 查询网站数据列表:其中category为空是全部分类；category="0000"为默认分类即没有分类的网站
	@RequestMapping("/querylist")
	public ResultObject<JSONObject> getWebDataList(@RequestBody WebParam mParams){
		SitemsApplication.logger.info("-------getWebDataList---------" + mParams);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getProvince() == "" || mParams.getProvince().length() == 0){
			mResult.setCode(-1);
			mResult.setMessage("省份信息为空！");
			return mResult;
		}
		String sResult= mWebService.getWebList(mParams);
		JSONObject mJSONObject = JSONObject.parseObject(sResult);
		
		mResult.setObject(mJSONObject);
		mResult.setCode(0);
		mResult.setMessage("查询成功！");
		
		return mResult;
	}
	
	// 添加新的网站数据
	@RequestMapping("/add")
	public ResultObject<WebObject> addNewWebData(@RequestBody WebObject mWebObject){
		SitemsApplication.logger.info("-------addNewWebData---------" + mWebObject);
		ResultObject<WebObject> mResult = new ResultObject<WebObject>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","添加",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if(mWebObject == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mWebObject.getProvince() == null || mWebObject.getProvince().isEmpty()){
			mResult.setCode(-3);
			mResult.setMessage("网站的省份信息为空！");
			return mResult;
		}
		
		if(mWebObject.getSiteName() == null || mWebObject.getSiteName().isEmpty()){
			mResult.setCode(-3);
			mResult.setMessage("网站名称为空！");
			return mResult;
		}
		
		if(mWebObject.getDomainName() == null || mWebObject.getDomainName().isEmpty()){
			mResult.setCode(-3);
			mResult.setMessage("网站域名为空！");
			return mResult;
		}
		
		mResult = mWebService.addNewWeb(mWebObject);
		return mResult;
	}
	
	// 修改网站数据
	@RequestMapping("/update")
	public ResultObject<JSONObject> updateWebData(@RequestBody WebObject mWebObject){
		SitemsApplication.logger.info("-------updateWebData---------" + mWebObject);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","修改",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if(mWebObject == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mWebObject.getProvince() == null || mWebObject.getProvince().isEmpty()){
			mResult.setCode(-3);
			mResult.setMessage("网站的省份信息为空！");
			return mResult;
		}
		
		if(mWebObject.getSiteName() == null || mWebObject.getSiteName().isEmpty()){
			mResult.setCode(-3);
			mResult.setMessage("网站名称为空！");
			return mResult;
		}
		
		if(mWebObject.getDomainName() == null || mWebObject.getDomainName().isEmpty()){
			mResult.setCode(-3);
			mResult.setMessage("网站域名为空！");
			return mResult;
		}
		
		String sResult = mWebService.updateWebData(mWebObject);
		JSONObject mJSONObject = JSONObject.parseObject(sResult);
		if(mJSONObject == null){
			mResult.setCode(-2);
			mResult.setMessage("修改网站失败！");
			return mResult;
		}
		
		if(mJSONObject.getString("code").equals("0")){
			mResult.setCode(0);
			mResult.setMessage("修改网站成功！");
		}else{
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}		
		return mResult;
	}
	
	/**
	 * 批量修改分类
	 * @param mParams: ids批量网站的id; category设置的分类
	 * @return
	 */
	@RequestMapping("/update_bulk_category")
	public ResultObject<JSONObject> updateBulkWebCategory(@RequestBody WebParam mParams){
		SitemsApplication.logger.info("-------updateBulkWebCategory---------" + mParams);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getIds() == null || mParams.getIds().size() == 0){
			mResult.setCode(-1);
			mResult.setMessage("ids错误！");
			return mResult;
		}
		
		if(mParams.getCategory_ids() == null || mParams.getCategory_ids().size() == 0){
			mResult.setCode(0);
			mResult.setMessage("分类信息错误！");
			return mResult;
		}
		
		String sResult = mWebService.updateWebCategoryBulk(mParams.getIds(),mParams.getCategory_ids());
		JSONObject mJSONObject = JSONObject.parseObject(sResult);
		if(mJSONObject == null){
			mResult.setCode(-2);
			mResult.setMessage("批量修改网站分类失败！");
			return mResult;
		}
		
		if(mJSONObject.getString("code").equals("0")){
			mResult.setCode(0);
			mResult.setMessage("批量修改网站分类成功！");
		}else{
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}
		
		return mResult;
	}
	
	// 删除网站：根据id
	@RequestMapping("/delete")
	public ResultObject<JSONObject> deleteWebData(@RequestBody WebParam mParams){
		SitemsApplication.logger.info("-------deleteWebData---------" + mParams);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","删除",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if(mParams == null || mParams.getId().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		String sResult = mWebService.deleteWeb(mParams.getId());
		JSONObject mJSONObject = JSONObject.parseObject(sResult);
		if(mJSONObject == null){
			mResult.setCode(-2);
			mResult.setMessage("删除网站失败！");
			return mResult;
		}
		
		if(mJSONObject.getString("code").equals("0")){
			mResult.setCode(0);
			mResult.setMessage("删除网站成功！");
		}else{
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}		
		return mResult;
	}
	
	// 删除网站：批量删除
	@RequestMapping("/delete_bulk")
	public ResultObject<JSONObject> deleteBulkWebData(@RequestBody WebParam mParams){
		SitemsApplication.logger.info("-------deleteBulkWebData---------" + mParams);
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("网站管理","批量删除",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getIds() == null || mParams.getIds().size() == 0){
			mResult.setCode(-1);
			mResult.setMessage("ids错误！");
			return mResult;
		}

		String sResult = mWebService.deleteBulkWeb(mParams.getIds());
		JSONObject mJSONObject = JSONObject.parseObject(sResult);
		if(mJSONObject == null){
			mResult.setCode(-2);
			mResult.setMessage("批量删除网站失败！");
			return mResult;
		}
		
		if(mJSONObject.getString("code").equals("0")){
			mResult.setCode(0);
			mResult.setMessage("批量删除网站成功！");
		}else{
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}	
		return mResult;
	}
	
	// 网站数据分类统计
	@RequestMapping("/counts_all")
	public ResultObject<JSONObject> queryAllWebCounts(@RequestBody WebParam mParams){
		ResultObject<JSONObject> mResult = new ResultObject<>();
		
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getProvince() == "" || mParams.getProvince().length() == 0){
			mResult.setCode(-1);
			mResult.setMessage("省份信息为空！");
			return mResult;
		}
		
        String sResult = mWebService.getAllWebCounts(mParams);		
        JSONObject mJSONObject = JSONObject.parseObject(sResult);
		if(mJSONObject == null){
			mResult.setCode(-2);
			mResult.setMessage("获取网站数据统计失败！");
			return mResult;
		}
		
		if(mJSONObject.getString("code").equals("0")){
			mResult.setCode(0);
			mResult.setMessage("获取网站数据统计成功！");
			mResult.setObject(mJSONObject.getJSONObject("result"));
		}else{
			mResult.setCode(-1);
			mResult.setMessage(mJSONObject.getString("msg"));
		}
		
		return mResult;
	}
	
	// 网站数据分类统计
	@RequestMapping("/counts_category")
	public ResultObject<JSONObject> queryWebCountsByCategory(@RequestBody WebParam mParams){
		ResultObject<JSONObject> mResult = new ResultObject<>();
			
			if(mParams == null){
				mResult.setCode(-1);
				mResult.setMessage("参数错误！");
				return mResult;
			}
			
			if(mParams.getProvince() == "" || mParams.getProvince().length() == 0){
				mResult.setCode(-1);
				mResult.setMessage("省份信息为空！");
				return mResult;
			}
			
	        String sResult = mWebService.getWebCountsByCategory(mParams);		
	        JSONObject mJSONObject = JSONObject.parseObject(sResult);
			if(mJSONObject == null){
				mResult.setCode(-2);
				mResult.setMessage("获取网站数据统计失败！");
				return mResult;
			}
			
			if(mJSONObject.getString("code").equals("0")){
				mResult.setCode(0);
				mResult.setMessage("获取网站数据统计成功！");
				mResult.setObject(mJSONObject.getJSONObject("result"));
			}else{
				mResult.setCode(-1);
				mResult.setMessage(mJSONObject.getString("msg"));
			}
			
			return mResult;
		}
}
