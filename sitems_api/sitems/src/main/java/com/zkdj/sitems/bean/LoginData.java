package com.zkdj.sitems.bean;

// 登录时要返回的数据
public class LoginData {
	private User mUser;  // 用户信息
	private Role mRole;  // 角色信息
	
	public LoginData(){
		super();
	}
	
	// mUser
	public void setUser(User mUser){
		this.mUser = mUser;
	}
	public User getUser(){
		return mUser;
	}
	
	// mRole
	public void setRole(Role mRole){
		this.mRole = mRole;
	}
	public Role getRole(){
		return mRole;
	}

}
