package com.zkdj.sitems.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;

import com.zkdj.sitems.bean.AlertPage;
import com.zkdj.sitems.bean.AlertWeb;
import com.zkdj.sitems.bean.WebObject;
import com.zkdj.sitems.model.DIllegalWeb;

public class ExcelUtils {
	 
    /**
     * 使用浏览器选择路径下载
     * @param response
     * @param fileName
     * @param data
     * @throws Exception
     */
    public void exportExcel(HttpServletResponse response, String fileName, List<WebObject> data) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xls", "utf-8"));
        exportExcel(response.getOutputStream(), data);
    }
 
    public int generateExcel(List<WebObject> data,String path) throws Exception { 	
        File f = new File(path);
        FileOutputStream out = new FileOutputStream(f);
        return exportExcel(out, data);
    }
 
    private int exportExcel(OutputStream out, List<WebObject> data) throws Exception {
//        XSSFWorkbook wb = new XSSFWorkbook();
        SXSSFWorkbook wb = new SXSSFWorkbook();
        int rowIndex = 0;
        try {
            String sheetName = "网站列表";
            SXSSFSheet sheet = wb.createSheet(sheetName);
            rowIndex = writeExcel(wb, sheet, data);
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            out.close();
        }
        return rowIndex;
    }
    
    /**
     * 表不显示字段
     * @param wb
     * @param sheet
     * @param data
     * @return
     */
//    private static int writeExcel(XSSFWorkbook wb, Sheet sheet, ExcelData data) {
//        int rowIndex = 0;
//        writeTitlesToExcel(wb, sheet, data.getTitles());
//        rowIndex = writeRowsToExcel(wb, sheet, data.getRows(), rowIndex);
//        autoSizeColumns(sheet, data.getTitles().size() + 1);
//        return rowIndex;
//    }
 
    /**
     * 表显示字段
     * @param wb
     * @param sheet
     * @param data
     * @return
     */
    private int writeExcel(SXSSFWorkbook wb, Sheet sheet, List<WebObject> data) {
        int rowIndex = 0;
        String[] titles = {"ID","省份","城市","DNS","域名","网站状态","主办单位","单位性质","网站备案许可证号",
        		"网站名称","联系邮箱","联系电话","注册商","联系人","网站级别","网站pr值",
        		"网站创建时间","网站过期时间","网站信息采集时间","域名服务器","网站信息分类"};
        
        rowIndex = writeTitlesToExcel(wb, sheet, titles);
        rowIndex = writeRowsToExcel(wb, sheet, data, rowIndex);
        autoSizeColumns(sheet, titles.length + 1);
        return rowIndex;
    }
    /**
     * 设置表头
     *
     * @param wb
     * @param sheet
     * @param titles
     * @return
     */
    private int writeTitlesToExcel(SXSSFWorkbook wb, Sheet sheet, String[] titles) {
        int rowIndex = 0;
        int colIndex = 0;
        Font titleFont = wb.createFont();
        //设置字体
        titleFont.setFontName("simsun");
        //设置粗体
        titleFont.setBoldweight(Short.MAX_VALUE);
        //设置字号
        titleFont.setFontHeightInPoints((short) 14);
        //设置颜色
        titleFont.setColor(IndexedColors.BLACK.index);
//        XSSFCellStyle titleStyle = wb.createCellStyle();
//        //水平居中
//        titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
//        //垂直居中
//        titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
//        //设置图案颜色
//        titleStyle.setFillForegroundColor(new XSSFColor(new Color(182, 184, 192)));
//        //设置图案样式
//        titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
//        titleStyle.setFont(titleFont);
//        setBorder(titleStyle, BorderStyle.THIN, new XSSFColor(new Color(0, 0, 0)));
        Row titleRow = sheet.createRow(rowIndex);
        titleRow.setHeightInPoints(25);
        colIndex = 0;
        for (String field : titles) {
            Cell cell = titleRow.createCell(colIndex);
            cell.setCellValue(field);
//            cell.setCellStyle(titleStyle);
            colIndex++;
        }
        rowIndex++;
        return rowIndex;
    }
 
    /**
     * 设置内容
     *
     * @param wb
     * @param sheet
     * @param rows
     * @param rowIndex
     * @return
     */
    private int writeRowsToExcel(SXSSFWorkbook wb, Sheet sheet, List<WebObject> weblist, int rowIndex) {
    	Font dataFont = wb.createFont();
        dataFont.setFontName("simsun");
        dataFont.setFontHeightInPoints((short) 14);
        dataFont.setColor(IndexedColors.BLACK.index);
 
//        XSSFCellStyle dataStyle = wb.createCellStyle();
//        dataStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
//        dataStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
//        dataStyle.setFont(dataFont);
//        setBorder(dataStyle, BorderStyle.THIN, new XSSFColor(new Color(0, 0, 0)));
        for (WebObject mWebObject : weblist) {
            Row dataRow = sheet.createRow(rowIndex);
            dataRow.setHeightInPoints(25);
            if(mWebObject != null) {
            	dataRow.createCell(0).setCellValue(mWebObject.getId());
            	dataRow.createCell(1).setCellValue(mWebObject.getProvince());
            	dataRow.createCell(2).setCellValue(mWebObject.getCity());
            	dataRow.createCell(3).setCellValue(mWebObject.getDns());
            	dataRow.createCell(4).setCellValue(mWebObject.getDomainName());
            	dataRow.createCell(5).setCellValue(mWebObject.getDomainNameStatus());
            	dataRow.createCell(6).setCellValue(mWebObject.getOrganizer());
            	String organizerNature = getOrganizerNature(mWebObject.getOrganizerNature());
            	dataRow.createCell(7).setCellValue(organizerNature);
            	dataRow.createCell(8).setCellValue(mWebObject.getIcpNo());           	
            	dataRow.createCell(9).setCellValue(mWebObject.getSiteName());
            	dataRow.createCell(10).setCellValue(mWebObject.getEmail());
            	dataRow.createCell(11).setCellValue(mWebObject.getPhone());
            	dataRow.createCell(12).setCellValue(mWebObject.getRegister());
            	dataRow.createCell(13).setCellValue(mWebObject.getContact());
            	dataRow.createCell(14).setCellValue(mWebObject.getLevel());
            	dataRow.createCell(15).setCellValue(mWebObject.getPrScore());
            	dataRow.createCell(16).setCellValue(mWebObject.getRegisterTime());
            	dataRow.createCell(17).setCellValue(mWebObject.getEndTime());
            	dataRow.createCell(18).setCellValue(mWebObject.getGatherTime());
            	dataRow.createCell(19).setCellValue(mWebObject.getDomainNameServer());
            	dataRow.createCell(20).setCellValue("");
            
            }                   
            rowIndex++;
        }
        return rowIndex;
    }
    
    private String getOrganizerNature(String str) {
    	String organizerNature = "";
    	if(str.equals("2000")) {
    		organizerNature = "个人";
    	}else if(str.equals("2001")) {
    		organizerNature = "企业";
    	}else if(str.equals("2002")) {
    		organizerNature = "事业单位";
    	}else if(str.equals("2003")) {
    		organizerNature = "社会团体";
    	}else if(str.equals("2004")) {
    		organizerNature = "政府机关";
    	}else if(str.equals("2999")) {
    		organizerNature = "其他";
    	}else {
    		organizerNature = str;
    	}
    	
    	return organizerNature;
    }
 
    /**
     * 自动调整列宽
     *
     * @param sheet
     * @param columnNumber
     */
    private static void autoSizeColumns(Sheet sheet, int columnNumber) {
        for (int i = 0; i < columnNumber; i++) {
            int orgWidth = sheet.getColumnWidth(i);
            sheet.autoSizeColumn(i, true);
            int newWidth = (int) (sheet.getColumnWidth(i) + 100);
            if (newWidth > orgWidth) {
                sheet.setColumnWidth(i, newWidth);
            } else {
                sheet.setColumnWidth(i, orgWidth);
            }
        }
    }
 
    /**
     * 设置边框
     *
     * @param style
     * @param border
     * @param color
     */
    private static void setBorder(XSSFCellStyle style, BorderStyle border, XSSFColor color) {
        style.setBorderTop(border);
        style.setBorderLeft(border);
        style.setBorderRight(border);
        style.setBorderBottom(border);
        style.setBorderColor(BorderSide.TOP, color);
        style.setBorderColor(BorderSide.LEFT, color);
        style.setBorderColor(BorderSide.RIGHT, color);
        style.setBorderColor(BorderSide.BOTTOM, color);
    }
    
    // 生成预警网站Excel表格
    public int generateAlertWebExcel(List<AlertWeb> data,String path) throws Exception { 	
        File f = new File(path);
        FileOutputStream out = new FileOutputStream(f);
        SXSSFWorkbook wb = new SXSSFWorkbook();
        int rowIndex = 0;
        try {
            String sheetName = "巡查预警网站列表";
            SXSSFSheet sheet = wb.createSheet(sheetName);
            rowIndex = writeRowsToAlertWebExcel(wb, sheet, data, rowIndex);
            int num = 14; // AlertWeb中元素的个数，即excel每行中要加入的元素个数
            autoSizeColumns(sheet, num + 1);
            
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            out.close();
        }
        return rowIndex;
    }
    
    private int writeRowsToAlertWebExcel(SXSSFWorkbook wb, Sheet sheet, List<AlertWeb> weblist, int rowIndex) {
    	Font dataFont = wb.createFont();
        dataFont.setFontName("simsun");
        dataFont.setFontHeightInPoints((short) 14);
        dataFont.setColor(IndexedColors.BLACK.index);
 
        for (AlertWeb mWebObject : weblist) {
            Row dataRow = sheet.createRow(rowIndex);
            dataRow.setHeightInPoints(25);
            if(mWebObject != null) {
            	dataRow.createCell(0).setCellValue(mWebObject.getIllegalCount());
            	dataRow.createCell(1).setCellValue(mWebObject.getCity());
            	dataRow.createCell(2).setCellValue(mWebObject.getLevel());
            	String organizerNature = getOrganizerNature(mWebObject.getOrganizerNature());
            	dataRow.createCell(3).setCellValue(organizerNature);
            	dataRow.createCell(4).setCellValue(mWebObject.getSubject());
            	dataRow.createCell(5).setCellValue(mWebObject.getSiteName());
            	dataRow.createCell(6).setCellValue(mWebObject.getUpdateTime());
            	dataRow.createCell(7).setCellValue(mWebObject.getProvince());
            	dataRow.createCell(8).setCellValue(mWebObject.getPhone());
            	dataRow.createCell(9).setCellValue(mWebObject.getContact());
            	dataRow.createCell(10).setCellValue(mWebObject.getDomainName());
            	dataRow.createCell(11).setCellValue(mWebObject.getId());
            	String strCategory = "";
            	if(mWebObject.getCategory() != null && mWebObject.getCategory().size() > 0) {
            		List<String> categoryids = mWebObject.getCategory();
            		for(String str:categoryids) {
            			strCategory = strCategory + str + ",";
            		}
            	}
            	dataRow.createCell(12).setCellValue(strCategory);
            	dataRow.createCell(13).setCellValue(mWebObject.getStatus());           
            }                   
            rowIndex++;
        }
        return rowIndex;
    }
    
    // 生成违规文章Excel表格
    public int generateAlertPageExcel(List<AlertPage> data,String path) throws Exception { 	
        File f = new File(path);
        FileOutputStream out = new FileOutputStream(f);
        SXSSFWorkbook wb = new SXSSFWorkbook();
        int rowIndex = 0;
        try {
            String sheetName = "巡查预警中的违规文章列表";
            SXSSFSheet sheet = wb.createSheet(sheetName);
            rowIndex = writeRowsToAlertPageExcel(wb, sheet, data, rowIndex);
            int num = 10; // AlertWeb中元素的个数，即excel每行中要加入的元素个数
            autoSizeColumns(sheet, num + 1);
            
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            out.close();
        }
        return rowIndex;
    }
    
    private int writeRowsToAlertPageExcel(SXSSFWorkbook wb, Sheet sheet, List<AlertPage> weblist, int rowIndex) {
    	Font dataFont = wb.createFont();
        dataFont.setFontName("simsun");
        dataFont.setFontHeightInPoints((short) 14);
        dataFont.setColor(IndexedColors.BLACK.index);
 
        for (AlertPage mWebObject : weblist) {
            Row dataRow = sheet.createRow(rowIndex);
            dataRow.setHeightInPoints(25);
            if(mWebObject != null) {           	
            	dataRow.createCell(0).setCellValue(mWebObject.getId());
            	dataRow.createCell(1).setCellValue(mWebObject.getUrl());
            	dataRow.createCell(2).setCellValue(mWebObject.getDomainName());
            	dataRow.createCell(3).setCellValue(mWebObject.getUrlMainHashCode());
            	dataRow.createCell(4).setCellValue(mWebObject.getCreateTime());
            	dataRow.createCell(5).setCellValue(mWebObject.getTitle());
            	dataRow.createCell(6).setCellValue(mWebObject.getCleanTitle());
            	String strHotWords = "";
            	if(mWebObject.getHotWords()!= null && mWebObject.getHotWords().size() > 0) {
            		List<String> mListHotWords = mWebObject.getHotWords();
            		for(String str:mListHotWords) {
            			strHotWords = strHotWords + str + ",";
            		}
            	}
            	dataRow.createCell(7).setCellValue(strHotWords);
            	String strWarning = "";
            	if(mWebObject.getWarning() != null && mWebObject.getWarning().size() > 0) {
            		List<Integer> mListWarnings = mWebObject.getWarning();
            		for(Integer i:mListWarnings) {
            			strWarning = strWarning + i + ",";
            		}
            	}
            	dataRow.createCell(8).setCellValue(strWarning);           	
            	dataRow.createCell(9).setCellValue(mWebObject.getIsWarning());         
            }                   
            rowIndex++;
        }
        return rowIndex;
    }
    
    // 违规协查网站列表：生成Excel表格
    public int generateIllegalWebExcel(List<DIllegalWeb> data,String path) throws Exception { 	
        File f = new File(path);
        FileOutputStream out = new FileOutputStream(f);
        SXSSFWorkbook wb = new SXSSFWorkbook();
        int rowIndex = 0;
        try {
            String sheetName = "违规协查中的网站列表";
            SXSSFSheet sheet = wb.createSheet(sheetName);
            rowIndex = writeRowsToIllegalWebExcel(wb, sheet, data, rowIndex);
            int num = 10; // 行中元素的个数，即excel每行中要加入的元素个数
            autoSizeColumns(sheet, num + 1);
            
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //此处需要关闭 wb 变量
            out.close();
        }
        return rowIndex;
    }
    
    private int writeRowsToIllegalWebExcel(SXSSFWorkbook wb, Sheet sheet, List<DIllegalWeb> weblist, int rowIndex) {
    	Font dataFont = wb.createFont();
        dataFont.setFontName("simsun");
        dataFont.setFontHeightInPoints((short) 14);
        dataFont.setColor(IndexedColors.BLACK.index);
 
        for (DIllegalWeb mWebObject : weblist) {
            Row dataRow = sheet.createRow(rowIndex);
            dataRow.setHeightInPoints(25);
            if(mWebObject != null) {           	
            	dataRow.createCell(0).setCellValue(mWebObject.getId());
            	dataRow.createCell(1).setCellValue(mWebObject.getDomainName());
            	dataRow.createCell(2).setCellValue(mWebObject.getSiteName());
            	dataRow.createCell(3).setCellValue(mWebObject.getCategoryTitle());
            	dataRow.createCell(4).setCellValue(mWebObject.getCity());
            	dataRow.createCell(5).setCellValue(mWebObject.getContact());
            	dataRow.createCell(6).setCellValue(mWebObject.getPhone());
            	dataRow.createCell(7).setCellValue(mWebObject.getLevel());
            	dataRow.createCell(8).setCellValue(mWebObject.getOrganizerNature());  
            	String times = TimeUtil.getFormatTime(mWebObject.getCreateTime());
            	dataRow.createCell(9).setCellValue(times);         
            }                   
            rowIndex++;           
        }
        return rowIndex;
    }
}
