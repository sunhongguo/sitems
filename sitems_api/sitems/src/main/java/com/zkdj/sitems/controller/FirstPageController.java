package com.zkdj.sitems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.AlertPageMessage;
import com.zkdj.sitems.bean.FirstPageIllegalMessageResult;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.FirstPageService;

/**
 * 首页控制器
 * 
 * @author DELL
 *
 */
@RestController
@RequestMapping("/firstpage")
public class FirstPageController {
	@Autowired
	private FirstPageService mFirstPageService;

	// 违规网站信息
	@RequestMapping("/illegal/page")
	public ResultObject<JSONObject> getIllegalPagesC(@RequestBody AlertPageMessage mParams) {
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getSubjectIds()== null || mParams.getSubjectIds().size() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("专题id为空！");
			return mResult;
		}

		String sResult = mFirstPageService.getAlertPageListBySubjectIds(mParams);
		JSONObject mJSONObject = JSONObject.parseObject(sResult);

		mResult.setObject(mJSONObject);
		mResult.setCode(0);
		mResult.setMessage("查询成功！");
		return mResult;
	}

	// 违规概况：显示今日待处理违规网站数量、今日已处理违规网站数量，并把今日违规网站按A B C级分类展示统计其数量
	@RequestMapping("/illegal/message")
	public ResultObject<FirstPageIllegalMessageResult> getIllegalMessageC() {
		ResultObject<FirstPageIllegalMessageResult> mResult = new ResultObject<FirstPageIllegalMessageResult>();
		FirstPageIllegalMessageResult mFirstPageIllegalMessageResult = mFirstPageService.getIllegalMessage();
		if (mFirstPageIllegalMessageResult != null) {
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mFirstPageIllegalMessageResult);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("查询失败！");
		}
		return mResult;
	}

	// 违规热词
	@RequestMapping("/illegal/hotword")
	public ResultObject<JSONObject> getIllegalHotWordC(@RequestBody AlertPageMessage mParams) {
		ResultObject<JSONObject> mResult = new ResultObject<JSONObject>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if(mParams.getStart_time() == null || mParams.getStart_time().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("缺少开始时间！");
			return mResult;
		}
		
		if(mParams.getEnd_time() == null || mParams.getEnd_time().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("缺少结束时间！");
			return mResult;
		}
		String sResult = mFirstPageService.getIllegalHotWord(mParams);
		JSONObject mJSONObject = JSONObject.parseObject(sResult);

		mResult.setObject(mJSONObject);
		mResult.setCode(0);
		mResult.setMessage("查询成功！");
		return mResult;
	}
}
