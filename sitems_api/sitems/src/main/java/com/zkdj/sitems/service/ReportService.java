package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.AlertPage;
import com.zkdj.sitems.bean.AlertPageResult;
import com.zkdj.sitems.bean.ReportListResult;
import com.zkdj.sitems.bean.ReportMessage;
import com.zkdj.sitems.bean.ReportResult;
import com.zkdj.sitems.http.HttpService;
import com.zkdj.sitems.mapper.IllegalMapper;
import com.zkdj.sitems.mapper.ReportMapper;
import com.zkdj.sitems.model.DAlertPage;
import com.zkdj.sitems.model.DEMail;
import com.zkdj.sitems.model.DIllegalWeb;
import com.zkdj.sitems.model.DReport;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.TimeUtil;
import com.zkdj.sitems.util.WordUtil;

/**
  *  属地简报的api处理类
 * @author DELL
 *
 */
@Service
public class ReportService {
	@Autowired
	public ReportMapper mReportMapper;
	@Autowired
	private IllegalMapper mIllegalMapper;
	@Autowired
	private HttpService mHttpService;
	
	// 简报：下载
	public ResultObject<String> downloadReport(int id) {
		System.out.println("-----downloadReport----");
		ResultObject<String> mResult = new ResultObject<>();
		ReportResult mReportResult = queryReportById(id);
		if(mReportResult == null) {
			mResult.setCode(-1);
			mResult.setMessage("查询简报内容错误！");
			return mResult;
		}
			
		/** 用于组装word页面需要的数据 */  
        Map<String, Object> dataMap = new HashMap<String, Object>();  
  
        /** 组装数据 */  
        dataMap.put("title", mReportResult.getReport_name());
        String start_time = TimeUtil.getFormatTime(mReportResult.getStart_time());
        dataMap.put("starttime", start_time); 
        String end_time = TimeUtil.getFormatTime(mReportResult.getEnd_time());
        dataMap.put("endtime", end_time); 
  
        dataMap.put("content", mReportResult.getReport_desc());  
  
        List<Map<String, Object>> listInfo = new ArrayList<Map<String, Object>>(); 
        
        List<DIllegalWeb> web_list_illegal = mReportResult.getWebs_illegal();
        List<DIllegalWeb> web_list_illegal_handled = mReportResult.getWebs_illegal_handled();
        if(web_list_illegal != null && web_list_illegal.size() > 0) { 
        	for(DIllegalWeb web:web_list_illegal) {
        		Map<String, Object> map = new HashMap<String, Object>();  
                map.put("web_name", web.getSiteName());  
                map.put("illegal_count", web.getIllegalCount());  
                map.put("category", web.getCategoryTitle());  
                map.put("city", web.getCity()); 
                map.put("organizer", web.getOrganizerNature()); 
                map.put("status", "未处理"); 
                listInfo.add(map); 
        	}
        }
        if(web_list_illegal_handled != null && web_list_illegal_handled.size() > 0) { 
        	for(DIllegalWeb web:web_list_illegal_handled) {
        		Map<String, Object> map = new HashMap<String, Object>();  
                map.put("web_name", web.getSiteName());  
                map.put("illegal_count", web.getIllegalCount());  
                map.put("category", web.getCategoryTitle());  
                map.put("city", web.getCity());
                map.put("organizer", web.getOrganizerNature()); 
                map.put("status", "已处理"); 
                listInfo.add(map); 
        	}
        }
        dataMap.put("listInfo", listInfo);  
        
        List<Map<String, Object>> listNews = new ArrayList<Map<String, Object>>();
        List<DAlertPage> page_new = mReportResult.getPage_new();
        if(page_new != null && page_new.size() > 0) {
        	for(DAlertPage page:page_new) {
        		Map<String, Object> map = new HashMap<String, Object>();  
                map.put("domain", page.getDomainName());  
                map.put("title", page.getTitle());  
                map.put("url", page.getUrl());  
                String stime = TimeUtil.getFormatTime(page.getCreateTime());
                map.put("createtime", stime); 
                listNews.add(map); 
        	}
        }
        dataMap.put("listNews", listNews);  
        
        List<Map<String, Object>> listImportant = new ArrayList<Map<String, Object>>();
        List<DAlertPage> page_important = mReportResult.getPage_important();
        if(page_important != null && page_important.size() > 0) {
        	for(DAlertPage page:page_important) {
        		Map<String, Object> map = new HashMap<String, Object>();  
                map.put("domain", page.getDomainName());  
                map.put("title", page.getTitle());  
                map.put("url", page.getUrl()); 
                String stime = TimeUtil.getFormatTime(page.getCreateTime());
                map.put("createtime", stime); 
                listImportant.add(map); 
        	}
        }
        dataMap.put("listImportant", listImportant);  
  
        //文件唯一名称  
        String ss = TimeUtil.getFormatTime3(TimeUtil.getNewTime());
//        Long ss = TimeUtil.getNewTime();
        String fileOnlyName = "属地简报_" + ss + ".doc";  
  
        /** 生成word */  
        try {
			WordUtil.createWord(dataMap, "reprot.ftl", fileOnlyName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mResult.setCode(-1);
			mResult.setMessage("生成简报失败");
			mResult.setErrmsg(e.toString());
			return mResult;
		} 
		
        mResult.setCode(0);
		mResult.setMessage("下载简报成功");
		String url = Config.url_word + fileOnlyName;
		mResult.setObject(url);
		return mResult;
	}
		
	// 简报：创建
	public ResultObject<String> createReport(long starttime, long endtime) {
		System.out.println("-----createReport----");
		ResultObject<String> mResult = new ResultObject<>();
		
		DReport mParams = new DReport();
		// 查询违规网站
		List<DIllegalWeb> mlist_Illegal = mIllegalMapper.getIllegalWebListByTime(starttime,endtime);
		List<DIllegalWeb> mlist_Handled = mIllegalMapper.getHandledIllegalWebListByTime(starttime,endtime);
		int count_illegal = 0, count_illegal_handled = 0,count_illegal_page = 0;
		if(mlist_Illegal != null && mlist_Illegal.size() > 0) {
			String webs = JSONObject.toJSONString(mlist_Illegal);
			mParams.setWebs_illegal(webs);
			for(DIllegalWeb mWeb:mlist_Illegal) {
				count_illegal_page = count_illegal_page + mWeb.getIllegalCount();
			}
			count_illegal = mlist_Illegal.size();
		}
		if(mlist_Handled != null && mlist_Handled.size() > 0) {
			String webs = JSONObject.toJSONString(mlist_Handled);
			mParams.setWebs_illegal_handled(webs);
			for(DIllegalWeb mWeb:mlist_Handled) {
				count_illegal_page = count_illegal_page + mWeb.getIllegalCount();
			}
			count_illegal_handled = mlist_Handled.size();
		}
		
		// 报告概述
		String format_start = TimeUtil.getFormatTime(starttime);
		String format_end = TimeUtil.getFormatTime(endtime);
		int allcount = count_illegal + count_illegal_handled;
		String desc = format_start + "--" + format_end +"期间，系统共监测到" + allcount + "个网站存在违规信息共" + count_illegal_page + "条,其中已处理"+
				count_illegal_handled + "个网站, 还有"+ count_illegal + "个网站待处理；详细报告如下：";
		mParams.setReport_desc(desc);
		mParams.setStart_time(starttime);
		mParams.setEnd_time(endtime);
		
		// 简报名称
		String report_name = "湖北省网信办简报(" + format_start + ")";
		mParams.setReport_name(report_name);
				
		// 最新违规信息
		List<DAlertPage> mListNewsPage = getNewsPages(starttime,endtime);
		if(mListNewsPage != null && mListNewsPage.size() > 0) {
			String page_news = JSONObject.toJSONString(mListNewsPage);
			mParams.setPage_new(page_news);
		}
		
		// 重点违规信息
		List<DAlertPage> mListPage = mReportMapper.getAlertPageList();
		if(mListPage != null && mListPage.size() > 0) {
			String page_important = JSONObject.toJSONString(mListPage);
			mParams.setPage_important(page_important);
		}
		
		System.out.println("---report---" + mParams);		
		int i = mReportMapper.insertReport(mParams);
		if(i > 0) {
			mResult.setCode(0);
			mResult.setMessage("添加成功！");
		}else {
			mResult.setCode(-1);
			mResult.setMessage("添加失败！");
		}
		return mResult;
	}

	// 最新违规信息
	public List<DAlertPage> getNewsPages(long starttime, long endtime) {
		int page_no = 1;
		int page_size = 20;
		
		String path = "/restserver/page/query/fullQuery?page_no=" + page_no + "&page_size=" + page_size + "&";
		JSONObject json = new JSONObject();
		
		String start_time = TimeUtil.getFormatTime4(starttime);
		if (start_time != "") {
			json.put("start_time", start_time);
		}

		String end_time = TimeUtil.getFormatTime4(endtime);
		if (end_time != "") {
			json.put("end_time", end_time);
		}

		json.put("sortField", "createTime");
		json.put("sortType", "desc");
		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		if (result != "" && result.length() > 0) {
			AlertPageResult mAlertPageResult = JSONObject.parseObject(result, AlertPageResult.class);
			if (mAlertPageResult != null && mAlertPageResult.getCode().equals("0")) {
				List<DAlertPage> mListNewsPage = new ArrayList<>();
				if (mAlertPageResult.getResult() != null && mAlertPageResult.getResult().size() > 0) {
					List<AlertPage> mlist= mAlertPageResult.getResult();
					for(AlertPage mAlertPage:mlist) {
						DAlertPage mDAlertPage = new DAlertPage();
						mDAlertPage.setUrl(mAlertPage.getUrl());
						mDAlertPage.setDomainName(mAlertPage.getDomainName());
						mDAlertPage.setTitle(mAlertPage.getTitle());
						mDAlertPage.setCleanTitle(mAlertPage.getCleanTitle());
						
						String create_time = mAlertPage.getCreateTime();
						long itime = TimeUtil.getUTCToTime(create_time);
						mDAlertPage.setCreateTime(itime);
						
						mListNewsPage.add(mDAlertPage);
					}
				}
				return mListNewsPage;
			}
		}
		return null;
	}
	
	// 简报：查询列表
	public ReportListResult queryReportList(ReportMessage mParams) {
		System.out.println("-----queryReportList----");
		ReportListResult mReportListResult = new ReportListResult();
		int ipage = 0, icount = 15; // 默认数据
		if (mParams.getPage() > 0) {
			ipage = mParams.getPage() - 1;
		}

		if (mParams.getCount() > 0) {
			icount = mParams.getCount();
		}
		int index = ipage * icount;
		List<DReport> mlist = mReportMapper.getReportList(index,icount);
		if(mlist != null) {
			int count = mReportMapper.getReportCount();
			mReportListResult.setMlist(mlist);
			mReportListResult.setCount(count);
		}
		return mReportListResult;
	}

	// 简报：根据id查询
	public ReportResult queryReportById(int id) {
		ReportResult mReportResult = new ReportResult();
		DReport mDReport = mReportMapper.getReportByID(id);
		if(mDReport != null) {
			mReportResult.setId(mDReport.getId());
			mReportResult.setEnd_time(mDReport.getEnd_time());
			mReportResult.setStart_time(mDReport.getStart_time());
			mReportResult.setReport_name(mDReport.getReport_name());
			mReportResult.setReport_desc(mDReport.getReport_desc());
			
			String webs_illegal = mDReport.getWebs_illegal();
			if(webs_illegal != null && webs_illegal.length() > 0) {
				List<DIllegalWeb> weblist = JSON.parseArray(webs_illegal, DIllegalWeb.class);
				mReportResult.setWebs_illegal(weblist);
			}
			
			String webs_illegal_handled = mDReport.getWebs_illegal_handled();
			if(webs_illegal_handled != null && webs_illegal_handled.length() > 0) {
				List<DIllegalWeb> weblist = JSON.parseArray(webs_illegal_handled, DIllegalWeb.class);
				mReportResult.setWebs_illegal_handled(weblist);
			}
			
			String page_new = mDReport.getPage_new();
			if(page_new != null && page_new.length() > 0) {
				List<DAlertPage> page_new_list = JSON.parseArray(page_new, DAlertPage.class);
				mReportResult.setPage_new(page_new_list);
			}
			
			String page_important = mDReport.getPage_important();
			if(page_important != null && page_important.length() > 0) {
				List<DAlertPage> page_important_list = JSON.parseArray(page_important, DAlertPage.class);
				mReportResult.setPage_important(page_important_list);
			}
		}
		return mReportResult;
	}

	// 简报：根据id删除
	public ResultObject<String> deleteReportById(int id) {
		System.out.println("-----deleteReportById----" + id);
		ResultObject<String> mResult = new ResultObject<>();
		DReport mDReport = mReportMapper.getReportByID(id);
		if(mDReport != null) {
			int i = mReportMapper.deleteReportByID(id);
			if(i > 0) {
				mResult.setCode(0);
				mResult.setMessage("删除成功！");
			}else {
				mResult.setCode(-1);
				mResult.setMessage("删除失败！");
			}
		}else {
			mResult.setCode(-2);
			mResult.setMessage("删除的简报不存在！");
		}
		return mResult;
	}
		
	// 违规信息添加到简报
	public ResultObject<String> addPageToReport(List<DAlertPage> mList) {
		System.out.println("-----addPageToReport----");
		ResultObject<String> mResult = new ResultObject<>();
		int i = mReportMapper.insertReportPageList(mList);
		if(i > 0) {
			mResult.setCode(0);
			mResult.setMessage("添加到简报成功！");
		}else {
			mResult.setCode(-1);
			mResult.setMessage("添加到简报失败！");
		}
		return mResult;
	}
	
	// 邮件接受人:添加
	public ResultObject<String> addEMail(DEMail mParams) {		
		ResultObject<String> mResult = new ResultObject<>();
		int num = mReportMapper.getEmailNumberByUserId(mParams.getUserId());
		if(num > 9) {
			mResult.setCode(-4);
			mResult.setMessage("邮箱最多不能超过10个！");
			return mResult;
		}
		int i = mReportMapper.insertEMail(mParams);
		if(i > 0) {
			mResult.setCode(0);
			mResult.setMessage("添加成功！");
		}else {
			mResult.setCode(-1);
			mResult.setMessage("添加失败！");
		}
		return mResult;
	}

	// 邮件接受人：获取列表
	public List<DEMail> getEMailList(int userId) {
        List<DEMail> mlist = mReportMapper.getEmailList(userId);
		return mlist;
	}

	// 邮件接受人:删除
	public ResultObject<String> deleteEMailById(int id) {
		ResultObject<String> mResult = new ResultObject<>();
		DEMail mDEMail = mReportMapper.getEmailByID(id);
		if(mDEMail != null) {
			int i = mReportMapper.deleteEMailByID(id);
			if(i > 0) {
				mResult.setCode(0);
				mResult.setMessage("删除成功！");
			}else {
				mResult.setCode(-1);
				mResult.setMessage("删除失败！");
			}
		}else {
			mResult.setCode(-2);
			mResult.setMessage("删除的邮箱不存在！");
		}
		return mResult;
	}
}
