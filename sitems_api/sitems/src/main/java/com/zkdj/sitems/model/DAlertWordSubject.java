package com.zkdj.sitems.model;
/**
 * 预警关键词和专题
 * @author DELL
 *
 */
public class DAlertWordSubject {
	private int id;
	private String word;     //预警关键词
	private int subject_id;  // 关键词所对应专题ID
	
	// id
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	// word
	public void setWord(String word) {
		this.word = word;
	}
	public String getWord() {
		return word;
	}
	
	// subject_id
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}
	public int getSubject_id() {
		return subject_id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "id: " + id + ", word: " + word + ",subject_id: " + subject_id; 
	}
}
