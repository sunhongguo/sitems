package com.zkdj.sitems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkdj.sitems.bean.ReportListResult;
import com.zkdj.sitems.bean.ReportMessage;
import com.zkdj.sitems.bean.ReportResult;
import com.zkdj.sitems.model.DAlertPage;
import com.zkdj.sitems.model.DEMail;
import com.zkdj.sitems.result.ResultList;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.service.ReportService;

/**
 * 属地简报API的控制器
 * 
 * @author DELL
 *
 */
@RestController
@RequestMapping("/report")
public class ReportController {
	@Autowired
	public ReportService mReportService;
    @Autowired
    public PermissionsService mPermissionsService;
	// 简报：下载
	@RequestMapping("/download")
	public ResultObject<String> downloadReportC(int id) {
		System.out.println("-----downloadReport----");
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("属地简报","下载",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		mResult = mReportService.downloadReport(id);
		return mResult;
	}

	// 简报：创建
	@RequestMapping("/create")
	public ResultObject<String> createReport(@RequestBody ReportMessage mParams) {
		System.out.println("-----createReport----" + mParams);
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("属地简报","生成报告",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getStart_time() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("没有添加开始时间！");
			return mResult;
		}

		if (mParams.getEnd_time() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("没有添加结束时间！");
			return mResult;
		}
		mResult = mReportService.createReport(mParams.getStart_time(), mParams.getEnd_time());
		return mResult;
	}

	// 简报：查询列表
	@RequestMapping("/query_list")
	public ResultObject<ReportListResult> queryReportList(@RequestBody ReportMessage mParams) {
		System.out.println("-----createReport----" + mParams);
		ResultObject<ReportListResult> mResult = new ResultObject<>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		ReportListResult mReportListResult = mReportService.queryReportList(mParams);
		if (mReportListResult != null) {
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mReportListResult);
		} else {
			mResult.setCode(-2);
			mResult.setMessage("查询失败！");
		}
		return mResult;
	}

	// 简报：根据id查询
	@RequestMapping("/query_id")
	public ResultObject<ReportResult> queryReportById(int id) {
		System.out.println("-----queryReportById----" + id);
		ResultObject<ReportResult> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("属地简报","查看报告",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (id == 0) {
			mResult.setCode(-1);
			mResult.setMessage("id错误！");
			return mResult;
		}
		ReportResult mReportResult = mReportService.queryReportById(id);
		if (mReportResult != null) {
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mReportResult);
		} else {
			mResult.setCode(-2);
			mResult.setMessage("查询失败！");
		}

		return mResult;
	}

	// 简报：根据id删除
	@RequestMapping("/delete_id")
	public ResultObject<String> deleteReportById(int id) {
		System.out.println("-----deleteReportById----" + id);
		ResultObject<String> mResult = new ResultObject<>();
		if (id == 0) {
			mResult.setCode(-1);
			mResult.setMessage("id错误！");
			return mResult;
		}
		mResult = mReportService.deleteReportById(id);
		return mResult;
	}

	// 违规信息添加到简报
	@RequestMapping("/page/add")
	public ResultObject<String> addPageToReport(@RequestBody List<DAlertPage> mList) {
		System.out.println("-----addPageToReport----");
		ResultObject<String> mResult = new ResultObject<>();
		if (mList == null || mList.size() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("传递参数错误！");
			return mResult;
		}

		for (DAlertPage m : mList) {
			System.out.println("-----alert page----" + m);
		}
		mResult = mReportService.addPageToReport(mList);
		return mResult;
	}

	// 邮件接受人:添加
	@RequestMapping("/email/add")
	public ResultObject<String> addEMailC(@RequestBody DEMail mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("属地简报","接收人管理","添加");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getEmail() == null || mParams.getEmail().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("邮箱地址为空！");
			return mResult;
		}
		
		if (mParams.getUserId() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("邮箱创建者ID为空！");
			return mResult;
		}

		mResult = mReportService.addEMail(mParams);
		return mResult;
	}

	// 邮件接受人：获取列表
	@RequestMapping("/email/list")
	public ResultList<DEMail> getEMailListC(int userId) {
		ResultList<DEMail> mResult = new ResultList<>();
		if(userId == 0) {
			mResult.setCode(-2);
			mResult.setMessage("参数错误，用户ID为空！");
		}
		List<DEMail> mlist = mReportService.getEMailList(userId);
		if (mlist == null) {
			mResult.setCode(-1);
			mResult.setMessage("查询失败！");
		} else {
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mlist);
		}
		return mResult;
	}

	// 邮件接受人:删除
	@RequestMapping("/email/delete")
	public ResultObject<String> deleteEMailByIdC(int id) {
		ResultObject<String> mResult = new ResultObject<>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("属地简报","接收人管理","删除");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (id == 0) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		mResult = mReportService.deleteEMailById(id);
		return mResult;
	}
}
