package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.IllegalWebMessage;
import com.zkdj.sitems.http.HttpService;
import com.zkdj.sitems.bean.IllegalWeb;
import com.zkdj.sitems.bean.IllegalWebListResult;
import com.zkdj.sitems.mapper.IllegalMapper;
import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.mapper.WebManageMapper;
import com.zkdj.sitems.model.DIllegalRecord;
import com.zkdj.sitems.model.DIllegalWeb;
import com.zkdj.sitems.model.DRegion;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.result.ResultList;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.TimeUtil;

/**
 * 违规协查的api处理service类
 * 
 * @author DELL
 *
 */
@Service
public class IllegalService {
    @Autowired
	private IllegalMapper mIllegalMapper;
    @Autowired
    private UserMapper mUserMapper;    
    @Autowired
	private WebManageMapper mWebManageMapper;
    @Autowired
    private HttpService mHttpService;
    
    // 统计网站数量：根据城市；待处理和已处理之和
    public ResultList<JSONObject> getWebCountsByCity() {
    	ResultList<JSONObject> mResult = new ResultList<>();
    	
    	DRegion mDRegion = mWebManageMapper.getRegion("湖南省");
    	if(mDRegion == null) {
    		mResult.setCode(-1);
    		mResult.setMessage("统计地区信息失败！");
    		return mResult;
    	}
    	
    	int parent_id = mDRegion.getId();
		List<DRegion> mlist = mWebManageMapper.getRegionList(parent_id);
		if(mlist == null || mlist.size() == 0){
			mResult.setCode(-1);
    		mResult.setMessage("查询地区信息失败！");
    		return mResult;
		}
		
		List<JSONObject> mCountlist = new ArrayList<>();
		for(DRegion mDR:mlist) {
			String city_name = mDR.getName();
			int count_unhandle = mIllegalMapper.getIllegalCountByCity(city_name);
			int count_handled = mIllegalMapper.getHandledIllegalCountByCity(city_name);
			int count = count_unhandle + count_handled;
			
			JSONObject mJSONObject = new JSONObject();
			mJSONObject.put("city", city_name);
			mJSONObject.put("count", count);
			mCountlist.add(mJSONObject);
		}
		
		mResult.setCode(0);
		mResult.setMessage("统计地区网站数量成功！");
		mResult.setObject(mCountlist);
		
		return mResult;
    }

	// 添加待处理网站：设置协查方案后，就是加入到待处理网站列表中
	public ResultObject<DIllegalWeb> addNewIllegalWeb(DIllegalWeb mParams) {
		ResultObject<DIllegalWeb> mResult = new ResultObject<>();
		// 先删除预警列表中的该网站：调用API通知第三方服务器
        int idelete = deleteIllegalWebById(mParams.getWebId());
        if(idelete == 1) {
        	mParams.setCreateTime(TimeUtil.getNewTime()); // 创建时间：时间戳
    		int i = mIllegalMapper.insertIllegalWeb(mParams);
    		if (i > 0) {
    			mResult.setCode(0);
    			mResult.setMessage("设置协查方案成功！");
    			mResult.setObject(mParams);
    		} else {
    			mResult.setCode(-3);
    			mResult.setMessage("设置协查方案失败！");
    		}
        }else {
        	mResult.setCode(-4);
			mResult.setMessage("删除预警列表中该网站失败！");
        }
		return mResult;
	}

	// 删除违规网站：根据id
	public int deleteIllegalWebById(String id) {
		String path = "/restserver/illegal/modify/delete?";
		JSONObject json = new JSONObject();
		json.put("id", id);
		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		if (result != "" && result.length() > 0) {
			JSONObject mJSONObject = JSONObject.parseObject(result);
			if(mJSONObject != null && mJSONObject.getString("code").equals("0")) {
				return 1;
			}
		}
		return -1;
	}
	
	// 添加协查反馈
	public ResultObject<String> addIllegalSchemeFeedBack(DIllegalWeb mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		
		// 查询待处理的表
		DIllegalWeb mDIllegalWeb = mIllegalMapper.getIllegalWebById(mParams.getId());
		if(mDIllegalWeb == null || mDIllegalWeb.getId() == 0) {
			mResult.setCode(-3);
			mResult.setMessage("没有查询到ID的数据！");
			return mResult;
		}
		if(mDIllegalWeb.getDomainName() == null || mDIllegalWeb.getDomainName().length() == 0) {
			mResult.setCode(-3);
			mResult.setMessage("网站的域名为空！");
			return mResult;
		}
		// 删除待处理中的数据
		int idelete = mIllegalMapper.deleteIllegalWebByID(mParams.getId());
		if(idelete <= 0) {
			mResult.setCode(-3);
			mResult.setMessage("清除ID的数据失败！");
			return mResult;
		}

		// 将数据添加到已处理、处理记录的表中
		DIllegalRecord mRecord = new DIllegalRecord();
		mRecord.setCount(mDIllegalWeb.getIllegalCount());
		mRecord.setCreateTime(TimeUtil.getNewTime()); // 创建时间：时间戳
		mRecord.setDomainName(mDIllegalWeb.getDomainName());
		mRecord.setScheme(mDIllegalWeb.getScheme());
		
		mRecord.setFeedBack(mParams.getFeedBack());
		mRecord.setUserId(mParams.getUserId());
		
		// 检查已处理表中是否有这个网站的数据，有就改变时间
		List<DIllegalWeb> mHandleList = mIllegalMapper.getHandledIllegalWebByDomainName(mDIllegalWeb.getDomainName());
		if(mHandleList != null && mHandleList.size() > 0) {
			mIllegalMapper.deleteHandledIllegalWebByDomainName(mDIllegalWeb.getDomainName());
		}
		mIllegalMapper.insertIllegalHandleRecord(mRecord);
		mDIllegalWeb.setCreateTime(TimeUtil.getNewTime());
		int i = mIllegalMapper.insertHandledIllegalWeb(mDIllegalWeb);
		if (i > 0) {
			mResult.setCode(0);
			mResult.setMessage("设置协查反馈成功！");
		} else {
			mResult.setCode(-3);
			mResult.setMessage("设置协查反馈失败！");
		}

		return mResult;
	}

	// 根据tag标志内容，获取相应的违规网站列表
	public IllegalWebListResult getIllegalWebList(IllegalWebMessage mParams) {
		System.out.println("---------getIllegalWebList-------------" + mParams);
		IllegalWebListResult mIllegalWebResult = new IllegalWebListResult();
		if(mParams.getType().equals("handled")) {
			// 已处理
			mIllegalWebResult  = getHandledIllegalWebList(mParams);
		}else if(mParams.getType().equals("unhandle")){
			// 待处理
			mIllegalWebResult = getUnHandleIllegalWebList(mParams);
		}
		return mIllegalWebResult;
	}
	
	// 待处理的网站列表
	public IllegalWebListResult getUnHandleIllegalWebList(IllegalWebMessage mParams) {
		int ipage = 0, icount = 15; // 默认数据
		if (mParams.getPage() > 0) {
			ipage = mParams.getPage() - 1;
		}

		if (mParams.getCount() > 0) {
			icount = mParams.getCount();
		}
		int index = ipage * icount;
		String like = "";
		if(mParams.getSearchValue() != null && mParams.getSearchValue().length() > 0) {
			like = '%' + mParams.getSearchValue() + "%";  // 搜索条件
		}	
		List<DIllegalWeb> mList = mIllegalMapper.getIllegalWebList(like,mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity(),index, icount);
		int count_unhandle = mIllegalMapper.getIllegalNumber(like,mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity());
		int count_handled = mIllegalMapper.getHandledIllegalNumber(like,mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity());
		List<IllegalWeb> mIllegalWebList = new ArrayList<>();
		if(mList != null && mList.size() > 0) {
			for(DIllegalWeb m : mList) {
				IllegalWeb mIllegalWeb = new IllegalWeb();
				mIllegalWeb.setId(m.getId());
				mIllegalWeb.setCategoryTitle(m.getCategoryTitle());
				mIllegalWeb.setCity(m.getCity());
				mIllegalWeb.setContact(m.getContact());
				
				String domainName = m.getDomainName();
				mIllegalWeb.setDomainName(domainName);
				List<DIllegalWeb> mHandleList = mIllegalMapper.getHandledIllegalWebByDomainName(domainName);
				if(mHandleList != null && mHandleList.size() > 0) {
					mIllegalWeb.setHandleTime(mHandleList.get(0).getCreateTime());
				}
				mIllegalWeb.setIllegalCount(m.getIllegalCount());
				mIllegalWeb.setLevel(m.getLevel());
				mIllegalWeb.setOrganizerNature(m.getOrganizerNature());
				mIllegalWeb.setPhone(m.getPhone());
				mIllegalWeb.setScheme(m.getScheme());
				mIllegalWeb.setSiteName(m.getSiteName());
				
				mIllegalWebList.add(mIllegalWeb);
			}		
		}
		
		IllegalWebListResult mIllegalWebResult = new IllegalWebListResult();
		mIllegalWebResult.setmList(mIllegalWebList);
		mIllegalWebResult.setCount_unhandle(count_unhandle);
		mIllegalWebResult.setCount_handled(count_handled);
		return mIllegalWebResult;
	}

	// 已处理的网站列表
	public IllegalWebListResult getHandledIllegalWebList(IllegalWebMessage mParams) {
		int ipage = 0, icount = 15; // 默认数据
		if (mParams.getPage() > 0) {
			ipage = mParams.getPage() - 1;
		}

		if (mParams.getCount() > 0) {
			icount = mParams.getCount();
		}
		int index = ipage * icount;
		String like = "";
		if(mParams.getSearchValue() != null && mParams.getSearchValue().length() > 0) {
			like = '%' + mParams.getSearchValue() + "%";  // 搜索条件
		}	
		List<DIllegalWeb> mList = mIllegalMapper.getHandledIllegalWebList(like,mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity(), index, icount);
		int count_unhandle = mIllegalMapper.getIllegalNumber(like,mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity());
		int count_handled = mIllegalMapper.getHandledIllegalNumber(like,mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity());
		List<IllegalWeb> mIllegalWebList = new ArrayList<>();
		if(mList != null && mList.size() > 0) {
			for(DIllegalWeb m : mList) {
				IllegalWeb mIllegalWeb = new IllegalWeb();
				mIllegalWeb.setId(m.getId());
				mIllegalWeb.setCategoryTitle(m.getCategoryTitle());
				mIllegalWeb.setCity(m.getCity());
				mIllegalWeb.setContact(m.getContact());
				mIllegalWeb.setDomainName(m.getDomainName());
				mIllegalWeb.setHandleTime(m.getCreateTime());				
				mIllegalWeb.setLevel(m.getLevel());
				mIllegalWeb.setOrganizerNature(m.getOrganizerNature());
				mIllegalWeb.setPhone(m.getPhone());
				mIllegalWeb.setScheme(m.getScheme());
				mIllegalWeb.setSiteName(m.getSiteName());
				
				int illegalCount = 0;
				List<DIllegalRecord> mRecordList = mIllegalMapper.getRecordList(m.getDomainName());
				if(mList != null && mList.size() > 0) {
					for(DIllegalRecord mRecord:mRecordList) {
						illegalCount = illegalCount + mRecord.getCount();
					}
				}
				mIllegalWeb.setIllegalCount(illegalCount);
				
				mIllegalWebList.add(mIllegalWeb);
			}		
		}
		
		IllegalWebListResult mIllegalWebResult = new IllegalWebListResult();
		mIllegalWebResult.setmList(mIllegalWebList);
		mIllegalWebResult.setCount_unhandle(count_unhandle);
		mIllegalWebResult.setCount_handled(count_handled);
		return mIllegalWebResult;
	}

	// 获取处理记录列表
	public List<DIllegalRecord> getRecordList(String domainName) {
		List<DIllegalRecord> mList = mIllegalMapper.getRecordList(domainName);
		if(mList != null && mList.size() > 0) {
			for(DIllegalRecord m:mList) {
				int user_id = m.getUserId();
				DUser mUser = mUserMapper.getUserByID(user_id);
				if(mUser != null) {
					m.setUserName(mUser.getUser_name());
				}
			}
		}
		return mList;
	}
	
	// 获取导出数据：根据条件
	public List<DIllegalWeb> exportWebListByQuerys(IllegalWebMessage mParams) {
		System.out.println("---------exportWebListByQuerys-------------" + mParams);
		List<DIllegalWeb> mlist = null;
		if(mParams.getType().equals("handled")) {
			// 已处理
			mlist = mIllegalMapper.getHandledIllegalWebListNoPage(mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity());
		}else if(mParams.getType().equals("unhandle")){
			// 待处理
			mlist = mIllegalMapper.getIllegalWebListNoPage(mParams.getOrganizerNature(),mParams.getLevel(),mParams.getCity());
		}
		return mlist;
	}
	
	// 获取导出数据：根据ids
	public List<DIllegalWeb> exportWebListByIDs(IllegalWebMessage mParams) {
		System.out.println("---------exportWebListByIDs-------------" + mParams);
		List<DIllegalWeb> mlist = new ArrayList<>();
		List<Integer> ids = mParams.getIds();
		if(mParams.getType().equals("handled")) {
			// 已处理
			for(int id : ids) {
				DIllegalWeb mDIllegalWeb = mIllegalMapper.getHandledIllegalWebById(id);
				if(mDIllegalWeb != null) {
					mlist.add(mDIllegalWeb);
				}				
			}
		}else if(mParams.getType().equals("unhandle")){
			// 待处理
			for(int id : ids) {
				DIllegalWeb mDIllegalWeb = mIllegalMapper.getIllegalWebById(id);
				if(mDIllegalWeb != null) {
					mlist.add(mDIllegalWeb);
				}
			}
		}
		return mlist;
	}
}
