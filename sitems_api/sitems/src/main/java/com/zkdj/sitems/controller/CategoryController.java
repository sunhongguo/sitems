package com.zkdj.sitems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkdj.sitems.bean.Category;
import com.zkdj.sitems.model.DCategory;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	private CategoryService mCategoryService;
	
	// 添加分类
	@RequestMapping("/add")
	public ResultObject<Category> addCategory(@RequestBody Category category){
		ResultObject<Category> mResult = new ResultObject<Category>();
		
		mResult =  mCategoryService.InsertCategory(category);
		return mResult;
	}
	
	// 重命名分类
	@RequestMapping("/updatename")
	public ResultObject<Category> updateCategoryName(@RequestBody Category category){
		ResultObject<Category> mResult = new ResultObject<Category>();
		
		mResult =  mCategoryService.UpdateCategoryName(category);
		return mResult;
	}
	
	// 查询所有分类
		@RequestMapping("/queryall")
		public ResultObject<List<DCategory>> queryAllCategory(){
			ResultObject<List<DCategory>> mResult = new ResultObject<List<DCategory>>();
			
			List<DCategory>  l=  mCategoryService.GetAllCategories();
			
			mResult.setCode(0);
			mResult.setMessage("查询成功");
			mResult.setObject(l);
			
			return mResult;
		}
		
		// 删除分类
		@RequestMapping("/delete")
	    public ResultObject<Object> DeleteCategoryByID(int category_id){
	    	
	    	ResultObject<Object> mResult = new ResultObject<Object>();

	    	mResult = mCategoryService.deleteCategoryByID(category_id);
	    	
	        return mResult;
	    }
		// 交换分类
		@RequestMapping("/move")
	    public ResultObject<Object> moveCategory(int id1,int id2){
	    	
	    	ResultObject<Object> mResult = new ResultObject<Object>();

	    	mResult = mCategoryService.changeCategory(id1, id2);
	    	
	        return mResult;
	    }
}
