package com.zkdj.sitems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.ServerParam;
import com.zkdj.sitems.mapper.SystemMapper;
import com.zkdj.sitems.result.ResultObject;

/**
 *   外部服务器访问的API
 * @author DELL
 *
 */
@RestController
@RequestMapping("/server")
public class ServerController {
	
	@Autowired
	private SystemMapper mSystemMapper;

	// 上传服务地址：用于保存预警关键词
	@RequestMapping("/alert_url")
	public ResultObject<String> getAlertAddress(@RequestBody ServerParam mParams) {
		SitemsApplication.logger.info("-------getAlertAddress---------" + mParams);
        ResultObject<String> mResult = new ResultObject<>();
		
		if(mParams == null){
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		String sAlertURL = mParams.getAnalysis();
		if(sAlertURL != null && sAlertURL.length() > 0) {
			mSystemMapper.updateSystemAlertURL(sAlertURL);
		}else {
			mResult.setCode(-2);
			mResult.setMessage("地址为空！");
			return mResult;
		}
		
		mResult.setCode(0);
		mResult.setMessage("上传地址成功！");
		
		return mResult;
	}
}
