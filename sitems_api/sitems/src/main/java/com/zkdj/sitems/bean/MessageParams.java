package com.zkdj.sitems.bean;

/**
 * 发送短信、邮件的参数
 * @author DELL
 *
 */
public class MessageParams {
	private String emails; //邮件接受人集合，多个时用逗号隔开
	private String title;  // 标题
	private String fromName;  // 邮件发送者
	
	private String phones;  // 手机号，多个时用逗号隔开
	private String content;  // 内容
	
	public String getEmails() {
		return emails;
	}
	public void setEmails(String emails) {
		this.emails = emails;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	public String getPhones() {
		return phones;
	}
	public void setPhones(String phones) {
		this.phones = phones;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "MessageParams [emails=" + emails + ", title=" + title + ", phones=" + phones + ", content=" + content
				+ "]";
	}	
	
}
