package com.zkdj.sitems.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;

@RestController
@RequestMapping("/file")
public class FileController {

	@GetMapping("/excel")
	public void getExcelFile(String excelname, HttpServletResponse response) throws Exception {
		SitemsApplication.logger.info("====获取文件===" + excelname + "\n");

		if (excelname == null) {
			ResultObject<Object> mResult = new ResultObject<>();
			mResult.setCode(-101);
			mResult.setMessage("文件名为空！");
			String jsonObjectStr = JSONObject.toJSONString(mResult);
			returnJson(response, jsonObjectStr);
			return;
		}

		String path = Config.path_excel + excelname;
		SitemsApplication.logger.info("====文件 path===" + path + "\n");
		FileInputStream inputStream = null;
		OutputStream stream = null;

		try {
			File file = new File(path);
			inputStream = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			inputStream.read(data);
			response.setHeader("content-Type", "application/vnd.ms-excel");
			// 下载文件的默认名称
			response.setHeader("Content-Disposition",
					"attachment;filename=" + URLEncoder.encode("网站信息列表.xlsx", "utf-8"));
			stream = response.getOutputStream();
			stream.write(data);
			stream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@GetMapping("/word")
	public void getWordFile(String wordname, HttpServletResponse response) throws Exception {
		SitemsApplication.logger.info("====获取文件===" + wordname + "\n");

		if (wordname == null) {
			ResultObject<Object> mResult = new ResultObject<>();
			mResult.setCode(-101);
			mResult.setMessage("文件名为空！");
			String jsonObjectStr = JSONObject.toJSONString(mResult);
			returnJson(response, jsonObjectStr);
			return;
		}

		String path = Config.path_excel + wordname;
		SitemsApplication.logger.info("====文件 path===" + path + "\n");
		File file = new File(path);
		if(file.exists()) {
			response.setContentType("application/msword");// 设置强制下载不打开            
			response.addHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode("属地简报.doc", "utf-8"));
			byte[] buffer = new byte[1024];
			FileInputStream fis = null;
			BufferedInputStream bis = null;
			try {
				fis = new FileInputStream(file);
				bis = new BufferedInputStream(fis);
				OutputStream outputStream = response.getOutputStream();
				int i = bis.read(buffer);
				while (i != -1) {
				    outputStream.write(buffer, 0, i);
				    i = bis.read(buffer);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// TODO: handle finally clause
				if(bis != null) {
					bis.close();
				}
				if(fis != null) {
					fis.close();
				}
			}
		}
	}

	private void returnJson(HttpServletResponse response, String json) throws Exception {
		PrintWriter writer = null;
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Headers", "sitems_token,Content-Type");

		try {
			writer = response.getWriter();
			writer.print(json);

		} catch (IOException e) {
			SitemsApplication.logger.info("-----Login Interceptor-----response error--------");
		} finally {
			if (writer != null)
				writer.close();
		}
	}
}
