package com.zkdj.sitems.model;

import java.io.Serializable;

public class DPlatform implements Serializable{
	
	/**
	 * 平台名称
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String platform;
	private String alerturl;  // 预警地址url

	public DPlatform(){
		super();
	}

	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// Platform
	public void setplatform(String platform){
		this.platform = platform;
	}
	public String getplatform(){
		return platform;
	}

	// alerturl
	public void setAlerturl(String alerturl) {
		this.alerturl = alerturl;
	}
	public String getAlerturl() {
		return alerturl;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ID: " + this.id + ", platform: " + this.platform + ", alerturl: " + alerturl;
	}
}

