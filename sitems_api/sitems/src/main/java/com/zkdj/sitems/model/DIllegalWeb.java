package com.zkdj.sitems.model;

/**
 * 违规协查网站信息的类
 * @author DELL
 *
 */
public class DIllegalWeb {
    private int id;
    private String webId;
    private String domainName;
    private String organizerNature;
    private int illegalCount;
    private String level;
    private String city;
    private String province;
    private int subject;
    private String siteName;
    private String contact;
    private String phone;
    private int status;
    private String categoryTitle;  // 网站分类
    private long createTime;
    
    private String scheme;
    private String feedBack;
    private int userId;  // 添加协查反馈的用户id
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getWebId() {
		return webId;
	}
	public void setWebId(String webId) {
		this.webId = webId;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getOrganizerNature() {
		return organizerNature;
	}
	public void setOrganizerNature(String organizerNature) {
		this.organizerNature = organizerNature;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public String getScheme() {
		return scheme;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public String getFeedBack() {
		return feedBack;
	}
	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}
	public int getIllegalCount() {
		return illegalCount;
	}
	public void setIllegalCount(int illegalCount) {
		this.illegalCount = illegalCount;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public int getSubject() {
		return subject;
	}
	public void setSubject(int subject) {
		this.subject = subject;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCategoryTitle() {
		return categoryTitle;
	}
	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "DIllegalWeb [id=" + id + ", webId=" + webId + ", domainName=" + domainName + ", organizerNature="
				+ organizerNature + ", illegalCount=" + illegalCount + ", level=" + level + ", city=" + city
				+ ", province=" + province + ", subject=" + subject + ", siteName=" + siteName + ", contact=" + contact
				+ ", phone=" + phone + ", status=" + status + ", categoryTitle=" + categoryTitle + ", createTime="
				+ createTime + ", scheme=" + scheme + ", feedBack=" + feedBack + ", userId=" + userId + "]";
	}
}
