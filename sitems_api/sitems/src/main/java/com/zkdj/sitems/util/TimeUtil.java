package com.zkdj.sitems.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtil {
	
	// 返回当前的时间戳
   public static Long getNewTime(){
	   Long time = new Date().getTime();
	   return time;
   }
   
   // 获取剩余天数：毫秒
   public static int getAccountDay(Long time){
	   int day = 0;
	   if(time != null && time > 0){
		   Long lday = time - getNewTime();
		   if(lday > 0){
			  day = (int) (lday/86400000);
		   }
	   }
	   return day;
   }
   
   // 将UTC时间字符串转换为时间戳
	public static long getUTCToTime(String utc) {
		DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			Date date = utcFormat.parse(utc);
			long time = date.getTime();
			return time;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
   
   // 根据时间戳获取固定格式的时间：2018-15-06 12:31
   public static String getFormatTime(Long itime){
	   if(itime != null && itime > 0){
		   String time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(itime);
		   return time;
	   }else{
		   return "";
	   }	   	   
   }
   
   // 根据时间戳获取固定格式的时间：2018.15.06
   public static String getFormatTime2(Long itime){
	   if(itime != null && itime > 0){
		   String time = new SimpleDateFormat("yyyy.MM.dd").format(itime);
		   return time;
	   }else{
		   return "";
	   }	   	   
   }
   
   // 文件名称中的时间：2018-15-04-12:34:23
   public static String getFormatTime3(Long itime){
	   if(itime != null && itime > 0){
		   String time = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss").format(itime);
		   return time;
	   }else{
		   return "";
	   }	   	   
   }
   
   // 根据时间戳获取固定格式的时间：2018-15-06 12:32:00
   public static String getFormatTime4(Long itime){
	   if(itime != null && itime > 0){
		   String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(itime);
		   return time;
	   }else{
		   return "";
	   }	   	   
   }
}
