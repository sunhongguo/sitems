package com.zkdj.sitems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.User;
import com.zkdj.sitems.model.DCaptcha;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.result.LoginResult;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.LoginService;

@RestController
public class LoginController {
	@Autowired
	private LoginService mLoginService;
	
	// 登录
	@RequestMapping("/login")
	public LoginResult login(@RequestBody DUser user){
		SitemsApplication.logger.info("----login---account------" + user.getAccount());
		SitemsApplication.logger.info("----login---password------" + user.getPassword());
		LoginResult mResult = mLoginService.userLogin(user.getAccount(), user.getPassword());
		SitemsApplication.logger.info("----login result-----" + mResult);
		return mResult;
	}

	/**
	 * 登录：忘记密码，发送验证码captcha
	 * @param 帐号account和手机号contact
	 * @return
	 */
	@RequestMapping("/login/captcha/send")
	public ResultObject<String> sendVerificationCode(@RequestBody DCaptcha mDCaptcha){
		ResultObject<String> mResult = new ResultObject<>();
		SitemsApplication.logger.info("----sendVerificationCode---------" + mDCaptcha);
		if(mDCaptcha == null) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数有误！");
			return mResult;
		}
		
		if(mDCaptcha.getAccount() == null || mDCaptcha.getAccount().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("帐号为空！");
			return mResult;
		}
		
		if(mDCaptcha.getContact() == null || mDCaptcha.getContact().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("手机号为空！");
			return mResult;
		}
		mResult = mLoginService.sendCaptcha(mDCaptcha);
		return mResult;
	}
	
	/**
	 * 登录：验证获取的验证码是否正确，是否过期
	 * @param 帐号account和验证码code
	 * @return
	 */
	@RequestMapping("/login/captcha/verify")
	public ResultObject<String> verifyVerificationCode(@RequestBody DCaptcha mDCaptcha){
		ResultObject<String> mResult = new ResultObject<>();
		if(mDCaptcha == null) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数有误！");
			return mResult;
		}
		
		if(mDCaptcha.getAccount() == null || mDCaptcha.getAccount().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("帐号为空！");
			return mResult;
		}
		
		if(mDCaptcha.getCode() == null || mDCaptcha.getCode().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("验证码为空！");
			return mResult;
		}
		mResult = mLoginService.verifyCaptcha(mDCaptcha.getAccount(),mDCaptcha.getCode());
		return mResult;
	}

	/**
	 * 登录：设置新密码
	 * @param 帐号account和密码password
	 * @return
	 */
	@RequestMapping("/login/setpwd")
	public ResultObject<String> setNewPassWord(@RequestBody User mUser){
		ResultObject<String> mResult = new ResultObject<>();	
		if(mUser == null) {
			mResult.setCode(-1);
			mResult.setMessage("上传参数有误！");
			return mResult;
		}
		
		if(mUser.getAccount() == null || mUser.getAccount().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("帐号为空！");
			return mResult;
		}
		
		if(mUser.getPassword() == null || mUser.getPassword().isEmpty()){
			mResult.setCode(-1);
			mResult.setMessage("新密码为空！");
			return mResult;
		}
		mResult = mLoginService.setPassWord(mUser.getAccount(),mUser.getPassword());
		return mResult;
	}
}
