package com.zkdj.sitems.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.LoginData;
import com.zkdj.sitems.bean.MessageParams;
import com.zkdj.sitems.bean.Role;
import com.zkdj.sitems.bean.User;
import com.zkdj.sitems.mapper.LoginMapper;
import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.model.DCaptcha;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.redis.RedisUtil;
import com.zkdj.sitems.result.LoginResult;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.JwtUtil;
import com.zkdj.sitems.util.TimeUtil;

@Service
public class LoginService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private LoginMapper mLoginMapper;

	@Autowired
	private RoleService mRoleService;
	@Resource
	private RedisUtil redisUtil;
	@Autowired
	private MessageService mMessageService;

	// 用户登录
	public LoginResult userLogin(String account, String pwd) {
		SitemsApplication.logger.info("---userLogin------");
		LoginResult mResult = new LoginResult();

		if (account == null || account.isEmpty()) {
			mResult.setCode(-1);
			mResult.setMessage("帐号为空！");
			return mResult;
		}

		if (pwd == null || pwd.isEmpty()) {
			mResult.setCode(-2);
			mResult.setMessage("密码为空！");
			return mResult;
		}

		DUser user = userMapper.getUserByAccount(account);
		if (user == null) {
			mResult.setCode(-3);
			mResult.setMessage("帐号错误或不存在！");
			return mResult;
		}

		// 判断帐号是否过期
		if (user.getAccount_endts() != null && user.getAccount_endts() > 0) {
			Long newTime = TimeUtil.getNewTime();
			if (user.getAccount_endts() < newTime) {
				mResult.setCode(-6);
				mResult.setMessage("帐号已过期 ！");
				return mResult;
			}
		}

		if (user.getPassword().equals(pwd)) {
			int role_id = user.getRole_id();
			if (role_id > 0) {
				Role mRole = mRoleService.getRole(role_id);

				LoginData mData = new LoginData();

				User mUser = new User(); // 返回的用户数据
				String userId = user.getId() + "";
				String jwtToken = JwtUtil.createJWT(user.getAccount(), userId);
				redisUtil.hset(Config.REDIS_KEY_USER, userId, user); // 用户信息缓存到redis
				mUser.setToken(jwtToken);
				mUser.setId(user.getId());
				mUser.setAccount(user.getAccount());
				mUser.setUser_name(user.getUser_name());
				mUser.setContact(user.getContact());
				mUser.setRole_id(user.getRole_id());

				String time_begin = "";
				if (user.getAccount_begints() != null && user.getAccount_begints() > 0) {
					time_begin = TimeUtil.getFormatTime2(user.getAccount_begints());
				} else {
					time_begin = TimeUtil.getFormatTime2(user.getCreate_time());
				}
				mUser.setAccount_begints(time_begin);

				String time_end = "";
				if (user.getAccount_endts() != null && user.getAccount_endts() > 0) {
					time_end = TimeUtil.getFormatTime2(user.getAccount_endts());
				} else {
					time_end = TimeUtil.getFormatTime2(user.getCreate_time());
				}
				mUser.setAccount_endts(time_end);

				int day = TimeUtil.getAccountDay(user.getAccount_endts());
				mUser.setAccount_day(day);

				String time = TimeUtil.getFormatTime(user.getCreate_time());
				mUser.setCreate_time(time);

				mData.setUser(mUser);
				mData.setRole(mRole);

				mResult.setCode(0);
				mResult.setMessage("登陆成功");
				mResult.setObject(mData);
			} else {
				mResult.setCode(-5);
				mResult.setMessage("该帐号没有相关的角色！");
				return mResult;
			}
		} else {
			mResult.setCode(-4);
			mResult.setMessage("密码错误");
		}

		return mResult;
	}

	// 验证码：发送
	public ResultObject<String> sendCaptcha(DCaptcha mDCaptcha) {
		SitemsApplication.logger.info("----sendCaptcha---------" + mDCaptcha);
		ResultObject<String> mResult = new ResultObject<String>();
		DUser user = userMapper.getUserByAccount(mDCaptcha.getAccount());
		if (user == null) {
			mResult.setCode(-2);
			mResult.setMessage("帐号错误或不存在！");
			return mResult;
		}

		// 判断用户信息中是否有联系方式
		if (user.getContact() == null || user.getContact().isEmpty()) {
			mResult.setCode(-3);
			mResult.setMessage("该账号未填写联系方式！");
			return mResult;
		}
		// 判断电话号码是否匹配
		if (!user.getContact().equals(mDCaptcha.getContact())) {
			mResult.setCode(-3);
			mResult.setMessage("手机号与账号联系方式不匹配！");
			return mResult;
		}

		// 发送短信
		MessageParams mParams = new MessageParams();
		mParams.setPhones(mDCaptcha.getContact());
		int newNum = (int) ((Math.random() * 9 + 1) * 100000);
		String code = String.valueOf(newNum);
		String content = "属地化网站短信验证码:" + code + ";有效时间为30分钟！"; 
		mParams.setContent(content);
		mDCaptcha.setCode(code);
		mDCaptcha.setCreate_time(TimeUtil.getNewTime());
		String result = mMessageService.sendShortMessage(mParams);
		if (result == null || result.isEmpty()) {
			mResult.setCode(-4);
			mResult.setMessage("发送验证码失败！");
			return mResult;
		}

		JSONObject mJSONObject = JSONObject.parseObject(result);
		if (mJSONObject != null && mJSONObject.getJSONObject("meta").getIntValue("code") == 0) {
			mLoginMapper.deleteCaptchaByAccount(mDCaptcha.getAccount());
			int i = mLoginMapper.insertCaptcha(mDCaptcha);
			if (i > 0) {
				mResult.setCode(0);
				mResult.setMessage("发送短信成功！");
			} else {
				mResult.setCode(-5);
				mResult.setMessage("保存验证码失败！");
			}
			return mResult;
		} else {
			mResult.setCode(-4);
			mResult.setMessage("发送短信验证码失败！");
		}
		return mResult;
	}

	// 验证码:验证
	public ResultObject<String> verifyCaptcha(String account, String code) {
		ResultObject<String> mResult = new ResultObject<String>();
		List<DCaptcha> mDCaptchaList = mLoginMapper.getCaptchaByAccount(account);
		if (mDCaptchaList == null || mDCaptchaList.size() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("查询验证码失败！");
			return mResult;
		}

		DCaptcha mDCaptcha = mDCaptchaList.get(0);
		// 判断验证码是否存在
		if (mDCaptcha.getCode() == null || mDCaptcha.getCode().isEmpty()) {
			mResult.setCode(-3);
			mResult.setMessage("验证码不存在！");
			return mResult;
		}
		// 判断电话号码是否匹配
		if (code.equals(mDCaptcha.getCode())) {
			Long current_time = TimeUtil.getNewTime();
			Long create_time = mDCaptcha.getCreate_time();
			Long i = current_time - create_time;
			if(i > 30*60*1000) {
				// 超过30分钟，验证码过期
				mResult.setCode(-5);
				mResult.setMessage("超过30分钟，验证码过期！");
				return mResult;
			}else {
				mResult.setCode(0);
				mResult.setMessage("验证通过！");
				return mResult;
			}
		}else {
			mResult.setCode(-4);
			mResult.setMessage("验证码错误！");
			return mResult;
		}
	}
	
	// 设置新密码
	public ResultObject<String> setPassWord(String account,String newPassWord){
		ResultObject<String> mResult = new ResultObject<String>();
		DUser user = userMapper.getUserByAccount(account);
		if (user == null) {
			mResult.setCode(-2);
			mResult.setMessage("帐号错误或不存在！");
			return mResult;
		}
		
		int i = userMapper.updataUserPassWordByAccount(account,newPassWord);
		if(i>0) {
			mResult.setCode(0);
			mResult.setMessage("设置新密码成功！");
		}else {
			mResult.setCode(-3);
			mResult.setMessage("设置新密码失败！");
		}
		return mResult;
	}

}
