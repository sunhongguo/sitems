package com.zkdj.sitems.http;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.zkdj.sitems.SitemsApplication;

/**
 * 通过http访问外部API
 * 
 * @author DELL
 *
 */
@Service
public class HttpService {  
	/**
	 * post请求
	 * @param url 相对地址
	 * @param body 字符串，可以是json格式
	 */
	public String postData(String url, String body) {
		String path = handleURL(HttpConfig.address, url);
		SitemsApplication.logger.info("---postData---path------" + path);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// body是Http消息体,例如json串
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> mResponseEntity = restTemplate.exchange(path, HttpMethod.POST, entity, String.class);
		String mResponse = mResponseEntity.getBody();
		return mResponse;
	}

	/**
	 * 添加新的网站数据：网站id是根据url的MD5获取的
	 * @param path：完整的地址
	 * @param body：添加的数据
	 * @return
	 */
	public String postTotalURLData(String path, String body) {
		SitemsApplication.logger.info("---postData---path------" + path);

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// body是Http消息体,例如json串
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> mResponseEntity = restTemplate.exchange(path, HttpMethod.POST, entity, String.class);
		String mResponse = mResponseEntity.getBody();
		SitemsApplication.logger.info("---postData---get result success!------");
		return mResponse;
	}
	
	/**
	  *  获取完整的地址
	 * @param address：ip和port
	 * @param url：相对地址
	 * @return
	 */
	public String handleURL(String address, String url){
		long c = System.currentTimeMillis() / 1000;
		String token = TokenUtil.generateToken(c);
		String newURL ="http://" + address + url + "call_id=" + c + "&token=" + token + "&appid=**";
		return newURL;
	}
	
	/**
	  * 上传预警关键词的操作
	 * @param url：相对地址
	 * @param body：添加的数据
	 * @return
	 */
	public String postWarnWord(String url, String body) {	
		String path = handleURL(HttpConfig.address_warn,url);
		SitemsApplication.logger.info("---post Warn Word-----path----" + path);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		// body是Http消息体,例如json串
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);

		ResponseEntity<String> mResponseEntity = restTemplate.exchange(path, HttpMethod.POST, entity, String.class);
		String mResponse = mResponseEntity.getBody();
		
		return mResponse;
	}
	
	/*
	 * 发送消息：短信和邮件
	 */
	public String sendMessage(String url, MultiValueMap<String, String> map) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> headerMap = Token2Util.createdHeader();
		headers.addAll(headerMap);
		 
//		MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
//		map.add("id", "1");
		System.out.println("----header-----" + headers); 
		System.out.println("----params-----" + map);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
		 
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> mResponseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		String mResponse = mResponseEntity.getBody();
		System.out.println(mResponse);
		return mResponse;
	}
}
