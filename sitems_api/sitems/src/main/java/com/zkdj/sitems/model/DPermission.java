package com.zkdj.sitems.model;

import java.io.Serializable;

//db_permission权限
public class DPermission implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id; // id
	private int module_id;  // 权限所属的模块id
	private String permission_name; // 权限名称
	private Long create_time; // 创建时间
	private String permission_info; // 备注
	private int status;    // 状态：表示角色是否选择了该内容；1表示选中；0表示未选中
	
	public DPermission(){
		super();
	}
	
	public DPermission(int module_id,String name,Long time,String info){
		this.module_id = module_id;
		this.permission_name = name;
		this.create_time = time;
		this.permission_info = info;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// status
	public void setStatus(int status){
		this.status = status;
	}
	public int getStatus(){
		return status;
	}
		
	// module_id
	public void setModule_id(int module_id){
		this.module_id = module_id;
	}
	public int getModule_id(){
		return module_id;
	}
		
	// permission_name
	public void setPermission_name(String permission_name){
		this.permission_name = permission_name;
	}
	public String getPermission_name(){
		return permission_name;
	}
	
	// create_time
	public void setCreate_Time(Long create_time){
		this.create_time = create_time;
	}
	public Long getCreate_Time(){
		return create_time;
	}

	// permission_info
	public void setPermission_info(String permission_info){
		this.permission_info = permission_info;
	}
	public String getPermission_info(){
		return permission_info;
	}
	
	@Override
	public String toString(){
		return "permission_name: " + this.permission_name + " ,module_id" + this.module_id + ", create_time: " + this.create_time + ", permission_info: " + permission_info;
	}

}
