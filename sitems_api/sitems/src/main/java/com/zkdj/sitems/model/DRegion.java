package com.zkdj.sitems.model;

/**
 * 区域信息
 * @author DELL
 *
 */
public class DRegion {
	private int id;
	private String name;   // 区域名称
	private int parent_id; // 父级区域id
	
	public DRegion(){
		super();
	}
	
	public DRegion(int id, String name, int parent_id){
		this.id = id;
		this.name = name;
		this.parent_id = parent_id;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// name
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	
	// parent_id
	public void setParent_id(int parent_id){
		this.parent_id = parent_id;
	}
	public int getParent_id(){
		return parent_id;
	}

}
