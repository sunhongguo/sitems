package com.zkdj.sitems.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.zkdj.sitems.bean.MessageParams;
import com.zkdj.sitems.http.HttpConfig;
import com.zkdj.sitems.http.HttpService;

/**
 * 发送消息：短信、邮件
 * @author DELL
 *
 */
@Service
public class MessageService {
    @Autowired
	private HttpService mHttpService;
    
	// 发送短信
	public String sendShortMessage(MessageParams mParams) {		
		String url = "http://" + HttpConfig.address_message + "/additional/sendShortMessage";
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("phones", mParams.getPhones());
		map.add("content", mParams.getContent());
		String result = mHttpService.sendMessage(url, map);
		return result;
	}
	
    // 发送邮件
	public String sendEMail(MessageParams mParams) {
		String url = "http://" + HttpConfig.address_message +"/additional/sendEmail";
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("fromName", mParams.getFromName());
		map.add("emails", mParams.getEmails());
		map.add("title", mParams.getTitle());
		map.add("content", mParams.getContent());
		String result = mHttpService.sendMessage(url, map);
		return result;
	}
}
