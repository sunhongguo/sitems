package com.zkdj.sitems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.IllegalWebMessage;
import com.zkdj.sitems.bean.IllegalWebListResult;
import com.zkdj.sitems.model.DIllegalRecord;
import com.zkdj.sitems.model.DIllegalWeb;
import com.zkdj.sitems.result.ResultList;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.IllegalService;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.ExcelUtils;
import com.zkdj.sitems.util.TimeUtil;

/**
 * 违规协查模块的控制器
 * 
 * @author DELL
 *
 */
@RestController
@RequestMapping("/illegal")
public class IllegalWebController {
	
	@Autowired
	private IllegalService mIllegalService;
	@Autowired
	private PermissionsService mPermissionsService;
	/*
	  * 根据地区统计网站数量
	 */
	@RequestMapping("/count/city")
	public ResultList<JSONObject> getIllegalWebCountByCity() {
		ResultList<JSONObject> mResult = new ResultList<>();
		mResult = mIllegalService.getWebCountsByCity();
		return mResult;
	}

	/*
	 * 添加协查方案:输入协查方案，并和网站信息一起保存到本地，同时调用接口删除第三方服务器上的网站内容 调入入口：巡查预警和事件排查，添加后要删除列表中的数据
	 */
	@RequestMapping("/scheme/add")
	public ResultObject<DIllegalWeb> setIllegalScheme(@RequestBody DIllegalWeb mParams) {
		ResultObject<DIllegalWeb> mResult = new ResultObject<>();
		
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("巡查预警","加入协查",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getWebId() == null || mParams.getWebId().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("网站ID为空！");
			return mResult;
		}
		
		if (mParams.getDomainName() == null || mParams.getDomainName().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("网站域名为空！");
			return mResult;
		}
		
		if (mParams.getScheme() == null || mParams.getScheme().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("协查方案内容为空！");
			return mResult;
		}
		
		mResult = mIllegalService.addNewIllegalWeb(mParams);
		return mResult;

	}

	/*
	 * 添加协查反馈：根据协查方案输入反馈内容
	 * post参数：协查列表中的id;反馈的内容feedback,反馈的用户id
	 */
	@RequestMapping("/scheme/feedback")
	public ResultObject<String> setIllegalFeedBack(@RequestBody DIllegalWeb mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("违规协查","协查反馈",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if (mParams.getId() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("ID为空！");
			return mResult;
		}
		
		if (mParams.getFeedBack() == null || mParams.getFeedBack().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("反馈内容为空！");
			return mResult;
		}
		
		if (mParams.getUserId() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("添加反馈的用户ID为空！");
			return mResult;
		}
		
		mResult = mIllegalService.addIllegalSchemeFeedBack(mParams);
		return mResult;

	}

	/*
	 * 查询违规协查网站列表：根据Type标志位确认查询类型; 已处理“handled”;未处理“unhandle”
	 */
	@RequestMapping("/weblist")
	public ResultObject<IllegalWebListResult> getIllegalWebList(@RequestBody IllegalWebMessage mParams){
		ResultObject<IllegalWebListResult> mResult = new ResultObject<>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if (mParams.getType() == null || mParams.getType().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("请添加网站是否处理的类型！");
			return mResult;
		}
		IllegalWebListResult mIllegalWebResult = mIllegalService.getIllegalWebList(mParams);
        if(mIllegalWebResult != null) {
        	mResult.setCode(0);
        	mResult.setMessage("查询违规网站列表成功！");
        	mResult.setObject(mIllegalWebResult);
        }else {
        	mResult.setCode(-2);
        	mResult.setMessage("查询违规网站列表失败！");
        }	
		return mResult;
	}
	
	/*
	 * 查询待处理的网站列表：有多个待处理记录时，显示多条
	 */
	@RequestMapping("/weblist_unhandle")
	public ResultObject<IllegalWebListResult> getUnHandleWebs(@RequestBody IllegalWebMessage mParams) {
		ResultObject<IllegalWebListResult> mResult = new ResultObject<>();

		IllegalWebListResult mIllegalWebResult = mIllegalService.getUnHandleIllegalWebList(mParams);
        if(mIllegalWebResult != null) {
        	mResult.setCode(0);
        	mResult.setMessage("查询待记录列表成功！");
        	mResult.setObject(mIllegalWebResult);
        }else {
        	mResult.setCode(-2);
        	mResult.setMessage("查询待处理记录失败！");
        }
        return mResult;
	}

	/*
	 * 查询已处理的网站列表：有多个已处理记录时，只显示一条；可以通过处理记录查看处理的历史记录
	 */
	@RequestMapping("/weblist_handled")
	public ResultObject<IllegalWebListResult> getHandledWebs(@RequestBody IllegalWebMessage mParams) {
		ResultObject<IllegalWebListResult> mResult = new ResultObject<>();

		IllegalWebListResult mIllegalWebResult = mIllegalService.getHandledIllegalWebList(mParams);
        if(mIllegalWebResult != null) {
        	mResult.setCode(0);
        	mResult.setMessage("查询已处理记录列表成功！");
        	mResult.setObject(mIllegalWebResult);
        }else {
        	mResult.setCode(-2);
        	mResult.setMessage("查询已处理记录失败！");
        }
        return mResult;
	}

	/*
	 * 获取处理记录：协查方案和协查反馈；同一个网站可以被处理多次，有多个记录；根据域名判断
	 */
	@RequestMapping("/handle/records")
	public ResultList<DIllegalRecord> getHandleRecords(@RequestParam("domainName") String domainName) {
        ResultList<DIllegalRecord> mResult = new ResultList<>();
        ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("违规协查","处理记录",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
        if(domainName == null || domainName.length() == 0) {
        	mResult.setCode(-1);
        	mResult.setMessage("参数错误！");
        	return mResult;
        }
        List<DIllegalRecord> mList = mIllegalService.getRecordList(domainName);
        if(mList != null) {
        	mResult.setCode(0);
        	mResult.setMessage("查询处理记录列表成功！");
        	mResult.setObject(mList);
        }else {
        	mResult.setCode(-2);
        	mResult.setMessage("查询处理记录失败！");
        }
        return mResult;
	}
	
	// 导出数据：数据导出到excel表格; 根据条件导出
	@RequestMapping("/export/weblist_querys")
	public ResultObject<String> exportIllegalWebsByQuerys(@RequestBody IllegalWebMessage mParams){
		ResultObject<String> mResult = new ResultObject<>();

		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}

		if (mParams.getType() == null || mParams.getType().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("请添加网站是否处理的类型！");
			return mResult;
		}
		
		List<DIllegalWeb> sResult = mIllegalService.exportWebListByQuerys(mParams);
		if (sResult == null) {
			mResult.setCode(-2);
			mResult.setMessage("查询网站信息失败！");
			return mResult;
		}
		
		if (sResult.size() == 0) {
			mResult.setCode(0);
			mResult.setMessage("获取网站信息为0条！");
			return mResult;
		}
				
		try {
			ExcelUtils mExcelUtils = new ExcelUtils();
			String name = "Excel_IllegalWeb_" + TimeUtil.getNewTime() + ".xlsx";
			String path = Config.path_excel + name;
			int index = mExcelUtils.generateIllegalWebExcel(sResult, path);
			if (index > 0) {
				mResult.setCode(0);
				mResult.setMessage("生成Excel表格成功！");
				String url = Config.url_excel + name;
				mResult.setObject(url);
			} else {
				mResult.setCode(-3);
				mResult.setMessage("生成Excel表格失败！");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mResult.setCode(-3);
			mResult.setMessage("生成Excel表格失败！");
		}

		return mResult;
	}

	// 导出数据：数据导出到excel表格; 根据IDs导出
	@RequestMapping("/export/weblist_ids")
	public ResultObject<String> exportIllegalWebsByIDs(@RequestBody IllegalWebMessage mParams){
		ResultObject<String> mResult = new ResultObject<>();

		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if (mParams.getType() == null || mParams.getType().length() == 0) {
			mResult.setCode(-2);
			mResult.setMessage("请添加网站是否处理的类型！");
			return mResult;
		}

		if (mParams.getIds() == null || mParams.getIds().size() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("ids错误！");
			return mResult;
		}

		List<DIllegalWeb> sResult = mIllegalService.exportWebListByIDs(mParams);
		if (sResult == null) {
			mResult.setCode(-2);
			mResult.setMessage("查询网站信息失败！");
			return mResult;
		}
		
		if (sResult.size() == 0) {
			mResult.setCode(0);
			mResult.setMessage("获取网站信息为0条！");
			return mResult;
		}
				
		try {
			ExcelUtils mExcelUtils = new ExcelUtils();
			String name = "Excel_IllegalWeb_" + TimeUtil.getNewTime() + ".xlsx";
			String path = Config.path_excel + name;
			int index = mExcelUtils.generateIllegalWebExcel(sResult, path);
			if (index > 0) {
				mResult.setCode(0);
				mResult.setMessage("生成Excel表格成功！");
				String url = Config.url_excel + name;
				mResult.setObject(url);
			} else {
				mResult.setCode(-3);
				mResult.setMessage("生成Excel表格失败！");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mResult.setCode(-3);
			mResult.setMessage("生成Excel表格失败！");
		}

		return mResult;
	}
}
