package com.zkdj.sitems.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.Role;
import com.zkdj.sitems.mapper.UserMapper;
import com.zkdj.sitems.model.DPermissionModule;
import com.zkdj.sitems.model.DRole;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.redis.RedisUtil;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.util.Config;
import com.zkdj.sitems.util.TimeUtil;

@Service
public class RoleService {
    @Autowired
	private UserMapper mUserMapper;
    @Resource
    private RedisUtil redisUtil;
    
    // 创建新的角色:判断角色是否已经存在，名称是否唯一
	public ResultObject<DRole> createNewRole(DRole mRole) throws Exception{
		ResultObject<DRole> mResult = new ResultObject<DRole>();
		if(mRole.getPermissions() != null && mRole.getPermissions().size() > 0){
			List<DRole> roles = mUserMapper.getRoleByName(mRole.getRole_name());
			if(roles != null && roles.size() > 0){
				mResult.setCode(-4);
				mResult.setMessage("该名称的角色已经存在！");
				return mResult;
			}
			mRole.setCreate_time(TimeUtil.getNewTime());
			String mJsonData = JSON.toJSONString(mRole.getPermissions());
			int i = mUserMapper.insertRole(mRole.getRole_name(),mRole.getCreate_time(),mRole.getRole_info(),mJsonData);
			SitemsApplication.logger.info("----insertRole-----" + i);
			if(i == 1){
				mResult.setCode(0);
				mResult.setMessage("创建角色成功！");
			}else{
				mResult.setCode(-3);
				mResult.setMessage("添加新的角色数据错误！");
			}
		}else{
			mResult.setCode(-2);
			mResult.setMessage("角色没有分配权限数据！");
		}
		
		return mResult;	
	}
	
	// 修改角色信息
	public ResultObject<DRole> updateRole(DRole mRole) throws Exception{
		ResultObject<DRole> mResult = new ResultObject<DRole>();
		SitemsApplication.logger.info("--updateRole--------" + mRole.getId());
		if(mRole.getId() > 0){
			DRole role = mUserMapper.getRoleByID(mRole.getId());
			if(role == null){
				mResult.setCode(-6);
				mResult.setMessage("该角色不存在,无法修改！");
		        return mResult;	
			}
		}else{
			mResult.setCode(-5);
			mResult.setMessage("角色ID为空！");
	        return mResult;		
		}
		
		if(mRole.getPermissions() != null && mRole.getPermissions().size() > 0){
			List<DRole> roles = mUserMapper.getRoleByName(mRole.getRole_name());
			if(roles != null && roles.size() > 0){
				for(DRole oldRole:roles){
					if(oldRole.getId() != mRole.getId() && oldRole.getRole_name().equals(mRole.getRole_name())){
						mResult.setCode(-4);
						mResult.setMessage("该名称的角色已经存在！");
						return mResult;
					}
				}		
			}
			
			String mJsonData = JSON.toJSONString(mRole.getPermissions());
			int i = mUserMapper.updateRole(mRole.getId(),mRole.getRole_name(),mRole.getRole_info(),mJsonData);
			SitemsApplication.logger.info("----insertRole-----" + i);
			if(i == 1){
				redisUtil.hdel(Config.REIDS_KEY_ROLE, mRole.getId()+"");
				mResult.setCode(0);
				mResult.setMessage("修改角色成功！");
			}else{
				mResult.setCode(-3);
				mResult.setMessage("修改角色数据错误！");
			}
		}else{
			mResult.setCode(-2);
			mResult.setMessage("角色没有分配权限数据！");
		}
		
		return mResult;	
	}
	
	// 获取角色列表
	public List<Role> getRoleList() throws Exception{
		List<Role> mRoleList = new ArrayList<>();
		List<DRole> mList = mUserMapper.getRoleList();
		for(DRole role:mList){			
			Role mRole = handleRole(role);
			mRoleList.add(mRole);					
		}
		
		return mRoleList;
	}
	
	// 根据id查询
	public Role getRole(int role_id){
		Role mRole = null;
		if(role_id > 0){
			DRole role = mUserMapper.getRoleByID(role_id);
			mRole = handleRole(role);
		}
		return mRole;
	}
	
	// 筛选要返回的数据
	public Role handleRole(DRole role){
		Role mRole = new Role();
		mRole.setId(role.getId());
		mRole.setRole_name(role.getRole_name());
		mRole.setRole_info(role.getRole_info());
		
		String time = TimeUtil.getFormatTime(role.getCreate_time());
		mRole.setCreate_time(time);
		
		List<DPermissionModule> permissions = JSON.parseArray(role.getMsg(), DPermissionModule.class);
		mRole.setPermissions(permissions);
		
		return mRole;
	}

	public ResultObject<?> deleteRole(int role_id) {
		// TODO Auto-generated method stub
		ResultObject<?> mResult = new ResultObject<>();
		List<DUser> users = mUserMapper.getUserListByRoleID(role_id);
		System.out.println(users);
		if(users != null && users.size() > 0)
		{
			SitemsApplication.logger.info("----users != null,deleteRole-----");
			mResult.setCode(-1);
			mResult.setMessage("此角色已绑定用户，不能删除！");
		    return mResult;
		}
		mUserMapper.deleteRoleByID(role_id);
		SitemsApplication.logger.info("------deleteRole,role_id: "+role_id+"----");
		redisUtil.hdel(Config.REIDS_KEY_ROLE, role_id+"");
		mResult.setCode(0);
		mResult.setMessage("删除成功！");
	    return mResult;
	}
}
