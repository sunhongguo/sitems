package com.zkdj.sitems.service;

import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.AlertPageMessage;
import com.zkdj.sitems.bean.FirstPageIllegalMessageResult;
import com.zkdj.sitems.http.HttpService;
import com.zkdj.sitems.mapper.IllegalMapper;
import com.zkdj.sitems.model.DIllegalWeb;

/**
 * 首页的api处理类
 * 
 * @author DELL
 *
 */
@Service
public class FirstPageService {
	@Autowired
	private IllegalMapper mIllegalMapper;
	
	@Autowired
	private HttpService mHttpService;
	
	// 违规网站信息
	public String getAlertPageListBySubjectIds(AlertPageMessage mParams) {
		int page_no = 0;
		int page_size = 0;

		if (mParams.getPage_no() == 0) {
			page_no = 1;
		} else {
			page_no = mParams.getPage_no();
		}

		if (mParams.getPage_size() == 0) {
			page_size = 10; // 默认10条
		} else {
			page_size = mParams.getPage_size();
		}

		String path = "/restserver/page/query/fullQuery?page_no=" + page_no + "&page_size=" + page_size + "&";
		JSONObject json = new JSONObject();

		if (mParams.getSubjectIds() != null && mParams.getSubjectIds().size() > 0) {
			json.put("subjectIds", mParams.getSubjectIds());
		}

		if (mParams.getStart_time() != null && mParams.getStart_time().length() > 0) {
			json.put("start_time", mParams.getStart_time());
		}

		if (mParams.getEnd_time() != null && mParams.getEnd_time().length() > 0) {
			json.put("end_time", mParams.getEnd_time());
		}

		json.put("sortField", "createTime");
		json.put("sortType", "desc");
		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		return result;
	}

	// 违规概况：显示今日待处理违规网站数量、今日已处理违规网站数量，并把今日违规网站按A B C级分类展示统计其数量
	public FirstPageIllegalMessageResult getIllegalMessage() {		
		long current=System.currentTimeMillis();//当前时间毫秒数
		long startTime=current/(1000*3600*24)*(1000*3600*24)-TimeZone.getDefault().getRawOffset();//今天零点零分零秒的毫秒数
		List<DIllegalWeb> mlist_Illegal = mIllegalMapper.getIllegalWebListByTime(startTime,current);
		List<DIllegalWeb> mlist_Handled = mIllegalMapper.getHandledIllegalWebListByTime(startTime,current);
		FirstPageIllegalMessageResult mResult = new FirstPageIllegalMessageResult();
		int count_A = 0,count_B = 0,count_C = 0;
		int count_all_today = 0;
		if(mlist_Illegal != null && mlist_Illegal.size() > 0) {
			mResult.setCount_illegal_webs(mlist_Illegal.size());
			count_all_today = count_all_today + mlist_Illegal.size();
			for(DIllegalWeb mWeb:mlist_Illegal) {
				if(mWeb.getLevel().equals("A")) {
					count_A = count_A + 1;
				}else if(mWeb.getLevel().equals("B")) {
					count_B = count_B + 1;
				}else if(mWeb.getLevel().equals("C")) {
					count_C = count_C + 1;
				}
			}
		}
		if(mlist_Handled != null && mlist_Handled.size() > 0) {
			mResult.setCount_illegal_webs_Handled(mlist_Handled.size());
			count_all_today = count_all_today + mlist_Handled.size();
			for(DIllegalWeb mWeb:mlist_Handled) {
				if(mWeb.getLevel().equals("A")) {
					count_A = count_A + 1;
				}else if(mWeb.getLevel().equals("B")) {
					count_B = count_B + 1;
				}else if(mWeb.getLevel().equals("C")) {
					count_C = count_C + 1;
				}
			}
		}
		mResult.setCount_level_A(count_A);
		mResult.setCount_level_B(count_B);
		mResult.setCount_level_C(count_C);
		mResult.setCount_all_illegal_webs_today(count_all_today);
		
		// 七天内违规网站数量
		long startTime_7 = current - 7*24*3600*1000;
		int num_illegal_7 = mIllegalMapper.getIllegalWebNumberByTime(startTime_7,current);
		int num_illegal_handled_7 = mIllegalMapper.getHandledIllegalWebNumberByTime(startTime_7, current);
		int count_all_7 = num_illegal_7 + num_illegal_handled_7;
		mResult.setCount_all_illegal_webs_7(count_all_7); 
		
		return mResult;
	}
	
	// 违规热词：根据开始时间到结束时间
	public String getIllegalHotWord(AlertPageMessage mParams) {
		String path = "/restserver/aggr/pageDataStatistics/aggrByTerms?";
		JSONObject json = new JSONObject();
		if (mParams.getStart_time() != null && mParams.getStart_time().length() > 0) {
			json.put("start_time", mParams.getStart_time());
		}

		if (mParams.getEnd_time() != null && mParams.getEnd_time().length() > 0) {
			json.put("end_time", mParams.getEnd_time());
		}
		
		json.put("counts", 30);
		json.put("key", "hotWords");
		System.out.println("-------json--------" + json.toString());
		String result = mHttpService.postData(path, json.toString());
		return result;
	}
}
