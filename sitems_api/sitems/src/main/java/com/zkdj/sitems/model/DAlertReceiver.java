package com.zkdj.sitems.model;

/**
 * 预警接收人
 * 
 * @author DELL
 *
 */
public class DAlertReceiver {
	private int id;
	private String name; // 接收人名称
	private String email; // 邮箱
	private String phone; // 电话

	public DAlertReceiver() {
		super();
	}

	public DAlertReceiver(int id, String name, String email, String phone) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
	}

	// id
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	// name
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	// email
	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	// phone
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	@Override
	public String toString() {
		return "id:" + id + ", name: " + name + ", email: " + email + ", phone: " + phone;
	}

}
