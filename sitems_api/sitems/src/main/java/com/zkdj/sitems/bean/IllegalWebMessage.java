package com.zkdj.sitems.bean;

import java.util.List;

/**
 * 违规协查中获取网站列表时，post提交的参数类
 * @author DELL
 *
 */
public class IllegalWebMessage {
	private String searchValue;     // 搜索的网站名称，默认为空
    private String organizerNature;
    private String level;
    private String city;
    
    private String type; // 已处理和未处理的标志位：已处理“handled”;未处理“unhandle”
    
    private int page;  // 第几页,从1开始
    private int count;  // 每页条数

    private List<Integer> ids;
    

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getOrganizerNature() {
		return organizerNature;
	}

	public void setOrganizerNature(String organizerNature) {
		this.organizerNature = organizerNature;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "IllegalWebMessage [searchValue=" + searchValue + ", organizerNature=" + organizerNature + ", level="
				+ level + ", city=" + city + ", type=" + type + ", page=" + page + ", count=" + count + ", ids=" + ids
				+ "]";
	}


}
