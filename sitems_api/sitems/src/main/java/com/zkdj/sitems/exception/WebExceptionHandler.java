package com.zkdj.sitems.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkdj.sitems.result.ResultObject;


@ControllerAdvice
@ResponseBody
public class WebExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(WebExceptionHandler.class);

    @ExceptionHandler
    public ResultObject<Object>  unknownException(Exception e) {
        log.error("发生了未知异常", e);
        
        ResultObject<Object> resultObject = new ResultObject<Object>();
        resultObject.setCode(-1);
        resultObject.setMessage("未知异常");
        resultObject.setErrmsg(e.toString());
        
        return resultObject;
    }
}