package com.zkdj.sitems.bean;

import java.util.List;

/**
 * 网站管理中网站数据
 * 
 * @author DELL
 *
 */
public class WebObject {
	private String domainName;       // 域名
	private String organizer;        // 主办单位名称
	private String organizerNature;  // 单位性质，枚举类型，详见附1
	private String icpNo;            // 网站备案许可证号
	private String siteName;         // 网站名称
	private String email;            // 联系邮箱
	private String phone;            // 联系电话
	private String register;         // 注册商
	private String contact;          // 联系人
	private String registerTime;     // 网站创建时间
	private String endTime;          // 网站过期时间
	private String gatherTime;       // 网站信息采集时间
	private String domainNameServer; // 域名服务器
	private int domainNameStatus;    // 网站状态，枚举类型：正常 0，禁止 1，其它 9
	private String dns;              // DNS
	private int prScore;             // 网站pr值，0-9
	private String level;            // 网站级别，枚举类型A B C
	private String province;         // 网站所属省份
	private String city;             // 网站所属地级市
	private String id;               // 网站信息id
	private List<String> category;         // 网站信息分类

	public WebObject(){
		super();
	}
	
	// domainName
	public void setDomainName(String domainName){
		this.domainName = domainName;
	}
	public String getDomainName(){
		return domainName;
	}
	
	// organizer
	public void setOrganizer(String organizer){
		this.organizer = organizer;
	}
	public String getOrganizer(){
		return organizer;
	}
	
	// organizerNature
	public void setOrganizerNature(String organizerNature){
		this.organizerNature = organizerNature;
	}
	public String getOrganizerNature(){
		return organizerNature;
	}
	
	// icpNo
	public void setIcpNo(String icpNo){
		this.icpNo = icpNo;
	}
	public String getIcpNo(){
		return icpNo;
	}
	
	// siteName
	public void setSiteName(String siteName){
		this.siteName = siteName;
	}
	public String getSiteName(){
		return siteName;
	}
	
	// email
	public void setEmail(String email){
		this.email = email;
	}
	public String getEmail(){
		return email;
	}
	
	// phone
	public void setPhone(String phone){
		this.phone = phone;
	}
	public String getPhone(){
		return phone;
	}
	
	// register
	public void setRegister(String register){
		this.register = register;
	}
	public String getRegister(){
		return register;
	}
	
	// contact
	public void setContact(String contact){
		this.contact = contact;
	}
	public String getContact(){
		return contact;
	}
	
	// registerTime
	public void setRegisterTime(String registerTime){
		this.registerTime = registerTime;
	}
	public String getRegisterTime(){
		return registerTime;
	}
	
	// endTime
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	public String getEndTime(){
		return endTime;
	}
	
	// gatherTime
	public void setGatherTime(String gatherTime){
		this.gatherTime = gatherTime;
	}
	public String getGatherTime(){
		return gatherTime;
	}
	
	// domainNameServer
	public void setDomainNameServer(String domainNameServer){
		this.domainNameServer = domainNameServer;
	}
	public String getDomainNameServer(){
		return domainNameServer;
	}
	
	// domainNameStatus 
	public void setDomainNameStatus(int domainNameStatus){
		this.domainNameStatus = domainNameStatus;
	}
	public int getDomainNameStatus(){
		return domainNameStatus;
	}
	
	// dns
	public void setDns(String dns){
		this.dns = dns;
	}
	public String getDns(){
		return dns;
	}
	
	// prScore
	public void setPrScore(int pr){
		this.prScore = pr;
	}
	public int getPrScore(){
		return prScore;
	}
	
	// level
	public void setLevel(String level){
		this.level = level;
	}
	public String getLevel(){
		return level;
	}
	
	// province
	public void setProvince(String province){
		this.province = province;
	}
	public String getProvince(){
		return province;
	}
	
	// city
	public void setCity(String city){
		this.city = city;
	}
	public String getCity(){
		return city;
	}
	
	// id
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return id;
	}
	
	// category
	public void setCategory(List<String> category){
		this.category = category;
	}
	public List<String> getCategory(){
		return category;
	}
}
