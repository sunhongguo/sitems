package com.zkdj.sitems.bean;

import java.util.List;

/**
 * 保存预警关键词时，接受post数据的类
 * @author DELL
 *
 */
public class AlertWordMessage {
	private String word;  // 预警关键词
	private int isalert;  // 是否预警开关：1表示开；0表示关; 默认开
	private int subject_id;  // 专题id
	private List<Integer>  alert_receiver;  // 预警接收人
	private List<String>   alert_type;    // 预警接受方式：邮箱email和短信message
	private String begin_time;   // 开始时间
	private String end_time;     // 结束时间
	
	// subject_id
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}
	public int getSubject_id() {
		return subject_id;
	}
	
	// word
	public void setWord(String word) {
		this.word = word;
	}
	public String getWord() {
		return word;
	}
	
	// isalert
	public void setIsalert(int isalert) {
		this.isalert = isalert;
	}
	public int getIsalert() {
		return isalert;
	}

	// alert_receiver
	public void setAlert_receiver(List<Integer> alert_receiver) {
		this.alert_receiver = alert_receiver;
	}
	public List<Integer> getAlert_receiver(){
		return alert_receiver;
	}
	
	// alert_type
	public void setAlert_type(List<String> alert_type) {
		this.alert_type = alert_type;
	}
	public List<String> getAlert_type(){
		return alert_type;
	}

	// begin_time
	public void setBegin_time(String begin_time) {
		this.begin_time = begin_time;
	}
	public String getBegin_time() {
		return begin_time;
	}
	
	// end_time
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "isalert: " + isalert + ",word: " + word;
	}
}
