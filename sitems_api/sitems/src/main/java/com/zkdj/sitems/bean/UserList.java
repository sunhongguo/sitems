package com.zkdj.sitems.bean;

import java.util.List;

public class UserList {
	private int icount;   // 数据库中数据的总数
	private List<User> userlist;
	
	public UserList(){
		super();
	}
	
	// icount
	public void setIcount(int i){
		this.icount = i;
	}
	public int getIcount(){
		return icount;
	}
	
	// userlist
	public void setUserList(List<User> list){
		this.userlist = list;
	}
	public List<User> getUserList(){
		return userlist;
	}

}
