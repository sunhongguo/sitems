package com.zkdj.sitems.model;

import java.io.Serializable;

// db_user关联的对象
public class DUser implements Serializable{
	
	/**
	 * 用户
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String account;
	private String user_name;
	private String password;
	private String contact;    // 联系电话
	private String avatar; // 头像
	private String token; // 访问令牌
	private int role_id;  // 关联的角色id
	private Long account_begints; // 账号开始时间
	private Long account_endts; // 账号结束时间
	private Long create_time;   // 帐号创建时间

	public DUser(){
		super();
	}

	public DUser(int id, String account,String user_name, String password, String contact,String avatar,String token,
			int role_id,Long begints, Long endts, Long create_time) {
		this.id = id;
		this.account = account;
		this.user_name = user_name;
		this.password = password;
		this.contact = contact;
		this.avatar = avatar;
		this.token = token;
		
		this.role_id = role_id;
		this.account_begints = begints;
		this.account_endts = endts;
		this.create_time = create_time;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// account
	public void setAccount(String account){
		this.account = account;
	}
	public String getAccount(){
		return account;
	}
		
	// user_name
	public void setUser_name(String user_name){
		this.user_name = user_name;
	}
	public String getUser_name(){
		return user_name;
	}

	// password
	public void setPassword(String passWord) {
		this.password = passWord;
	}
	public String getPassword() {
		return password;
	}
	
	// contact
	public void setContact(String phone){
		this.contact = phone;
	}
	public String getContact(){
		return contact;
	}
	
	// avatar
	public void setAvatar(String url){
		this.avatar = url;
	}
	public String getAvatar(){
		return avatar;
	}
	
	// token
	public void setToken(String token){
		this.token = token;
	}
	public String getToken(){
		return token;
	}
	
	// role_id
	public void setRole_id(int role_id){
		this.role_id = role_id;
	}
	public int getRole_id(){
		return role_id;
	}
	
	// account_begints
	public void setAccount_begints(Long begints){
		this.account_begints = begints;
	}
	public Long getAccount_begints(){
		return account_begints;
	}
	
	// account_endts
	public void setAccount_endts(Long endts){
		this.account_endts = endts;
	}
	public Long getAccount_endts(){
		return account_endts;
	}
	
	// create_time
	public void setCreate_time(Long create_time){
		this.create_time = create_time;
	}
	public Long getCreate_time(){
		return create_time;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "id: " + this.id + ",account: " + this.account + " ,userName: " + this.user_name + ", pasword: " + this.password + ", role_id: " + role_id;
	}
}
