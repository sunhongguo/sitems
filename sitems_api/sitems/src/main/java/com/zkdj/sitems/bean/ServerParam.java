package com.zkdj.sitems.bean;

/**
  *  外部服务器访问时post中参数
 * @author DELL
 */
public class ServerParam {
	private String analysis;
	
	// analysis
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}
	public String getAnalysis() {
		return analysis;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "analysis: " + analysis;
	}
}
