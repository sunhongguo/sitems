package com.zkdj.sitems.controller;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.SitemsApplication;
import com.zkdj.sitems.bean.User;
import com.zkdj.sitems.bean.UserList;
import com.zkdj.sitems.model.DUser;
import com.zkdj.sitems.redis.RedisUtil;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.PermissionsService;
import com.zkdj.sitems.service.UserService;
import com.zkdj.sitems.util.Config;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService mUserService;
	@Resource
    private RedisUtil redisUtil;
	@Autowired
	private PermissionsService mPermissionsService;
	// 获取用户列表
	@RequestMapping("/querylist")
	public ResultObject<UserList> getUserList(@Param("page")Integer page, @Param("count")Integer count){
		ResultObject<UserList> mResult = new ResultObject<UserList>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","账号信息",null);
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}	
		
		int ipage = 0,icount = 0;
		if(page != null){
			ipage = page;
		}
		
		if(count != null){
			icount = count;
		}
		mResult = mUserService.getUserList(ipage,icount);
		return mResult;	
	}
	
	// 根据id获取用户的详细信息
	@RequestMapping("/queryid")
	public ResultObject<User> getUserByID(int id){
		ResultObject<User> mResult = new ResultObject<User>();   	
    	if(id > 0){
    		mResult = mUserService.getUserByID(id);
    	}else{
    		mResult.setCode(-2);
			mResult.setMessage("上传参数有误！");
    	}   	
        return mResult;
	}
	
	// 根据account获取用户的详细信息
	@RequestMapping("/query_account")
	public ResultObject<User> getUserByAccount(String account){
		ResultObject<User> mResult = new ResultObject<User>();   	
	    if(account != null && account.length() > 0){
	    	mResult = mUserService.getUserByAccount(account);
	    }else{
	    	mResult.setCode(-2);
			mResult.setMessage("上传参数有误！");
	    }   	
	       return mResult;
	}

	// 创建新帐号
    @RequestMapping("/create")
    public ResultObject<User> createNewUser(@RequestBody DUser user) {
    	SitemsApplication.logger.info("----create New user-----" + user);
    	ResultObject<User> mResult = new ResultObject<User>();
    	
    	ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","用户管理","添加");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}	
		
    	if(user.getAccount() == null || user.getAccount().length() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("帐号为空！");	
    		return mResult;	
    	}
    	
    	if(user.getPassword() == null || user.getPassword().length() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("密码为空！");	
    		return mResult;			
		} 
    	
    	if(user.getUser_name() == null || user.getUser_name().length() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("姓名为空！");	
    		return mResult;			
		}
    	
    	if(user.getRole_id() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("角色为空！");	
    		return mResult;			
		}
    	
    	mResult = mUserService.createNewUser(user); 
		return mResult;
    }
    
	// 修改帐号
    @RequestMapping("/update_user")
    public ResultObject<User> updateUser(@RequestBody DUser user) {
    	SitemsApplication.logger.info("----update user-----" + user);
    	ResultObject<User> mResult = new ResultObject<User>();
    	
    	ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","用户管理","修改");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
    	if(user.getId()== 0){
    		mResult.setCode(-1);
    		mResult.setMessage("帐号ID为空！");	
    		return mResult;	
    	}
    	
    	if(user.getAccount() == null || user.getAccount().length() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("帐号为空！");	
    		return mResult;	
    	}
    	
    	if(user.getPassword() == null || user.getPassword().length() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("密码为空！");	
    		return mResult;			
		} 
    	
    	if(user.getUser_name() == null || user.getUser_name().length() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("姓名为空！");	
    		return mResult;			
		}
    	
    	if(user.getRole_id() == 0){
    		mResult.setCode(-1);
    		mResult.setMessage("角色为空！");	
    		return mResult;			
		}
    	
    	mResult = mUserService.updateUser(user); 
		return mResult;
    }
    
    //删除用户账号
    @RequestMapping("/delete")
    public ResultObject<DUser> DeleteUserByID(int userid){
    	SitemsApplication.logger.info("----delete user-----");
    	ResultObject<DUser> mResult = new ResultObject<DUser>();
    	
    	ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","用户管理","删除");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
    	if(userid == 0){
    		mResult.setCode(-2);
        	mResult.setMessage("上传参数错误！");
        	return mResult;
    	}
    	
    	DUser mUser = mUserService.getDUserByID(userid);
    	if(mUser == null){
    		mResult.setCode(-2);
        	mResult.setMessage("没有查找到该帐号！");
        	return mResult;
    	}
    	
    	if(mUser.getAccount().equals("root")){
    		mResult.setCode(-13);
        	mResult.setMessage("root帐号不允许删除！");
        	return mResult;
    	}
    	
    	String sRedis_UserId = (String) redisUtil.get(Config.REDIS_USERID_KEY);
    	if(sRedis_UserId != null && sRedis_UserId.length() > 0){
    		String delete_user_id = userid + "";
    		if(sRedis_UserId.equals(delete_user_id)){
				mResult.setCode(-12);
            	mResult.setMessage("正在操作的帐号,无法删除！");
			}else{
				mResult = mUserService.deleteUserByID(userid);
			}
    	}else{
    		mResult.setCode(-10);
        	mResult.setMessage("数据错误！");
    	}
        return mResult;
    }
    
    //更改用户角色
/* 	@RequestMapping("/update_roleid")
  	public ResultObject<DUser> UpdataUserRole(@RequestBody String role){
  		JSONObject object = JSON.parseObject(role);
      	int userid = object.getIntValue("userid");
      	int role_id = object.getIntValue("role_id");
  		ResultObject<DUser> mResult = mUserService.UpdataUserRole(userid,role_id);
  		System.out.println("----UpdataUserRole-----" + mResult);
  		return mResult;
  	}*/
    
	//更改用户姓名
	@RequestMapping("/update_username")
	public ResultObject<DUser> updataUserName(@RequestBody String Cname){
		ResultObject<DUser> mResult = new ResultObject<DUser>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","账号信息","修改姓名");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
		JSONObject object = JSON.parseObject(Cname);
    	String account = object.getString("account");
    	String user_name = object.getString("user_name");
		mResult = mUserService.UpdataUserName(account,user_name);
		SitemsApplication.logger.info("----UpdataUserName-----" + mResult);
		return mResult;
	}
	
    //修改用户联系方式
    @RequestMapping("/update_contact")
  	public ResultObject<DUser> updataUserContact(@RequestBody String Contact){
    	ResultObject<DUser> mResult = new ResultObject<DUser>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","账号信息","修改联系方式");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
    	JSONObject object = JSON.parseObject(Contact);
    	String account = object.getString("account");
    	String contact = object.getString("contact");
    	
    	mResult = mUserService.UpdataUserContact(account,contact);
    	SitemsApplication.logger.info("----UpdataUserContact-----" + mResult);
  		return mResult;
  	}
    
    //修改用户密码
    @RequestMapping("/update_passWord")
  	public ResultObject<DUser> updataUserPassWord(@RequestBody String PassWord){
    	ResultObject<DUser> mResult = new ResultObject<DUser>();
		ResultObject<String> mPerResult = mPermissionsService.judgeUserPermission("个人中心","账号信息","重置密码");
		if(mPerResult != null) {
			if(mPerResult.getCode() == 100) {
				mResult.setCode(-100);
			    mResult.setMessage("用户没有操作该项的权限！");
			    return mResult;
			}else if(mPerResult.getCode() == 101) {	
				
			}else {
				mResult.setCode(-101);
				mResult.setMessage(mPerResult.getMessage());
				return mResult;
			}
		}else {
			mResult.setCode(-101);
			mResult.setMessage("验证权限失败！");
			return mResult;
		}
		
    	JSONObject object = JSON.parseObject(PassWord);
    	String account = object.getString("account");
    	String oldPassWord = object.getString("oldPassWord");
    	String newPassWord = object.getString("newPassWord");
    	mResult = mUserService.UpdataUserPassWord(account,oldPassWord,newPassWord);
    	SitemsApplication.logger.info("----UpdataUserPassWord-----" + mResult);
  		return mResult;
  	}   
    
    //获取用户信息
    @RequestMapping("/getinfo")
  	public ResultObject<DUser> getUserInfo(String account){
    	ResultObject<DUser> mResult = mUserService.GetUserInfo(account);
    	SitemsApplication.logger.info("----getUserInfo-----" + mResult);
  		return mResult;
  	}  
    
   
}
