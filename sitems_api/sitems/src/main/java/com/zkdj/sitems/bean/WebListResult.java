package com.zkdj.sitems.bean;

import java.util.List;

/*
 * 网站列表请求时返回的数据
 */
public class WebListResult {

	private String msg;
	private List<WebObject> result;
	private String code;
	private String count;
	private String page;
	private String maxPage;

	public WebListResult(String msg, List<WebObject> result, String code,String count, String page,String maxPage){
		this.msg = msg;
		this.result = result;
		this.code = code;
		this.count = count;
		this.page = page;
		this.maxPage = maxPage;
	}
	
	// msg
	public void setMsg(String msg){
		this.msg = msg;
	}
	public String getMsg(){
		return msg;
	}
	
	// result
	public void setResult(List<WebObject> mResult) {
		this.result = mResult;
	}

	public List<WebObject> getResult() {
		return result;
	}

	// code
	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	// count
	public void setCount(String count) {
		this.count = count;
	}

	public String getCount() {
		return count;
	}

	// page
	public void setPage(String page) {
		this.page = page;
	}

	public String getPage() {
		return page;
	}

	// maxPage
	public void setMaxPage(String maxPage) {
		this.maxPage = maxPage;
	}

	public String getMaxPage() {
		return maxPage;
	}
}
