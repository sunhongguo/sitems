package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zkdj.sitems.model.DAlertPage;
import com.zkdj.sitems.model.DEMail;
import com.zkdj.sitems.model.DReport;

@Mapper
public interface ReportMapper {
	// db_report
	// 简报：查询
	@Select("SELECT id,report_name,start_time,end_time FROM db_report limit #{index}, #{count}")
	List<DReport> getReportList(@Param("index")int index, @Param("count")int count);
	
	// 简报：总行数
	@Select("SELECT COUNT(*) FROM db_report")
	int getReportCount();

	// 简报:根据id查询
	@Select("SELECT * FROM db_report WHERE id = #{id}")
	DReport getReportByID(int id);

	// 简报:根据id删除
	@Delete("DELETE FROM db_report WHERE id = #{id}")
	int deleteReportByID(int id);

	// 简报:添加
	@Insert("INSERT INTO db_report(report_name, start_time, end_time, report_desc, webs_illegal, webs_illegal_handled, page_new, page_important)"
			+ " VALUES(#{report_name}, #{start_time}, #{end_time}, #{report_desc}, #{webs_illegal}, #{webs_illegal_handled}, #{page_new}, #{page_important})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertReport(DReport mDReport);

	// db_report_email
	// 邮件接收人：查询
	@Select("SELECT * FROM db_report_email WHERE userId = #{userId}")
	List<DEMail> getEmailList(int userId);

	// 邮件接收人：根据userID查询数量
	@Select("SELECT COUNT(*) FROM db_report_email WHERE userId = #{userId}")
	int getEmailNumberByUserId(int userId);
		
	// 邮件接收人:根据id查询
	@Select("SELECT * FROM db_report_email WHERE id = #{id}")
	DEMail getEmailByID(int id);

	// 邮件接收人:根据id删除
	@Delete("DELETE FROM db_report_email WHERE id = #{id}")
	int deleteEMailByID(int id);

	// 邮件接收人:添加
	@Insert("INSERT INTO db_report_email(email, userId) VALUES(#{email}, #{userId})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertEMail(DEMail mDEMail);
	
	// db_report_page
	// 违规信息：查询简报中的重点违规信息
	@Select("SELECT * FROM db_report_page")
	List<DAlertPage> getAlertPageList();
	
	// 违规信息:批量添加	
	@Insert({
		 "<script>",
		 "insert into db_report_page(url,domainName,title,cleanTitle,createTime) values ",
		 "<foreach collection='mlist' item='item' index='index' separator=','>",
		 "(#{item.url}, #{item.domainName}, #{item.title}, #{item.cleanTitle}, #{item.createTime})",
		 "</foreach>",
		 "</script>"
		})
	int insertReportPageList(@Param(value="mlist") List<DAlertPage> mlist);
}
