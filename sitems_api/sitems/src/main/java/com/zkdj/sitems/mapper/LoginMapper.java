package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.zkdj.sitems.model.DCaptcha;

@Mapper
public interface LoginMapper {
	// db_captcha
	// 验证码:根据id查询
	@Select("SELECT * FROM db_captcha WHERE id = #{id}")
	DCaptcha getCaptchaByID(int id);
	
	// 验证码:根据account查询
	@Select("SELECT * FROM db_captcha WHERE account = #{account} ORDER BY create_time DESC")
	List<DCaptcha> getCaptchaByAccount(String account);

	// 验证码:根据id删除
	@Delete("DELETE FROM db_captcha WHERE id = #{id}")
	int deleteCaptchaByID(int id);
	
	// 验证码:根据account删除
	@Delete("DELETE FROM db_captcha WHERE account = #{account}")
	int deleteCaptchaByAccount(String account);

	// 验证码:添加
	@Insert("INSERT INTO db_captcha(account, contact, code, create_time)"
			+ " VALUES(#{account}, #{contact}, #{code}, #{create_time})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertCaptcha(DCaptcha mDCaptcha);
}
