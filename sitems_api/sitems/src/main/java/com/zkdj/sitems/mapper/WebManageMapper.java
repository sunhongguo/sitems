package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zkdj.sitems.model.DRegion;

@Mapper
public interface WebManageMapper {
	// db_region
	
	// 查询：根据名称name
	@Select("SELECT * FROM db_region WHERE name = #{name}")
	DRegion getRegion(@Param("name")String name);

	// 查询：根据父级ID parent_id
	@Select("SELECT * FROM db_region WHERE parent_id = #{parent_id}")
	List<DRegion> getRegionList(@Param("parent_id")int parent_id);
}
