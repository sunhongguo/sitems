package com.zkdj.sitems.bean;

import java.util.List;

import com.zkdj.sitems.model.DAlertPage;
import com.zkdj.sitems.model.DIllegalWeb;

/*
 * 返回的简报数据
 */
public class ReportResult {
	private int id;
	private String report_name;
	private long start_time;
	private long end_time;

	private String report_desc;          //概述
	private List<DIllegalWeb> webs_illegal;          // 违规网站:未处理
	private List<DIllegalWeb> webs_illegal_handled;  // 违规网站:已处理
	private List<DAlertPage> page_new;      // 最新违规信息
	private List<DAlertPage> page_important;  // 重点违规信息
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReport_name() {
		return report_name;
	}
	public void setReport_name(String report_name) {
		this.report_name = report_name;
	}
	public long getStart_time() {
		return start_time;
	}
	public void setStart_time(long start_time) {
		this.start_time = start_time;
	}
	public long getEnd_time() {
		return end_time;
	}
	public void setEnd_time(long end_time) {
		this.end_time = end_time;
	}
	public String getReport_desc() {
		return report_desc;
	}
	public void setReport_desc(String report_desc) {
		this.report_desc = report_desc;
	}
	public List<DIllegalWeb> getWebs_illegal() {
		return webs_illegal;
	}
	public void setWebs_illegal(List<DIllegalWeb> webs_illegal) {
		this.webs_illegal = webs_illegal;
	}
	public List<DIllegalWeb> getWebs_illegal_handled() {
		return webs_illegal_handled;
	}
	public void setWebs_illegal_handled(List<DIllegalWeb> webs_illegal_handled) {
		this.webs_illegal_handled = webs_illegal_handled;
	}
	public List<DAlertPage> getPage_new() {
		return page_new;
	}
	public void setPage_new(List<DAlertPage> page_new) {
		this.page_new = page_new;
	}
	public List<DAlertPage> getPage_important() {
		return page_important;
	}
	public void setPage_important(List<DAlertPage> page_important) {
		this.page_important = page_important;
	}
	@Override
	public String toString() {
		return "ReportResult [id=" + id + ", report_name=" + report_name + ", start_time=" + start_time + ", end_time="
				+ end_time + ", report_desc=" + report_desc + ", webs_illegal=" + webs_illegal
				+ ", webs_illegal_handled=" + webs_illegal_handled + ", page_new=" + page_new + ", page_important="
				+ page_important + "]";
	}

}
