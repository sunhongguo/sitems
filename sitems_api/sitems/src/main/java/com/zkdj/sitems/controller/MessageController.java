package com.zkdj.sitems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.zkdj.sitems.bean.MessageParams;
import com.zkdj.sitems.result.ResultObject;
import com.zkdj.sitems.service.MessageService;

/**
 * 发送消息：短信、邮件
 * 
 * @author DELL
 *
 */
@RestController
@RequestMapping("/msg")
public class MessageController {
	@Autowired
	private MessageService mMessageService;
	
	// 发送短信
	@RequestMapping("/shortmessage")
	public ResultObject<String> sendMessageC(@RequestBody MessageParams mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getPhones() == null || mParams.getPhones().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("手机号为空！");
			return mResult;
		}
		
		if(mParams.getContent() == null || mParams.getContent().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("短信内容为空！");
			return mResult;
		}
		
		String result = mMessageService.sendShortMessage(mParams);
		
		if(result != null && result.length() > 0) {
			JSONObject mJSONObject = JSONObject.parseObject(result);
			if(mJSONObject != null && mJSONObject.getJSONObject("meta").getIntValue("code") == 0) {
				mResult.setCode(0);
				mResult.setMessage("发送短信成功！");
				return mResult;
			}
		}else {
			mResult.setCode(-2);
			mResult.setMessage("发送短信失败！");
		}
		return mResult;
	}

	// 发送邮件
	@RequestMapping("/email")
	public ResultObject<String> sendEMailC(@RequestBody MessageParams mParams) {
		ResultObject<String> mResult = new ResultObject<>();
		if (mParams == null) {
			mResult.setCode(-1);
			mResult.setMessage("参数错误！");
			return mResult;
		}
		
		if(mParams.getFromName() == null || mParams.getFromName().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("邮件发送者为空！");
			return mResult;
		}
		
		if(mParams.getEmails() == null || mParams.getEmails().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("邮箱为空！");
			return mResult;
		}
		
		if(mParams.getTitle() == null || mParams.getTitle().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("邮件标题为空！");
			return mResult;
		}
		
		if(mParams.getContent() == null || mParams.getContent().length() == 0) {
			mResult.setCode(-1);
			mResult.setMessage("邮件内容为空！");
			return mResult;
		}
		
		String result = mMessageService.sendEMail(mParams);
		
		if(result != null && result.length() > 0) {
			JSONObject mJSONObject = JSONObject.parseObject(result);
			if(mJSONObject != null && mJSONObject.getJSONObject("meta").getIntValue("code") == 0) {
				mResult.setCode(0);
				mResult.setMessage("发送邮件成功！");
				return mResult;
			}
		}else {
			mResult.setCode(-2);
			mResult.setMessage("发送邮件失败！");
		}
		return mResult;
	}
}
