package com.zkdj.sitems.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zkdj.sitems.model.DIllegalRecord;
import com.zkdj.sitems.model.DIllegalWeb;

@Mapper
public interface IllegalMapper {
	// db_illegal_web 待处理表
	// 待处理网站:添加
	@Insert("INSERT INTO db_illegal_web(webId,illegalCount,domainName,organizerNature,level,city,province,siteName,categoryTitle,contact,phone,createTime,scheme) "
			+ "VALUES(#{webId}, #{illegalCount}, #{domainName}, #{organizerNature}, #{level}, #{city}, #{province}, #{siteName}, #{categoryTitle}, #{contact}, #{phone}, #{createTime}, #{scheme})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertIllegalWeb(DIllegalWeb mDIllegalWeb);
	
	 @Select("<script>"
	            + "SELECT * FROM db_illegal_web "
	            + "<where> "
	            + "<if test='organizerNature!=null and organizerNature.length()>0'> organizerNature = #{organizerNature} </if>"
	            + "<if test='level!=null and level.length()>0'>AND level = #{level} </if>"
	            + "<if test='city!=null and city.length()>0'>AND city = #{city} </if>"
	            + "<if test='siteName!=null and siteName.length()>0'>AND siteName like #{siteName} </if>" 
	            + "</where>"
	            + "ORDER BY createTime DESC "
	            + "limit #{index}, #{count}"
	            + "</script>")
//	@Select("SELECT * FROM db_illegal_web WHERE level = #{level} order by createTime limit #{index}, #{count}")
	List<DIllegalWeb> getIllegalWebList(@Param("siteName")String siteName, @Param("organizerNature")String organizerNature, @Param("level")String level,
			@Param("city")String city, @Param("index")int index, @Param("count")int count);
	 
	 @Select("<script>"
	            + "SELECT * FROM db_illegal_web "
	            + "<where> "
	            + "<if test='organizerNature!=null and organizerNature.length()>0'> organizerNature = #{organizerNature} </if>"
	            + "<if test='level!=null and level.length()>0'> level = #{level} </if>"
	            + "<if test='city!=null and city.length()>0'> city = #{city} </if>"
	            + "</where>"
	            + "</script>")
	List<DIllegalWeb> getIllegalWebListNoPage(@Param("organizerNature")String organizerNature, @Param("level")String level,
			@Param("city")String city);
	 
	 // 获取待处理网站列表：根据时间段
	 @Select("<script>"
	            + "SELECT * FROM db_illegal_web "
	            + "<where> "
	            + "<if test='startTime!=null and startTime>0'> createTime <![CDATA[>=]]> #{startTime} </if>"
	            + "<if test='endTime!=null and endTime>0'>AND createTime <![CDATA[<=]]>#{endTime} </if>"
	            + "</where>"
	            + "</script>")
	List<DIllegalWeb> getIllegalWebListByTime(@Param("startTime")long startTime, @Param("endTime")long endTime);
	
	 // 获取待处理网站数量：根据时间段
	 @Select("<script>"
	            + "SELECT COUNT(*) FROM db_illegal_web "
	            + "<where> "
	            + "<if test='startTime!=null and startTime>0'> createTime <![CDATA[>=]]> #{startTime} </if>"
	            + "<if test='endTime!=null and endTime>0'>AND createTime <![CDATA[<=]]>#{endTime} </if>"
	            + "</where>"
	            + "</script>")
	int getIllegalWebNumberByTime(@Param("startTime")long startTime, @Param("endTime")long endTime);

	// 统计总数:根据条件
	 @Select("<script>"
	            + "SELECT COUNT(*) FROM db_illegal_web "
	            + "<where> "
	            + "<if test='organizerNature!=null and organizerNature.length()>0'> organizerNature = #{organizerNature} </if>"
	            + "<if test='level!=null and level.length()>0'>AND level = #{level} </if>"
	            + "<if test='city!=null and city.length()>0'>AND city = #{city} </if>"
	            + "<if test='siteName!=null and siteName.length()>0'>AND siteName like #{siteName} </if>"
	            + "</where>"
	            + "</script>")
//	@Select("SELECT COUNT(*) FROM db_illegal_web")
	int getIllegalNumber(@Param("siteName")String siteName, @Param("organizerNature")String organizerNature, @Param("level")String level,
			@Param("city")String city);
	
	// 根据城市统计
	@Select("SELECT COUNT(*) FROM db_illegal_web WHERE city = #{city}")
	int getIllegalCountByCity(String city);
	
	@Select("SELECT * FROM db_illegal_web WHERE id = #{id}")
	DIllegalWeb getIllegalWebById(int id);

	@Delete("DELETE FROM db_illegal_web WHERE id = #{id}")
	int deleteIllegalWebByID(int id);
	
	// db_illegal_web_handled  已处理表
	@Insert("INSERT INTO db_illegal_web_handled(webId,domainName,organizerNature,level,city,province,siteName,categoryTitle,contact,phone,createTime) "
			+ "VALUES(#{webId}, #{domainName}, #{organizerNature}, #{level}, #{city}, #{province}, #{siteName}, #{categoryTitle}, #{contact}, #{phone}, #{createTime})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertHandledIllegalWeb(DIllegalWeb mDIllegalWeb);
	
//	@Select("SELECT * FROM db_illegal_web_handled order by createTime limit #{index}, #{count}")
	@Select("<script>"
            + "SELECT * FROM db_illegal_web_handled "
            + "<where> "
            + "<if test='organizerNature!=null and organizerNature.length()>0'> organizerNature = #{organizerNature} </if>"
            + "<if test='level!=null and level.length()>0'>AND level = #{level} </if>"
            + "<if test='city!=null and city.length()>0'>AND city = #{city} </if>"
            + "<if test='siteName!=null and siteName.length()>0'>AND siteName like #{siteName} </if>" 
            + "</where>"
            + "ORDER BY createTime DESC "
            + "limit #{index}, #{count}"
            + "</script>")
	List<DIllegalWeb> getHandledIllegalWebList(@Param("siteName")String siteName, @Param("organizerNature")String organizerNature, @Param("level")String level,
			@Param("city")String city, @Param("index")int index, @Param("count")int count);
	

	@Select("<script>"
            + "SELECT * FROM db_illegal_web_handled "
            + "<where> "
            + "<if test='organizerNature!=null and organizerNature.length()>0'> organizerNature = #{organizerNature} </if>"
            + "<if test='level!=null and level.length()>0'> level = #{level} </if>"
            + "<if test='city!=null and city.length()>0'> city = #{city} </if>"
            + "</where>"
            + "</script>")
	List<DIllegalWeb> getHandledIllegalWebListNoPage(@Param("organizerNature")String organizerNature, @Param("level")String level,
			@Param("city")String city);
	
	 // 获取已处理网站列表：根据时间段
	 @Select("<script>"
	            + "SELECT * FROM db_illegal_web_handled "
	            + "<where> "
	            + "<if test='startTime!=null and startTime>0'>createTime <![CDATA[>=]]> #{startTime} </if>"
	            + "<if test='endTime!=null and endTime>0'>AND createTime <![CDATA[<=]]>#{endTime} </if>"
	            + "</where>"
	            + "</script>")
	List<DIllegalWeb> getHandledIllegalWebListByTime(@Param("startTime")long startTime, @Param("endTime")long endTime);
	
	 // 获取已处理网站数量：根据时间段
	 @Select("<script>"
	            + "SELECT COUNT(*) FROM db_illegal_web_handled "
	            + "<where> "
	            + "<if test='startTime!=null and startTime>0'>createTime <![CDATA[>=]]> #{startTime} </if>"
	            + "<if test='endTime!=null and endTime>0'>AND createTime <![CDATA[<=]]>#{endTime} </if>"
	            + "</where>"
	            + "</script>")
	int getHandledIllegalWebNumberByTime(@Param("startTime")long startTime, @Param("endTime")long endTime);

	 
//	@Select("SELECT COUNT(*) FROM db_illegal_web_handled")
	@Select("<script>"
	            + "SELECT COUNT(*) FROM db_illegal_web_handled "
	            + "<where> "
	            + "<if test='organizerNature!=null and organizerNature.length()>0'> organizerNature = #{organizerNature} </if>"
	            + "<if test='level!=null and level.length()>0'> level = #{level} </if>"
	            + "<if test='city!=null and city.length()>0'> city = #{city} </if>"
	            + "<if test='siteName!=null and siteName.length()>0'>AND siteName like #{siteName} </if>" 
	            + "</where>"
	            + "</script>")
	int getHandledIllegalNumber(@Param("siteName")String siteName, @Param("organizerNature")String organizerNature, @Param("level")String level,
			@Param("city")String city);
	
	// 根据城市统计
	@Select("SELECT COUNT(*) FROM db_illegal_web_handled WHERE city = #{city}")
	int getHandledIllegalCountByCity(String city);
	
	@Select("SELECT * FROM db_illegal_web_handled WHERE domainName = #{domainName}")
	List<DIllegalWeb> getHandledIllegalWebByDomainName(String domainName);
	
	@Select("SELECT * FROM db_illegal_web_handled WHERE id = #{id}")
	DIllegalWeb getHandledIllegalWebById(int id);
	
	@Delete("DELETE FROM db_illegal_web_handled WHERE domainName = #{domainName}")
	int deleteHandledIllegalWebByDomainName(String domainName);
	
	// db_illegal_handle_records 处理记录表
	@Insert("INSERT INTO db_illegal_handle_records(domainName,userId,count,scheme,feedBack,createTime) "
			+ "VALUES(#{domainName}, #{userId}, #{count}, #{scheme}, #{feedBack}, #{createTime})")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int insertIllegalHandleRecord(DIllegalRecord mDIllegalRecord);
	
	@Select("SELECT * FROM db_illegal_handle_records WHERE domainName = #{domainName} order by createTime")
	List<DIllegalRecord> getRecordList(String domainName);
}
