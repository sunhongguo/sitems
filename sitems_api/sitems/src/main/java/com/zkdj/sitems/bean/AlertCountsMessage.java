package com.zkdj.sitems.bean;

/**
 * 查询巡查预警网站的统计数据时，接受条件参数的类
 * 
 * @author DELL
 *
 */
public class AlertCountsMessage {
	private String province; // 所在省份
	private String city; // 所在城市，默认全部,非必须

	private String key; // 统计字段, 必须
	private Integer counts; // 返回数量，默认为10,非必须
	private String start_time; // 开始时间,非必须
	private String end_time; // 结束时间,非必须

	// province
	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvince() {
		return province;
	}

	// city
	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	// key
	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	// counts
	public void setCounts(Integer counts) {
		this.counts = counts;
	}

	public Integer getCounts() {
		return counts;
	}

	// start_time
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getStart_time() {
		return start_time;
	}

	// end_time
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	@Override
	public String toString() {
		return "province：" + province + ",city: " + city + ",key: " + key + ",counts: " + counts + ",start_time: "
				+ start_time + ",end_time: " + end_time;
	}

}
