package com.zkdj.sitems.model;

/*
  * 属地简报中邮件接收人的类
 */
public class DEMail {
	private int id;
	private String email;
	private int userId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "DEMail [id=" + id + ", email=" + email + ", userId=" + userId + "]";
	}

}
