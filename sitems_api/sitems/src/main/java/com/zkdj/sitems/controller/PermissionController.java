package com.zkdj.sitems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zkdj.sitems.model.DPermissionModule;
import com.zkdj.sitems.result.ResultList;
import com.zkdj.sitems.service.PermissionService;

@RestController
@RequestMapping("/permission")
public class PermissionController {
	
	@Autowired
	private PermissionService mPermissionService;
	
	@RequestMapping("/total")
	public ResultList<DPermissionModule> getPermissionData(){
		ResultList<DPermissionModule> mResult = new ResultList<DPermissionModule>();
		List<DPermissionModule> mlist =  mPermissionService.getAllPermissionMessage();
		if(mlist != null){
			mResult.setCode(0);
			mResult.setMessage("查询成功！");
			mResult.setObject(mlist);
		}else{
			mResult.setCode(-1);
			mResult.setMessage("查询权限信息失败！");
		}
		return mResult;
	}

}
