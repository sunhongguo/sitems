package com.zkdj.sitems.bean;

import java.util.List;

/**
 *  查询违规网站列表时返回的结果
 * @author DELL
 *
 */
public class IllegalWebListResult {
    private List<IllegalWeb> mList;
    private int count_unhandle;    // 条数:待处理
    private int count_handled;   // 条数：已处理
    
	public List<IllegalWeb> getmList() {
		return mList;
	}
	public void setmList(List<IllegalWeb> mList) {
		this.mList = mList;
	}
	
	public int getCount_unhandle() {
		return count_unhandle;
	}
	public void setCount_unhandle(int count_unhandle) {
		this.count_unhandle = count_unhandle;
	}
	public int getCount_handled() {
		return count_handled;
	}
	public void setCount_handled(int count_handled) {
		this.count_handled = count_handled;
	}
	
	
}
