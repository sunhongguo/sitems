package com.zkdj.sitems.model;

/**
 * 巡查预警的专题分类
 * @author DELL
 *
 */
public class DAlertCategory {
	private int id;
	private String name;   // 专题分类名称
	private int parent_id; // 父级区域id
	private int sort;      // 排序
	private int isalert;   // 是否预警开关：1表示开；0表示关; 默认开
	private int isshow;    // 是否在首页显示：1表示显示；0表示不显示; 默认显示
	
	private int account_id;   // 该专题的创建帐号id
	
	private String alert_receiver;  // 接受人id数组，转换为字符串保存到数据库
	private String alert_type;      // 接受方式：邮箱或短信，String数组，转换为字符串保存到数据库
	private String begin_time;
	private String end_time;
	
	public DAlertCategory(){
		super();
	}
	
	public DAlertCategory(int id, String name, int parent_id,int sort, int isalert,int isshow,int account_id){
		this.id = id;
		this.name = name;
		this.parent_id = parent_id;
		this.sort = sort;
		this.isalert = isalert;
		this.isshow = isshow;
		this.account_id = account_id;
	}
	
	// id
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return id;
	}
	
	// name
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	
	// parent_id
	public void setParent_id(int parent_id){
		this.parent_id = parent_id;
	}
	public int getParent_id(){
		return parent_id;
	}

	// sort
	public void setSort(int sort) {
		this.sort = sort;
	}
	public int getSort() {
		return sort;
	}
		
	// isalert
	public void setIsalert(int isalert) {
		this.isalert = isalert;
	}
	public int getIsalert() {
		return isalert;
	}
	
	// isshow
	public void setIsshow(int isshow) {
		this.isshow = isshow;
	}
	public int getIsshow() {
		return isshow;
	}
	
	// account_id
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public int getAccount_id() {
		return account_id;
	}
	
	
	
	public String getAlert_receiver() {
		return alert_receiver;
	}

	public void setAlert_receiver(String alert_receiver) {
		this.alert_receiver = alert_receiver;
	}

	public String getAlert_type() {
		return alert_type;
	}

	public void setAlert_type(String alert_type) {
		this.alert_type = alert_type;
	}

	public String getBegin_time() {
		return begin_time;
	}

	public void setBegin_time(String begin_time) {
		this.begin_time = begin_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	@Override
	public String toString(){
		return "id:" + id + ", name: " + name + ", parent_id: " + parent_id + ", isalert: " + isalert
				+ ", isshow: " + isshow + ", sort: " + sort + ", account_id: " + account_id;
	}
}
