import Vue from 'vue'
import Vuex from 'vuex'
import user from './module/user'
import app from './module/app'
import authority from './module/authority'
import role from './module/role'
import menu from './module/menu'

Vue.use(Vuex)
export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        authority,
        role,
        user,
		menu,
        app
    }
})
