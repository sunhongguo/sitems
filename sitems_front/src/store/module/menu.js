export default {
    state: {
		user_manager_select:0,
		home_menu_select:0,
		in_home_pager:true
    },
    mutations: {
        setUserManagerSelect (state, select) {
            state.user_manager_select = select
        },
		setInHomeStatus (state, status) {
		    state.in_home_pager = status
		},
    },
	getters: {
		getUserManagerMenuSelect: state => {
			return state.user_manager_select
		},
		getInHomeStatus:state =>  {
		   return  state.in_home_pager
		}
	  },
    actions: {
        selectUserManagerMenu ({ commit }, select){
            commit('setUserManagerSelect',select)
        },
		setInHomeStatus ({ commit }, status){
		    commit('setInHomeStatus',status)
		}
    }
}