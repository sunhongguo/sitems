export default {
  state: {
	  projectTitle:'属地化网站管理平台'
  },
  getters: {
		getProjectTitle: state => {
		  return state.projectTitle
		} 
  },
  mutations: {
	  setProjectTitle (state, title) {
	      state.projectTitle = title
	  },
  },
  actions: {
	   setProjectTitle ({ commit }, title){
			commit('setProjectTitle',title)
	  },
  }
}
