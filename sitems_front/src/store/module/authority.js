
export default {
    state: {
        authority: [],
    },
    mutations: {
        setAuthority (state, authority_new) {
            state.authority = authority_new
        },
    },
    getters: {
        messageToken:state => state.access_token,
    },
    actions: {
        handleUserAuthority ({ commit }, { authority }){
            commit('setAuthority',authority)
        }
    }
}