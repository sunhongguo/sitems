
import Cookies from 'js-cookie'
import md5 from 'js-md5'

import {
    userLogin
} from '@/api/data'

const state = {
    user_account: '',
    user_contact: '',
    user_id: '',
    user_name: ''
}

const getters = {
    // Filtering currentUser
    currentUser: state => {
        return {
            account: state.user_account,
            contact: state.user_contact,
            name: state.user_name,
            id: state.user_id
        }
    }
}

const mutations = {
    updateData(state, payload) {
        switch (payload.name) {
            case 'account':
                state.user_account = payload.value
                break
            case 'contact':
                state.user_contact = payload.value
                break
            case 'name':
                state.user_name = payload.value
                break
            default:
                window.console.log('Error:Dont directly mutate Vuex store')
        }
    },


    // getLocalUser(state) {
    //     state.user_account = localStorage.getItem('account')
    //     state.user_contact = localStorage.getItem('contact')
    //     state.user_name = localStorage.getItem('name')
    // },


    setUser(state, payload) {
        state.user_account = payload.account
        state.user_contact = payload.contact
        state.user_name = payload.name
        state.user_id = payload.id
    },


    logout(state) {
        Cookies.remove('accessToken')
        localStorage.removeItem('account')
        localStorage.removeItem('contact')
        localStorage.removeItem('user_name')
        localStorage.removeItem('user_id')
        state.user_account = ''
        state.user_contact = ''
        state.user_name = ''
        state.user_id = ''
    }
}

const actions = {
    /**
     * Login
     * new Promise((resolve, reject) => {})
     * Authorization: 'Bearer ' + token
     * email: payload.email
     * pass: payload.pass
     * name: payload.name
     */
    login({commit}, payload) {
        return new Promise((resolve, reject) => {
            userLogin(payload.username, md5(payload.password)).then(res => {
                if(res.data.code == 0){
                    //console.log("登录")
                    //console.log(res)
                    Cookies.set('accessToken', res.data.object.user.token);
                    Cookies.set('user_id', res.data.object.user.id);
                    Cookies.set('user_name', res.data.object.user.user_name);
                    commit({
                        type: 'setUser',
                        account: res.data.object.user.account,
                        contact: res.data.object.user.contact,
                        name: res.data.object.user.user_name,
                        id: res.data.object.user.id
                    })

                    localStorage.setItem('account', res.data.object.user.account)
                    localStorage.setItem('contact', res.data.object.user.contact)
                    localStorage.setItem('user_name', res.data.object.user.user_name)
                    localStorage.setItem('user_id', res.data.object.user.id)
					localStorage.setItem('account_start_time', res.data.object.user.account_begints)
					localStorage.setItem('account_end_time', res.data.object.user.account_endts)
					localStorage.setItem('account_day', res.data.object.user.account_day)


                    localStorage.setItem('rolePermissions', JSON.stringify(res.data.object.role.permissions))

                }
                resolve(res)
            }).catch(err => {
                reject(err)
            })
        })
    },
    /**
     * Register
     * new Promise((resolve, reject) => {})
     * email: payload.email
     * pass: payload.pass
     * name: payload.name
     */
    // register({commit}, payload) {
    //     return new Promise((resolve, reject) => {
    //         request
    //             .post('https://douban.herokuapp.com/user/')
    //             .send({
    //                 email: payload.email,
    //                 pass: payload.pass,
    //                 name: payload.name
    //             })
    //             .then(res => {
    //                 localStorage.setItem('token', res.body.token)
    //                 localStorage.setItem('email', res.body.email)
    //                 localStorage.setItem('name', res.body.name)
    //
    //                 commit({
    //                     type: 'setUser',
    //                     email: res.body.email,
    //                     token: res.body.token,
    //                     name: res.body.name
    //                 })
    //                 resolve(res)
    //             }, err => {
    //                 reject(err)
    //             })
    //     })
    // }
}

export default {
    state,
    getters,
    mutations,
    actions
}