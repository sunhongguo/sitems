export default {
    state: {
        role_info: '',
        role_name: '',
        role_id: ''
    },
    mutations: {
        setRoleinfo (state, role_info) {
            state.role_info = role_info
        },
        setRolename (state, role_name) {
            state.role_name = role_name
        },
        setRoleid (state, role_id) {
            state.role_id = role_id
        },
    },
    getters: {
        messageToken:state => state.access_token,
    },
    actions: {
        handleUserRole ({ commit }, { role }){
            commit('setRole',role)
        }
    }
}