import axios from 'axios'
import Cookies from 'js-cookie'
class HttpRequest {
	getToken(){
		return Cookies.get('accessToken')
	}
  constructor (baseUrl = baseURL) {
    this.baseUrl = baseUrl
    this.queue = {}
  }
  getInsideConfig () {
    const config = {
      baseURL: this.baseUrl
    }
    return config
  }
  destroy (url) {
    delete this.queue[url]
    if (!Object.keys(this.queue).length) {

    }
  }
  interceptors (instance, url) {

    // 请求拦截
    instance.interceptors.request.use(config => {
      this.queue[url] = true
	  var token = this.getToken()
	  if(token){
		config.headers = {
				'Content-Type': 'application/json',
				'sitems_token': token
		}
	  }else{
		 config.headers = {
		 		'Content-Type': 'application/json',
		 }
	  }
      return config
    }, error => {
      return Promise.reject(error)
    })
    // 响应拦截
    instance.interceptors.response.use(res => {
      this.destroy(url)
      const { data, status } = res
      return { data, status }
    }, error => {
      this.destroy(url)
      let errorInfo = error.response
      if (!errorInfo) {
        const { request: { statusText, status }, config } = JSON.parse(JSON.stringify(error))
        errorInfo = {
          statusText,
          status,
          request: { responseURL: config.url }
        }
      }
      return Promise.reject(error)
    })
  }
  request (options) {
    const instance = axios.create()
    options = Object.assign(this.getInsideConfig(), options)
    this.interceptors(instance, options.url)
    return instance(options)
  }

  requestUpload(options,config){
	  const instance = axios.create()
	  options = Object.assign(config, options)
	  this.interceptors(instance, options.url)
	  return instance(options)
  }

}
export default HttpRequest
