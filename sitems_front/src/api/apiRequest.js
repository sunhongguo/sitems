import axios from '@/api/request'
import Cookies from 'js-cookie'

class ApiRequest {
    //***************忘记密码**********************

    //忘记密码：获取验证码
    getPhoneCode(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/login/captcha/send',
            data: datas,
            method: 'post'
        })
    }

    // 忘记密码：验证获取的验证码
    checkPhoneCodeRequest(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/login/captcha/verify',
            data: datas,
            method: 'post'
        })
    }

    // 忘记密码：设置新密码
    setNewPassword(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/login/setpwd',
            data: datas,
            method: 'post'
        })
    }



    getToken() {
        return Cookies.get('accessToken')
    }

    getRoleList() {
        let that = this
        return axios.request({
            url: 'sitems/role/querylist',
            method: 'get'
        })
    }

    getAuthorityList() {
        let that = this
        return axios.request({
            url: 'sitems/permission/total',
            method: 'get'
        })
    }

    createNewRole(datas) {
        let that = this
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/role/create',
            data: datas,
            method: 'post'
        })
    }

    editRole(datas) {
        let that = this
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/role/update',
            data: datas,
            method: 'post'
        })
    }

    deleteRole(id) {
        let that = this
        return axios.request({
            url: 'sitems/role/delete',
            params: {
                role_id: id
            },
            method: 'get'
        })
    }

    getClassifyList() {
        let that = this
        return axios.request({
            url: 'sitems/category/queryall',
            method: 'get'
        })
    }
	
	getClassifyItemCount() {
	    let that = this
	    return axios.request({
	        url: 'sitems/category/queryall',
	        method: 'get'
	    })
	}

    createClassify(datas) {
        let that = this
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/category/add',
            data: datas,
            method: 'post'
        })
    }

    deleteClassify(id) {
        let that = this
        return axios.request({
            url: 'sitems/category/delete',
            params: {
                category_id: id
            },
            method: 'get'
        })
    }

    moveClassify(top, bottom) {
        let that = this
        return axios.request({
            url: 'sitems/category/move',
            params: {
                id1: top,
                id2: bottom
            },
            method: 'get'
        })
    }

    renameClassify(datas) {
        let that = this
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/category/updatename',
            data: datas,
            method: 'post'
        })
    }

    getSiteWebAreaData() {
        let that = this
        return axios.request({
            url: 'sitems/web/regionlist',
            method: 'get'
        })
    }

    addNewWebSite(datas) {
        let that = this
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/web/add',
            data: datas,
            method: 'post'
        })
    }

    editNewWebSite(datas) {
        let that = this
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/web/update',
            data: datas,
            method: 'post'
        })
    }

    deleteWebSite(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/web/delete',
            data: datas,
            method: 'post'
        })
    }

    getWebSiteData(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/web/querylist',
            data: datas,
            method: 'post'
        })
    }

    //批量修改分类
    batchUpdateCategory(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/web/update_bulk_category',
            data: datas,
            method: 'post'
        })
    }

    //批量删除网站
    batchDeletewebsites(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/web/delete_bulk',
            data: datas,
            method: 'post'
        })
    }
	
	//条件导出
	exportWebDataByCondition(datas){
		return axios.request({
		    transformRequest: [function (data) {
		        data = JSON.stringify(data)
		        return data
		    }],
		    url: 'sitems/web/export_querys',
		    data: datas,
		    method: 'post'
		})
	}
	
	//根据id导出
	exportWebDataByIds(datas){
		return axios.request({
		    transformRequest: [function (data) {
		        data = JSON.stringify(data)
		        return data
		    }],
		    url: 'sitems/web/export_ids',
		    data: datas,
		    method: 'post'
		})
	}
	
	exportFile(data){
		return axios.request({
		    url: data,
			responseType: 'blob',
		    method: 'get'
		})
	}
	
	getClassifyWebDataCount(datas){
		return axios.request({
		    transformRequest: [function (data) {
		        data = JSON.stringify(data)
		        return data
		    }],
		    url: 'sitems/web/counts_category',
		    data: datas,
		    method: 'post'
		})
	}
	
	uploadFile(datas,config){
		return axios.requestUpload({
			url: 'sitems/web/excel_upload',
			data: datas,
			method: 'post'
		},config)
		
	}
}

var api = new ApiRequest()
export default api