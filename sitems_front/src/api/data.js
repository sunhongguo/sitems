import axios from '@/api/request'

//1.登录
export const userLogin = (account, password) => {

    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url:'sitems/login',
        //发送格式为json
        data: {
            account: account,
            password: password
        },
        method: 'post'
    })
}


//2.添加用户
export const addUser = (account, user_name, contact, password, role_id, account_endts) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/user/create',
        method: 'post',

        data: {
            account: account,
            user_name: user_name,
            contact: contact,
            password: password,
            role_id: role_id,
            account_endts: account_endts
        },
    })
}

//2.修改用户
export const updateUserMessage = (user_id, account, user_name, contact, password, role_id, account_endts) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/user/update_user',
        method: 'post',

        data: {
            id: user_id,
            account: account,
            user_name: user_name,
            contact: contact,
            password: password,
            role_id: role_id,
            account_endts: account_endts
        },
    })
}

//3.账户详细信息
export const userDetailMessage = (id) => {
    return axios.request({
        url: 'sitems/user/queryid',
        params: {
            id: id
        },
        method: 'get'
    })
}


//4.账户列表获取
export const getUsersList = (page, count) => {
    return axios.request({
        url: 'sitems/user/querylist',
        params: {
            role: '',
            page:page,
            count: count
        },
        method: 'get'
    })
}

//5.账户角色修改
export const changeUserRoletype = (userid, role_id) => {
    return axios.request({
        url: 'sitems/user/update',
        params: {
            userid: userid,
            role_id: role_id
        },
        method: 'get'
    })
}

//6. 修改用户姓名
export const changeUsername = (account, user_name) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/user/update_username',
        method: 'post',

        data: {
            account: account,
            user_name: user_name
        },
    })
}

//7. 修改用户联系方式
export const changeUsercontact = (account, contact) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/user/update_contact',
        method: 'post',

        data: {
            account: account,
            contact: contact
        },
    })
}

//8. 修改用户密码
export const changeUserpassword = (account, oldPassWord, newPassWord) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/user/update_passWord',
        method: 'post',

        data: {
            account: account,
            oldPassWord: oldPassWord,
            newPassWord: newPassWord
        },
    })
}

//9. 账户删除
export const deleteUser = (userid) => {
    return axios.request({
        url: 'sitems/user/delete',
        params: {
            userid: userid
        },
        method: 'get'
    })
}

//10. 添加角色
export const creatRoletype = (name, info, permissions) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/role/create',
        method: 'post',

        data: {
            name: name,
            info: info,
            permissions: permissions
        },
    })
}

//11. 角色列表获取
export const getRolelist = () => {
    return axios.request({
        url: 'sitems/role/querylist',
        params: {},
        method: 'get'
    })
}

//12. 角色权限修改
export const changeRolepermissions = (name, info, permissions) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/role/update',
        method: 'post',

        data: {
            name: name,
            info: info,
            permissions: permissions
        },
    })
}


//13. 角色删除：删除之前一定要提示用户，有相关账户已关联到给角色
export const deleteRoletype = (roleid) => {
    return axios.request({
        url: 'sitems/role/delete',
        params: {
            roleid:roleid
        },
        method: 'get'
    })
}


//16. 获取平台名称
export const getPlatformName = () => {
    return axios.request({
        url: 'sitems/platform',
        params: {},
        method: 'get'
    })
}

//17. 修改平台名称
export const changePlatformName = (Sname) => {
    return axios.request({
        transformRequest: [function(data) {
            data = JSON.stringify(data)
            return data
        }],
        url: 'sitems/update/platform',
        method: 'post',
        data: {
            Sname: Sname
        }
    })
}

//18. 查询用户名（添加用户是，可以判断用户名是否可用）
export const searchAccount = (account) => {
    return axios.request({
        url: 'sitems/user/query_account',
        params: {
            account: account
        },
        method: 'get'
    })
}