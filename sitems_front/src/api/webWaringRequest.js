import axios from '@/api/request'
import Cookies from 'js-cookie'

class webWaringRequest {
    getToken() {
        return Cookies.get('accessToken')
    }
    //预警网站：根据条件，统计数量
    getWebSiteCountBycondition(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/count_term',
            data: datas,
            method: 'post'
        })
    }

    //巡查预警网站：获取列表
    getWebSiteData(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/web_list',
            data: datas,
            method: 'post'
        })
    }

    //违规文章：根据条件，获取违规文章列表
    getViolationWebList(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/get_pagelist',
            data: datas,
            method: 'post'
        })
    }

    // 违规文章：根据id查询单个
    getViolationWebDetail(web_id) {
        return axios.request({
            url: 'sitems/alert/get_pagebyid',
            params: {
                id: web_id,
            },
            method: 'get'
        })
    }

    // 专题分类：获取列表
    warningCategorylist(user_id) {
        return axios.request({
            url: 'sitems/alert/category_list',
            params: {
                account_id: user_id,
            },
            method: 'get'
        })
    }

    // 专题分类：获取列表
    getCategoryDetailContent(subject_id) {
        return axios.request({
            url: 'sitems/alert/get_word_message',
            params: {
                subjectid: subject_id,
            },
            method: 'get'
        })
    }
    //专题分类：添加
    addCategory(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/category_add',
            data: datas,
            method: 'post'
        })
    }
    //专题分类：重命名
    renameCategory(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/category_rename',
            data: datas,
            method: 'post'
        })
    }
    //专题分类：设置是否在首页显示
    showCategory(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/category_show',
            data: datas,
            method: 'post'
        })
    }
    //专题分类：交互顺序
    moveClassify(top, bottom) {
        return axios.request({
            url: 'sitems/alert/category_sort',
            params: {
                firstid: top,
                secondid: bottom
            },
            method: 'get'
        })
    }
    //专题分类：根据id删除
    deleteCategory(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/category_delete',
            data: datas,
            method: 'post'
        })
    }

    //保存预警词
    saveSettingword(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/setting_word',
            data: datas,
            method: 'post'
        })
    }


    //预警接收人：获取列表
    getReceiverList() {
        return axios.request({
            url: 'sitems/alert/receiver_list',
            method: 'get'
        })
    }

    //预警接收人：添加
    addReceiver(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/receiver_add',
            data: datas,
            method: 'post'
        })
    }

    //预警接收人：修改
    updateReceiver(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/receiver_update',
            data: datas,
            method: 'post'
        })
    }

    //预警接收人：根据id删除
    deleteReceiver(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/receiver_delete',
            data: datas,
            method: 'post'
        })
    }

    //Excel导出：预警网站
    exportWarningWebs(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/export_weblist',
            data: datas,
            method: 'post'
        })
    }

    //Excel导出：预警网站，根据ids
    exportWarningWebsByIds(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/export_weblist_ids',
            data: datas,
            method: 'post'
        })
    }

    //Excel导出：违规文章
    exportViolationWebs(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/export_pagelist',
            data: datas,
            method: 'post'
        })
    }

    //Excel导出：违规文章，根据ids
    exportViolationWebsByIds(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/alert/export_pagelist_ids',
            data: datas,
            method: 'post'
        })
    }


    //***************违规协查接口**********************

    //添加协查方案
    addInvestigationPlan(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/illegal/scheme/add',
            data: datas,
            method: 'post'
        })
    }

    //添加协查方案的反馈
    addInvestigationFeedback(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/illegal/scheme/feedback',
            data: datas,
            method: 'post'
        })
    }

    //获取网站列表：根据type类型
    getIllegalWebsData(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/illegal/weblist',
            data: datas,
            method: 'post'
        })
    }
    //统计网站数量：根据城市
    getIllegalWebsCountBycondition() {
        return axios.request({
            url: 'sitems/illegal/count/city',
            method: 'get'
        })
    }

    //获取处理记录
    getProcessingRecords(domainName) {
        return axios.request({
            url: 'sitems/illegal/handle/records',
            params: {
                domainName: domainName,
            },
            method: 'get'
        })
    }

    //Excel导出：预警网站
    exportIlligalWebs(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/illegal/export/weblist_querys',
            data: datas,
            method: 'post'
        })
    }

    //导出数据：根据条件
    exportIlligalWebsByIds(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/illegal/export/weblist_ids',
            data: datas,
            method: 'post'
        })
    }

    //***************简报中心接口**********************

    //违规信息添加到简报
    addToReport(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/report/page/add',
            data: datas,
            method: 'post'
        })
    }

    //邮件接受人：添加
    addReceiverEmail(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/report/email/add',
            data: datas,
            method: 'post'
        })
    }

    //邮件接受人：获取列表
    getReceiverEmailData(user_id) {
        return axios.request({
            url: 'sitems/report/email/list',
            params: {
                userId: user_id,
            },
            method: 'get'
        })
    }
    //邮件接受人：根据id删除
    deleteReceiverEmail(id) {
        return axios.request({
            url: 'sitems/report/email/delete',
            params: {
                id: id,
            },
            method: 'get'
        })
    }

    //简报：生成新简报
    createReport(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/report/create',
            data: datas,
            method: 'post'
        })
    }

    //简报：查询列表
    getReportList(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/report/query_list',
            data: datas,
            method: 'post'
        })
    }

    //简报：根据id查询
    getReportDetail(id) {
        return axios.request({
            url: 'sitems/report/query_id',
            params: {
                id: id,
            },
            method: 'get'
        })
    }

    //简报：根据id删除
    deleteReport(id) {
        return axios.request({
            url: 'sitems/report/delete_id',
            params: {
                id: id,
            },
            method: 'get'
        })
    }

    //简报：下载Word文档
    downloadReport(id) {
        return axios.request({
            url: 'sitems/report/download',
            params: {
                id: id,
            },
            method: 'get'
        })
    }


    //***************首页接口**********************

    //违规概况
    illegalSummary() {
        return axios.request({
            url: 'sitems/firstpage/illegal/message',
            method: 'get'
        })
    }

    //获取违规信息
    getHomeIllegalList(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/firstpage/illegal/page',
            data: datas,
            method: 'post'
        })
    }

    //获取热词：默认30条
    getHomeIllegalHotword(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/firstpage/illegal/hotword',
            data: datas,
            method: 'post'
        })
    }

    //***************发送短信或邮件**********************
    //发送邮件
    sendEmail(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/msg/email',
            data: datas,
            method: 'post'
        })
    }

    //发送短信
    sendMessage(datas) {
        return axios.request({
            transformRequest: [function (data) {
                data = JSON.stringify(data)
                return data
            }],
            url: 'sitems/msg/shortmessage',
            data: datas,
            method: 'post'
        })
    }


}

var api = new webWaringRequest()
export default api