export default [
	  {
		path: '/login',
		name: 'login',
		meta: {
		  title: '欢迎登录',
		},
		component: () => import('@/views/login/login.vue')
	  },

	  {
		path: '/change_password',
		name: 'change_password',
		meta: {
		  title: '修改密码',
		},
		component: () => import('@/views/change_password/change_password.vue')
	  },
	  {
		path: '/',
		name: 'home',
		meta: {
		  title: '首页',
		},
		component: () => import('@/views/home/home.vue'),
	  },
	  {
		path: '/site_manger',
		name: 'site_manger',
		meta: {
		  title: '网站管理',
		},
		 component: () => import('@/views/site_manger/site_manger.vue')
	  },
	  {
		path: '/site_waring',
		name: 'site_waring',
		meta: {
		  title: '网站预警'
		},
		component: () => import('@/views/site_waring/site_waring.vue')
	  },
	  {
		path: '/irregularities_investigation',
		name: 'irregularities_investigation',
		meta: {
		  title: '违规协查'
		},
		component: () => import('@/views/irregularities_investigation/irregularities_investigation.vue')
	  },
	  {
		path: '/screening_events',
		name: 'screening_events',
		meta: {
		  title: '事件排查'
		},
		component: () => import('@/views/screening_events/screening_events.vue')
	  },
	  {
		path: '/brief_report',
		name: 'brief_report',
		meta: {
		  title: '属地简报'
		},
		component: () => import('@/views/brief_report/brief_report')
	  },
	  {
		path: '/user_manager',
		name: 'user_manager',
		meta: {
		  title: '个人中心'
		},
		component: () => import('@/views/user_manager/user_manager.vue')
	  },
	  {
		path: '/create_role',
		name: 'create_role',
		meta: {
		  title: '新建角色'
		},
		component: () => import('@/views/create_role/create_role.vue')
	  },
    {
        path: '/site_waring_setting',
        name: 'site_waring_setting',
        meta: {
            title: '违规网站列表'
        },
        component: () => import('@/views/site_waring_setting/site_waring_setting.vue')
    },
    {
        path: '/violationWebList',
        name: 'violationWebList',
        meta: {
            title: '违规网站列表'
        },
        component: () => import('@/views/violationWebList/violationWebList.vue')
    },
    {
        path: '/violationWebDetail',
        name: 'violationWebDetail',
        meta: {
            title: '违规网站详情'
        },
        component: () => import('@/views/violationWebDetail/violationWebDetail.vue')
    },
    {
        path: '/brief_report_detail',
        name: 'brief_report_detail',
        meta: {
            title: '简报详情'
        },
        component: () => import('@/views/brief_report_detail/brief_report_detail.vue')
    }





]