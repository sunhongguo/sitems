﻿# 网站管理表    

## 1. 目录分类表db_menu：网站分类目录   
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      | 分类id
   menu_name | String | 分类名称
   menu_info | String | 备注 | 分类的备注信息
   parent_id | int | 父级分类的id | 该分类的上一级
   depth     | int  | 深度    | 表示第几级
   order     | String | 顺序 | 下一级的id，按顺序保存，中间用“&”分隔
   create_time | Long | 创建时间 | 创建时间戳

## 2.网站分类表