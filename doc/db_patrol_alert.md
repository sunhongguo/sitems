﻿# 巡查预警表    

## 1. 专题分类表db_alert_category
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      | 专题分类id
   name | String | 专题名称
   parent_id | int | 父级ID 
   sort | int | 顺序
   isalert | int |是否预警开关   | 1表示开；0表示关
   isshow  | int | 是否在首页显示 | 1表示显示；0表示不显示
   
## 2. 预警接收人表db_alert_receiver  
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      | 接收人id
   name | String | 接收人名称
   email | String | 接收人邮箱 
   phone | String | 接收人电话 




