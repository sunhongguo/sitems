﻿# 表结构设计

## 1. 网站分类

### 网站分类表 db_site_category
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  分类唯一id    |  分类唯一id，自增    
   category_name | String | 分类名称 | 分类名称
   parent_id | int | 上级id | 所属分类，-1 表示为顶层分类
   sort| int | 同级排序 | 同一级排序，默认为分类id
   




