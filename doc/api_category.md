﻿# 网站管理API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```

### 1. 网站分类创建      
#### 接口地址URL    
POST   /sitems/category/add
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  parent_id        |int | 上级分类id  | 必须，-1 表示顶层分类
  category_name    |String | 分类名称  | 必须
  
#### 请求示例
```python
{
   "parent_id":3,
   "category_name":"分类33"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "创建成功！",
    "object": {
        "id": 9,
        "category_name": "分类33",
        "parent_id": 3,
        "sort": 0
    }
}
```
### 2.分类重命名
#### 接口地址URL
POST   /sitems/category/updatename
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id	|  String |	分类id
  category_name|  String |	分类名称 

#### 请求示例
```python
{
   "id":2,
   "category_name":"分类222xxxxx"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "重命名分类成功！",
    "object": {
        "id": 2,
        "category_name": "分类222xxxxx",
        "parent_id": 0,
        "sort": 0
    }
}
```

### 3.网站分类查询
#### 接口地址URL
GET   /sitems/category/queryall
#### 请求参数说明：参数已json形式上传
   
#### 返回示例
```python
{
    "code": 0,
    "message": "查询成功",
    "object": [
        {
            "id": 1,
            "parent_id": -1,
            "sort": 0,
            "title": "分类1",
            "children": [
                {
                    "id": 4,
                    "parent_id": 1,
                    "sort": 0,
                    "title": "分类11",
                    "children": null
                }
            ]
        },
        {
            "id": 2,
            "parent_id": -1,
            "sort": 1,
            "title": "分类222xxxxx",
            "children": [
                {
                    "id": 5,
                    "parent_id": 2,
                    "sort": 0,
                    "title": "分类21",
                    "children": null
                },
                {
                    "id": 6,
                    "parent_id": 2,
                    "sort": 0,
                    "title": "分类22",
                    "children": null
                }
            ]
        },
        {
            "id": 3,
            "parent_id": -1,
            "sort": 0,
            "title": "分类3xxxxxx",
            "children": [
                {
                    "id": 7,
                    "parent_id": 3,
                    "sort": 0,
                    "title": "分类31",
                    "children": null
                },
                {
                    "id": 8,
                    "parent_id": 3,
                    "sort": 0,
                    "title": "分类32",
                    "children": null
                },
                {
                    "id": 9,
                    "parent_id": 3,
                    "sort": 0,
                    "title": "分类33",
                    "children": null
                }
            ]
        }
    ]
}
```

### 4.分类删除
#### 接口地址URL
get   /sitems/category/delete?category_id=2

#### 返回参数
```python
{
    "code": 0,
    "message": "分类删除成功",
    "object": null
}
```

### 5.分类移动 (同级分类)
#### 接口地址URL
get   /sitems/category/move?id1=13&id2=14

#### 返回参数
```python
{
    "code": 0,
    "message": "success",
    "errmsg": null,
    "object": null
}
```

