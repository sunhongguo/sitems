﻿# 首页API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```

### 1.获取违规信息
#### 接口地址URL
POST   /sitems/firstpage/illegal/page
#### 请求参数说明
  参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  page_no    |  int |	页数  | 必须
  page_size   |  int |  条数  |  必须
  subjectIds  |  List《String》 |	专题ids | 必须
  start_time  |  String |  开始时间  |  必须
  end_time     |  String |	接受时间  | 必须
   
#### 请求示例
```python
{
	"subjectIds":["2","3"],
	"start_time":"2019-09-12 00:00:00",
	"end_time":"2019-10-15 00:00:00"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": [],
        "code": "0",
        "count": "0",
        "page": "1",
        "maxPage": "0"
    }
}
```

### 2.违规概况
#### 接口地址URL
GET   /sitems/firstpage/illegal/message
#### 请求参数说明
   无参数
   
#### 请求示例
```python
http://localhost:8080/sitems/firstpage/illegal/message
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "count_illegal_webs": 0,
        "count_illegal_webs_Handled": 0,
        "count_level_A": 0,
        "count_level_B": 0,
        "count_level_C": 0
    }
}
```

### 3.获取热词：默认30条
#### 接口地址URL
POST   /sitems/firstpage/illegal/hotword
#### 请求参数说明
  参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  start_time  |  String |  开始时间  |  必须
  end_time     |  String |	接受时间  | 必须
   
#### 请求示例
```python
{
	"start_time":"2019-09-12 00:00:00",
	"end_time":"2019-10-15 00:00:00"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": {},
        "code": "0"
    }
}
```




