﻿# 网站管理API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token

获取网站列表：其中category为空是全部分类；category="0000"为默认分类即没有分类的网站
```

### 1. 获取城市区域列表   
#### 接口地址URL    
GET   /sitems/web/regionlist
#### 请求参数说明
   无参数，默认获取湖南省所有市的列表
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "name": "湖南省",
        "id": 1,
        "object": [
            {
                "id": 2,
                "name": "长沙市",
                "parent_id": 1
            },
            {
                "id": 3,
                "name": "株洲市",
                "parent_id": 1
            },
           。。。。。。。。
            {
                "id": 13,
                "name": "怀化市",
                "parent_id": 1
            },
            {
                "id": 14,
                "name": "娄底地区",
                "parent_id": 1
            },
            {
                "id": 15,
                "name": "湘西土家族苗族自治州",
                "parent_id": 1
            }
        ]
    }
}
```

### 2. 网站列表获取      
#### 接口地址URL    
POST   /sitems/web/querylist
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  page_no         |int  | 页数  | 非必须
  page_size       |int  | 条数  | 非必须
  searchField	  | String	|查询字段 | 枚举值:siteName和contact;siteName 网站名称;contact 联系人
  searchValue	|	String |	搜索条件
  province        |String | 所在省份  | 必须
  city            |String | 所在城市，默认全部  | 非必须
  organizerNature |String | 主办单位  | 非必须
  category        |String | 网站分类  | 非必须；"0000"为默认分类
  level           |String | 等级  | 非必须 枚举值: A B C 默认全部
  sortField	 |	String |排序字段 | 支持Index中除siteName和contact之外的所有字段
  sortType	|	String	| 排序方式 | 枚举值：asc和desc

#### 请求示例
```python
{
	"searchField": "contact",
	"searchValue": "james",
	"province": "安徽",
	"city": "黄山",
	"level": "A",
	"organizerNature": 2000,
	"category": "1001"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": [
            {
                "icpNo": "闽ICP备09026995号-4",
                "city": "龙岩",
                "level": "C",
                "organizerNature": "企业",
                "registerTime": "2015-10-09T00:00:00.000Z",
                "dns": "dns10.hichina.com dns9.hichina.com",
                "siteName": "抽奖工坊",
                "domainNameStatus": 0,
                "domainNameServer": "grs-whois.hichina.com",
                "prScore": 0,
                "province": "福建省",
                "phone": "95187",
                "organizer": "龙岩市乐智网络科技有限公司",
                "contact": "",
                "domainName": "drawdiy.com",
                "gatherTime": "2019-08-16T15:30:26.000Z",
                "endTime": "2019-10-09T00:00:00.000Z",
                "id": "ffc3699630d1513d311e26484fd7a2d9",
                "category": [],
                "email": "DomainAbuse@service.aliyun.com",
                "register": "Alibaba Cloud Computing (Beijing) Co., Ltd"
            },
            {
                "icpNo": "闽ICP备13005200号-1",
                "city": "龙岩",
                "level": "C",
                "organizerNature": "企业",
                "registerTime": "2016-05-26T00:00:00.000Z",
                "dns": "jm1.dns.com jm2.dns.com",
                "siteName": "龙岩蓝海装饰工程有限公司",
                "domainNameStatus": 0,
                "domainNameServer": "whois.paycenter.com.cn",
                "prScore": 0,
                "province": "福建省",
                "phone": "*****27926",
                "organizer": "龙岩蓝海装饰工程有限公司",
                "contact": "",
                "domainName": "0597lhzs.com",
                "gatherTime": "2019-08-16T15:34:01.000Z",
                "endTime": "2020-05-26T00:00:00.000Z",
                "id": "ff6293536b1b74e2be6c796ef3d16500",
                "category": [],
                "email": "supervision@xinnet.com",
                "register": "Xin Net Technology Corporation"
            },
            {
                "icpNo": "闽ICP备16030715号-1",
                "city": "龙岩",
                "level": "C",
                "organizerNature": "企业",
                "registerTime": "2016-08-02T00:00:00.000Z",
                "dns": "NS1.EXMETIS.COM NS2.EXMETIS.COM",
                "siteName": "友佳财税咨询有限公司",
                "domainNameStatus": 9,
                "domainNameServer": "whois.oray.com",
                "prScore": 0,
                "province": "福建省",
                "phone": "",
                "organizer": "龙岩市友佳财税咨询有限公司",
                "contact": "",
                "domainName": "lyyjcs.com",
                "gatherTime": "2019-08-16T15:33:30.000Z",
                "endTime": "2020-08-02T00:00:00.000Z",
                "id": "fe8488a4d24c00bfd3b385d4e0503383",
                "category": [],
                "email": "",
                "register": "Shanghai Best Oray Information S&T Co., Ltd"
            }
        ],
        "code": "0",
        "count": "178",
        "page": "1",
        "maxPage": "60"
    }
}
```

### 3.添加网站
#### 接口地址URL
POST   /sitems/web/add
#### 请求参数说明：参数已json形式上传，id由服务器端添加
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  domainName	|  String |	域名
  organizer|  String |	主办单位名称
  organizerNature  |  String | 	单位性质，枚举类型，详见附1
  icpNo  |  String |	网站备案许可证号
  siteName  |  String |	网站名称
  email  |  String |	联系邮箱
  phone  |  String |	联系电话
  register  |  String |	注册商
  contact  |  String |	联系人
  registerTime  |  String |	网站创建时间
  endTime |  String |	网站过期时间
  gatherTime |  String |	网站信息采集时间
  domainNameServer |  String |	域名服务器
  domainNameStatus |  String |	网站状态，枚举类型：正常  0，禁止  1，其它  9
  dns	 |  String | DNS
  prScore	 |  int | 网站pr值，0-9
  level	|  String | 网站级别，枚举类型A  B  C
  province |  String |	网站所属省份
  city  |  String | 	网站所属地级市
  id	|  String | 网站信息id
  category |  List<String> |	网站信息分类

#### 请求示例
```python
{
	"category": ["1002"],
	"city": "合肥",
	"contact": "REDACTEDFORPRIVACY",
	"dns": "ns3.myhostadmin.net ns4.myhostadmin.net",
	"domainName": "laibopeixun.com",
	"domainNameServer": "whois.west.cn",
	"domainNameStatus": 0,
	"email": "",
	"endTime": "2021-07-11T00:00:00.000Z",
	"gatherTime": "2019-08-05T16:21:28.000Z",
	"icpNo": "皖ICP备19013851号-1",
	"id": "579496772c2c4868d510fdca1936747c",
	"level": "C",
	"organizer": "合肥莱博教育咨询有限公司",
	"organizerNature": 2001,
	"phone": "",
	"prScore": 0,
	"province": "安徽",
	"register": "Chengdu west dimension digital technology Co., LTD",
	"registerTime": "2019-07-11T00:00:00.000Z",
	"siteName": "莱博培训"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "添加新网站成功！",
    "errmsg": null,
    "object": null
}
```

### 4.网站信息修改
#### 接口地址URL
POST   /sitems/web/update
#### 请求参数说明：参数已json形式上传
   详情见（2.添加网站）中参数说明
#### 请求示例
```python
{
	"category": ["1002"],
	"city": "合肥",
	"contact": "REDACTEDFORPRIVACY",
	"dns": "ns3.myhostadmin.net ns4.myhostadmin.net",
	"domainName": "laibopeixun.com",
	"domainNameServer": "whois.west.cn",
	"domainNameStatus": 0,
	"email": "",
	"endTime": "2021-07-11T00:00:00.000Z",
	"gatherTime": "2019-08-05T16:21:28.000Z",
	"icpNo": "皖ICP备19013851号-1",
	"id": "579496772c2c4868d510fdca1936747c",
	"level": "C",
	"organizer": "合肥莱博教育咨询有限公司",
	"organizerNature": 2001,
	"phone": "",
	"prScore": 0,
	"province": "安徽",
	"register": "Chengdu west dimension digital technology Co., LTD",
	"registerTime": "2019-07-11T00:00:00.000Z",
	"siteName": "莱博培训"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "修改网站成功！",
    "errmsg": null,
    "object": null
}
```

### 5.批量修改分类
#### 接口地址URL
POST   /sitems/web/update_bulk_category
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  ids   |  List<String>  |  多个网站ids
  category_ids   |  List<String>  |  分类ids:包括父级、子级
#### 请求示例
```python
{
	"ids":["f95e630938a9219f9ec0ec314cd455c7","f8dfa54b236c2522659959cac03bf069"],
	"category_ids":["1","4"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "批量修改网站分类成功！",
    "errmsg": null,
    "object": null
}
```

### 6.网站删除
#### 接口地址URL
POST   /sitems/web/delete
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id   |  String  |  网站信息id
#### 请求示例
```python
{
  "id": " 579496772c2c4868d510fdca19367e32 "
｝
```
#### 返回参数
```python
{
    "code": 0,
    "message": "删除网站成功！",
    "errmsg": null,
    "object": null
}
```

### 7.批量网站删除
#### 接口地址URL
POST   /sitems/web/delete_bulk
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  ids   |  String  |  网站信息ids
#### 请求示例
```python
{
	"ids": ["c95965fa76f90493be0d947a20ffd941",
            "00d0a6d6d2f3185552d5f4bc61207290"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "批量删除网站成功！",
    "errmsg": null,
    "object": null
}
```

### 8.网站数据统计
#### 接口地址URL
POST   /sitems/web/counts_all
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  province        |String | 所在省份  | 必须
  city            |String | 所在城市，默认全部  | 非必须
#### 请求示例
```python
{
	"province": "湖南省",
	"city": "长沙市"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取网站数据统计成功！",
    "errmsg": null,
    "object": {
        "all": 2
    }
}
```

### 9.查询不同分类下网站数量
#### 接口地址URL
POST   /sitems/web/counts_category
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  searchField	  | String	|查询字段 | 枚举值:siteName和contact;siteName 网站名称;contact 联系人
  searchValue	|	String |	搜索条件
  province        |String | 所在省份  | 必须
  city            |String | 所在城市，默认全部  | 非必须
  organizerNature |String | 主办单位  | 非必须
  category        |String | 网站分类  | 非必须
  level           |String | 等级  | 非必须 枚举值: A B C 默认全部
#### 请求示例
```python
{
	"searchField": "contact",
	"searchValue": "james",
	"province": "安徽",
	"city": "黄山",
	"level": "A",
	"organizerNature": 2000,
	"category": "1001"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取网站数据统计成功！",
    "errmsg": null,
    "object": {
        "1": 1,
        "4": 2,
        "11": 2,
        "22": 1,
        "25": 3,
        "29": 3,
        "32": 3,
        "undefined": 1
    }
}
```

### 10.导出：根据选择的ids
#### 接口地址URL
POST   /sitems/web/export_ids
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  ids	  | List<String>	|  网站ids | 所选的网站id组合
#### 请求示例
```python
{
	"ids":["f95e630938a9219f9ec0ec314cd455c7","f8dfa54b236c2522659959cac03bf069"]
}
```
#### 返回参数：object中为excel表格的地址
```python
{
    "code": 0,
    "message": "生成网站信息Excel表格！",
    "errmsg": null,
    "object": "http://18.18.154.148:8080/file/excel?excelname=Excel_1567067470160.xls"
}
```

### 11.导出：根据条件
#### 接口地址URL
POST   /sitems/web/export_querys
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  searchField	  | String	|查询字段 | 枚举值:siteName和contact;siteName 网站名称;contact 联系人
  searchValue	|	String |	搜索条件
  province        |String | 所在省份  | 必须
  city            |String | 所在城市，默认全部  | 非必须
  organizerNature |String | 主办单位  | 非必须
  category        |String | 网站分类  | 非必须
  level           |String | 等级  | 非必须 枚举值: A B C 默认全部
#### 请求示例
```python
{
	"searchField": "contact",
	"searchValue": "james",
	"province": "安徽",
	"city": "黄山",
	"level": "A",
	"organizerNature": 2000,
	"category": "1001"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成网站信息Excel表格！",
    "errmsg": null,
    "object": "http://18.18.154.148:8080/file/excel?excelname=Excel_1567067470160.xls"
}
```

### 12.导入Excel表格，添加网站列表
#### 接口地址URL
POST   /sitems/web/excel_upload
#### 请求参数说明：form-data形式
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  filename	  | File	|  文件 |  excel文件
#### 请求示例

#### 返回参数
```python
{
    "code": 0,
    "message": "批量添加新网站成功！",
    "errmsg": null,
    "object": null
}
```

### 13.查询进度：Excel导入进度
#### 接口地址URL
GET   /sitems/web/excel_percent
#### 请求参数说明
   无参数
#### 返回参数
```python
{
    "code": 0,
    "message": "获取Excel导入进度成功！",
    "errmsg": null,
    "object": {
        "percent": 5.43
    }
}
```

