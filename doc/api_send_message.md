﻿# 发送邮件、短信API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```

### 1.发送邮件
#### 接口地址URL
POST   /sitems/msg/email
#### 请求参数说明
  参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  fromName    |  String |	发送者  | 必须
  emails   |  Strings |  接收人邮箱，多个时用“逗号”隔开  |  必须
  title  |  String |	标题 | 必须
  content  |  String |  内容  |  必须
   
#### 请求示例
```python
{
	"fromName":"sun",
	"emails":"476717110@qq.com,373812558@qq.com",
	"title":"邮件标题",
	"content":"预警邮件内容1452"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "发送邮件成功！",
    "errmsg": null,
    "object": null
}
```    

### 2.发送短信
#### 接口地址URL
POST   /sitems/msg/shortmessage
#### 请求参数说明
  参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  phones   |  Strings |  接收人手机号，多个时用“逗号”隔开  |  必须
  content  |  String |  内容  |  必须
   
#### 请求示例
```python
{
	"phones":"15996313709,18602540508",
	"content":"预警短信测试123！"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "发送短信成功！",
    "errmsg": null,
    "object": null
}
```



