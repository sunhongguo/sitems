﻿# 违规协查API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```

### 1. 统计网站数量：根据城市
#### 接口地址URL
GET   /sitems/illegal/count/city
#### 请求参数说明：参数已json形式上传
   无参数
  
#### 请求示例
```python
http://localhost:8080/sitems/illegal/count/city
```
#### 返回参数
```python
{
    "code": 0,
    "message": "统计地区网站数量成功！",
    "object": [
        {
            "city": "长沙市",
            "count": 2
        },
        {
            "city": "株洲市",
            "count": 3
        },
        {
            "city": "湘潭市",
            "count": 0
        },
        。。。。。。
        {
            "city": "娄底地区",
            "count": 0
        },
        {
            "city": "湘西土家族苗族自治州",
            "count": 0
        }
    ]
}
```

### 2.添加协查方案
#### 接口地址URL
POST   /sitems/illegal/scheme/add
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  webId	 |  String |	原网站的id  | 必须
  domainName  |  String |  域名   |  必须
  organizerNature | String |  主办单位  
  illegalCount | int |    违规数量
  level     |    String |  等级
  city    |  String |    城市
  province  |  String |   省份
  subject | int |       专题
  siteName | String |   网站名称
  contact |  String |   负责人
  phone  | String |     电话
  status | int |         状态
  categoryTitle | String |   网站分类 | 直接填分类名称
  scheme | String |  协查方案   |  必须
  
#### 请求示例
```python
{
    "illegalCount": 10,
    "city": "",
    "level": "C",
    "organizerNature": "个人",
    "subject": 177,
    "siteName": "个人技术123",
    "province": "",
    "phone": "",
    "contact": "山黎明",
    "domainName": "51xiancheng.com",
    "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
    "categoryTitle": "分类02",
    "status": 0,
    "scheme":"协查方案0002"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "设置协查方案成功！",
    "errmsg": null,
    "object": {
        "id": 2,
        "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
        "domainName": "51xiancheng.com",
        "organizerNature": "个人",
        "illegalCount": 10,
        "level": "C",
        "city": "",
        "province": "",
        "subject": 177,
        "siteName": "个人技术123",
        "contact": "山黎明",
        "phone": "",
        "status": 0,
        "categoryTitle": "分类02",
        "createTime": 1569573498622,
        "scheme": "协查方案0002",
        "feedBack": null,
        "userId": 0
    }
}
```

### 3.添加协查方案的反馈
#### 接口地址URL
POST   /sitems/illegal/scheme/feedback
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
   id	 |  int |	id  |  违规网站在待处理列表中的id，必须
  feedBack  |  String |  协查反馈   |  必须
  userId | int |  反馈的用户id    | 必须 
  
#### 请求示例
```python
{
	"id": 1,
	"feedBack":"反馈001",
	"userId":3
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "设置协查反馈成功！",
    "errmsg": null,
    "object": null
}
```

### 4. 获取网站列表：根据type类型
#### 接口地址URL
POST   /sitems/illegal/weblist
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
   type   | String  | 类型 | 必须：已处理“handled”;未处理“unhandle”
   page	 |  int |	第几页  |  从1开始，非必须，默认为第一页
  count  |  int |  每页条数   |  非必须，默认15条
  organizerNature | String |  主办单位   | 非必须 
  level | String |  等级   | 非必须 
  city | String |  地区   | 非必须 
  searchValue | String  | 网站名称 | 非必须，搜索条件，模糊搜索
  
#### 请求示例
```python
{
	"searchValue":"技术",
	"level": "C",
	"type":"unhandle"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询违规网站列表成功！",
    "errmsg": null,
    "object": {
        "mList": [
            {
                "id": 6,
                "domainName": "sina.com.cn",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "C",
                "city": "",
                "province": null,
                "siteName": "个人技术文章",
                "contact": "山黎明",
                "phone": "",
                "categoryTitle": "未分类",
                "handleTime": 0,
                "scheme": "是发v和巴萨两"
            },
            {
                "id": 2,
                "domainName": "51xiancheng.com",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "C",
                "city": "长沙市",
                "province": null,
                "siteName": "个人技术123",
                "contact": "山黎明",
                "phone": "",
                "categoryTitle": "分类02",
                "handleTime": 1569573918492,
                "scheme": "协查方案0002"
            }
        ],
        "count_unhandle": 2,
        "count_handled": 0
    }
}
```

### 5. 获取处理记录
#### 接口地址URL
GET   /sitems/illegal/handle/records
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  domainName | String |  域名   | 必须 
  
#### 请求示例
```python
http://localhost:8080/sitems/illegal/handle/records?domainName=51xiancheng.com
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询处理记录列表成功！",
    "object": [
        {
            "id": 1,
            "domainName": "51xiancheng.com",
            "count": 0,
            "scheme": "协查方案0001",
            "feedBack": "反馈001",
            "createTime": 1569573918459,
            "userId": 3,
            "userName": "sun5555"
        }
    ]
}
```
### 6. 导出数据：根据条件
#### 接口地址URL
POST  /sitems/illegal/export/weblist_querys
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
   type   | String  | 类型 | 必须：已处理“handled”;未处理“unhandle”
  organizerNature | String |  主办单位   | 非必须 
  level | String |  等级   | 非必须 
  city | String |  地区   | 非必须 
  
#### 请求示例
```python
{
	"type":"handled"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成Excel表格成功！",
    "errmsg": null,
    "object": "sitems/file/excel?excelname=Excel_IllegalWeb_1569829011115.xlsx"
}
```

### 7. 导出数据：根据ids
#### 接口地址URL
POST   /sitems/illegal/export/weblist_ids
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
   type   | String  | 类型 | 必须：已处理“handled”;未处理“unhandle”
  ids | list.Integer |  id数组   | 必须 
  
#### 请求示例
```python
{
	"type":"unhandle",
	"ids":[4,5]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成Excel表格成功！",
    "errmsg": null,
    "object": "sitems/file/excel?excelname=Excel_IllegalWeb_1569829011115.xlsx"
}
```

