﻿# 表结构设计

## 1. 个人中心

### 系统配置表 db_system
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      
   platform | String | 平台名称 | 系统配置信息
   
### 用户表 db_user
  字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      
   role_id |  int |  角色id    |  账户关联角色，唯一的
   account |  String   | 用户帐号       | 登录账户，唯一
   user_name |  String   | 用户姓名       
   password |  String   | 用户密码       | md5 加密密码
   contact |  String | 联系方式     | 联系手机号
   avatar |  String | 头像url     
   token   |  String | accessToken认证
   account_begints | long | 账号开始时间 | 账号开始时间戳
   account_endts | long | 账号结束时间 | 账号结束时间戳
   create_time | Long | 创建时间 | 创建时间戳
   
### 角色表 db_role
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      
   role_name | String | 角色名称 | 角色名称
   create_time | Long | 创建时间 | 创建时间戳
   role_info | String | 备注信息 | 备注信息
   permissions| Json  | 权限数据  | 角色关联的所有权限数据，保存为Json格式

### 权限模块表db_permission_module
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id  
   parent_id | int | 父级模块id
   module_name | String | 模块名称
   module_info | String | 备注 | 模块的备注信息
   
### 权限表db_permission
   字段 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id   |  int   |  id      
   permission_name | String | 权限名称
   permission_info | String | 备注 | 权限的备注信息
   module_id | int | 权限模块id | 权限所属的模块
   create_time | Long | 创建时间 | 创建时间戳




