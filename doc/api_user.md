﻿# 用户API
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```

### 1. 账户登录
#### 接口地址URL
POST   /sitems/login
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account  | String   | 账户   | 必须，唯一的
  password  | String   | 密码  | MD5加密
#### 请求示例：
```python
{
	"account":"00001",
	"password":"e10adc3949ba59abbe56e057f20f883e"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "登陆成功",
    "object": {
        "user": {
            "id": 1,
            "account": "00001",
            "user_name": "user01",
            "contact": "15999666123",
            "token": "000011565169600621",
            "role_id": 0,
            "account_begints": 0,
            "account_endts": 0,
            "create_time": 0
        },
        "role": {
            "id": 1,
            "role_name": "管理员",
            "create_time": "2019-08-08 14:17",
            "role_info": "创建管理员角色",
            "permissions": [
                {
                    "id": 1,
                    "parent_id": 0,
                    "module_name": "网站管理",
                    "module_info": "网站管理权限模块",
                    "status": 1,
                    "moduleList": null,
                    "permissionList": [
                        {
                            "id": 1,
                            "module_id": 1,
                            "permission_name": "添加",
                            "permission_info": "网站管理-添加",
                            "status": 1,
                            "create_Time": 1565076127
                            。
                            。
                            。
    }
}
```

### 2. 忘记密码：获取验证码
#### 接口地址URL
POST  /sitems/login/captcha/send
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account  | String   | 账户   | 必须，唯一的
  contact  | String   | 电话号码  | 必须
#### 请求示例：
```python
{
	"account":"00002",
	"contact":"15996313709"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "发送短信成功！",
    "errmsg": null,
    "object": null
}
```

### 3. 忘记密码：验证获取的验证码
#### 接口地址URL
POST   /sitems/login/captcha/verify
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account  | String   | 账户   | 必须，唯一的
  code  | String   | 验证码  | 必须
#### 请求示例：
```python
{
	"account":"00002",
	"code":"665566"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "验证通过！",
    "errmsg": null,
    "object": null
}
```

### 3. 忘记密码：设置新密码
#### 接口地址URL
POST   /sitems/login/setpwd
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account  | String   | 账户   | 必须，唯一的
  password  | String   | 密码  | MD5加密
#### 请求示例：
```python
{
	"account":"00001",
	"password":"e10adc3949ba59abbe56e057f20f883e"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "设置新密码成功！",
    "errmsg": null,
    "object": null
}
```
  
### 4. 账户添加
#### 接口地址URL
POST   /sitems/user/create
#### 请求参数说明:参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account  | String   | 账号  | 必须，登录的账号
  user_name  | String   | 用户名  | 必须
  contact  | String   | 联系方式 | 非必须
  password  | String   | 密码  | 必须 MD5加密
  role_id  | int   | 角色id   | 必须，对应唯一的角色 
  account_begints | Long | 账号开始时间 | 非必须
  account_endts | Long | 账号结束时间  | 非必须
#### 请求示例：
```python
    ｛
    "account":"00003",
	"user_name":"sun1233",
	"password":"e10adc3949ba59abbe56e057f20f883e",
	"contact":"1234566",
	"role_id":2
    ｝
```
  
#### 返回数据
```python
{
    "code": 0,
    "message": "创建成功！",
    "object": {
        "id": 3,
        "account": "00003",
        "user_name": "sun1233",
        "password": "e10adc3949ba59abbe56e057f20f883e",
        "contact": "1234566",
        "avatar": "",
        "token": null,
        "role_id": 2,
        "account_begints": null,
        "account_endts": null,
        "create_time": 1565168659554
    }
}
```

### 5. 账户修改
#### 接口地址URL
POST   /sitems/user/update_user
#### 请求参数说明:参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id     |  int  |  帐号id   | 必须
  account  | String   | 账号  | 必须，登录的账号
  user_name  | String   | 用户名  | 必须
  contact  | String   | 联系方式 | 非必须
  password  | String   | 密码  | 必须 MD5加密
  role_id  | int   | 角色id   | 必须，对应唯一的角色 
  account_begints | Long | 账号开始时间 | 非必须
  account_endts | Long | 账号结束时间  | 非必须
#### 请求示例：
```python
    ｛
    "id":3
    "account":"00003",
	"user_name":"sun1233",
	"password":"e10adc3949ba59abbe56e057f20f883e",
	"contact":"1234566",
	"role_id":2
    ｝
```
#### 返回数据
```python
{
    "code": 0,
    "message": "修改成功！",
    "object": {
        "id": 4,
        "account": "00005",
        "user_name": "sun5555",
        "contact": "1234566",
        "token": null,
        "role_id": 2,
        "account_begints": null,
        "account_endts": null,
        "create_time": 1565334304331
    }
}
```
  
### 6. 账户详细信息
#### 接口地址URL
GET   /sitems/user/queryid
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id  | int   | 用户id  | 必须
#### 请求示例
```python
http://{ip}:8080/sitems/user/queryid?id=1
```
#### 返回数据
```python
{
    "code": 0,
    "message": "查询成功！",
    "object": {
        "id": 1,
        "account": "00001",
        "passWord": "123456",
        "avatar": "",
        "token": "",
        "role_id": 0,
        "account_begints": 0,
        "account_endts": 0,
        "create_time": 0,
        "userName": "user01",
        "phone": "15999666123"
    }
}
```

### 7. 账户查询：根据account
#### 接口地址URL
GET   /sitems/user/query_account
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account  | String   | 帐号  | 必须
#### 请求示例
```python
http://{ip}:8080/sitems/user/query_account?account=00001
```
#### 返回数据
```python
{
    "code": 0,
    "message": "查询成功！",
    "object": {
        "id": 0,
        "account": "00001",
        "user_name": "果果",
        "contact": "18888886666",
        "token": null,
        "role_id": 1,
        "account_begints": 0,
        "account_endts": 0,
        "create_time": 0
    }
}
```

### 8. 账户列表获取
#### 接口地址URL
GET   /sitems/user/querylist
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  page  | int   |  页数  | 非必须
  count | Int   | 条数   | 非必须
#### 返回数据
```python
{
    "code": 0,
    "message": "查询用户列表成功！",
    "object": [
        {
            "id": 0,
            "account": "00001",
            "user_name": "user01",
            "contact": "15999666123",
            "token": null,
            "role_id": 0,
            "account_begints": 0,
            "account_endts": 0,
            "create_time": 0
        },
        {
            "id": 0,
            "account": "00002",
            "user_name": "user02",
            "contact": "14777888454",
            "token": null,
            "role_id": 0,
            "account_begints": 0,
            "account_endts": 0,
            "create_time": 0
        }
    ]
}
```

### 9. 修改用户姓名
#### 接口地址URL
POST   /sitems/user/update_username
#### 请求参数说明
参数名 |  类型  | 描述   |  备注  
:--------: | :-----:  | :----:| :-----: 
  account  | String   |   账号名称
  user_name  | String   |   用户姓名
#### 请求示例
```python
post  http://{ip}:8080/sitems/user/update_username
```
#### 返回结果
```python 
{
  "code": 0,
  "message": "修改用户姓名成功！",
  "object": {
    "id": 0,
    "account": "1",
    "user_name": "chenlin",
    "passWord": "njqy1234",
    "contact": "18602540508",
    "avatar": "123",
    "token": "123",
    "role_id": 1,
    "account_begints": 1,
    "account_endts": 2,
    "create_time": 3
  }
}
```

### 10. 修改用户联系方式
#### 接口地址URL
POST   /sitems/user/update_contact
#### 请求参数说明
参数名 |  类型  | 描述   |  备注  
:--------: | :-----:  | :----:| :-----: 
  account  | String   |   账号名称
  contact  | String   |   用户联系方式
#### 请求示例
```python
post  http://{ip}:8080/sitems/user/update_contact
```
#### 返回结果
```python 
{
  "code": 0,
  "message": "修改联系方式成功！",
  "object": {
    "id": 0,
    "account": "1",
    "user_name": "chenlin",
    "passWord": "njqy1234",
    "contact": "15195926666",
    "avatar": "123",
    "token": "123",
    "role_id": 1,
    "account_begints": 1,
    "account_endts": 2,
    "create_time": 3
  }
}
```

### 11. 修改用户密码
#### 接口地址URL
POST   /sitems/user/update_passWord
#### 请求参数说明
参数名 |  类型  | 描述   |  备注  
:--------: | :-----:  | :----:| :-----: 
  account  | String   |   账号名称
  oldPassWord  | String   |   当前用户密码
  newPassWord  | String   |   新的用户密码
#### 请求示例
```python
post  http://{ip}:8080/sitems/user/update_passWord
```
#### 返回结果
```python 
{
  "code": 0,
  "message": "修改用户密码成功！",
  "object": {
    "id": 0,
    "account": "1",
    "user_name": "chenlin",
    "passWord": "12345678",
    "contact": "15195926666",
    "avatar": "123",
    "token": "123",
    "role_id": 1,
    "account_begints": 1,
    "account_endts": 2,
    "create_time": 3
  }
}
```

### 12. 账户删除
#### 接口地址URL
GET   /sitems/user/delete
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  userid  | String   | 用户id  | 必须

### 13. 角色创建
POST   /sitems/role/create
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  role_name  | String   | 角色名称
  role_info  | String   | 备注   | 非必须
  permissions  | String   | 权限信息 |包含模块和权限的列表
#### 请求示例：
```python
{
	"role_name":"审核员",
	"role_info":"创建审核员角色",
	"permissions":[
        {
            "id": 1,
            "parent_id": 0,
            "module_name": "网站管理",
            "module_info": "网站管理权限模块",
            "status": 1,
            "moduleList": null,
            "permissionList": [
                {
                    "id": 1,
                    "module_id": 1,
                    "permission_name": "添加",
                    "permission_info": "网站管理-添加",
                    "status": 1,
                    "create_Time": 1565076127
                }
            ····
            ]
        }]
```
#### 返回参数
```python
{
    "code": 0,
    "message": "创建角色成功！",
    "object": null
}
```  
  
### 14. 角色列表获取
GET   /sitems/role/querylist
#### 请求参数说明
 无，获取全部列表，因角色不会太多，所以暂不用分页
#### 返回参数
```python
{
    "code": 0,
    "message": "查询角色列表成功！",
    "object": [
        {
            "id": 1,
            "role_name": "管理员",
            "create_time": 1565245035098,
            "role_info": "创建管理员角色",
            "permissions": [
                {
                    "id": 1,
                    "parent_id": 0,
                    "module_name": "网站管理",
                    "module_info": "网站管理权限模块",
                    "status": 1,
                    "moduleList": null,
                    "permissionList": [
                        {
                            "id": 1,
                            "module_id": 1,
                            "permission_name": "添加",
                            "permission_info": "网站管理-添加",
                            "status": 1,
                            "create_Time": 1565076127
                        },
                        ·····
                        {
                            "id": 6,
                            "module_id": 1,
                            "permission_name": "批量删除",
                            "permission_info": "网站管理-批量删除",
                            "status": 0,
                            "create_Time": 1565076127
                        }
                    ]
                },
                
```  
  
### 15. 角色修改
POST   /sitems/role/update
#### 请求参数说明：json格式
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id  | int   | 角色id  | 根据id修改角色  
  role_name  | String   | 角色名称    
  role_info  | String   | 备注   | 必须   
  permissions  | String   | 权限信息 |包含模块和权限的列表
#### 请求示例：
```python
{
    "id":3,
	"role_name":"审核员",
	"role_info":"创建审核员角色",
	"permissions":[
        {
            "id": 1,
            "parent_id": 0,
            "module_name": "网站管理",
            "module_info": "网站管理权限模块",
            "status": 1,
            "moduleList": null,
            "permissionList": [
                {
                    "id": 1,
                    "module_id": 1,
                    "permission_name": "添加",
                    "permission_info": "网站管理-添加",
                    "status": 1,
                    "create_Time": 1565076127
                }
            ····
            ]
        }]
```
#### 返回参数
```python
{
    "code": 0,
    "message": "修改角色成功！",
    "object": null
}
``` 
  
### 16. 角色删除：删除之前一定要提示用户，有相关账户已关联到给角色
#### 接口地址URL
GET   /sitems/role/delete
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  roleid  | String   | 角色id  | 必须

### 17. 查询全部权限类别
GET   /sitems/permission/total
#### 请求参数说明
  无   
#### 请求示例
```python
get  http://{ip}:8080/sitems/permission/total
```
#### 返回结果
```python
{
    "code": 0,
    "message": "查询成功！",
    "object": [
        {
            "id": 1,
            "parent_id": 0,
            "module_name": "网站管理",
            "module_info": "网站管理权限模块",
            "status": 0,
            "moduleList": null,
            "permissionList": [
                {
                    "id": 1,
                    "module_id": 1,
                    "permission_name": "添加",
                    "permission_info": "网站管理-添加",
                    "status": 0,
                    "create_Time": 1565076127
                },
               
                {
                    "id": 6,
                    "module_id": 1,
                    "permission_name": "批量删除",
                    "permission_info": "网站管理-批量删除",
                    "status": 0,
                    "create_Time": 1565076127
                }
            ]
        },
        {
            "id": 2,
            "parent_id": 0,
            "module_name": "巡查预警",
            "module_info": "巡查预警权限模块",
            "status": 0,
            "moduleList": [
                {
                    "id": 7,
                    "parent_id": 2,
                    "module_name": "巡查设置",
                    "module_info": "巡查预警-巡查设置",
                    "status": 0,
                    "moduleList": null,
                    "permissionList": [
                        {
                            "id": 12,
                            "module_id": 7,
                            "permission_name": "新建专题",
                            "permission_info": "巡查设置-新建专题",
                            "status": 0,
                            "create_Time": 1565076127
                        },
                        
                        {
                            "id": 16,
                            "module_id": 7,
                            "permission_name": "首页显示/隐藏",
                            "permission_info": "巡查设置-首页显示/隐藏",
                            "status": 0,
                            "create_Time": 1565076127
                        }
                    ]
                },
                {
                    "id": 8,
                    "parent_id": 2,
                    "module_name": "预警接收人设置",
                    "module_info": "巡查预警-预警接收人设置",
                    "status": 0,
                    "moduleList": null,
                    "permissionList": [
                        {
                            "id": 17,
                            "module_id": 8,
                            "permission_name": "添加",
                            "permission_info": "预警接收人设置-添加",
                            "status": 0,
                            "create_Time": 1565076127
                        },
                        {
                            "id": 18,
                            "module_id": 8,
                            "permission_name": "修改",
                            "permission_info": "预警接收人设置-修改",
                            "status": 0,
                            "create_Time": 1565076127
                        },
                        {
                            "id": 19,
                            "module_id": 8,
                            "permission_name": "删除",
                            "permission_info": "预警接收人设置-删除",
                            "status": 0,
                            "create_Time": 1565076127
                        }
                    ]
                }
            ],
            "permissionList": [
                {
                    "id": 7,
                    "module_id": 2,
                    "permission_name": "导出数据",
                    "permission_info": "巡查预警-导出数据",
                    "status": 0,
                    "create_Time": 1565076127
                },
                
                {
                    "id": 10,
                    "module_id": 2,
                    "permission_name": "加入简报",
                    "permission_info": "巡查预警-加入简报",
                    "status": 0,
                    "create_Time": 1565076127
                }
            ]
        },
        {
            "id": 3,
            "parent_id": 0,
            "module_name": "违规协查",
            "module_info": "违规协查权限模块",
            "status": 0,
            "moduleList": null,
            "permissionList": [
                {
                    "id": 20,
                    "module_id": 3,
                    "permission_name": "协查反馈",
                    "permission_info": "违规协查-协查反馈",
                    "status": 0,
                    "create_Time": 1565076127
                },
                {
                    "id": 21,
                    "module_id": 3,
                    "permission_name": "处理记录",
                    "permission_info": "违规协查-处理记录",
                    "status": 0,
                    "create_Time": 1565076127
                }
            ]
        }
    ]
}
```


### 18. 权限添加
POST   /sitems/permission/add
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  moduleid  | int   |   权限模块id
  name  | String   |   权限名称
  info  | String   | 备注
  
### 19. 权限删除
#### 接口地址URL
GET   /sitems/permission/delete
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  permissionid  | String   | 权限id  | 必须

### 20. 获取平台名称
#### 接口地址URL
GET   /sitems/platform
#### 请求参数说明
参数名 |  类型  | 描述   |  备注  
无需参数
#### 请求示例
```python
get  http://{ip}:8080/sitems/platform
```
#### 返回结果
```python 
{
  "code": 0,
  "message": "获取成功！",
  "object": {
    "id": 0,
    "platform": "测试平台"
  }
}
```

### 21. 修改平台名称
#### 接口地址URL
POST   /sitems/update/platform
#### 请求参数说明
参数名 |  类型  | 描述   |  备注  
:--------: | :-----:  | :----:| :-----: 
  Sname  | String   |   平台名称
#### 请求示例
```python
post  http://{ip}:8080/sitems/update/platform
```
#### 返回结果
```python 
{
  "code": 0,
  "message": "修改平台名称成功！",
  "object": {
    "id": 0,
    "platform": "属地化网站管理平台"
  }
}
```


