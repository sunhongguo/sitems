﻿# 巡查预警API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```
### 1.巡查预警网站：获取列表
#### 接口地址URL
POST   /sitems/alert/web_list
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  page_no	 |  int |	页数  | 从1开始
  page_size  |  int |  条数   | 默认10条
  searchValue  |  String |  搜索条件：网站名称  | 非必须
  province   | String   | 所在省份
  city       | String| 所在城市 | 默认全部,非必须
  organizerNature |String | 主办单位 | 非必须
  level      | String    | 等级 |非必须 枚举值: A B C 默认全部
  subjectId  | String   | 专题id
  start_time | String | 开始时间 | 格式：“2019-09-12 00:00:00”
  end_time   | String | 结束时间 | 格式：“2019-09-12 00:00:00”
  sortField  | String | 排序字段 | 支持domainName和siteName字段
  sortType  | String  | 排序方式 | 枚举值：asc和desc
  
#### 请求示例
```python
{
	"subjectId": "177",
	"level": "C"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": [
            {
                "illegalCount": 10,
                "city": "",
                "level": "C",
                "organizerNature": "个人",
                "subject": 177,
                "siteName": "个人技术文章",
                "updateTime": "2019-09-12T16:08:02.625Z",
                "province": "",
                "phone": "",
                "contact": "山黎明",
                "domainName": "51xiancheng.com",
                "id": "f5e2ec9e719b666c7216a1a8e81be81d",
                "category": [
                    "0"
                ],
                "status": 0
            },
            {
                "illegalCount": 123,
                "city": "",
                "level": "C",
                "organizerNature": "个人",
                "subject": 177,
                "siteName": "个人技术文章",
                "updateTime": "2019-09-12T16:08:02.539Z",
                "province": "",
                "phone": "",
                "contact": "山黎明",
                "domainName": "sina.com.cn",
                "id": "c2fdb39ec815b282337350dd23db742e",
                "category": [
                    "0"
                ],
                "status": 0
            },
            {
                "illegalCount": 60,
                "city": "",
                "level": "C",
                "organizerNature": "个人",
                "subject": 177,
                "siteName": "个人技术文章",
                "updateTime": "2019-09-12T16:08:02.585Z",
                "province": "",
                "phone": "",
                "contact": "山黎明",
                "domainName": "weibo.com",
                "id": "4ea43b877864e79085843e843daa6b22",
                "category": [
                    "0"
                ],
                "status": 0
            }
        ],
        "code": "0",
        "count": "3",
        "page": "1",
        "maxPage": "1"
    }
}
```

### 2.预警网站：根据条件，统计数量
#### 接口地址URL
POST   /sitems/alert/count_term
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  province  |  String |  所在省份 | 必须
  key      | String    | 统计字段 | 必须 
  counts   |  int     | 返回数量 | 默认为10,非必须
  start_time | String | 开始时间 | 格式：“2019-09-12 00:00:00”
  end_time   | String | 结束时间 | 格式：“2019-09-12 00:00:00”
#### 请求示例
```python
{
	"key":"city",
	"province":"湖北省",
	"counts":13
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": {},
        "code": "0"
    }
}
```

### 3.违规文章：根据条件，获取违规文章列表
#### 接口地址URL
POST   /sitems/alert/get_pagelist
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  page_no	 |  int |	页数  | 从1开始
  page_size  |  int |  条数   | 默认10条
  mustKeyWords  |  List.String |  全部被包含关键词| 默认为空,非必须
  shouldKeyWords   | List.String   | 任意被包含关键词| 默认为空
  unrelated       | List.String| 排除关键词 | 默认为空,非必须
  domainName    |List.String | 网站域名 | 非必须
  isWarning      | String    | 是否预警 |非必须 枚举值: 0 未预警 1 已预警 默认全部
  start_time | String | 开始时间 | 格式：“2019-09-12 00:00:00”
  end_time   | String | 结束时间 | 格式：“2019-09-12 00:00:00”
  sortField  | String | 排序字段 | 支持createTime和domainName字段
  sortType  | String  | 排序方式 | 枚举值：asc和desc
#### 请求示例
```python
{
   "page_no":1,
   "page_size":10,
	"domainName":["51xiancheng.com"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": [
            {
                "cleanTitle": "震撼史上最全版本卡门来南京了鲜城",
                "hotWords": [
                    "重庆"
                ],
                "isWarning": 1,
                "createTime": "2019-09-24T15:21:30.065Z",
                "domainName": "51xiancheng.com",
                "warning": [
                    "12"
                ],
                "id": "db62bb42cb72b4b33d3509140c28a052",
                "title": "震撼！史上最全版本《卡门》来南京了-鲜城",
                "content": "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n震撼！史上最全版本《卡门》来南京了-鲜城\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!--START 
        }
        ],
        "code": "0",
        "count": "18",
        "page": "1",
        "maxPage": "2"
    }
}
```

### 4.违规文章：根据id查询单个
#### 接口地址URL
GET   /sitems/alert/get_pagebyid
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id  |  String |  文章ID |  必须
#### 请求示例
```python
http://localhost:8080/sitems/alert/get_pagebyid?id=db62bb42cb72b4b33d3509140c28a052
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "msg": "操作成功",
        "result": {
            "cleanTitle": "震撼史上最全版本卡门来南京了鲜城",
            "hotWords": [
                "重庆"
            ],
            "isWarning": 1,
            "createTime": "2019-09-24T15:21:30.065Z",
            "domainName": "51xiancheng.com",
            "warning": [
                "12"
            ],
            "id": "db62bb42cb72b4b33d3509140c28a052",
            "title": "震撼！史上最全版本《卡门》来南京了-鲜城",
            "content": "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n震撼！史上最全版本《卡门》来南京了- img').attr('src',\"/interface/common/qrCode?len=160&data=\"+encodeURIComponent(''));\r\n\r\n})\r\n\r",
            "url": "http://nanjing.51xiancheng.com/article/76149"
        },
        "code": "0"
    }
}
```

### 5. 专题分类：获取列表
#### 接口地址URL    
GET   /sitems/alert/category_list
#### 请求参数说明
参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  account_id |  int |	用户ID  | 必须
   
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "object": [
        {
            "name": "传媒",
            "id": 1,
            "object": [
                {
                    "id": 2,
                    "name": "新闻媒体",
                    "parent_id": 1,
                    "sort": 1,
                    "isalert": 1,
                    "isshow": 1
                },
                {
                    "id": 3,
                    "name": "视频网站",
                    "parent_id": 1,
                    "sort": 2,
                    "isalert": 1,
                    "isshow": 1
                },
                {
                    "id": 4,
                    "name": "体育资讯",
                    "parent_id": 1,
                    "sort": 3,
                    "isalert": 1,
                    "isshow": 1
                }
            ]
        }
    ]
}
```

### 6.专题分类：添加新建
#### 接口地址URL
POST   /sitems/alert/category_add
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  name	 |  String |	名称  | 必须
  parent_id  |  int |  上级id  | 必须,最上级填0
  account_id  |  int |  创建用户id  | 必须

#### 请求示例
```python
{
	"name": "测试02",
	"parent_id": 1,
	"account_id": 1
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "添加新的专题分类成功！",
    "errmsg": null,
    "object": {
        "id": 10,
        "name": "测试02",
        "parent_id": 1,
        "sort": 4,
        "isalert": 1,
        "isshow": 1
    }
}
```

### 7.专题分类：重命名
#### 接口地址URL
POST   /sitems/alert/category_rename
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id     | int     |  id      | 必须
  name	 |  String |	名称  | 必须

#### 请求示例
```python
{
	"id": 10,反馈
	"name": "测试02222"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "重命名成功！",
    "errmsg": null,
    "object": null
}
```

### 8.专题分类：设置是否在首页显示
#### 接口地址URL
POST   /sitems/alert/category_show
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  id     |  int    |   id      | 必须
  isshow |  int   |	是否显示   | 必须； 1表示显示；0表示不显示; 默认显示

#### 请求示例
```python
{
	"id": 10,
	"isshow": 0
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "修改首页显示设置成功！",
    "errmsg": null,
    "object": null
}
```

### 9.专题分类：交互顺序
#### 接口地址URL
GET   /sitems/alert/category_sort
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  firstid    |  int    |   第一个专题id      | 必须
  secondid  |  int   |	第二个专题id   | 必须

#### 请求示例
```python
http://localhost:8080/sitems/alert/category_sort?firstid=2&secondid=4
```
#### 返回参数
```python
{
    "code": 0,
    "message": "移动顺序设置成功！",
    "errmsg": null,
    "object": null
}
```

### 10.专题分类：根据id删除
#### 接口地址URL
POST   /sitems/alert/category_delete
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  id     |  int    |   id      | 必须
#### 请求示例
```python
{
	"id":3
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "删除成功！",
    "errmsg": null,
    "object": null
}
```

### 11. 预警接收人：获取列表
#### 接口地址URL    
GET   /sitems/alert/receiver_list
#### 请求参数说明
   无参数，默认获取所有的预警接收人列表
#### 返回参数
```python
{
    "code": 0,
    "message": "查询预警接收人成功！",
    "object": [
        {
            "id": 1,
            "name": "receiver001",
            "email": "124ef3@QQ.com",
            "phone": "1259633"
        },
        {
            "id": 2,
            "name": "receiver002",
            "email": "88888888@QQ.com",
            "phone": "152433555"
        }
    ]
}
```

### 12.预警接收人：添加
#### 接口地址URL
POST   /sitems/alert/receiver_add
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  name	 |  String |	名称  | 必须
  email  |  String |	邮箱
  phone  |  String | 	电话

#### 请求示例
```python
{
	"name": "receiver001",
	"email": "124ef3@QQ.com",
	"phone": "1259633"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "添加新的预警接收人成功！",
    "errmsg": null,
    "object": {
        "id": 1,
        "name": "receiver001",
        "email": "124ef3@QQ.com",
        "phone": "1259633"
    }
}
```

### 13.预警接收人：修改
#### 接口地址URL
POST   /sitems/alert/receiver_update
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  id     |  int    |   id      | 必须
  name	 |  String |	名称   | 必须
  email  |  String |	邮箱
  phone  |  String | 	电话

#### 请求示例
```python
{
    "id": 2,
    "name": "receiver002",
    "email": "2222222222@QQ.com",
    "phone": "15299999999"
	
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "修改接收人信息成功！",
    "object": null
}
```

### 14.预警接收人：根据id删除
#### 接口地址URL
POST   /sitems/alert/receiver_delete
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  id     |  int    |   id      | 必须
#### 请求示例
```python
{
	"id":3
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "删除成功！",
    "object": null
}
```

### 15.保存预警关键词
#### 接口地址URL
POST   /sitems/alert/setting_word
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  word     |  String    |   预警词      | 必须
  isalert  |  int       |  预警开关 | 1表示开；0表示关; 必须
  subject_id | int      | 专题ID    | 必须
  alert_receiver | List<Integer>      | 预警接收人id列表   | 必须
  alert_type | List<String>    | 预警接收方式   | email表示邮箱，message表示短信
  begin_time | String      | 预警开始时间    | 非必须
  end_time   | String      | 预警接受时间    | 非必须
#### 请求示例
```python
{
	"word":"安全,刀,人生",
	"isalert": 1,
	"subject_id":3,
	"alert_receiver":[2,3],
	"alert_type":["email","message"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "保存预警词成功！",
    "errmsg": null,
    "object": null
}
```

### 16.Excel导出：预警网站，根据条件
#### 接口地址URL
POST   /sitems/alert/export_weblist
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  province   | String   | 所在省份 | 必须
  city       | String   | 所在城市 | 默认全部,非必须
  organizerNature |String | 主办单位 | 非必须
  level      | String    | 等级 |非必须 枚举值: A B C 默认全部
  category  | String   | 网站分类   | 非必须
#### 请求示例
```python
{
	"province":"湖北省"
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成Excel表格成功！",
    "errmsg": null,
    "object": "sitems/file/excel?excelname=Excel_AlertPage_1569483572826.xlsx"
}
```

### 17.Excel导出：预警网站，根据ids
#### 接口地址URL
POST   /sitems/alert/export_weblist_ids
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  ids   | List.string   | 勾选的id数组 | 必须
#### 请求示例
```python
{
	"ids" : ["f5e2ec9e719b666c7216a1a8e81be81d","c2fdb39ec815b282337350dd23db742e"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成Excel表格成功！",
    "errmsg": null,
    "object": "sitems/file/excel?excelname=Excel_AlertWeb_1569548064077.xlsx"
}
```

### 18.Excel导出：违规文章，根据条件
#### 接口地址URL
POST   /sitems/alert/export_pagelist
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  mustKeyWords  |  List.String |  全部被包含关键词| 默认为空,非必须
  shouldKeyWords   | List.String   | 任意被包含关键词| 默认为空
  unrelated       | List.String| 排除关键词 | 默认为空,非必须
  domainName    |List.String | 网站域名 | 非必须
  isWarning      | String    | 是否预警 |非必须 枚举值: 0 未预警 1 已预警 默认全部
  start_time | String | 开始时间 | 格式：“2019-09-12 00:00:00”
  end_time   | String | 结束时间 | 格式：“2019-09-12 00:00:00”
#### 请求示例
```python
{
	"domainName":["51xiancheng.com"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成Excel表格成功！",
    "errmsg": null,
    "object": "sitems/file/excel?excelname=Excel_AlertPage_1569483572826.xlsx"
}
```

### 19.Excel导出：违规文章，根据ids
#### 接口地址URL
POST   /sitems/alert/export_pagelist_ids
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  ids   | List.string   | 勾选的id数组 | 必须
#### 请求示例
```python
{
	"ids" : ["db62bb42cb72b4b33d3509140c28a052"]
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "生成Excel表格成功！",
    "errmsg": null,
    "object": "sitems/file/excel?excelname=Excel_AlertWeb_1569548064077.xlsx"
}
```