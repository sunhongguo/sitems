﻿# 属地简报API    
```python
{ip} = 18.18.154.148
http请求头Header中添加sitems_token，验证是否已登录,内容为用户信息中的token
```

### 1.违规信息添加到简报
#### 接口地址URL
POST   /sitems/report/page/add
#### 请求参数说明：参数为违规信息列表,即[违规信息1,违规信息2,.....];其中违规信息参数如下：       

  参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  url         |  String |	消息url  | 必须
  domainName  |  String |  域名  |  必须
  title       |  String |	标题 | 必须
  cleanTitle  |  String |  精简标题  |  非必须
  content     |  String |	内容  | 非必须
  createTime  |  long |  创建时间  |  必须，暂定为时间戳
  
#### 请求示例
```python
[
	{
		"url":"sddfdlkd.com",
		"domainName":"www.baidu.com",
		"title":"title01",
		"cleanTitle":"clean-标题0001",
		"createTime":1569661084031
	},
	{
    	"url":"152223552.com",
    	"domainName":"www.baid03.com",
    	"title":"title03",
		"cleanTitle":"clean-标题0003",
		"createTime":1569661084200
	}
]
```
#### 返回参数
```python
{
    "code": 0,
    "message": "添加到简报成功！",
    "errmsg": null,
    "object": null
}
```

### 2.邮件接受人：添加
#### 接口地址URL
POST   /sitems/report/email/add
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  email |  String |	邮箱  | 必须
  userId |  int |	创建者id  | 必须
  
#### 请求示例
```python
{
	"email":"4566254524@qq.com",
	"userId":3
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "添加成功！",
    "errmsg": null,
    "object": null
}
```

### 3.邮件接受人：获取列表
#### 接口地址URL
GET   /sitems/report/email/list
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----:
  userId |  int |	创建者id  | 必须
  
#### 请求示例
```python
http://localhost:8080/sitems/report/email/list?userId=3
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "object": [
        {
            "id": 4,
            "email": "4566254524@qq.com",
            "userId": 3
        },
        {
            "id": 5,
            "email": "456632334524@qq.com",
            "userId": 3
        },
        {
            "id": 6,
            "email": "4566sdfdf4524@qq.com",
            "userId": 3
        }
    ]
}
```

### 4.邮件接受人：根据id删除
#### 接口地址URL
GET   /sitems/report/email/delete
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id |  int |	删除的邮箱id  | 必须
  
#### 请求示例
```python
http://localhost:8080/sitems/report/email/delete?id=1
```
#### 返回参数
```python
{
    "code": 0,
    "message": "删除成功！",
    "errmsg": null,
    "object": null
}
```

### 5.简报：生成新简报
#### 接口地址URL
POST   /sitems/report/create
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  start_time |  long |	开始时间：时间戳  | 必须
  end_time  |  long |  结束时间：时间戳  |  必须
  
#### 请求示例
```python
{
    "start_time": 1569661000000,
    "end_time": 1569780000000
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "添加成功！",
    "errmsg": null,
    "object": null
}
```

### 6.简报：查询列表
#### 接口地址URL
POST   /sitems/report/query_list
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  page |  int |	页数  | 非必须
  count  |  int |  条数  |  非必须
  
#### 请求示例
```python
{
    "page": 1,
    "count": 10
}
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "object": [
        {
            "id": 1,
            "report_name": "湖北省网信办简报",
            "start_time": 1569661000000,
            "end_time": 1569780000000,
            "report_desc": null,
            "webs": null,
            "page_new": null,
            "page_important": null
        },
        {
            "id": 2,
            "report_name": "湖北省网信办简报(1569661000000)",
            "start_time": 1569661000000,
            "end_time": 1569780000000,
            "report_desc": null,
            "webs": null,
            "page_new": null,
            "page_important": null
        }
    ]
}
```
### 7.简报：根据id查询
#### 接口地址URL
GET   /sitems/report/query_id
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id |  int |	简报id  | 必须
  
#### 请求示例
```python
http://localhost:8080/sitems/report/query_id?id=2
```
#### 返回参数
```python
{
    "code": 0,
    "message": "查询成功！",
    "errmsg": null,
    "object": {
        "id": 2,
        "report_name": "湖北省网信办简报(1569661000000)",
        "start_time": 1569661000000,
        "end_time": 1569780000000,
        "report_desc": "开始时间-接受时间期间，系统共监测到XXX个网站存在违规信息共XXX条,其中已处理XXX个网站,还有XXX个网站待处理；详细报告如下：",
        "webs": [
            {
                "id": 2,
                "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
                "domainName": "51xiancheng.com",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "C",
                "city": "长沙市",
                "province": "",
                "subject": 0,
                "siteName": "个人技术123",
                "contact": "山黎明",
                "phone": "",
                "status": 0,
                "categoryTitle": "分类02",
                "createTime": 1569573498622,
                "scheme": "协查方案0002",
                "feedBack": null,
                "userId": 0
            },
            {
                "id": 3,
                "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
                "domainName": "51xiancheng.com",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "B",
                "city": "长沙市",
                "province": "",
                "subject": 0,
                "siteName": "个人",
                "contact": "山黎明",
                "phone": "",
                "status": 0,
                "categoryTitle": "未分类",
                "createTime": 1569652331285,
                "scheme": "fgbdfgb",
                "feedBack": null,
                "userId": 0
            },
            {
                "id": 4,
                "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
                "domainName": "51xiancheng.com",
                "organizerNature": "企业",
                "illegalCount": 0,
                "level": "A",
                "city": "株洲市",
                "province": "",
                "subject": 0,
                "siteName": "个人技术475",
                "contact": "山黎明",
                "phone": "",
                "status": 0,
                "categoryTitle": "未分类",
                "createTime": 1569661084031,
                "scheme": "ergsergv重点班的爆发式的发布十点半水电费水电费部署到发布aergsergv重点班的爆发式的发布十点半水电费水电费部署到发布aergsergv重点班的爆发式的发布十点半水电费水电费部署到发布发个",
                "feedBack": null,
                "userId": 0
            },
            {
                "id": 5,
                "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
                "domainName": "51xiancheng.com",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "A",
                "city": "株洲市",
                "province": "",
                "subject": 0,
                "siteName": "856",
                "contact": "山黎明",
                "phone": "",
                "status": 0,
                "categoryTitle": "未分类",
                "createTime": 1569661357161,
                "scheme": "svafv 让步闪电风暴伤风败俗地方闪电风暴是是的发v的发v是的发v的发vsvafv 让步闪电风暴伤风败俗地方闪电风暴是是的发v的发v是的发v的发vsvafv 让步闪电风暴伤风败俗地方闪电风暴是是的发",
                "feedBack": null,
                "userId": 0
            },
            {
                "id": 6,
                "webId": "c2fdb39ec815b282337350dd23db742e",
                "domainName": "sina.com.cn",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "C",
                "city": "",
                "province": "",
                "subject": 0,
                "siteName": "个人技术文章",
                "contact": "山黎明",
                "phone": "",
                "status": 0,
                "categoryTitle": "未分类",
                "createTime": 1569750063268,
                "scheme": "是发v和巴萨两",
                "feedBack": null,
                "userId": 0
            },
            {
                "id": 4,
                "webId": "f5e2ec9e719b666c7216a1a8e81be81d",
                "domainName": "51xiancheng.com",
                "organizerNature": "个人",
                "illegalCount": 0,
                "level": "C",
                "city": "",
                "province": "",
                "subject": 0,
                "siteName": "个人技术文章",
                "contact": "山黎明",
                "phone": "",
                "status": 0,
                "categoryTitle": "未分类",
                "createTime": 1570513373796,
                "scheme": null,
                "feedBack": null,
                "userId": 0
            }
        ],
        "page_new": null,
        "page_important": [
            {
                "id": "1",
                "url": "sddfdlkd.com",
                "domainName": "www.baidu.com",
                "createTime": "1569661084031",
                "title": "title01",
                "cleanTitle": "clean-标题0001",
                "content": null
            },
            {
                "id": "2",
                "url": "152223552.com",
                "domainName": "www.baid03.com",
                "createTime": "1569661084200",
                "title": "title03",
                "cleanTitle": "clean-标题0003",
                "content": null
            }
        ]
    }
}
```

### 8.简报：根据id删除
#### 接口地址URL
GET   /sitems/report/delete_id
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id |  int |	简报id  | 必须
  
#### 请求示例
```python
http://localhost:8080/sitems/report/delete_id?id=1
```
#### 返回参数
```python
{
    "code": 0,
    "message": "删除成功！",
    "errmsg": null,
    "object": null
}
```

### 9.简报：下载word文档
#### 接口地址URL
GET   /sitems/report/download
#### 请求参数说明：参数已json形式上传
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  id |  int |	简报id  | 必须
  
#### 请求示例
```python
http://localhost:8080/sitems/report/download?id=3
```
#### 返回参数
```python
{
    "code": 0,
    "message": "下载简报成功",
    "errmsg": null,
    "object": "sitems/file/word?wordname=属地简报_20191014_96.doc"
}
```



